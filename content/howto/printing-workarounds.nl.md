---
author: harcok
date: '2022-05-25T13:44:50Z'
keywords: []
lang: nl
title:  Workarounds voor printen
aliases:
wiki_id: '855'
---
{{< notice info >}}
dit document moet worden bijgewerkt
{{< /notice >}}

## Algemeen

Péage is het print/scan/copy systeem van de Radboud Universiteit voor
medewerkers en studenten. Vanaf maart 2019 heet de nieuwe server
`print.hosting.ru.nl` en de nieuwe printqueue “FollowMe”. De Library of
Science beschikt over
[gastenpassen](http://www.ru.nl/facilitairbedrijf/printen/gastenpas/).
Algemene informatie is te vinden op de [Péage
website](http://www.ru.nl/printen).

Op deze pagina geeft C&CZ informatie die specifiek is voor medewerkers
en studenten van FNWI.

## Terminologie

-   MFP: multifunctionele printer (die kan printen, kopiëren en scannen)

## PrintNightmare probleem workaround

-   Sinds ca. 22 september 2021 werkt Peage niet meer op zelfbeheerde/BYOD
    Windows/macOS/Linux computers. Op die dag heeft het ISC de patches
    voor [het beveiligingslek
    PrintNightmare](https://borncity.com/win/2021/09/22/windows-printnightmare-patch-stand-und-workarounds-22-sept-2021/)
    geïnstalleerd op de printservers. Het ISC en C&CZ hebben daarna voor
    door hen beheerde pc’s een workaround geïnstalleerd. Een workaround
    voor \*alle\* apparaten is het gebruik van de web-upload op [de
    Peage website](https://peage.ru.nl). Vanaf 28 oktober 2021 werden die
    upload-jobs in kleur geprint i.p.v. zwart/wit. Vanaf 1 januari 2024 werden die
    upload-jobs dubbelzijdig in zwart-wit geprint.

    -   Voor zelfbeheerde Windows pc’s kan ook de C&CZ workaround
        gebruikt worden. Deze workaround is gebaseerd op een oude
        printer-installatie-methode uit het Windows 98/XP tijdperk. Dit
        is geen echte oplossing. Indien Microsoft de ‘PrintNightmare’
        problemen opgelost heeft, dan is het aan te raden deze
        printer-installatie op te ruimen. De werking van deze workaround
        wordt niet gegarandeerd. Hieronder en stappenplan voor deze
        workaround. De C&CZ helpdesk kan hier evt. bij helpen.

    -   Van belang is dat u uw RU accountgegevens in de credential
        manager hebt ingevoerd:

        `Internet or network address: print.hosting.ru.nl`\
        `User name: [Voornaam.Achternaam@ru.nl]`\
        `Password: [uw RU wachtwoord]`

    -   Als u dit correct ingevoerd heeft, kunt u in de Windows Explorer
        in de adresbalk naar het adres \\\\print.hosting.ru.nl\\ gaan en
        ziet dan een lijst printers.

1: Download de FollowMe driver van Konica-Minolta
<https://www.konicaminolta.eu/eu-en/support/download-centre> Kies voor
Product Type: A3 MultiFunctional Colour en Product: C658.

Om compatibel te zijn met de drivers geïnstalleerd door het ISC, kies
voor Versie 11.1.2.0 Jul 26, 2018 PCL6.

Dit leverde ten tijde van deze workaround [een lange URL bij
dl.konicaminolta.eu](https://dl.konicaminolta.eu/en?tx_kmanacondaimport_downloadproxy%5BfileId%5D=c9bde5fb7d1de8e581c797666d089ed2&tx_kmanacondaimport_downloadproxy%5BdocumentId%5D=1719&tx_kmanacondaimport_downloadproxy%5Bsystem%5D=KonicaMinolta&tx_kmanacondaimport_downloadproxy%5Blanguage%5D=EN&type=1558521685)
op. Plaats het driver zip bestand op een bekende locatie en unzip de
driver.

2: Ga via ‘Start’ -\> ‘Settings’ -\> ‘Devices’, naar ‘Printers &
scanners’ en start ‘Add a printer or scanner’. Er verschijnt een optie
‘The printer that I want isn’t listed’, selecteer deze optie.

3: Er verschijnt een window ‘Find a printer by other options’. Selecteer
‘Add a local printer or network printer with manual settings’. Klik
‘next’. Er verschijnt ‘Choose a printer port’, laat deze voorlopig staan
op ‘Use an existing port: LPT1 (Printer Port)’. Klik ‘next’.

4: Er verschijnt een window ‘Install the printer driver’, selecteer
‘Have Disk…’ en browse naar de uitgepakte driver. Selecteer het enige
‘INF’ bestand en klik op de ‘Open’ knop en daarna ‘OK’. Er kan gekozen
worden uit uiteenlopende modellen. Kies voor \`KONIKA MINOLTA
C658SeriesPCL’ en klik op ‘Next’.

5: Er verschijnt een window ‘Type a printer name’ met vooringevuld
‘KONICA MINOLTA C658SeriesPCL’. Klik ‘next’. Er komt een verzoek voor
het Administrator-wachtwoord. Na invoering van het wachtwoord wordt de
driver geïnstalleerd.

6: Er komt een window ‘Printer Sharing’, kies voor de default ‘Do not
share this printer’ en kies ‘Next’ en daarna ‘Finish’.

7: We gaan de geïnstalleerde printer aanpassen. In ‘Settings’ -\>
‘Devices’ -\> ‘Printers & scanners’: selecteer de geïnstalleerde printer
en klik op ‘Manage’. Klik daarna op ‘Printer properties’.

8: Een nieuw window ‘KONICA MINOLTA C658SeriesPCL Properties’
verschijnt. Onder het tabblad ‘Ports’ kies ‘Add Port…’, selecteer ‘Local
Port’ en vervolgens ‘New Port…’. In het window ‘Port Name’ met het
‘Enter a port name:’ invoerveld vul in
‘\\\\print.hosting.ru.nl\\FollowMe’. Klik ‘OK’, ‘Close’ en ‘Close’.

## Bekende problemen

-   Een E-nummer gebruiker kan niet printen: dat kan RBS-beheer
    oplossen:

    `Nummer Kamer Naam                              Afdeling     Email `\
    `52246  HG02.047 Ir. J.J. Zijlstra, (Jan-Jetze) Facbureau NW J.Zijlstra@science.ru.nl `

-   Remote Downlevel Document: Wanneer vanaf Linux geprint wordt,
    geeft de MFP niet de bestandsnaam weer, maar “Remote Downlevel
    Document”.
-   Evince print in A3 formaat. Verwijder het bestand
    **\~/.config/evince/print-settings**.
-   Okular onder Ubuntu 18.04 mist Advanced Options om bv in kleur te
    printen - Ubuntu probleem.
-   **lpq** probleem.
    Update default printer. Edit het bestand
    **\~/.cups/lpoptions**.
-   In de Linux Document Viewer moet je twee keer printen voor eenmaal
    output.
-   Het is verstandig om printers welke niet meer in het printsysteem
    voorkomen van uw computer te verwijderen.
-   [`cups-2.2.7` en `smbclient-4.8.1` hebben een API discrepantie
    waardoor printen niet werkt op Linux (gezien op Arch Linux). Een
    oplossing is om [`/usr/lib/cups/backend/smb` te
    patchen](https://twitter.com/mrngm/status/993492483786117121) (of
    `smbclient` te updaten).][`cups-2.2.7` and `smbclient-4.8.1`
    conflict in their command-line API which breaks printing on Linux
    (seen on Arch Linux). A solution is to [patch
    `/usr/lib/cups/backend/smb`](https://twitter.com/mrngm/status/993492483786117121)
    (or to update `smbclient`).]

## Printen met Péage

### Inloggen met campuskaart/ru-card

De eerste keer dat je print op een nieuwe printer is je (campus)kaart
nog niet gekoppeld.

-   Houd je kaart aan de voorzijde van de machine waar de kaart sticker
    zit.
-   Wanneer de kaart door de printer wordt herkend zal er een popup
    window verschijnen waarmee je kan inloggen.
-   Meld je (eenmalig) aan met je U, E, S of F nummer en RU-wachtwoord

Bij de volgende keer kan je inloggen op de printer door alleen je kaart
voor de lezer te houden.

### Onderscheid gebruikers

Péage kent 1 printserver en 1 wachtrij voor FollowMe printen:

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  printer server              print wachtrij    UNC - Windows style                                          URI - IPP style
  ---------------------------------------------- ----------------------------------- ------------------------------------------------------------ ---------------------------------------------------------------
  **print.hosting.ru.nl**   **FollowMe**   **\\\\print.hosting.ru.nl\\FollowMe**   **<smb://print.hosting.ru.nl/FollowMe>**


  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Voor printertjes zonder display/toetsenbord moet direct de wachtrij van
de printer aangekoppeld worden:

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  printer server              print wachtrij   UNC - Windows style                                         URI - IPP style
  ---------------------------------------------- ---------------------------------- ----------------------------------------------------------- --------------------------------------------------------------
  **print.hosting.ru.nl**   **KM-xxxx**   **\\\\print.hosting.ru.nl\\KM-xxxx**   **<smb://print.hosting.ru.nl/KM-xxxx>**


  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Péage portal: Printen met Péage op een willekeurig Internet-device

Met de [Péage portal](https://peage.ru.nl) kan vanaf smartphone, tablet
of elk ander Internet-device dat verbonden is met het
universiteitsnetwerk geprint worden naar [Peage](/nl/howto/peage/), via
bv web-upload. Een web-upload wordt altijd dubbelzijdig afgedrukt.

### Printen met Péage op een C&CZ-beheerde Windows machine

De printer “FollowMeScience” is te zien bij de printers op de server
“print.science.ru.nl”. Hiervoor moet wel uw U- of s-nummer geregistreerd
staan bij C&CZ. Dit kunt u controleren op de [Doe-Het-Zelf
site](https://dhz.science.ru.nl).

### Printen met Péage op een zelf beheerde Windows 7/8/8.1/10 machine)

Het onderstaande geldt als je met je Science-account of een
prive-account ingelogd bent. Als je ingelogd bent met RU\\U-nummer of
RU\\s-nummer, dan kun je direct naar stap 4. N.B.: de instellingen
worden dan niet opgeslagen.

Om de printer aan te koppelen, heb je je RU-account (u/f/e/s-nummer en
RU-wachtwoord, niet de Peage PIN-code) nodig om printopdrachten te
versturen.

-   Stap 1\
    Selecteer **Start**, in het
    **Help / Search** veld voer
    **Credential Manager** (of
    **Referentiebeheer**) in en start de gevonden
    applicatie.

-   Stap 2\
    De Windows kluis wordt gestart. Klik op **Add a
    Windows credential** rechts naast de rubriek
    **Windows Credentials**. Voer
    credential gegevens in: Internet or network address:
    **print.hosting.ru.nl** User name: je
    RU-account, voorafgegaan door
    **ru\\**, bv.
    **ru\\u123456** (medewerkers) of
    **ru\\s123456** (studenten) Password:
    je bijbehorend RU-password (niet de Peage PIN-code)

-   Klik vervolgens op <strong>OK</strong> and sluit de
        <strong>Credential Manager</strong> af.

-   Stap 3\
    Druk de **“windows”** toets en
    **“r”** tegelijk in. Vul in het
    venster **\\\\print.hosting.ru.nl**
    in en druk op **enter** of klik op
    **ok**

-   Stap 4\
    Dubbel klik op de **FollowMe**
    printer. Deze installeert zichzelf. Als de Printque tevoorschijn
    komt, is de printer goed geïnstalleerd.

-   Stap 5\
    Ga verder onder [Afdrukken van een printjob bij de
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

### Printen met Péage op een Mac OS X machine

Het ISC heeft een tool ter beschikking gesteld voor het installeren van
Péage printers: Zie [Mac OS X
instructies](https://www.ru.nl/facilitairbedrijf/printen/printen/macos/).

#### Handmatige installatie

Je hebt je RU-account (u/f/e/s-nummer en RU-wachtwoord, niet de Peage
PIN-code) nodig om een printopdracht te versturen.

-   Stap 1\
    Ga naar **System Preferences**,
    selecteer **Printers & Scanners**.

-   Stap 2\
    Klik onder de lijst met printers op het
    **+** (plusje). Kijk of je in de
    knoppenbalk de knop **Advanced** hebt
    staan. Staat deze knop er niet: rechts klik in deze knoppenbalk en
    kies **Customize Toolbar…** en sleep
    de knop **Advanced** naar de
    knoppenbalk. Klik vervolgens op
    **Done**. Klik in de knoppenbalk op
    **Advanced**.

-   Stap 3\
    Vul de volgende gegevens in: Type: **Windows printer
    via spoolss** Device: **Another
    Device** URL:
    **<smb://print.hosting.ru.nl/FollowMe>**
    Name: Naar wens invullen, bv.
    **Peage** Location: Vrij in te vullen
    **Péage Konica Minolta printers**
    Use: Choose a Driver… select **Generic Postscript
    Printer**. Opmerking: als je wilt dat de printer ook
    nietjes in je prints niet, dan moet je een printerspecifieke driver
    voor jouw specifieke printer installeren. Konica Minolta heeft
    helaas geen generieke printer driver voor al zijn printers voor
    MacOS. Voor MS-Windows hebben ze die wel, en daar werkt nieten ook
    standaard. Dus als je het slechts een enkele keer nodig hebt, kun je
    ook aan een collega met een MS-Windows machine vragen of hij het
    voor je wil printen met nietjes. Een specifieke driver voor een
    specifieke printer voor MacOS kun je downloaden op de Konica Minolta
    website. Je moet dan ook in de printerconfiguratie nog extra
    hardware addon modules configuren. Namelijk een “paper source unit”,
    “finisher” en “punch unit”. Welke specifieke hardware modules jouw
    printer heeft kun je het beste opzoeken in de printer configuratie
    van een collega die Windows als besturingssysteem heeft. Klik daarna
    op **Add**.

-   Er komt een scherm met opties waarbij o.a. de optie voor
        <strong>Duplex Printing Unit</strong> wordt weergeven.
        Zorg dat duplex aangevinkt staat en klik daarna op
        <strong>OK</strong>.

-   Stap 4\
    Nu kun je de voorkeur voor de **Options &
    Supplies…** opslaan. Wij raden aan om
    **zwart-wit printen** en
    **dubbelzijdig** in te stellen als
    **default**.

-   Stap 5\
    Het invoeren van je RU-account en RU-wachtwoord (niet je Peage
    PIN-code) gaat als volgt. Je hebt een document dat je wil printen en
    geef een eerste printopdracht. Als je het **printer
    icon** hebt ingesteld zal dit in je
    **Dock** tevoorschijn komen met een
    **1** erbij. Klik op dat
    **icon** om de printer-opdrachtlijst
    naar boven te halen. Je kunt ook klikken op de printer in
    **Printers & Scanners** in
    **System Preferences**. De status zal
    zijn dat de opdracht verstuurd wordt, maar na een tijdje springt
    deze op **Hold for Authentication**.
    Selecteer de opdracht in de printer-opdrachtlijst en klik op de knop
    genaamd **Resume**. Er verschijnt een
    dialoogvenster voor je inloggegevens. Voer hier het RU-account in,
    voorafgegaan door **ru\\**
    (Voorbeeld: **ru\\u123456**) en het
    bijbehorende RU-wachtwoord (niet je Peage PIN-code).

-   Gebruik je het printericoontje niet, dan krijg je meteen het
        inlogscherm om in te loggen met je RU-account
        (<strong>ru\\u123456</strong>) en je RU-wachtwoord (niet je Peage
        PIN-code).
        Selecteer desgewenst ook <strong>Remember this password in my
        keychain</strong>, zodat voortaan het printen automatisch gaat.
        Wanneer op <strong>OK</strong> wordt geklikt zal na een tijdje
        (afhankelijk van de grootte van de printopdracht) de opdracht uit de
        lijst verdwijnen wat betekent dat de opdracht in de wachtrij staat
        op de server.

-   Stap 6\
    Ga verder onder [Afdrukken van een printjob bij de
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

### Printen met Péage op een C&CZ-beheerde Linux machine

De printer “FollowMe” is te zien bij de printers. Ook command line
printing met `lpr -PFollowMe` is mogelijk. Hiervoor moet wel uw U- of
s-nummer geregistreerd staan bij C&CZ. Dit kunt u controleren op de
[Doe-Het-Zelf site](https://dhz.science.ru.nl).

### Printen met Péage op een zelfbeheerde Linux machine

Je hebt je RU-account (u/f/e/s-nummer en RU-wachtwoord, niet je Peage
PIN-code) nodig om een printopdracht te versturen.\
Hieronder volgt een beknopte beschrijving voor Ubuntu 18.04 (Bionic
Beaver). Deze procedure zal waarschijnlijk ook werken voor andere
moderne Linux systemen, gebruikmakend van de Gnome Desktop. Controleer
dat volgende packages:

-   **smbclient**
-   **python3-smbc**
-   **dbus-x11**

geinstalleerd zijn. Voeg ze anders toe via je package manager.

-   Stap 1\
    Als u een C&CZ beheerde Linux werkplek gebruikt, dan is de printer
    reeds geïnstalleerd. Ga naar stap 6. Ga via
    **Settings** naar
    **Printers**. Of installeer de
    **system-config-printer** tool en
    start deze via de commandline met het commando:
    `system-config-printer`.

-   Stap 2\
    Klik onder de de Printer lijst op **Additional
    Printer Settings…** en vervolgens binnen de pop-up
    app **Printers - localhost** op de
    **Add** button. Er verschijnt
    mogelijk een window **Authenticate**,
    voer het gevraagde root wachtwoord in.

-   Stap 3\
    Er verschijnt een Window **New
    Printer**, selecteer hier **Network
    Printer** en vervolgens **Windows
    Printer via SAMBA**. Aan de rechterkant kun je nu
    gegevens invullen: SMB Printer:
    **<smb://print.hosting.ru.nl/FollowMe>**
    Authentication: **Prompt user if authentication is
    required**. Als deze optie niet aanwezig is, dan
    herstart de installatie met de system-config-printer utility welke
    deze wel heeft.

-   Klik op <strong>Forward</strong>.

-   Stap 4\
    In het volgende scherm moet je een driver kiezen: Selecteer
    **Provide PPD file**. Selecteer
    tevens het
    [FollowMe.ppd](/downloads/2022/FollowMe.ppd)
    (Postscript Printer Description) bestand, welke je eerst moet
    downloaden via rechtsklikken op de link. Klik op
    **Forward**. Je ziet nu een window
    **Installable Options** waarin je
    instellingen kunt aanpassen. Indien je de corrrecte PPD file hebt
    gedownload, hoeft je niets aan te passen. Klik op
    **Forward**.

-   Stap 5\
    Vul nu onderstaande gegevens in: Printer Name: FollowMe *(gebruik de
    naam van de printwachtrij, zie [Onderscheid
    gebruikers](#.5bonderscheid_gebruikers.5d.5buser_groups.5d))*
    Description: KONICA MINOLTA C759SeriesPS/P Location: Virtual
    printqueue / available at all Peage KM printers

-   Klik op <strong>Apply</strong>.
        Er verschijnt mogelijk een window <strong>Authenticate</strong>
        Voer bij <strong>Username</strong> in: eerst de domain prefix
        <strong>ru\\</strong> direct gevolgd door je u/s/e/f-nummer, bv
        <strong>ru\\u123456</strong>, en bij <strong>Password</strong> je
        RU-wachtwoord (niet je Peage PIN-code).
        Klik <strong>Cancel</strong> op de vraag <strong>Would you like to
        print a test page?</strong>.

-   Stap 6\
    Eerst stop de cups server voordat je config aanpast:

`   sudo /etc/init.d/cups stop`

-   Edit de file /etc/cups/printers.conf en wijzig in de voor deze
        printer specifieke configuration de regel

`   AuthInfoRequired none`\
` `

naar

`   AuthInfoRequired username,password`

-   herstart de cups server:

`   sudo /etc/init.d/cups start`

-   Stap 7\
    De printer is geïnstalleerd en klaar voor gebruik. Het beste is om
    eerst met Firefox een test print te maken om de credentials te
    initialiseren en op te slaan in de gnome keyring. Gebruik Firefox,
    want niet alle Linux programma’s doen authentificatie correct, maar
    nadat je credentials correct zijn opgeslagen in de gnome-keyring,
    zal printen met alle Linux-applicaties goed werken. Dus als je de
    eerste keer gebruik maakt van de printer zal er een bericht
    **Authentication is required to print
    …** worden getoond waarin tevens naar je credentials
    wordt gevraagd. Het kan zijn dat je printjob wordt gepauzeerd tbv
    authenticatie. In dat geval open de print queue en klik met de
    rechter muis knop op de printjob. In het context menu dat verschijnt
    selecteer de authenticate option. Het is een goed idee op de print
    queue applet geopend te houden en te controleren op printjobs die
    geauthenticeerd dienen te worden. Voer bij
    **Username** in: eerst de domain
    prefix **ru\\** direct gevolgd door
    je u/s/e/f-nummer, bv
    **ru\\u123456**, en bij
    **Password** je RU-wachtwoord (niet
    je Peage PIN-code).

-   Credentials worden in de gnome-keyring opgeslagen en kunnen worden
        bekeken of gewijzigd met de tool seahorse. Seahorse is mogelijk niet
        standaard geinstalleerd en je zal het moeten installeren met je
        package manager.

-   Stap 8\
    Ga verder onder [Afdrukken van een printjob bij de
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

#### Fix voor verkeerde credentials in keyring

Credentials are stored in the gnome-keyring which can be viewed or
modified with the tool seahorse. Before using seahorse you probably need
to install it first with your package manager.

method 1: Update your password in the gnome-keyring:

-   start the program: "Seahorse" also called "Passwords and Keys"
        at the left sidebar select "Logins" under the "Passwords" section.
        at the right select the entry with label
        "<ipp://localhost:631/printers/FollowMe>"
        double click this entry to open a popup window
        in the popup window edit your ru-password
        close the popup window by pressing the X on the window title bar
        before it closes the window you are asked: "Save changes for this
        item?
        answer by pressing "OK", and the windows close
        close the "Seahorse/Passwords and Keys" program
        DONE

method 2: however sometimes updating your new password in the
gnome-keyring doesn’t solve the authentication problem. In that case
follow the steps below:

-   Verwijder je huidige keyrings directory in \~/.local/share/keyrings
        **Log uit en log onmiddellijk weer in.** Dit zorgt ervoor dat de
        keyring server opnieuw wordt opgestart, welke weer netjes de
        keyrings/ folder weer aanmaakt. Check dat er een nieuwe keyrings
        directory is gemaakt.
        Start Firefox en print een pagina.
        Je moet nu een dialoogvenster krijgen met een checkbox "Remember
        password". Vul je RU credentials in en check de checkbox om je
        credentials op te slaan in de gnome keyring.
        Vanaf nu zou je moeten kunnen printen zonder dat het nodig is om je
        credentials in te voeren, omdat ze uit je keyring genomen worden.
        Die keyring is versleuteld met het wachtwoord waarmee je inlogt op
        deze computer.

### Printen met Péage met gebruik van smbclient (op Linux)

Als je U-nummer u123456 is en je het bestand “iets.pdf” wil printen:

-   Step 1\
    smbclient -U “ru\\u123456” //print.hosting.ru.nl/FollowMe -c “print
    iets.pdf” Tik je RU-wachtwoord als daarom gevraagd wordt en loop
    naar een MFP om de printjob uit te draaien.

### Printen met Péage met gebruik van CUPS (op Linux)

If printing using the smbclient command works, a regular print queue can
be set up with CUPS. Usually this requires root access.

-   Step 1\
    With a browser open: <https://localhost:631> and select “Adding
    Printers and Classes”, “Add Printer” [Username: root, Password:
    `<rootpassword>`“, Other Network Printers:”Windows Printer
    via SAMBA“,”Continue".

-   Step 2\
    Enter connection:
    <smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe>, “Continue” We
    could enter the actual \_USER and \_PASSWORD, but is more secure to
    keep the strings “\_USER” and “\_PASSWORD”, and set them later (see
    below).

-   Step 3\
    Provide a PPD File: [Browse], upload the ppd file
    [FollowMe.ppd](https://wiki.cncz.science.ru.nl/images/c/cd/FollowMe.ppd),
    and click “Add Printer”. After this, “lpq -P FollowMe” should show
    “FollowMe is ready”.

-   Step 4\
    CUPS will store the information from step 3 into
    /etc/cups/printers.conf There should be details of the printer, and
    also these lines: AuthInfoRequired username,password DeviceURI
    <smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe> The actual
    \_USER and \_PASSWORD can be set, and the AuthInfoRequired can be
    set: AuthInfoRequired none After restarting cups (“service cups
    restart”) printing should work. To make this the default add “export
    PRINTER=FollowMe” to \~/.bashrc and print with lpr, or using, e.g.,
    okular or evince. The default setting is to print in grayscale. If
    you want to print in color, change the settings. With lpr, use
    `-o SelectColor=Color`. To set this option permanently use:
    `"/usr/bin/lpoptions -o     SelectColor=Color"`. This will store the
    setting in `/etc/cups/lpoptions`.

-   Step 5\
    After a reboot, AuthInfoRequired in /etc/cups/printers.conf may be
    reset to “username,password”. Also, you may not like a password in
    plain ASCII. This can be fixed by changing the backend: the “smb” in
    printer.conf refers to /usr/lib/cups/backend/smb, which usually is a
    link to /usr/bin/smbspool. Change /usr/lib/cups/backend/smb into a
    bash script: \#!/bin/bash export AuthInfoRequired none export
    DEVICE\_URI=“<smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe>”
    /usr/bin/smbspool “\$@” You can now use the actual RU-username and
    password, or create a script that has a more secure way of obtaining
    the password

### Afdrukken van een printjob bij de MFP

-   Stap 1\
    Log in met je RU-account, zonder de
    **ru\\** prefix, bv.
    **u123456** en RU-wachtwoord of uw 6
    cijferige Péage PIN op een van de Konica Minolta MFP’s. Alle MFP’s
    hebben een paslezer, dus de gekoppelde kaart (campuskaart of andere)
    swipen is voldoende. Er is een [handleiding om een kaart te
    koppelen](https://www.ru.nl/facilitairbedrijf/printen-nieuwe-mfp/uitloggen-printer/inloggen-campuskaart/).
    Kies **Print**, selecteer de
    printopdracht(en). Bij **Details**
    naast de printopdracht kun je zien hoeveel deze printopdracht kost.
-   Stap 3\
    Druk op de blauwe knop om de printopdracht(en) uit te voeren.
-   Stap 4\
    Logout

## Genereer PIN voor Péage

-   Stap 1\
    Bezoek de site <https://peage.ru.nl/>. Deze site is wereldwijd
    beschikbaar.
-   Stap 2\
    Selecteer **Peage Portal**.
-   Stap 3\
    Je wordt nu doorgestuurd naar de website <https://safeq.ru.nl/>.\
    Log in met je RU-account, weer zonder de
    **ru\\** prefix, bv.
    **u123456** en RU-wachtwoord of Péage
    PIN op de site.
-   Stap 4\
    Klik de link “Generate PIN” welke dan een 6 cijferige pincode voor
    je zal genereren.
-   Stap 5\
    Log out.

## Overzicht gebruik, stand en geschiedenis budget Péage

-   Stap 1\
    Bezoek de site <https://peage.ru.nl/>. Deze site is wereldwijd
    beschikbaar.
-   Stap 2\
    Selecteer **Peage Portal**.
-   Stap 3\
    Je wordt nu doorgestuurd naar de website <https://safeq.ru.nl/>.\
    Log in met je RU-account, weer zonder de
    **ru\\** prefix, bv.
    **u123456** en RU-wachtwoord of Péage
    PIN op de site.
-   Stap 4\
    Linksboven in de pagina klik de tab “Reports” welke dan de reports
    pagina voor je toont.
-   Stap 5\
    Log out.
