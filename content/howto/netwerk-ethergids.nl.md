---
author: petervc
date: '2022-07-05T11:29:47Z'
keywords: []
lang: nl
tags:
- netwerk
title: Netwerk ethergids
wiki_id: '92'
---
## C&CZ Ethernet gids

De ethernet gegevens worden grotendeels automatisch in een database
bijgehouden. Men kan deze gegevens hieronder opvragen door te zoeken op
MAC-adres, IP-adres, kamernummer, outletnummer of machinenaam.

[FNWI Ethergids](https://rbsgids.science.ru.nl/cgi-scripts/ethergids.php)

Fout in gids gevonden? Neem [contact](../contact) met ons op!
