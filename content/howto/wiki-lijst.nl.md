---
author: polman
date: '2020-12-04T14:58:04Z'
keywords: []
lang: nl
tags:
- wiki
title: Wiki lijst
wiki_id: '633'
---
# Wikis beheerd door C&CZ

## IIwerkplaats

- [algemeen](https://lab.cs.ru.nl/algemeen)

## Science

- [clean](https://wiki.clean.cs.ru.nl/clean)
- [exaktueel](https://wiki.exaktueel.science.ru.nl/)
- [gi](https://wiki.gi.science.ru.nl/)
- [hef](https://wiki.hef.science.ru.nl/)
- [icis-intra](https://wiki.icis-intra.cs.ru.nl/)
- [isp](https://wiki.isp.science.ru.nl/)
- [mbs](https://wiki.mbs.science.ru.nl/)
- [microbiology](https://wiki.microbiology.science.ru.nl/)
- [natuurkundepracticum](https://wiki.natuurkundepracticum.science.ru.nl/)
- [new-devices-lab](https://new-devices-lab.cs.ru.nl)
- [Planco](https://wiki.planco.science.ru.nl/)
- [privacy](https://wiki.privacy.cs.ru.nl)
- [tfpie](https://wiki.tfpie.science.ru.nl)
- [theochem](https://wiki.theochem.ru.nl/)
