---
author: petervc
date: '2022-10-27T15:25:24Z'
keywords: []
lang: en
tags: []
title: X on Windows
wiki_id: '906'
---
## Using X applications from your Windows PC

[MobaXterm](http://mobaxterm.mobatek.net/) is software which is available on the
[S-disc](/en/howto/s-schijf/). From the MobaXterm website: “MobaXterm is
an enhanced terminal for Windows with an X11 server, a tabbed SSH client
and several other network tools for remote computing (VNC, RDP, telnet,
rlogin). MobaXterm brings all the essential Unix commands to Windows
desktop, in a single portable exe file which works out of the box.” The
support of OpenGL could also be a reason to start using MobaXterm. If
you use it professionally, you should consider subscribing to
[[MobaXterm Professional
Edition](http://mobaxterm.mobatek.net/download.html).
