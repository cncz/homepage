---
author: petervc
date: '2022-11-08T09:49:45+01:00'
keywords: []
lang: en
tags:
- overcncz
title: System Development
wiki_id: '132'
---
## System Development and Administration

System development and administration is located in room HG00.545.

## Who are they?

-   {{< author "remcoa" >}}\
    {{< author "bram" >}}\
    {{< author "miek" >}}\
    {{< author "alexander" >}}\
    {{< author "wim" >}}\
    {{< author "ericl" >}}\
    {{< author "sioo" >}}\
    {{< author "polman" >}}\
    {{< author "arnoudt" >}}\
    {{< author "visser" >}}

## What do they do?

System Administration takes care of

-   Management of the [servers of the faculty and of various departments](/en/howto/hardware-servers/).
-   Back-up and restore of the data on the servers.
-   Management of workstations in the departments and in the [computer labs and publicly available workstations](/en/howto/terminalkamers/):
    Managed PC’s with MS-Windows and/or Ubuntu Linux.

For all other computers (hundreds of MS-Windows PCs and tens of Linux
PCs and MACs, not managed by C&CZ) it is understood that the owner
(department) takes care of the systems management. For these systems,
C&CZ system administration can try to answer questions.

-   Support for education renewal and digital provision of information.
-   Internet address administration (*nameserver*, DNS/DHCP) for the
    Faculty of Science.
