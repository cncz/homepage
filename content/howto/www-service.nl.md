---
author: polman
date: '2007-05-30T12:05:18Z'
keywords: []
lang: nl
tags:
- internet
title: WWW Service
wiki_id: '76'
---
## Een nieuwe WWW service beginnen

Dit document beschrijft in het kort welke beslissingen er genomen moeten
worden voordat een nieuwe WWW service binnen de B-faculteit gestart kan
worden en biedt bovendien de mogelijkheid om C&CZ rechtstreeks vanuit
WWW een verzoek tot het starten van een nieuwe WWW service te sturen.

### Keuze van de server

De server is de computer waarop de WWW *daemon*, dat is het programma
dat de WWW pagina’s de wereld in stuurt, draait. Er zijn twee
mogelijkheden om uit te kiezen:

1.  Een eigen server die niet in beheer is bij C&CZ.

In dit geval kan C&CZ helpen bij het installeren van de daemon en vragen
beantwoorden bij het opzetten van de WWW service, maar de vakgroep of
afdeling beheert alles verder zelf.

1.  Een van de centrale servers waar al een daemon op draait.

Het voordeel van deze opzet is dat een nieuwe service zeer snel in de
lucht kan zijn.

### Keuze van de naam van de service

Net zoals met e-mail adressen dient het veranderen van de naam van een
WWW service vermeden te worden. Het is daarom van groot belang om direct
een goede naam of afkorting te kiezen. Als wordt gekozen voor een
service op een (de)centrale, door C&CZ beheerde server, moet de naam
uniek zijn binnen het domein en wordt aangeraden om een korte naam te
nemen. Een naam of afkorting van maximaal 5 letters en cijfers heeft een
paar kleine organisatorische voordelen.

### Wie gaan de pagina’s onderhouden

Vervolgens moet worden gekeken wie (d.w.z. welke logins) de pagina’s
gaan onderhouden. Een groepje van enkele mensen heeft het voordeel dat
men niet afhankelijk is van (de aanwezigheid van) 1 persoon terwijl het
werk toch te overzien en coördineren blijft.

### Wie is (zijn) de contactperso(o)n(en)

Het is een goede gewoonte om onderaan elke WWW pagina aan te geven met
wie contact opgenomen moet worden als er vragen of problemen zijn met
betrekking tot de WWW service. Gebruikelijk is om in elk geval een
e-mail adres te geven. Het adres wordt ook door C&CZ gebruikt om contact
met de groep op te kunnen nemen.

### Een nieuwe WWW service aanvragen

Als de bovenstaande vier vragen zijn beantwoord en de keuze is gevallen
op een (de)centrale server, dan kan men bij C&CZ een nieuwe WWW service
aanvragen door een [mailtje te sturen](../contact). Er zal dan een [standaard
setup](/nl/howto/apachesetup/) worden aangeboden.

### Is er een database nodig

Veel websites maken gebruik van een database backend, met name de
combinatie php en mysql wordt veel gebruikt. C&CZ beheert een aantal
mysql servers waar gebruik van gemaakt kan worden. Indien je een
database nodig hebt kun je deze aanvragen door een [mail te sturen](../contact) 
met daarin de naam van de database en de website waarvoor je de database wilt gebruiken.
