---
author: miek
date: '2023-07-05T15:12:45Z'
keywords: [ zfs ]
lang: nl
title: Snapshots met ZFS
tags:
- storage
cover:
  image: img/2023/storage.jpg
---

Netwerkschijven met [ZFS](https://openzfs.org/wiki/Main_Page) als bestandssysteem hebben de
mogelijkheid tot het maken van *snapshots*. C&CZ heeft dit standaard aan staan.

Een *snapshot* is een alleen-lezen kopie van de schijf die op een bepaald moment gemaakt is. In zo'n
snapshot kun je oudere versie van bestanden terug vinden. Op de meeste schijven gaan de snapshots
zo'n 45 dagen terug, daarna worden ze verwijderd.

{{< notice tip >}}
Als je een bestand verwijderd, kun je dus nog 45 dagen "terug in de tijd", daarna is het bestand ook
_echt_ weg van de schijf.

Een snapshot is *geen* alternatieve bewaarplek voor je bestanden.
{{< /notice >}}

Hieronder kun je lezen hoe je kunt zien hoe ZFS snapshots werken onder Linux en Windows.

## Linux

In je (network)schijf of home directory het je een `.zfs` met daarin een `shapshot` directory. Als
de directory-inhoud bekijkt:

~~~ txt
# ls -l .zfs/snapshot/
total 22
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230630_043047_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230630_203047_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230701_043046_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230701_163047_000
~~~

Elke directory hier heeft de prefix 'cncz_' en dan de datum en tijd wanneer de snapshot is gemaakt.
Binnen die directory vind je een "kopie" van jouw schijf zoals die was op die datum en tijd.

## Windows

Als je rechts klikt op een folder op je netwerk schijf en dan "Eigenschappen" kiest, zie je het
volgende:

{{< figure src="/img/2023/zfs-snapshot-1.png" >}}

Als je dan op het tabblad "Vorige Versies" (Previous Versions) klikt, krijg je de vorige (snapshot)
versies van die folder te zien.

{{< figure src="/img/2023/zfs-snapshot-2.png" >}}

Je kunt dan rondneuzen in vorige versies van je bestanden of ze terugzetten.
