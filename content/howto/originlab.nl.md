---
author: petervc
date: '2022-11-08T16:37:04Z'
keywords: []
lang: nl
tags:
- software
title: OriginLab
wiki_id: '943'
---
### OriginLab

Origin van [Originlab](http://www.originlab.com/) is software voor
wetenschappelijke grafieken en data-analyse.

### Licentie

C&CZ heeft voor FNWI een netwerklicentie afgesloten voor het gebruik van
OriginPro. Men kan de software en installatiesleutels van OriginPro
krijgen van [C&CZ
systeembeheer](/nl/howto/systeemontwikkeling/).

De netwerklicentie kan alleen door computers die aan het Internet zitten
gebruikt worden. Voor standalone gebruik zijn ook home-use licenties in
deze licentie inbegrepen. Het huidige onderhoudscontract loopt tot augustus 2026.

Als je een melding krijgt dat de licentie binnenkort verloopt en je
gebruikt een standalone versie dan kun je de licentie bijwerken door
onder het Help menu te kiezen voor Activate.

Het gebruik van OriginPro wordt op de license server gelogd.

Op de door C&CZ beheerde machines met MS-Windows is OriginPro
beschikbaar.
