---
author: petervc
date: '2017-10-30T09:09:35Z'
keywords: []
lang: nl
tags: []
title: IPad
wiki_id: '857'
---
## Algemene opmerkingen

De iPad heeft een stringent security-model:

-   Alle apps hebben in principe alleen toegang tot hun eigen data, niet
    tot de data van andere apps.
-   Er is geen gezamenlijk data-gebied (zoals de harddisk in een laptop)
    waar alle apps bij kunnen.
-   Apps kunnen wel data doorgeven aan andere apps, b.v.
    -   een document of foto doorgeven aan de **Mail** app om het als
        attachment te versturen;
    -   een foto doorgeven aan een app die foto’s kan bewerken.

*Bestanden op de iPad zetten betekent dus dat die bestanden in een
bepaalde app worden opgenomen, maar niet automatisch ook voor alle
andere apps beschikbaar zijn.*\
De iPad kan daarom het beste beschouwd en gebruikt worden als een
“viewer” op data die elders beschikbaar is:

-   op het Internet (websites) via **Safari** of een andere browser app;
-   op een applicatie-server via apps als **NS**, **buienradar**,
    **NU.nl**, enz.;
-   op media-servers via apps als **YouTube**, **Radio 1**,
    **UitzendingGemist**, enz.;
-   op game-servers via apps als **Wordfeud**;
-   op een fileserver via speciale apps als **FileBrowser** of
    **FileApp**;
-   op een mailserver via de standaard **Mail** app of een alternatief;
-   op een calendar server via de standaard **Cal** app of een
    alternatief zoals **WeekCal**;
-   enz.

Door geen data op de iPad te zetten:

-   ontstaat er geen verwarring over verschillende versies van een
    document;
-   hoeft er geen data van de iPad gebackupped te worden;
-   kan er geen data verloren gaan als de iPad gestolen wordt of
    anderszins verloren gaat.

Deze werkwijze maakt een andere *workflow* noodzakelijk dan gewoonlijk
op een laptop wordt gebruikt. (VERDER UITWERKEN)\
Er zijn overigens wel apps die automatisch bestanden synchroniseren
tussen b.v. de eigen PC of laptop en de iPad, zoals **GoodReader**,
**FileBrowser** en **FileApp**. Het is dus wel mogelijk om de iPad als
een soort laptop te gebruiken.

## Gebruik

### Ingebruikname

Om apps te kunnen downloaden moet een Apple ID (ook wel iTunes account
genaamd) worden aangemaakt.

-   Ga naar <https://appleid.apple.com/> en creeer een account.

Om de iPad te initialiseren moet de iPad verbonden worden met een PC of
Mac waarop iTunes is geinstalleerd [
[afbeelding]]{._toggler--connect_itunes}.

::: {#connect_itunes style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_01\_connect\_to\_itunes.png" title="IPad\_Init\_01\_Connect\_to\_iTunes.png" >}}
:::

-   Ga naar <http://itunes.apple.com/> om iTunes te downloaden.
-   Installeer iTunes op de PC, laptop of Mac die gebruikt gaat worden
    om de iPad mee te beheren.
    -   Door de iPad aan deze computer te hangen wordt een backup
        gemaakt.
    -   Foto’s en andere bestanden kunnen via iTunes van en naar de iPad
        worden getransporteerd.
    -   Software updates voor de iPad kunnen via iTunes worden
        geinstalleerd.
-   Zet de iPad aan en verbindt hem met de gekozen computer.
-   De iPad wordt geinitialiseerd [
    [afbeelding]]{._toggler--init_ready}. De 3G versie van de iPad kan
    functioneren zonder sim als er Wi-Fi beschikbaar is.

::: {#init_ready style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_02\_ready.png" title="IPad\_Init\_02\_Ready.png" >}}
:::

-   Er wordt gevraagd om de iPad te registreren en een Apple ID aan te
    maken [ [afbeelding]]{._toggler--init_register}.

::: {#init_register style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_03\_register.png" title="IPad\_Init\_03\_Register.png" >}}
:::

-   Als deze iPad al eens gebackupped is op deze computer wordt de
    mogelijkheid gegeven om deze backup terug te zetten. Dit is nuttig
    na een volledige reset van de iPad. De iPad kan ook als nieuwe iPad
    geinitialiseerd worden [ [afbeelding]]{._toggler--init_setup}.

::: {#init_setup style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_04\_setup.png" title="IPad\_Init\_04\_SetUp.png" >}}
:::

-   Als er geen backup wordt teruggezet, wordt gevraagd naar een naam
    voor de iPad en of muziek, foto’s en apps moeten worden
    gesynchroniseerd [ [afbeelding]]{._toggler--init_sync}.

::: {#init_sync style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_05\_sync.png" title="IPad\_Init\_05\_Sync.png" >}}
:::

-   Het kan handig zijn om iTunes automatisch te laten starten als de
    iPad met de computer wordt verbonden [
    [afbeelding]]{._toggler--init_itunes}.

::: {#init_itunes style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_06\_itunes.png" title="IPad\_Init\_06\_iTunes.png" >}}
:::

-   De huidige configuratie wordt gebackupped; muziek, foto’s en apps
    worden gesynchroniseerd [ [afbeelding]]{._toggler--init_syncing}.

::: {#init_syncing style="display: none; border: 1px dashed #cccccc; background: white; margin: 0px; padding: 0px"}
{{< figure src="/img/old/ipad\_init\_07\_syncing.png" title="IPad\_Init\_07\_Syncing.png" >}}
:::

### De iPad beheren

Hang de iPad regelmatig aan de beheer-PC of -Mac om:

-   de configuratie en inhoud van de apps te backuppen;
-   de op de iPad gedownloade apps te backuppen;
-   evt. adresboeken, agenda-gegevens, enz. te synchroniseren;
-   iOS updates te installeren;
-   apps te updaten.

Download nieuwe foto’s en video’s van de iPad

-   op een Mac via iPhoto (start automatisch als de iPad wordt
    aangesloten);
-   op een PC door de iPad als externe USB disk te benaderen.

### Apps installeren en updaten

### Wi-Fi Eduroam

Het Eduroam draadloze netwerk is in alle gebouwen op de campus
beschikbaar en ook op veel andere universiteiten in de wereld, zie
[<http://www.eduroam.nl> Eduroam.nl.

-   Browse naar <http://www.ru.nl/wireless> voor alle informatie en
    configuratie-handleidingen.

-   Als er om gebruikersnaam en wachtwoord wordt gevraagd: dit kan een
    van de volgende twee combinaties zijn:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| \| Gebruikersnaam \| Wachtwoord \|- \| keuze 1 \| *u-nummer\@ru.nl*
\| *RU-wachtwoord (dat ook voor [Radboudnet](http://www.radboudnet.nl)
wordt gebruikt)* \|- \| keuze 2 \| *sciencelogin*\@science.ru.nl \|
*Science-wachtwoord (dat ook voor de [Doe-Het-Zelf
website](http://dhz.science.ru.nl) wordt gebruikt)* \|}\
Controleer evt. de instellingen van het netwerk (blauwe pijltje), hier
zal o.a. staan:

-   IP-adres: een adres uit de reeks 145.116.128.0 - 145.116.191.255
-   Zoekdomeinen: ru.nl

## Configuratie mail en agenda

De iPad heeft voor de meest populaire mail voorzieningen (Google,
Exchange, Hotmail, enz.) directe ondersteuning en ondersteunt ook SMTP,
IMAP/POP en CalDav zodat alle mail- en agenda-servers die zich aan deze
open standaarden houden worden ondersteund.

### Science mail

-   Open **Instellingen**
-   Kies *E-mail, contacten, agenda’s*
-   Kies *Voeg account toe…*
-   Kies *Anders*
-   Kies *Voeg mailaccount toe*

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Naam \| *Voornaam Achternaam* \|- \| Adres \|
*V.Achternaam*\@science.ru.nl \|- \| Wachtwoord \| *het wachtwoord dat
hoort bij de science loginnaam* \|- \| Beschrijving \| *naar eigen
inzicht, b.v.* “Science” \|}\
\* Kies *Volgende*, de iPad probeert zelf de servers te vinden maar
heeft daar hulp bij nodig:\
{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Type \| IMAP \|- \| colspan=“2” \| *Server inkomende post* \|- \|
Hostnaam \| post.science.ru.nl \|- \| Gebruikersnaam \| *science
loginnaam* \|- \| Wachtwoord \| *is al ingevuld* \|- \| colspan=“2”\|
*Server uitgaande post* \|- \| Hostnaam \| smtp.science.ru.nl \|- \|
Gebruikersnaam \| *science loginnaam* \|- \| Wachtwoord \| *hetzelfde
wachtwoord als boven* \|}\
\* Kies *Volgende*, de toegang wordt gecontroleerd

-   Zet E-mail (synchronisatie) aan, Notities uit
-   Kies *Bewaar*, het account wordt toegevoegd

Controleer of het werkt:

-   Open de **Mail** app
-   Ga helemaal terug naar *Postbussen*
-   Kies het nieuwe account:
    -   Onder *Inkomend* voor alleen de inkomende mail
    -   Onder *Accounts* voor alle folders van dit account, kies
        vervolgens de gewenste folder

### Google mail en agenda

-   Open **Instellingen**
-   Kies *E-mail, contacten, agenda’s*
-   Kies *Voeg account toe…*
-   Kies *Google Mail*

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Naam \| *Voornaam Achternaam* \|- \| Adres \| *een bestaand
Gmail-adres* \|- \| Wachtwoord \| *het wachtwoord dat hoort bij dit
Gmail-adres* \|- \| Beschrijving \| *naar eigen inzicht, b.v.* “Gmail”
\|}\
\* Kies *Volgende*, de toegang wordt gecontroleerd

-   Zet naar wens synchronisatie aan van E-mail, Agenda’s en Notities
-   Kies *Bewaar*, het account wordt toegevoegd

Controleer of het werkt:

-   Open de **Mail** app
-   Ga helemaal terug naar *Postbussen*
-   Kies het nieuwe account:
    -   Onder *Inkomend* voor alleen de inkomende mail
    -   Onder *Accounts* voor alle folders van dit account, kies
        vervolgens de gewenste folder

### Google agenda (CalDAV)

Alleen synchroniseren met een Google agenda zonder een Gmail
mail-account toe te voegen kan ook.

-   Open **Instellingen**
-   Kies *E-mail, contacten, agenda’s*
-   Kies *Voeg account toe…*
-   Kies *Anders*
-   Kies *Voeg CalDAV-account toe*

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Server \| www.google.com \|- \| Gebruikersnaam \| *een bestaand
Gmail-adres* \|- \| Wachtwoord \| *het wachtwoord dat hoort bij dit
Gmail-adres* \|- \| Beschrijving \| *naar eigen inzicht, b.v.* “Google”
\|}\
\* Kies *Volgende*, de toegang wordt gecontroleerd (CHECK)

-   Kies *Bewaar*, het account wordt toegevoegd (CHECK)

Controleer of het werkt:

-   Open de **Agenda** app
    -   Controleer dat er afspraken uit Google zichtbaar zijn, of
    -   Druk op *Agenda’s* en controleer dat de Google agenda
        beschikbaar is

### Exchange (RU mail)

-   Open **Instellingen**
-   Kies *E-mail, contacten, agenda’s*
-   Kies *Voeg account toe…*
-   Kies *Microsoft Exchange*
-   Vul in:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| E-mail \| *V.Achternaam*\@fnwi.ru.nl \|- \| Domein \| RU \|- \|
Gebruikersnaam \| *U-nummer* \|- \| Wachtwoord \| *het wachtwoord bij
het U-nummer (hetzelfde als van Intranet, FLEX)* \|- \| Beschrijving \|
*naar eigen inzicht, b.v.* “Exchange” \|}\
\* Kies *Volgende*, de iPad probeert zelf de server te vinden maar heeft
daar hulp bij nodig:\
{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Server \| outlookweb.ru.nl \|}\
\* Druk op *Volgende*

-   Kies welke gegevens gesynchroniseerd moeten worden (E-mail,
    Contacten, Agenda)
-   Druk op *Bewaar*, het account wordt toegevoegd

Controleer of het werkt:

-   Open de **Mail** app
-   Ga via het drop-down menu linksboven helemaal terug naar
    *Postbussen*
-   Kies het nieuwe account uit onderstaande mogelijkheden:
    -   Onder *Inkomend* voor alleen de inkomende mail
    -   Onder *Accounts* voor alle folders van dit account, kies
        vervolgens de gewenste folder

## Configuratie VPN

Zie de [ISC
VPN](http://www.ru.nl/ict/medewerkers/off-campus-werken/vpn/) webpagina.

## Toegang tot een netwerk-disk

### C&CZ Samba shares

Deze opzet werkt in principe voor alle zogenaamde “Samba” (ook wel “smb”
of “CIFS”) netwerk shares.

Van buiten de campus is altijd een [VPN](/nl/howto/vpn/)-verbinding nodig
om een schijf te mogen aankoppelen, voor enkele meer afgeschermde shares
is dat ook op de campus nodig.

Er is een app nodig, de iPad ondersteunt uit zichzelf geen Samba. Het
voorbeeld gaat uit van **FileBrowser**. Een alternatief is
**GoodReader**.

De FileBrowser app kan Office documenten tonen maar niet editen.
Installeer daarvoor de app **Documents To Go**. FileBrowser kan ook PDF
documenten tonen.\
\* Bepaal de naam van de fileserver. Gangbare situaties zijn:

-   -   *Science home-directory*: log in op [de Doe-Het-Zelf
        site](http://dhz.science.ru.nl) en kijk op de Home page achter
        “Eigen schijf op” - Hier staat dan iets als:
        \\\\pile.science.ru.nl\\*mylogin*
    -   *Gebackupte netwerkschijf* (ook wel “volletje” genoemd) of een
        *web-server directory*: ga op de eigen werkplek na wat het
        netwerk-pad van deze netwerk share is
        -   Via “Mijn Computer” onder Windows, rechts-klik op de
            betreffende netwerk-schijf en bekijk de eigenschappen: het
            netwerkpad ziet er uit als
            \\\\*servernaam*.science.ru.nl\\*sharenaam*, met servernaam
            gelijk aan sharenaam-srv.
        -   Via de “Finder” onder MacOS X, klik op de betreffende
            netwerk-schijf en bekijk de eigenschappen met Command-I: het
            netwerkpad ziet er uit als
            <smb://>*servernaam*.science.ru.nl/*sharenaam*

\* Installeer de app **FileBrowser**

-   Open **FileBrowser**
-   Kies *Menu*
-   Kies *+* om een nieuwe netwerk server toe te voegen
-   Vul in:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Address \| \\\\*servernaam*.science.ru.nl (Windows-notatie) **of**
<smb://>*servernaam*.science.ru.nl (Mac-notatie) \|- \| colspan=“2” \|
Schakel *Show More Settings* in \|- \| Display Name \| *naar eigen
inzicht te kiezen, b.v.* “Home” \|- \| User Name \| Kies *Edit…* en vul
de *science loginnaam* in \|- \| Password \| Veilig is *On demand* zodat
het wachtwoord elke keer ingevuld moet worden, kies evt. *Edit…* en vul
het science wachtwoord in \|- \| colspan=“2”\| Schakel *Advanced
Settings* in \|- \| Enable NetBIOS \| ingeschakeld - de waarschuwing kan
genegeerd worden als het een C&CZ fileserver betreft, klik op *OK* \|-
\| Compatibility Mode \| ingeschakeld - de waarschuwing kan genegeerd
worden als het een C&CZ fileserver betreft, klik op *OK* \|- \| Auto
List Shares \| ingeschakeld (default) als er meer dan 1 netwerkschijf
van deze server nodig is, anders uitgeschakeld \|- \| SMB Pipelining \|
uitgeschakeld \|- \| SMB Port Number \| 445 (default) \|}\
\* Kies *Save*, FileBrowser controleert of de server kan worden bereikt.

-   Er verschijnt een pop-up als het wachtwoord niet van tevoren was
    ingevuld.
    -   Vul het wachtwoord bij het *science account* in en druk op
        *Connect*.
-   Als *Auto List Shares* is:
    -   *ingeschakeld*: dan worden meteen alle shares op deze server
        getoond die toegankelijk zijn;
    -   *uitgeschakeld*: kies dan *Add Share* en vul de *sharenaam* in
        en kies *Add*.
-   Navigeren op de netwerk disk:
    -   Kies een directory (folder) om er in te gaan;
    -   Kies een document om het te openen, FileBrowser gebruikt een
        interne viewer (Stratosferix) om documenten te tonen;
    -   Tap op het document om het menu onder in beeld te krijgen:
        -   Kies de “export” optie om het document in een andere app te
            openen, b.v. in Documents To Go;
        -   Kies *+* om het huidige document te bookmarken;
        -   Kies de “bookmarks” optie om alle bookmarks te zien, dit is
            een handige manier om te switchen tussen documenten.
