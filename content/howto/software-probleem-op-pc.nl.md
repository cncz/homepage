---
author: petervc
date: '2012-08-17T14:35:48Z'
keywords: []
lang: nl
tags:
- storingen
- software
title: Software probleem op pc
wiki_id: '623'
---
Als u vragen heeft over systeemsoftware of applicaties op uw PC, en het
is niet goed mogelijk om dit per telefoon of e-mail
(helpdesk\@science.ru.nl) aan de helpdesk duidelijk te maken, dan kunt u
bij C&CZ uw probleem demonstreren op een van de twee PC’s die daarvoor
speciaal zijn ingericht.
