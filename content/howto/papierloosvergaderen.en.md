---
author: petervc
date: '2015-06-24T12:57:18Z'
keywords: []
lang: en
tags: []
title: PapierloosVergaderen
wiki_id: '988'
---
## Paperless meetings with Android tablets

The reason for writing this page is that the
[FGV](http://www.ru.nl/fnwi/over_de_faculteit/organisatie/facultaire_0/)
will try paperless meetings starting September 1, 2015. For the main
settings of an Android tablet or phone, see the
[Android](/en/howto/android/) page. Below we deal especially with the
topics that are of interest for paperless meetings: an app for
synchronizing the meeting documents to the tablet and one for annotating
PDF files. Finally we describe how the annotated files can be
synchronized to a backed-up disk.

### First things for the FGV

-   [Android](/en/howto/android/) tablet or smartphone, connected to a
    network, e.g. [Eduroam](/en/howto/android/).
-   [Science account](/en/howto/login/), that has been made member of the
    group “fgv” by the Faculty Bureau.
-   VPN correctly setup and connected.

### Synchronizing from and to a network drive

-   To do your FGV-work, it is necessary that you have your own set of
    FGV meeting files, placed on your tablet and that you can make notes
    in these files. It is also important that there is a daily backup of
    these set of meeting files, in order not to lose your remarks. You
    can do that by copying your set of files (including your notes) from
    your tablet to your home directory. Of course you could also mail
    yourself the annotated documents. For both synchronizations you can
    use the same app. Folder Sync (free with advertising: Folder Sync
    Lite) is an app to do this. There are also alternative applications,
    eg. SyncMe Wireless, but Folder Sync works properly. The steps are
    further explained below.

-   Start FolderSync, press “Create New Sync” at the bottom of the
    screen, give it a name and press “Next”.

[500px](/en/howto/bestand:android-foldersync-1-createnewsync.png/)

-   Press Add Account and select SMB/CIFS as Account Type:

[500px](/en/howto/bestand:android-foldersync-3-account.png/)

-   Give it a unique name, fill in the Connection and Login data and
    press Save:

[500px](/en/howto/bestand:android-foldersync-5-addaccount.png/)

-   Press “Folder pairs” in the main screen:

[500px](/en/howto/bestand:android-foldersync-6-folderpairs.png/)

-   Press the green “+” in the lower right corner to add a folder pair
    and fill in Unique name, Account, Sync type, Remote folder, Local
    folder and Scheduling:

[500px](/en/howto/bestand:android-foldersync-7-addfolderpair.png/)

-   Choose the source folder that you want to synchronize to your tablet
    and choose also the local target folder where the files will be
    synchronized to:

[500px](/en/howto/bestand:screenshot-2015-05-12-16-58-38-foldersync-synchronisatie-mappen.png/)

-   Choose at “Scheduling” how often synchronization will take place and
    press Save:

[500px](/en/howto/bestand:screenshot-2015-05-12-16-59-55-foldersync-synchronisatie-instellen.png/)

-   By pressing “Sync”, the synchronization is started manually:

[500px](/en/howto/bestand:android-foldersync-8-synced.png/)

-   Making a folder pair to synchronize to your home directory worsk the
    same way. First look in [DHZ](/en/howto/dhz/) what the location of
    your home directory is. If that is listed as:

`               Home disk at    :   \\bundle.science.ru.nl\yourloginname`

you have to use in the new account the Server address:
<smb://bundle.science.ru.nl/yourloginname>

### Annotation of PDF files

-   An app to annotate PDF files: **Foxit MobilePDF**. There are
    alternative apps, but Foxit works properly. Save annotated files
    under a different name, in order not to lose them during the next
    synchronization.

[500px](/en/howto/bestand:foxit-save-as.png/)
