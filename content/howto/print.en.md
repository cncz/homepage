---
author: wim
date: '2021-05-10T19:13:27Z'
keywords: []
lang: en
title: Printing
aliases:
- copiers
- kamerbreed
- km
- peage
- printbudget
- printers-en-printen
ShowToc: true
TocOpen: true
wiki_id: '47'
---
# Printing
> Through the Radboud University printing system, students and employees are able to print, scan, and copy anywhere on campus, regardless of the location or building at which they study or work. When printing, you send your print job to a print queue instead of a specific printer. The next step is to simply retrieve your print job from any printer. [Read more...](https://www.ru.nl/en/services/services-and-facilities/ict/printing-copying-and-scanning/printing)

## Printing from own laptop.
In case printing does not work on self managed devices, documents can be uploaded to <https://peage.ru.nl>. Other workarounds are described [here](../printing-workarounds).

## Binding, cutting, stapling and booklets
In the [Library of Science](http://www.ru.nl/library/library/library-locations/library-of-science/)
you can buy binders for your reports, etc. Also there is a cutting tool,
a 23 hole punch and several staplers. All Konica Minolta printers can
staple automatically.

In the printer at HG00.002 a punch unit and a saddle-unit that can fold are
available. This makes it possible to produce complete booklets. One can
punch 2 or 4 holes. At maximum 20 sheets can be saddle-stitched and
folded, so one can produce an A4 booklet from A3 sheets or an A5 booklet
from A4 sheets.

At the C&CZ office you can also find a cutting tool, a 23 hole punch and a
big stapler.

# Posters
If you're looking to get posters printed, we offer a range of materials to choose from:

| Material    | Description                                                                                                                | Maximum width | A0 costs |
|-------------|----------------------------------------------------------------------------------------------------------------------------|--------------:|---------:|
| Photo paper | HP Everyday Pigment Ink Satin Photo                                                                                        |         91 cm |  € 30.00 |
| Canvas      | Canvas prints do not show cracks as easily as paper prints, can even be carefully folded and included in the hand luggage. |         91 cm |  € 40.00 |

{{< notice info >}}
To ensure a smooth process, please deliver your print job in PDF format via email to <helpdesk@science.ru.nl>. Please let us know your preferred material and budget code (budgetnumber/verbijzondering) when submitting your print job.
{{< /notice >}}

# 3D printing
C&CZ operates a 3D print service for staff and students of the Faculty
of Science.

The cost for this service is €0.15 per gram.

If you are interested in using our 3D printing service or have any
questions, please [contact us](../contact).

{{< rawhtml >}}
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height=1800> 
{{< /rawhtml >}}
