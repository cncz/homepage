---
author: bram
date: '2022-09-19T16:30:58Z'
keywords: []
lang: nl
tags:
- netwerk
title: Netwerk draadloos
wiki_id: '86'
---
# Draadloos netwerk

Het [draadloze netwerk](http://www.ru.nl/wifi) dat geadviseerd wordt is
[eduroam](http://www.eduroam.org/). Authenticatie kan op de volgende
manieren:

-   Voornaam.Achternaam\@ru.nl en RU-wachtwoord
-   Science-loginnaam\@science.ru.nl en Science-wachtwoord

Medewerkers van O&O afdelingen binnen FNWI kunnen [Eduroam Visitor
Access accounts](https://eva.eduroam.nl/inloggen), die gasten toegang
geven tot een apart deel van het draadloze netwerk, aanvragen bij
[C&CZ-Werkplekondersteuning](/nl/howto/werkplekondersteuning/), tel.
20000. Elke RU-medewerker kan een gast ook toegang voor de rest van de
dag geven, door de gast een dagcode (sleutelwoord) te geven. Die code is
te vinden op [de RU portal](https://portal.ru.nl). Voor meer info zie de
[ILS website](https://www.ru.nl/ict/algemeen/wifi/wifi-gasten/). Gasten
kunnen zich hiervoor ook melden bij de Library of Science.
