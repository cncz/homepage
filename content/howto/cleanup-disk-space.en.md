---
author: bram
date: '2023-11-24T12:17:00Z'
keywords: []
lang: en
tags:
- storage
aliases:
ShowToc: true
TocOpen: true
title: Cleanup disk space
---
You might have received an email that your personal or group network disk is almost full.
This articles describes some tools to identify large files/directories and cleanup disk
space.

## Finding large files / directories with WinDirStat (graphical, Windows)

![windirstat screenshot](/img/2023/windirstat.png)


[WinDirStat](https://windirstat.net) is a disk cleanup tool for Windows that provides a
visual representation of disk space usage. It helps you to quickly identify and
understand which files and folders are consuming the most space. The program offers
detailed information about each file, including type, last modified date, and file
path. In addition to visualization, WinDirStat includes built-in cleanup options,
enabling you to directly delete or move files within the program.

{{< notice info >}}
On C&CZ managed windows systems, WinDirStat is installed in `S:\windirstat`
tice
{{< /notice >}}


## Finding large files & directories with Ncdu (text based, Linux)

![ncdu screenshot](/img/2023/ncdu.png)

[Ncdu](https://code.blicky.net/yorhel/ncdu/) is a command-line utility for Linux that
provides a simple and fast way to analyze disk usage. It presents a clear and interactive
interface, allowing users to navigate through directories, view file sizes, and identify
space-consuming files. Unlike graphical tools, ncdu is lightweight and operates
efficiently in terminal environments, making it a practical choice for users who prefer
command-line interactions.

To analye your home directory:

``` console
ssh lilo.science.ru.nl
~$ ncdu
```

For a network disk, use something like:

``` console
~$ ncdu /vol/mynetworkdisk
```

## Other useful commands, Linux

### Quota

For quota based filesystems, use the `quota` command to inspect the current quota and usage.
Ie:
``` console
bram@lilo8:/home/bram$ quota -s
Disk quotas for user bram (uid 8451):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
home1.science.ru.nl:/VGsda01/bram
                 37244K   4750M   5000M             906   1900k   2000k
```

{{< notice info >}}
For the [newer ZFS home volumes](/en/howto/zfs-storage/) there is no (soft) quota, only a hard quota
which is the size of your home directory, so if `quota` command gives no output, you the `df`
command as shown below.
{{< /notice >}}

### df

`df` stands for disk free. For disk based filesystems, it reports the total size and used space.
 To report the statistics of the disk that holds your home directory, use:
``` console
bram@lilo8:/home/bram$ df -h .
Filesystem                         Size  Used Avail Use% Mounted on
home1.science.ru.nl:/VGsda01/bram  197G   91G  102G  48% /home/bram
```

{{< notice info >}}
While disk space refers to the total storage capacity available on a filesystem, inodes
represent the data structures that store information about files and directories. Even
if there's ample disk space, running out of inodes can occur, preventing the creation
of additional files or directories. Disk reporting utilities such as `df` and `du`
(discussed below) have the `--inodes` option to report inodes.
{{< /notice >}}

### du

Disk usage of files or directory trees can be determined with `du`. Just like `df`, it has
a `-h` flag for *human readable* units. It converts bytes to something you can actually
understand. For example, to find your the disk usage of your `src` directory:

```console
~$ du -h -s ~/src
14G	/home/bram/src
```

I'm also using the `-s` flag here to summarize the output for each argument (`~/src` in
this case).


### Finding duplicate files: Fdupes

`fdupes` searches  the  given  path for duplicate files. Such files are found by
comparing file sizes and MD5 signatures,  followed  by  a  byte-by-byte
comparison. Fdupes has options to list or delete duplicate files. See `man fdupes`.

{{< notice warning >}}
Be cautious with fdupes in combination with a snapshotted file system such as ZFS.
Ensure that you do not include the `.snapshots` directory as one of the arguments.
{{< /notice >}}

## Cache

A common directory where applications store temporary files is the `~/.cache` directory
in you home directory. Removing files there is generally a safe action. If there are
a lot of files in your cache directory, ncdu will list them.
