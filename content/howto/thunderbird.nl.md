---
author: bram
date: '2022-09-19T16:07:13Z'
keywords: []
lang: nl
tags:
- email
title: Thunderbird
wiki_id: '22'
---
Mozilla Thunderbird is een opensource-e-mailclient ontwikkeld door het Mozilla-project. Hieronder instructies om Thunderbird in te stellen voor RU MS365 mail en voor Science mail.

##  Thunderbird instellen voor RU MS365 mail.

De [RU MS365 instructies](https://www.ru.nl/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/e-mail-en-agenda) noemen Thunderbird niet,
maar er is wel een [door het MS365-project gemaakte handleiding](/downloads/2023/Mozilla-Thunderbird-instellingen-voor-RU-MS365.pdf).

## Thunderbird instellen voor Science Email.

Let op: Bij een *zelf geïnstalleerde* PC moet u bij het installeren van
Thunderbird aan het eind **Launch Mozilla Thunderbird now** uitvinken en
klik op **Finish**.
{{< figure src="/img/old/tb-no-bw-install.jpg" title="Laatste installatie dialoogvenster" >}}

### Profiel aanmaken

Let Op! Heeft u al eerder Thunderbird *geïnstalleerd* of *opgestart*,
zonder eerst een profiel aan te maken, lees dan eerst
[Thunderbird\#Thunderbird\_opgestart\_zonder\_eerst\_een\_profiel\_aan\_te\_maken](/nl/howto/thunderbird#thunderbird-opgestart-zonder-eerst-een-profiel-aan-te-maken/)

  ---------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------
  Start de profiel manager op, ga naar **Start -\>     {{< figure src="/img/old/tb-profile-manager-leeg.jpg" title="Profile Manager" >}}
  Programs -\> Mozilla Thunderbird -\> Profile         
  Manager**.                                           

  En klik op **Create Profile…**.                      {{< figure src="/img/old/tb-create-profile-wizard.jpg" title="Create Profile Wizard" >}}

  Klik op **Next**. Vul bij **Enter new profile        {{< figure src="/img/old/tb-completing-create-profile-wizard.jpg" title="Completing 'Create Profile Wizard'" >}}
  name:** eventueel een beschrijvende naam in (zolang  
  het invulveld maar niet leeg is).                    

  Klik op **Choose Folder…** om een map aan te maken   {{< figure src="/img/old/tb-create-profile-choose-folder.jpg" title="Choose Folder - 'Create Profile Wizard'" >}}
  waarin de instellingen, adresboek e.d. (en als de    
  “Local Folders” gebruikt worden ook mail) in bewaard 
  gaat worden. Selecteer de home directory (U: schijf) 
  onder **My Computer**                                

  Klik op **Make New Folder**, en geef deze een naam,  {{< figure src="/img/old/tb-finishing-create-profile-wizard.jpg" title="Finishing - 'Create Profile Wizard'" >}}
  b.v. **thunderbird**, en klik vervolgends op **OK**. 
  Controleer of onder **Your user settings,            
  preferences, bookmarks and mail will be stored in:** 
  iets staat als **U:\\thunderbird** of de door u      
  gekozen nieuwe directory op de U: schijf. Klik op    
  **Finish**, er is nu een profiel aangemaakt.         
  ---------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------

### Thunderbird voor de 1e keer starten

  ------------------------------------------------- -------------------------------------------------------------------------------------------------------
  Start Thunderbird door op de profielnaam te
  dubbelklikken. In het vervolg kan thunderbird ook 
  via het dektop icoon gestart worden. Het 1e keer  
  starten van Thunderbird kan *enkele minuten*      
  duren, heb geduld - dit duurt alleen de 1e keer   
  zo lang.                                          

  ------------------------------------------------- -------------------------------------------------------------------------------------------------------

### Account Wizard doorlopen

  ----------------------------------------------------- ---------------------------------------------------------------------------------------------------------------
  Thunderbird start nu met de Account Wizard.

  Selecteer **Email account** en klik op **Next**. Vul
  bij **Your Name** uw naam in zoals u wilt dat andere  
  deze bij “Van:” of “From:” te zien krijgen. Vul bij   
  **Email address** uw email address is (of een van uw  
  aliassen) zoals vermeld bij de DHZ website achter:    
  “Email adres: / Mail address:” of een van uw aliassen 
  (zoals bij DHZ als “alias” vermeld).                  

  Klik op **Next**. Selecteer bij **Select the type of
  incoming server you are using** het keuzebolletje     
  **IMAP**. Vul bij **Incoming Server** in wat bij DHZ  
  staat achter: “Mail verzamelen op: / Imap/pop         
  mailserver:”. Vul bij **Outgoing Server** in:         
  **smtp.science.ru.nl**                                

  Klik op **Next** Vul bij **Incoming User Name** in    {{< figure src="/img/old/tb-account-wizard-user-names.jpg" title="Account Wizard - User Names" >}}
  wat bij DHZ staat achter “Gebruikersnaam / Username”. 
  Vul bij **Outgoing User Name** in wat bij DHZ staat   
  achter “Gebruikersnaam / Username”.                   

  Klik op **Next**. Laat bij **Account Name** de        {{< figure src="/img/old/tb-account-wizard-account-name.jpg" title="Account Wizard - Account Name" >}}
  ingevulde waarde staan - uw email adres - of vul een  
  beschrijvende naam in.                                

  Klik op **Next**. Controleer de samenvatting van de   {{< figure src="/img/old/tb-account-wizard-finish.jpg" title="Account Wizard - Finishing" >}}
  instellingen die gemaakt zijn.                        

  Klik op **Finish**. Het alert-venster
  verschijnt. Dit heeft *niets* te maken met “Maximum    
  number of connections”, maar met nog niet correct     
  ingestelde instellingen, waardoor er nog geen         
  verbinding met de mailserver gemaakt kan worden. Klik 
  op **OK**.                                            
  ----------------------------------------------------- ---------------------------------------------------------------------------------------------------------------

### Opties instellen

  ------------------------------------------------------ ----------------------------------------------------------------------------------------------------------------------
  Het hoofdvenster van Thunderbird verschijnt. Ga via de
  menubalk naar **Tools -\> Options…**. Selecteer        
  bovenin het Options venster **Composition**, ga naar   
  het tabblad **General**. Veel mensen vinden het        
  prettiger om als ze een bericht forwarden of een       
  ge-forward bericht ontvangen, het door-te-sturen       
  bericht in de body (is de inhoud) van het bericht      
  staat i.p.v. als attachment.                           

  Verander bij **Forward messages** via de drop-down
  **As Attachment** in **Inline**. Ga naar het tabblad   
  **Addressing**. Meestal is het handiger om             
  emailadressen van mensen waarheen u mail stuurt        
  gescheiden te houden van de persoonlijke adresboek,    
  voor het overzicht. Verander bij **Automatically add   
  outgoing email addresses to my** via de drop-down      
  **Personal Address Book** in **Collected Addresses**.  
  ------------------------------------------------------ ----------------------------------------------------------------------------------------------------------------------

### Account settings instellen

Ga via de menubalk naar **Tools -\> Account Settings…**.

Verander bij **Forward messages** via de drop-down
{{< figure src="/img/old/tb-account-settings-overview.jpg" title="Account Settings" >}}

Klik op **Server Settings** onder het
zojuist aangemaakte account. Zet in de **Security
Settings** box het **Use secure
connection** keuzebolletje op **SSL**.

{{< figure src="/img/old/tb-account-settings-server-settings.jpg" title="Account Settings - Server Settings" >}}

Klik op **Advanced…** Vink
**Show only subscribed folders** uit.
Wilt u uw mappen niet in de map inbox hebben staan vul dan
**INBOX.** (let op de punt) bij
**IMAP server directory** in.

Klik op **OK**. Klik op
**Copies & Folders** onder het zojuist
aangemaakte account.

De standaardinstellingen zijn meestal goed. Als u bijvoorbeeld al een
andere **Sent** folder heeft, en wilt
blijven gebruiken kan per item, via het
**Other** keuzebolletje een andere folder
onder uw accountnaam gekozen worden.

{{< figure src="/img/old/tb-account-settings-copies-and-folders-alternate.jpg" title="Account Settings -&gt; Copies &amp; Folders" >}}

Klik op **Composition & Addressing**onder
het zojuist aangemaakte account. Veelal is het prettiger om het antwoord
op een reply boven het originele bericht te zetten. Verander hiervoor
bij **Automatically quote the original message when
replying** achter
**Then** in de drop-down
**start my reply below the quote** in
**start my reply above the quote**.

{{< figure src="/img/old/tb-account-settings-composition-and-addressing.jpg" title="Account Settings - Composition - Addressing" >}}

Als u emails in “platte tekst” op wilt maken, vink dan
**Compose messages in HTML format** uit.
Selecteer **Outgoing Server (SMTP)**.

Klik op **Edit…**. Selecteer het
keuzebolletje bij **TLS, if available**.
Verander eventueel het poortnummer, bij
**Port** in
**587**, zie de
`<a href="email_authsmtp" title="wikilink">`{=html}Instellen van
Authenticated SMTP`</a>`{=html} handleiding voor uitleg hierover. Vul
eventueel een **Description** in. Dit kan
handig zijn als er meerdere smtp servers ingesteld worden.

Voor nieuwere versies van Tunderbird bij gebruik poort 587: Kies
“connection security” “STARTTLS” en zorg dat “use secure authentication”
uitgevinkt staat.

Klik het **Account Settings** venster met
**OK** dicht.

{{< figure src="/img/old/tb-account-settings-outgoing-server-smtp-server.jpg" title="Account Settings - Outgoing Server (SMTP) - SMTP Server" >}}

### Accountinstellingen controleren.

  ---------------------------------------------------------- -------------------------------------------------------------------------------------------
  Selecteer onder de accountnaam **Inbox**, en klik op **Get {{< figure src="/img/old/tb-get-first-mail.jpg" title="Eerste mail binnenhalen." >}}
  Mail**.                                                    

  Vul waar gevraagd het wachtwoord in, en klik op **OK**.    {{< figure src="/img/old/tb-testmail-binnen.jpg" title="Overzicht met een mailtje." >}}
  Bij **Inbox** staat nu een plusteken. Klik hierop om deze  
  uit te vouwen. Er zouden nu ten minste 2 mappen, met de    
  naam “Virus” en “Spam” moeten staan. Klik op **Write**,    
  vul bij **To:** uw e-mailadres in, als **Subject:** b.v.   
  test. Klik nu op **Send**. Klik weer op **Get Mail**. Als  
  alles goed is heeft u nu het testmailtje in uw Inbox.      
  ---------------------------------------------------------- -------------------------------------------------------------------------------------------

## Thunderbird opgestart zonder eerst een profiel aan te maken

### Er is nog niets gedaan in Thunderbird - alleen een keer opgestart, account wizard niet doorlopen

Als er nog niets met Thunderbird gedaan is, en u hier *absoluut zeker*
van bent kunt u het profiel veilig verwijderen. Volg dan de volgende
stappen.

  ----------------------------------------- -----------------------------------------------------------------------------------------------------
  Ga naar de Thunderbird profile manager    {{< figure src="/img/old/tb-profile-manager-verwijderen.jpg" title="Profile Manager" >}}
  Klik op **Start -\> Programs -\> Mozilla  
  Thunderbird -\> Profile Manager**.        

  Klik op **Delete Profile…** Klik op       {{< figure src="/img/old/tb-profile-manager-verwijderen-ookfiles.jpg" title="Profile Manager" >}}
  **Delete Files** Klik op **Exit**.        
  ----------------------------------------- -----------------------------------------------------------------------------------------------------

U kunt nu zonder problemen de handleiding voor het instellen van een
Email account volgen.

### Er zitten al gegevens in Thunderbird die behouden of verhuisd moeten worden

Als u het profiel wilt verplaatsen naar een goede locatie, volg dan de
volgende stappen.

  ------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------
  Zoek eerst het profiel op en lees de [\#3        
  uitleg]. Verplaats de profile directory naar een  
  goede locatie. B.v. van **C:\\Documents and       
  Settings\\loginnaam\\Application                  
  Data\\Thunderbird\\Profiles\\uy1yi71r.default**   
  naar **U:\\thunderbird**.                         

  Ga naar de Thunderbird profile manager. Klik op    {{< figure src="/img/old/tb-profile-manager-verwijderen.jpg" title="Profile Manager" >}}
  **Start -\> Programs -\> Mozilla Thunderbird -\>  
  Profile Manager**.                                

  Klik op **Delete Profile…** Als u het
  onderstaande venster krijgt, klik dan op **Don’t  {{< figure src="/img/old/tb-profile-manager-leeg.jpg" title="Profile Manager" >}}
  Delete Files** - en controleer of de profiel      
  directory goed verplaatst is.                     

  En klik op **Create Profile…**.

  Klik op **Next**. Vul bij **Enter new profile     {{< figure src="/img/old/tb-completing-create-profile-wizard.jpg" title="Completing 'Create Profile Wizard'" >}}
  name:** eventueel een beschrijvende naam in       
  (zolang het invulveld maar niet leeg is).         

  Klik op **Choose Folder…** om de map te kiezen    {{< figure src="/img/old/tb-create-profile-choose-folder.jpg" title="Choose Folder - 'Create Profile Wizard'" >}}
  waarheen het profiel verplaatst is - b.v.         
  **U:\\thunderbird**. Blader naar de map toe.      

  Selecteer de map, en klik vervolgens op **OK**.  {{< figure src="/img/old/tb-finishing-create-profile-wizard.jpg" title="Finishing - 'Create Profile Wizard'" >}}
  Controleer of onder **Your user settings,         
  preferences, bookmarks and mail will be stored    
  in:** de goede map geselecteerd is. Klik op       
  **Finish**.                                       

  Thunderbird starten. Start Thunderbird door op de
  profielnaam te dubbelklikken. In het vervolg kan  
  Thunderbird ook via het dektop icoon gestart      
  worden.                                           
  ------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------

## Profielen

Er zijn 2 soorten profielen die op deze pagina genoemd worden. Dit is
het Windows gebruikersprofiel en het Thunderbird profiel.

### Het Windows gebruikersprofiel

Dit zijn de Windows instellingen die op een server worden gebackupt
indien u een [beheerde werkplek PC](/en/howto/windows-beheerde-werkplek/) heeft. Het
Windows gebruikersprofiel staat op de systeempartitie (meestal de **C:
schijf**) in een subdirectory van **Documents and Settings** genoemd
naar uw **loginnaam**. Zie het onderstaande voorbeeld.

{{< figure src="/img/old/tb-documents-and-settings.jpg" title="Documents and Settings" >}}

*Als er in deze pagina verder nog verwezen wordt naar “profiel” wordt,
indien niet verder aangegeven het Thunderbird profiel bedoeld.*

### Het Thunderbird profiel

Dit is de verzameling instellingen en gegevens van Thunderbird
opgeslagen in een directory. Gegevens die zoal hierin staan zijn
contactpersonen, van welke server mail opgehaald moet worden, eventueel
lokaal opgeslagen emails, enz. Een profiel wordt standaard aangemaakt in
uw Windows profiel. Dit is voor thuisgebruik goed, maar uw lokale mail
en instellingen worden niet ge-backupped! In het geval van een [beheerde
werkplek](/en/howto/windows-beheerde-werkplek/) wordt dit ook op deze locatie
ge-backupped - de ruimte is hier zeer beperkt. Daarom moet uw
Thunderbird profiel in principe op een netwerkschijf staan die
gebackupped wordt. In de verdere FAQ’s wordt ervan uitgegaan dat de
ge-backupte locatie waar u uw Thunderbird profiel wilt -of- heeft
opgeslagen uw **homedirectory** is.

Om achter de locatie te komen waar **nu** uw profiel opgeslagen is,
doe het volgende:

  ------------------------------------------------------------ --------------------------------------------------------------------------------------------------------------
  Ga naar de Thunderbird profile manager, **Start -\> Programs {{< figure src="/img/old/tb-profile-manager-leeg-geenselectie.jpg" title="Profile Manager" >}}
  -\> Mozilla Thunderbird -\> Profile Manager**.               

  Is deze leeg, dan heeft u *nog geen Thunderbird profiel*.
U kunt dan zonder problemen de          
  handleiding voor het instellen van een Email account volgen. 
  De onderstaande punten hoeven dan *niet* doorlopen te        
  worden. Is er wel een profiel naam aanwezig, zoals in de     
  onderstaande afbeelding - sluit de profile manager door op   
  **Exit** te klikken en ga dan verder met de onderstaande     
  stappen.                                                     

  Zorg dat verborgen bestanden zichtbaar zijn. Open **My
  Computer** op het bureaublad. Ga via het menu naar **Tools - Folder Options…**,
  selecteer het **View** tabblad.       

  Selecteer bij **Hidden files and folders** het keuzebolletje 
  **Show hidden files and folders**. Deze optie kan waar       
  teruggezet worden wanneer het bekijken / verhuizen /         
  verwijderen van het profiel klaar is. Klik op **OK**.        

  Bekijk het profiles.ini bestand. Ga naar uw “Windows
  gebruikersprofiel directory”, **Application Data** en        
  vervolgens **Thunderbird**. B.v. “**C:\\Documents and       
  Settings\\loginnaam\\Application Data\\Thunderbird**”. In    
  deze directory staat een **profiles.ini**. Dit bestand       
  verwijst naar de locatie waar het profiel opgeslagen is.     
  Open het bestand door hierop te dubbelklikken. In dit        
  bestand staan twee regels die nu relevant zijn. Achter       
  **IsRelative** zal of **0** of **1** staan. Bij **Path** zal 
  een relatief - bij een IsRelative waarde van 1, of absoluut  
  pad - bij een IsRelative waarde van 0, staan.                

  **IsRelative** = 1 **Path** waarde is **Profiles/\<directory
  naam\>** waarbij “directorynaam” staat voor een willekeurige 
  letters-met-cijfers combinatie met “.default” erachter.      
  Hieronder een voorbeeld waarbij het profiel onder het        
  Windows-profiel staat - dit is, voor niet-thuissituaties,    
  niet goed! Het **Path** is hier                              
  **Profiles/uy1yi71r.default**. De locatie van het profiel is 
  de directory waarin het **profiles.ini** bestand zich bevindt
  met eraan vast de waarde van **Path**. De locatie van het     
  profiel is bij dit voorbeeld **C:\\Documents and             
  Settings\\loginnaam\\Application                             
  Data\\Thunderbird\\Profiles\\uy1yi71r.default**.

  **IsRelative** = 0 **Path** waarde is het volledige pad naar
  de profiel directory. In het volgende voorbeeld staat het     
  profiel op de homedirectory. De **Path** regel heeft de      
  waarde **U:\\thunderbird**. De locatie van het profiel is de 
  waarde van **Path**, in dit voorbeeld **U:\\thunderbird**    .
  ------------------------------------------------------------ --------------------------------------------------------------------------------------------------------------

## Hoe stel ik een LDAP adresboek in?

U kunt het Science LDAP adresboek toevoegen door de onderstaande stappen
uit te voeren.

  --------------------------------------------------------- ----------------------------------------------------------------------------------------------------
  Activeer en open de directory servers lijst. Ga via het
  menu naar **Tools -\> Options…**. Selecteer bovenin het   
  Options venster **Composition**, ga naar het tabblad      
  **Addressing**. Vink **Directory Server** aan, en klik op 
  **Edit Directories…**.                                    

  Voeg een nieuwe toe.

  Klik op **Add**. Vul bij het **Directory Server           {{< figure src="/img/old/tb-ldap-adresboek-toevoegen.jpg" title="Directory Server toevoegen" >}}
  Properties** venster, bij het tabblad **General** in:     
  **Name** een beschrijvende naam, b.v.:                    
  **Science-Adresboek** **Hostname** :                      
  **ldap.science.ru.nl** **Base DN** : **o=addressbook**    
  **Port number** heeft als defaultwaarde al **389** -      
  hoeft niet veranderd te worden. En klik alle openstaande  
  optie vensters met **OK** dicht.                          
  --------------------------------------------------------- ----------------------------------------------------------------------------------------------------

### Gebruik van het Science-Adresboek

Het LDAP adresboek kunt u op 2 plaatsen gebruiken.

#### LDAP Adresboek vanuit “Address Book” gebruiken

Klik in het hoofdvenster op de knop **Address Book**

{{< figure src="/img/old/tb-ldap-adresboek-resultaat-book.jpg" title="LDAP Adresboek vanuitAddress Book gebruiken" >}}

Aan de linkerkant onder ziet u een lijst met beschikbare adresboeken.
Hier staat nu het **Science-Adresboek** bij, klik hierop. Als u nu in
het zoekvanster een stuk van de naam of het emailadres invult, ziet U
het venster eronder vullen - tot maximaal 100 matches - met items die
hieraan voldoen.

#### LDAP Adresboek vanuit een “Compose” venster gebruiken

Klik in het hoofdvenster op de knop **Write**

{{< figure src="/img/old/tb-ldap-adresboek-resultaat-newmail.jpg" title="LDAP Adresboek vanuit een Compose venster gebruiken" >}}

Er verschijnt een **Compose** venster. Klik op de **Contacts** knop en
selecteer bij **Address Book** het **Science-Adresboek**. Als u nu in
het zoekvenster een stuk van de naam of het emailadres invult, ziet u
het venster eronder vullen - tot maximaal 100 matches - met items die
hieraan voldoen.

### Opmerkingen

Niet alle adresboek items hebben ook een email adres, hou
hier rekening mee! Als u wilt weten wat er bij een item aan attributen
aanwezig zijn kunt u rechts klikken op een item, en **Properties**
selecteren.

Vanwege de beveiliging kunnen er **geen items veranderd worden** in het
LDAP adresboek, wel kan er een kopie gemaakt worden in een lokaal
adresboek door het betreffende item uit het “Science-Adresboek” te
slepen naar b.v. het “Personal Address Book”. Het LDAP adresboek is
**alleen bereikbaar** voor machines vanaf het RU-netwerk of via VPN.

## Tips / trucs

[Thunderbird tips](http://email.about.com/od/mozillathunderbirdtips/)

## Add-ons

Add-ons are small pieces of software that can add new features or tiny
tweaks to your Thunderbird. See the [Thunderbird
Add-ons](/en/howto/thunderbird-add-ons/) page.

How to change font size in From and To lines in ThunderBird

-   Click on Menu bar -\> Tools -\> Add-ons
-   Click on “Get Add-ons”
-   Search for Add-on “Theme Font & Size Changer”
-   Install. Add-on provides a ’Restart now" item.
-   Restart Thunderbird
-   Click on Menu bar -\> Tools -\> Theme Font & Size Changer

[Categorie:Thunderbird](/en/tags/thunderbird)
