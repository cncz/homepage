---
author: sioo
date: '2022-06-28T14:51:55Z'
keywords: []
lang: nl
tags:
- overcncz
title: Helpdesk, Operations, Gebruikersondersteuning
wiki_id: '128'
---
## Waar kun je ons vinden?

Helpdesk/Operations is te vinden in kamer HG00.051.

## Met wat voor vragen kun je er terecht?

-   Gebruikersondersteuning voor studenten en medewerkers, algemene IT vragen
-   Installatie en onderhoud van de studentenvoorzieningen, met name
    de [pc-onderwijszalen en openbare werkplekken](/nl/howto/terminalkamers/)
-   Algemene ondersteuning van computerapparatuur op onderzoeksafdelingen
-   Hulp of bemiddeling bij
    -   het [installeren van PC’s](/nl/howto/installeren-werkplek/) en netwerksoftware
    -   [reparaties van PC’s en laptops](/nl/howto/reparaties/)
