---
author: bram
date: "2022-09-23T07:16:08Z"
keywords: []
lang: nl
tags:
  - email
title: Email Codes
wiki_id: "926"
---

## Overzicht van email codes

### ALICE-1

Dit is een email waarin wordt uitgenodigd om aan een cursusevaluatie
deel te nemen.

- Aan wie? Alle studenten die zich voor het tentamen van de
  betreffende cursus hebben ingeschreven in OSIRIS.
- Wanneer? Normaliter op de dag van het tentamen (ongeveer om 17:00).

### ALICE-2

Dit is een herinneringsmail waarin nogmaals wordt verzocht om aan een
cursusevaluatie deel te nemen.

- Aan wie? Alle studenten die zich voor het tentamen van de
  betreffende cursus hebben ingeschreven in OSIRIS en de enquête nog
  niet hebben ingevuld.
- Wanneer? Vijf dagen na de ALICE-1 email, dus normaliter vijf dagen
  na het tentamen.

### ALICE-3

Dit is een email waarin het resultaat van de feedback van een cursus
wordt teruggekoppeld aan studenten.

- Aan wie? Alle studenten die de enquête hebben kunnen invullen.
- Wanneer? De cursussen van een bepaald kwartaal worden aan het eind
  van het erop volgende kwartaal teruggekoppeld.

### ALICE-4

Dit is een email waarin het resultaat van de feedback wordt
teruggekoppeld aan de docent/cursuscoordinator van die cursus.

- Aan wie? De docent/cursuscoordinator van de cursus.
- Wanneer? De cursussen van een bepaald kwartaal worden aan het eind
  van het erop volgende kwartaal teruggekoppeld.

### CUDO-1

Dit is een email waarin de docent/cursuscoordinator op de hoogte wordt
gesteld van het feit dat de studentenevaluatie klaar is (en de
resultaten in het cursusdossier staan) en tevens wordt uitgenodigd om de
docentevaluatie te schrijven.

- Aan wie? De docent/cursuscoordinator van de cursus.
- Wanneer? Op het moment dat de studentenevaluatie klaar is,
  normaliter twee weken na het tentamen.

### CUDO-2

Dit is een email waarin de docent/cursuscoordinator eraan wordt
herinnerd om de docentevaluatie te schrijven.

- Aan wie? De docent/cursuscoordinator van de cursus.
- Wanneer? Twee weken nadat de studentenevaluatie klaar is, normaliter
  vier weken na het tentamen.
- Extra voorwaarde? Er staat nog geen docentenevaluatie PDF bestand in
  het cursusdossier.

### CUDO-3

Dit is een email naar de secretarissen/voorzitters van een OLC waarin
wordt gemeld welke cursussen de week erop in principe door de
betreffende OLC beoordeeld zou moeten kunnen worden.

- Aan wie? De secretaris/voorzitter van een OLC.
- Wanneer? Normaliter vijf weken na het tentamen.

### PEERFEEDBACK-1

Dit is een mail aan alle studenten voor wie een [PeerFeedback](https://peerfeedback.edutools.science.ru.nl) evaluatie
opengegaan is.

- Aan wie? Studenten uit een BrightSpace groep waarvoor de evaluatie opengezet is
- Wanneer? Binnen vijf minuten na het opengaan van de evaluatie

### PEERFEEDBACK-2

Dit is een reminder naar alle studenten die nog een evaluatie open hebben staan
in [PeerFeedback](https://peerfeedback.edutools.science.ru.nl).
Per evaluatie kan de docent instellen met welke frequentie er een reminder voor de betreffende evaluatie gestuurd wordt.
We sturen maximaal één mail per dag, dus indien nodig bundelen we de herinneringen aan meerdere openstaande evaluaties
in één mail.

- Aan wie? Studenten uit PeerFeedback die nog een evaluatie open hebben staan.
- Wanneer? Dagelijks rond 06:00 uur 's ochtends.

### REMA-1

Dit is een email die verzonden wordt zodra er een nieuw incident wordt
gerapporteerd via het LimeSurvey incidentenformulier.

- Aan wie? Medewerkers van de ARBO-dienst van de faculteit waar het
  incident plaatsvond.
- Wanneer? Direct na versturing van het formulier.

### ROUNDCUBE-1

- Aan wie? Gebruikers van de
  [Roundcube](http://roundcube.science.ru.nl) webmaildienst.
- Wanneer? Als er bij het instellen van filters in Roundcube bestaande
  [Sieve](/nl/howto/email-sieve/) filters worden aangetroffen.
- Waarom? Bestaande Sieve filters zijn wel actief, maar worden niet
  weergegeven in Roundcube.
- Wat te doen? Verwijder de Sieve filters en stel ze opnieuw in met
  Roundcube. Zegt dit u niks, neem dan [contact op met
  C&CZ!](/nl/howto/contact/).

### DIY-1

Deze mail wordt gestuurd om u te informeren over het bereiken van de
controledatum van [Science login](../login).

- Aan wie? ICT contactpersonen.
- Wanneer? Elke maandagmorgen om 10 uur. En alleen als de
  controledatum van 1 of meerdere logins is bereikt.

Op de "Mijn accounts"-pagina van [DHZ](http://dhz.science.ru.nl) kunt u zien van
wie u ICT contactpersoon bent.

### DIY-2

Deze mail wordt gestuurd om u te informeren over het bereiken van de
controledatum van uw [Science login](../login).

- Aan wie? Aan eigenaar login.
- Wanneer? Om 10:00 op de dag dat de controle datum is bereikt.

### LOGIN-1

Deze mail wordt gestuurd om u te informeren over het aanmaken van een
[Science login](../login) voor u.

- Aan wie? Degene waarvoor een Science login (account, loginnaam en
  wachtwoord) gemaakt is.
- Wanneer? Elke werkdag om 19:00 uur wordt een mail gestuurd naar de
  die dag aangemaakte logins.

### LOGIN-3

Deze e-mail dient als herinnering dat [Science Logins](../login) die aan uw account is gekoppeld binnenkort verloopt.

- Ontvanger: De aangewezen contactpersoon voor de [Science Login] die binnenkort verloopt.
- Timing: Elke maandag (indien van toepassing)

### NEWLOGIN-1

Deze mail wordt naar de contacpersoon gestuurd die een nieuwe [Science login](../login) heeft
aangevraagd.

- Aan wie? Contactpersonen die een Science login hebben aangevraagd.
- Wanneer? Zodra de login is aangemaakt.

### WELCOME-1

Deze mail wordt naar nieuwe [Science logins](../login) gestuurd.

- Aan wie? Gebruikers van Science logins.
- Wanneer? Ongeveer een uur nadat een login is aangemaakt.

### PASSWORDRESET-1

Deze mail wordt gestuurd om u een nieuw initiëel wachtwoord voor uw
[Science login](../login) te geven.

- Aan wie? Degene die om een wachtwoord reset van zijn science login
  heeft gevraagd.
- Wanneer? Direct nadat het wachtwoord door een systeembeheerder is
  aangepast.

### PASSWORDRESET-2

Deze mail wordt aan u als contactpersoon gestuurd na het indienen van een wachtwoord-reset verzoek van een van de door u beheerde [Science logins](../login)

- Aan wie? Aanvrager van een wacthwoord reset.
- Wanneer? Direct nadat het wachtwoord door een systeembeheerder is
  aangepast.

### VOLDETAILS-1

Deze mail wordt gestuurd aan de aanvrager van een [netwerkschijf](../storage/#netwerkschijven).

- Aan wie? De eigenaren van de groep die gekoppeld is aan de netwekschijf.
- Wanneer? Zodra de schijf is aangemaakt of op verzoek.

{{< rawhtml >}}
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height=1800>
{{< /rawhtml >}}
