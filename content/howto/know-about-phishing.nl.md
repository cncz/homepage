---
title: Over phishing mails
author: sioo
date: 2022-12-16
keywords: []
tags: [email, security]
cover:
  image: img/2022/phishing.png
ShowToc: false
TocOpen: false
---
# Phishing

Het via misleiding achterhalen van bruikbare data, login gegevens, bij gebruikers voor het misbruiken van andermans ICT infrastructuur.

## Methodes van phishing

In het algemeen werkt phishing via een (klikbare) link verwerkt in een mailtje met een zogenaamd belangrijke boodschap omtrent salaris, of betalingen, of andere vormen van geld of beloning, danwel boetes of straffen. De intentie is het afleiden van onze ratio en de emotionele reacties triggeren om de link te gebruiken.

{{< notice tip >}}
Controleer altijd of de link waar je op klikt, de URL in de adresbalk, een voor jou bekend adres is, bijv. eindigend op `.ru.nl`.

| url | veilig/onveilig |
| ----| :----------------: |
| `https://mail.ru.nl/`  |    veilig  |
| `https://mail-ru-nl.blatherbeastoftraal.in/`  |    onveilig  |
| `https://webmail.science.ru.nl/`  |    veilig  |
| `https://scienceroundcube.lukewarmmail.com/`  |    onveilig  |
| `https://roundcube.pages.science.ru.nl/`  |   ??? |

Beter voorzichtig, de laatste kan ook een aanvalspoging zijn van binnen de RU!
{{< / notice >}}

Vervolgens is de gelinkte pagina zodanig opgezet dat het zo vertrouwd mogelijk eruit ziet om je login gegevens in te vullen. De achterkant van de phishing website bewaart dan wat je intypt en dan is het kwaad geschied. De gephishte login is bemachtigd door de hacker!

De gelekte login gegevens zijn formeel gezien een datalek en dit moet onmiddelijk gemeld worden bij de ICT afdeling!

## Gevolgen van succesvolle phishing

Met een geldige login kan een aanvaller alles wat de gebruiker normaal ook kan. Aangezien de hacker meestal buiten de RU is, zijn aanvallen op de digitale infrastructuur die voor gebruik extern gemaakt is aantrekkelijk.

## Gevolgen voor de Radboud Universiteit

- IT afdelingen moeten de schade beperken, dus accounts blokkeren, functionaliteit inperken, al dan niet tijdelijk.
- Papiermolen om het datalek af te handelen
- Reputatieschade bij misbruik van e-mail faciliteiten en daarmee hinder voor gebruikers van alle Radboud e-mail
- Indien er een ransomware aanval is gedaan, verlies van data doordat oude backups opgehaald moeten worden
- Erger kan ook nog...

## Gevolgen voor de eigenaar van de login

- het account (of alle accounts!) geblokkeerd wordt
- een nieuw wachtwoord gezet moet worden
- mogelijk verlies van data (omdat de aanvaller mogelijk toegang heeft gehad tot de data van de eigenaar)
- veel `undeliverable` mails, ook voor anderen wiens e-mail adres was gebruikt als afzender bij een mailing

## Specifiek geval; *spearphishing*

Een specifieke variant: ***Spearphishing*** is het specifiek aanvallen van een persoon door het gebruik van de naam en of contactgegevens. Deze variant van phishing komt vaak voor bij personen die wat meer macht hebben in de organisatie, zodat misbruik een grotere impact heeft.

Wat veel voorkomt is een bericht van een leidinggevende/prof naar zijn/haar werknemer/phd: "ik zit in een meeting en ik heb snel zus of zo tegoedbonnen nodig, kun je me die meteen sturen!" (Of een variatie hierop)

{{< notice tip >}}
Controleer het afzender adres heel erg goed, desnoods door de headers van de e-mail te proberen te lezen. Meestal wordt al snel duidelijk dat dit niet van de persoon zelf afkomstig is, maar van een namaak adres.
{{< / notice >}}
