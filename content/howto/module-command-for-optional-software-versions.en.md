---
title: Module command for optional software versions
author: petervc
date: 2023-08-25
keywords: []
tags: []
ShowToc: false
TocOpen: false
---

On C&CZ managed Ubuntu hosts, the "module" command is available. This makes it easy to use specific versions of available software.

To view which modules are available, use:

``` sh
module avail
```

and to use Matlab-R2023a:

``` sh
module add Matlab-R2023a`
```

For more info, see [the Environment Modules website](https://modules.readthedocs.io/en/v5.3.1/module.html).
