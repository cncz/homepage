---
author: bram
date: '2022-09-19T13:25:45Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
title: Nieuwe studenten
wiki_id: '164'
---
## C&CZ Informatie voor nieuwe studenten en medewerkers

### Twee logins

Om toegang te krijgen tot computervoorzieningen heeft men normaal
gesproken een combinatie van loginnaam en wachtwoord nodig. Als student
of medewerker van FNWI heeft men te maken met 2 logins (computer
accounts): een RU-nummer en een Science-naam. Hieronder wordt uitgelegd
voor welke diensten elk van beide te gebruiken zijn. FNWI streeft ernaar
om ook de facultaire voorzieningen toegankelijk te maken voor het
RU-loginnummer, op dit moment is dat nog niet voor alle diensten
mogelijk.

#### RU-nummer (U123456 / s1234567)

-   Studenten: Bij het begin van een studie aan de RU krijgen alle
    studenten een brief met hun studentnummer (iets als *s1234567*) als
    loginnaam en een **RU-wachtwoord**. Je studentnummer kun je niet
    wijzigen. Over het RU-wachtwoord, waar het gebruikt kan worden en
    hoe het gewijzigd kan worden, is te lezen op de
    [RU-wachtwoord](https://www.ru.nl/ict/algemeen/wachtwoord/)
    web-pagina.

Studenten met vragen over het RU-account of de student.ru.nl mailbox
kunnen het beste direct contact opnemen met
[Studentenzaken](http://www.ru.nl/studenten/actueel/contactadressen/).

-   Medewerkers: Een nieuw personeelslid krijgt bij het begin van een
    contract een personeelsnummer (U-nummer, iets als *U123456*) van
    [P&O](http://www.ru.nl/fnwi/pz/). Het personeelsnummer kan men niet
    wijzigen. Het RU-wachtwoord kan men zelf aanpassen via de [RU IdM
    website](http://www.ru.nl/idm), waar men ook kan lezen waar het
    gebruikt kan worden. Voor medewerkers van FNWI kan
    [C&CZ](/nl/howto/contact/) hulp bieden bij problemen en het
    wachtwoord resetten.

#### Science login (vachternaam)

Omdat de faculteit (FNWI) sinds jaar en dag eigen voorzieningen heeft,
waarvan alleen FNWI-medewerkers en FNWI-studenten gebruik kunnen of
mogen maken, en het vaak onmogelijk is om hiervoor dezelfde
loginnaam/wachtwoord te gebruiken, wordt door de FNWI ook een
**Science** loginnaam gemaakt, gebaseerd op de eigen initialen, roepnaam
en achternaam, iets als *jdevries*. Deze loginnaam kun je niet
(makkelijk) wijzigen. Het wijzigen van je **Science** wachtwoord kan op
de [Doe Het Zelf website](http://dhz.science.ru.nl/), waar ook de regels
waar het wachtwoord aan moet voldoen uitgelegd staan. Deze zijn i.h.a.
iets minder streng dan de nieuwere RU-wachtwoordregels, een
RU-wachtwoord moet bv minstens 8 tekens zijn, een Science-wachtwoord
minstens 6. Bij het gebruik van deze Science-login moeten studenten zich
aan de [regels voor logins](/nl/howto/login/) houden.

De twee logins, RU-nummer of Science naam, zijn dus eenvoudig uit elkaar
te houden, de wachtwoorden zijn (om te beginnen) verschillend, je kunt
ze natuurlijk evt. aan elkaar gelijk maken als je dat prettig vindt.
Hieronder een lijstje van de belangrijkste voorzieningen waarvan je met
deze logins gebruik kunt maken.

### Welke login voor welke diensten?

PC’s in [pc-cursuszalen en
studielandschap](/nl/howto/terminalkamers/) kunnen met beide logins
gebruikt worden: domein B-FAC met Science-loginnaam (B-FAC\\jdevries) of
domein RU met studentnummer/personeelsnummer (RU\\s1234567). Als op de
campus op een PC het domein B-Fac beschikbaar is, dan kan men daar met
de Science-login inloggen. Voorbeelden zijn de [PC’s voor alle
RU-studenten](http://www.ru.nl/studenten/voorzieningen/pc-voorzieningen_0/).

-   Met de facultaire **Science** login kun je gebruik maken van:
    -   De [Doe Het Zelf website](http://dhz.science.ru.nl/) om je
        wachtwoord en andere instellingen te wijzigen.
    -   Science [netwerkschijfruimte](/nl/howto/diskruimte/).
    -   Linux [login-servers](/nl/howto/hardware-servers/).
    -   Mail: [Science mail](/nl/tags/email). Het bijbehorende
        emailadres eindigt op **science.ru.nl**, bijvoorbeeld:
        J.deVries\@student.science.ru.nl. Extra emailadressen van de
        vorm Jan.deVries\@student.science.ru.nl zijn op aanvraag aan te
        maken. Facultaire instanties zullen dit mailadres gebruiken. Je
        kunt deze mail, indien gewenst, automatisch door laten sturen
        naar een ander mail-adres via de [Doe Het Zelf
        website](http://dhz.science.ru.nl/).
-   Met de **RU** login krijg je toegang tot [ruwweg 100
    RU-applicaties](http://www.ru.nl/idm/faq/faq/#applicaties), de
    belangrijkste zijn:
    -   Printers: alle
        [Peage](http://www.ru.nl/gdi/voorzieningen/printen-scannen/peage/)
        printers.
    -   De RU [Portal](http://portal.ru.nl).
    -   De RU [Brightspace](https://brightspace.ru.nl/) digitale
        leeromgeving.
    -   De RU mailservice, met [@ru.nl en/of @student.ru.nl email (RU
        Exchange Outlook Web App email)](http://mail.ru.nl/). Het
        bijbehorende emailadres is Jan.deVries\@ru.nl of
        J.deVries\@student.ru.nl. Universitaire instanties zullen dit
        mailadres gebruiken. Als je je mail niet op deze server wil
        lezen, dan moet je deze mail automatisch door laten sturen naar
        een ander mail-adres. Zie hiervoor [de RU Mail
        handleiding](http://www.ru.nl/ict/medewerkers/mail-agenda/handleidingen/forward-instellen/).
    -   Wifi: het draadloze netwerk [Eduroam](http://www.eduroam.org/),
        gebruik als loginnaam `s-nummer@ru.nl` of `U-nummer@ru.nl`
    -   Software: [SURFspot](http://www.surfspot.nl/), voor relatief
        goedkope software, vaak ook voor thuisgebruik.
    -   RU Intranet: [RadboudNet](http://www.radboudnet.nl).
    -   [BASS](/nl/howto/bass/), het financieel/logistiek systeem
        (BASS-FinLog) en personeelssysteem (BASS-HRM) van de RU.
