---
author: harcok
date: '2018-06-27T05:42:46Z'
keywords: []
lang: nl
tags:
- email
title: Email sieve
wiki_id: '13'
---
## Sieve

Dit is in [Roundcube webmail](http://roundcube.science.ru.nl) toegankelijk via Instellingen -> Filters. 


### Achtergrond

Wanneer men op de IMAP-server `post.science.ru.nl` mail die op de server
binnenkomt automatisch in mappen wil laten sorteren of op een speciale
manier filteren, dan kan men dat met behulp van “Sieve”.\
Een sievescript kan met een mailprogramma dat sieve ondersteunt, op de
mailserver worden gezet. Dit is vrij eenvoudig in [Roundcube
webmail](http://roundcube.science.ru.nl) via Instellingen -\> Filters,
maar het kan ook met KMail of met Thunderbird met de [sieve
addon](http://sieve.mozdev.org/).\
Op lilo4 krijg je met het commando `sieve-connect postvak.science.ru.nl`
een interactieve commandline waar je met het commando *put* een sieve
script kunt uploaden.\
De Sieve taal staat beschreven in
[RFC3028](http://www.ietf.org/rfc/rfc3028.txt?number=3028).

### Sieve gebruiken in KMail

Na het toevoegen van een account met behulp van de wizard van KMail (met
behulp van de instellingen op [:Categorie:Email](/nl/tags/email)) moet
nog aan KMail duidelijk gemaakt worden dat de server Sieve ondersteunt.
Dit gaat via Settings, Configure KMail, Accounts, Modify, Filtering,
Server supports Sieve. Vervolgens kunnen de Sieve-scripts worden
benaderd via Settings, Manage Sieve Scripts.

### Voorbeelden van Sieve-scripts

-   Als je sommige mail automatisch in een **bestaande** folder wil
    zetten, kun je iets als het onderstaande gebruiken:

`require [ "fileinto", "include" ];`\
`#`\
`# Messages into folders.`\
`#`\
`if header :contains "X-VirusscanResult" "infected" {`\
`        fileinto "INBOX.Virus";`\
`        stop;`\
`}`\
`if address :is ["From", "To"] [`\
`              "Symposium2008@science.ru.nl",`\
`              "New-symposium2008@science.ru.nl"`\
`        ]`\
`{`\
`        fileinto "INBOX.Symposium2008";`\
`        stop; `\
`}`\
`include "spam";`\
`include "vacation";`

Als men de mail door wil sturen die overblijft nadat het spamfilter zijn
werk gedaan heeft, moet men na de `include "spam";` regel invoegen:

`redirect "field@example.edu";`
