---
author: petervc
date: '2022-06-24T12:17:55Z'
keywords: []
lang: en
tags: []
title: Address
wiki_id: '99'
---
## Visiting address

C&CZ is situated in the Huygens building, ground floor, next to the
entrance of wing 5. The room number is HG00.051, the [systematics behind
the room numbers](https://www.ru.nl/fnwi/faculteit/organisatie/ondersteunende-diensten/huisvesting-logistiek/kopie/)
is explained on the website of the [housing & logistics department HL](http://www.ru.nl/fnwi/ihz https://www.radboudnet.nl/science/faculty-services/faculty-services-d-tot-en-met-k/housing-logistics-hl/).

~~~ txt
C&CZ, FNWI, Radboud University Nijmegen
HG00.051
Heyendaalseweg 135
6525 AJ Nijmegen
~~~

## Postal address

~~~ txt
Radboud Universiteit Nijmegen
Faculty of Science
C&CZ
Postbus 9010
6500 GL Nijmegen
~~~

Further questions or information [contact C&CZ](/en/howto/contact).
