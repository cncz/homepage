---
author: petervc
date: '2022-06-24T12:45:46Z'
keywords: []
lang: en
tags:
- software
title: Software licences
wiki_id: '145'
---
## Science software licences negotiated by C&CZ

All info can be found on the
[Science licenses pages](https://cncz.pages.science.ru.nl/licenses/).
Questions: please [contact C&CZ](../contact).

Most software can be found on the install share.
On a PC with MS-Windows (or a Linux “Samba client”), one can connect
to the service:
```
\install.z.science.ru.nl\install
```

## RU software licences negotiated by ILS

ILS negotiated license agreements for the RU for [a number of software
packages](https://www.ru.nl/en/services/campus-facilities-buildings/ict/software).
This makes that software relatively cheap (bulk license) or even
free (campus license)
for use by students and employees.

A lot of license software can be bought at
[Surfspot](http://www.surfspot.nl/) by employees and students by using
their RU account and the corresponding
[RU-password](https://www.ru.nl/password). Some of this (primarily MS-Windows)
[software can be borrowed](/en/howto/microsoft-windows/) from C&CZ.

-   Security: F-Secure
    -   for Windows/macOS computers purchased with RU funds that are privately
        administered, please contact C&CZ.
    -   for 1 privately owned device one can get free of charge
        “F-Secure Safe (RU Nijmegen)” for 1 year at
        [Surfspot](http://www.surfspot.nl), see [the ILS protect your computer page](https://www.ru.nl/en/staff/services/services-and-facilities/ict/security/protecting-your-computer).
