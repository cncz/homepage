---
title: Mailman lists
author: bram
date: 2022-10-15
keywords: []
tags:
- email
aliases:
- mailman
---
## Mailing lists
Mailman lists are highly configurable and moderatable [mailman mailing lists](http://mailman.science.ru.nl/), e.g. the list of [FNWI-employees](http://mailman.science.ru.nl/mailman/listinfo/fnwi-medewerkers) or the list of [FNWI-students](http://mailman.science.ru.nl/mailman/listinfo/fnwi-studenten). Usually one can add oneself to or remove oneself from such a mailman mailing list. Every mailman mailing list has a moderator, who can approve or reject messages to the mailman mailing list. For mailman list admins: To filter spam one can choose in the `Privacy options...` under `Spam filters` as `Spam filter regexp:`

```
X-Spam-Score:.*\*\*\*\*\*
```

and check the radio button `Discard` and hit `Submit Your Changes` on the bottom of the web page.

Moreover it is advisable to accept only messages from members (moderated or unmoderated) and a specified list of addresses to accept and discard all other messages.This makes life for the moderator a lot easier. You can achieve this by clicking on `Privacy Options ...` subsequently click on `Sender filters` and then for `Action to take for postings from non-members for which no explicit action is defined.` choose `Discard` and a bit further down `Should messages from non-members, which are automatically discarded, be forwarded to the list moderator?` choose `No` and finally click on `Submit Your Changes`. 
