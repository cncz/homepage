---
author: petervc
date: '2021-12-05T23:34:06Z'
keywords: []
lang: en
tags:
- software
title: Mathematica
wiki_id: '150'
---
[Mathematica](http://www.wolfram.com/products/mathematica/) is a mathematical software package made
by [Wolfram Research](http://www.wolfram.com/).

C&CZ manages the network license for the concurrent use
of Mathematica within Radboud University.

The license for a small number, ca. 20, of concurrent users of Mathematica, is paid for by C&CZ and may be used freely by students and staff
of the Faculty of Science.  Until the end of 2023, there temporarily was an unlimited license for all RU staff and students.
Users/departments from other parts of the campus can use Mathematica for a yearly reimbursement of the license costs.

One can get the
installation and license information from [C&CZ system
administration](/en/howto/systeemontwikkeling/).

The use of Mathematica is logged by the license server.

On the Linux PCs that are managed by C&CZ, Mathematica can be found in
/vol/mathematica. On MS-Windows PC’s Mathematica can be found on the
S-disk (software).
