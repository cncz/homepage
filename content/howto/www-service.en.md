---
author: polman
date: '2007-05-30T12:05:18Z'
keywords: []
lang: en
tags:
- internet
title: WWW Service
wiki_id: '76'
---
## Starting a new WWW service

`This document gives a short description which decisions have to be made before a new WWWW service within the Science Faculty can be started`

and provides the possibility to directly send a request to C&CZ to start
a new WWW service

### Choice of the server

The server is the computer running the WWW *daemon*, that is the program
that replies to http request from a browser. There are two possibilities
to choose from:

1.  A server not managed by C&CZ.

In this case, C&CZ can provide help installing the daemon and answer
questions regarding setting up a web server but maintenance etc. is done
by the group itself.

1.  One of the main web servers

The advantage is that a new website can be up and running very fast and
all maintenance is taken care off by C&CZ.

### A name for the service

As for e-mail addresses, changing the name of a WWW service should be
avoided. It is therefore advisable to directly choose a good name or
abbreviation. The name has to be unique within the domain and it is
advisable to choose a short name.

### Who is going to maintain the web site

Next one has to decide who (that is which logins) is going to maintain
the pages. A (small) group of people has the advantage that the site is
not depending on (the availability of) one person whereas the work
remains manageable.

### Who is (are) the contact(s) for the website

To be able to contact the maintainer(s) of the website in case of
problems we need at least one mail address.

### Requesting a new WWW service

If the above questions have been answered and one can request a new WWW
service by sending [an email](../contact). One will then get a
[standard setup](/en/howto/apachesetup/).

### Do you need a database

Many websites use a database backend, notably the combination of php and
mysql is commonly used. C&CZ manages a number of mysql servers one may
use. If you need a database you can request one by [sending an email](../contact) 
containing the name of the database and the website which is going to use it.
