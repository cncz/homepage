---
author: wim
date: '2022-03-09T11:59:07Z'
keywords: []
lang: nl
tags:
- medewerkers
- studenten
title: Sleutelprocedure terminalkamers
wiki_id: '496'
---
## Sleutels PC-cursuszalen

### Algemeen

-   Deze procedure voor de sleutels van de door C&CZ beheerde
    [PC-cursuszalen](/nl/howto/terminalkamers/) geldt het hele jaar, dus
    ook in de vakanties.
-   Alle sleutels zijn in beheer bij de receptiedienst (HG00.040).

### Geautoriseerde gebruikers

-   Alle studenten die een geldige collegekaart van de faculteit
    Natuurwetenschappen, Wiskunde en Informatica hebben.
-   Docenten en medewerkers uit de faculteit NWI, waaronder medewerkers
    van C&CZ en de schoonmaakdienst.
-   Studenten van buiten de faculteit die aan cursussen deelnemen die
    door docenten van FNWI gegeven worden. De docent zorgt voor een
    lijst met namen bij de receptiedienst.

### Sleutelprocedure

-   De receptie opent en sluit volgens het onderstaande schema.

  ---------------------------------------- ---------------------------------- --------------------- ----------------------- --------------------------
                                                                              maandag -           [vrijdag   zaterdag
                                                                              donderdag][Monday -                         
                                                                              Thursday]                                    

  TK023                                    openen/sluiten   8.30-21.30            8.30-21.30              9.30-16.00

  TK029, TK075, TK206, TK053, TK625        openen/sluiten   8.30-18.00            8.30-18.00              

  Studielandschap   openen/sluiten   8.30-22.00            8.30-20.00              9.30-16.00

  FNWI Bibliotheek[open/close]   8.30-22.00            8.30-20.00              9.30-16.00
  Science]                                                                                                                 
  ---------------------------------------- ---------------------------------- --------------------- ----------------------- --------------------------

### Overige afspraken

-   C&CZ zorgt voor bordjes in PC-cursuszalen die duidelijk aangeven:
    -   dat reserveringen voorrang hebben;
    -   wat de sluitingstijden zijn;
    -   dat alleen bevoegden (in principe studenten van FNWI) toegang
        hebben;
    -   dat er niet gerookt/gedronken mag worden.
-   Externe cursussen worden via zaalreservering van Huisvestings
    & Logistiek (HL)
    gereserveerd. Deze zijn te zien in het zaalreserveringssysteem.
    Hierbij is echter geen docentnaam vermeld. Zaalreservering zal
    daarom de naam van de docent aan de receptiedienst moeten doorgeven.
-   Bijna alle PC-cursuszalen kunnen gereserveerd worden voor cursussen.
    In dat geval heeft een docent/toezichthouder/receptiedienst het
    recht om alle anderen dan deelnemers aan de betreffende cursus te
    verzoeken de kamer te verlaten.
-   Omwille van de beperking van de milieubelasting worden de PC’s in
    alle PC-cursuszalen na sluitingstijd automatisch uitgeschakeld, met
    uitzondering van de zalen die op dat moment ’s nachts als
    rekencluster gebruikt worden. Bij reservering van een PC-cursuszaal
    buiten de reguliere openingstijden is het dus van belang om de
    beschikbaarheid van de PC’s met C&CZ af te stemmen.

