---
author: bram
date: '2023-01-16T15:12:45Z'
keywords: []
lang: nl
title: Koppelen van een netwerk share
tags:
- storage
aliases:
- netwerkschijf
wiki_id: '685'
cover:
  image: img/2023/leaf.jpg
ShowToc: true
TocOpen: true
---
# Welk pad te koppelen?
Op de storage-pagina worden de netwerkpaden beschreven voor je [home directory](../storage#homedirectory-paden) en [netwerkschijven](../storage#paden-voor-netwerkschijven). Als het pad eenmaal bekend is, kun je de onderstaande stappen volgen voor jouw besturingssysteem.

{{< notice info >}}
Voor toegang tot deze paden van buiten het RU-netwerk is een [VPN](../vpn) verbinding vereist.
{{< /notice >}}

> Mocht het na het volgen van deze instructies toch niet lukken om een netwerkschijf te koppelen? Twijfel niet om dan [contact](../contact) op te nemen. We zullen je met alle plezier helpen!

# Windows

Neem de volgende stappen voor het koppelen van een schijfletter aan een netwerk schijf:

- Open Verkenner, bijvoorbeeld met _Windowstoets + e_, en ga naar _Deze Computer_
- In het knoppenmenu, selecteer _Computer_ -> _Netwerkverbinding maken_ -> _Map network drive_
- Selecteer een stationsletter in de lijst Station.
- Typ in het vak Map het _netwerk pad_ in, deze begint met `\\`
- Selecteer _Opnieuw verbinding maken bij aanmelden_
- Selecteer _Verbinding maken met andere referenties_
- Klik op _Voltooien_
- Vul vervolgens je [Science loginnaam](../login) en je wachtwoord in
- Selecteer _Onthoud mijn gegevens_ en klik _Ok_

> De netwerkschijf zou nu in Verkenner zichtbaar moeten zijn. Zo niet? neem [contact](../contact) op!

# macOS

Neem de volgende stappen voor het verbinden van een netwerkschijf op je macOS systeem:

- Open Finder
- Bij het _Ga_ menu, kies _Verbind met Server_ 
- Of gebruik de sneltoets _Command + K_ om er in een keer te komen
- Vul het pad in van de netwerkschijf, dit begint met `smb://...`, en klik _Verbinden_
- Gebruik je [Science account](../login) en het bijbehorende wachtwoord voor de authenticatie

> De netwerkschijf zou nu in Finder gekoppeld moeten zijn. Zo niet? neem [contact](../contact) op!

# Linux

Voor het mounten van een netwerkschijf onder Ubuntu Linux:

- Open de bestandsbeheerder _Files_
- Aan de linkerkant, klik _+Other Locations_
- Vul bij _Connect to Server_, het pad van de netwerkschijf in, dit begint met `smb://...`
- Klik op _Connect_
- In het volgende dialoog wordt gevraagd naar hoe je wilt verbinden
- Selecteer bij _Connect as_ de optie _Registered User_
- Vul je [Science account](../login) en wachtwoord in
- (Laat het veld _Domain_ voor wat het is, deze waarde wordt niet gebruikt)
- Kies voor het al dan niet bewaren van je wachtwoord
- Klik op de _Connect_ knop

> De netwerkschijf zou nu in Files gekoppeld moeten zijn. Zo niet? neem [contact](../contact) op!


# Mobiele apparaten

Mobile apparaten bieden standaard geen functionaliteit om netwerkschijven te benaderen. Er bestaan verschillende apps die deze leemte vullen.
Deze apps bieden ondersteuning voor netwerkschijven:

| Android                                                                                                                                                                                               | Beschrijving       |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| [Astro File Browser](https://play.google.com/store/apps/details?id=com.metago.astro) + [Astro SMB module](https://play.google.com/store/apps/details?id=com.metago.astro.smb)                         | File manager       |
| [SolidExplorer](https://play.google.com/store/apps/details?id=pl.solidexplorer2)                                                                                                                      | File manager       |
| [SyncMe Wireless](https://play.google.com/store/apps/details?id=com.bv.wifisync)                                                                                                                      | Synchronisatie app |
| [FolderSync Lite](https://play.google.com/store/apps/details?id=dk.tacit.android.foldersync.lite) or [FolderSync Pro](https://play.google.com/store/apps/details?id=dk.tacit.android.foldersync.full) | Synchronisatie app |

| iOS                                                                                   | Beschrijving                                |
| ------------------------------------------------------------------------------------- | ------------------------------------------- |
| [FileBrowser](https://apps.apple.com/nl/app/filebrowser-document-manager/id364738545) | File manager met ingebouwde document viewer |
| [GoodReader](https://apps.apple.com/nl/app/goodreader-pdf-editor-viewer/id777310222)  | Voornamelijk voor PDF documenten            |
