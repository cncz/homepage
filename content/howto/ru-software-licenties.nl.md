---
author: petervc
date: '2022-06-24T12:45:46Z'
keywords: []
lang: nl
tags:
- software
title: RU softwarelicenties
wiki_id: '145'
---
## Science softwarelicenties afgesloten door C&CZ

Alle info is te vinden op
de [Science licentiepagina's](https://cncz.pages.science.ru.nl/licenses/).
Bij vragen neem contact op met C&CZ.

De meeste software is op de install-schijf te vinden.
Op een PC met MS-Windows (of een Linux “Samba client”), kan de
schijf-verbinding gemaakt worden via de service
    [**\\\\install.science.ru.nl\\install**](https://wiki.cncz.science.ru.nl/Install_share).

## RU softwarelicenties afgesloten door ILS

ILS heeft voor de RU voor [een aantal
software-pakketten](https://www.ru.nl/services/campusfaciliteiten-gebouwen/ict/software)
licentie-overeenkomsten met de leverancier afgesloten. Daardoor is die
software gratis (campuslicentie) of relatief goedkoop (bulklicentie)
voor studenten en medewerkers te verkrijgen.

Veel licentie software is bij [Surfspot](http://www.surfspot.nl/) te
koop voor medewerkers en studenten met hun RU-account
en hun [RU-wachtwoord](http://www.ru.nl/wachtwoord). C&CZ heeft hiervan
sommige (voornamelijk MS-Windows) [software te
leen](/nl/howto/microsoft-windows/).

-   Security: F-secure
    -   voor MS-Windows PC’s die met geld van de RU gekocht zijn, kan
        men een licentie krijgen via
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/download-secure/).
    -   voor Apple computers die met geld van de RU gekocht zijn, kan
        men een licentie krijgen via
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/aanvraagformulier/).
    -   voor individueel/thuisgebruik op 1 eigen computer kan men bij
        [Surfspot](http://www.surfspot.nl) gratis “F-Secure Safe (RU
        Nijmegen)” aanschaffen.
