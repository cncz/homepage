---
author: polman
date: '2023-09-13T17:07:00Z'
keywords: []
lang: nl
tags:
- storage
aliases:
- quota-bekijken
title: Diskquota
wiki_id: '26'
---
Je persoonlijke quota bepaalt alleen hoeveel ruimte je eigen data mag
innemen en hoeveel files en folders je mag hebben (bv. op je eigen
Windows `U:\` schijf of Linux home-directory). Ook andere disks kunnen quota hebben.
De mail servers gebruiken ook quota om het diskruimtegebruik in ieders mailbox
te limiteren.

## Windows

Op Windows werkplekken waar de home-directory als `U:\` schijf aangekoppeld
is, kan men door met de rechtermuisknop op de schijf te klikken en dan
de `Eigenschappen` (`Properties`) te bekijken, zien hoeveel ruimte (in
kbytes) er in gebruik is en nog beschikbaar is binnen de persoonlijke
quota.

Men kan ook `WinDirStat` voor een grafisch overzicht gebruiken.
Op een beheerde werkplek te vinden onder: `S:\windirstat` .

## Linux

Op Linux werkplekken kan men de volgende twee commando's gebruiken om te
bepalen wat je quota op dit moment is:

`quota`

Je ziet nu regels zoals:

```
Disk quotas for user login (uid 1000): 
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
homeX.science.ru.nl:/disk/login
                4899120* 4864000 5120000   7days    2880  1900000 2000000        
```

-   blocks : Dit is de hoeveelheid diskruimte in KB (kilobytes) die je op
    dit moment in gebruik hebt.\
    quota: Dit is de hoeveelheid diskruimte die je mag gebruiken.\
    Onder Linux mag je tijdelijk (7 dagen) meer gebruiken dan wat quota
    aangeeft. Onder Windows is dit een harde grens.
-   limit : Dit is de hoeveelheid diskruimte die je maximaal kunt
    gebruiken.\
    timeleft : De tijd die je nog hebt om onder de quota grens te
    komen.\
    Als je te lang wacht (er verschijnt EXPIRED) dan kun je geen
    bestanden meer maken voordat je genoeg opgeruimd hebt (weer onder je
    quota zit). Als je in deze situatie een file schrijft (nieuw of
    oud), bestaat het risico dat er data verloren gaat.
-   files : Dit is de hoeveelheid bestanden die je op dit moment in
    gebruik hebt.\
    quota : Dit is de hoeveelheid bestanden die mag gebruiken.\
    Onder Linux mag je tijdelijk (7 dagen) meer gebruiken dan wat quota
    aangeeft. Onder Windows is dit weer een harde grens.
-   limit : Dit is de hoeveelheid bestanden die je maximaal kunt
    gebruiken.\
    timeleft : De tijd die je nog hebt om onder de quota grens te
    komen.\
    Als je te lang wacht (er verschijnt EXPIRED) dan kun je geen
    bestanden meer maken voordat je genoeg opgeruimd hebt (weer onder je
    quota zit).

`df`

Als `quota` geen output heeft, heb je een homedirectory op ZFS. Het ZFS file systeem heeft geen softquota
of een timeleft concept. Om te zien hoe groot je homedirectory is gebruik `df`, je ziet uitvoer zoals:

~~~ console
df -h ~
Filesystem                                                             Size  Used Avail Use% Mounted on
f6ea112e23d448e498d4374805f60174.home.science.ru.nl:/export/mijnlogin  5.0G  3.6G  1.5G  71% /home/mijnlogin
~~~

## Bepalen waar je diskruimte voor gebruikt wordt

Ga op een Linux loginserver eerst naar je eigen home directory

` cd`\
` pwd`

Het `pwd` commando behoort iets te laten zien als `/home/mijnlogin`.

Typ nu het volgende commando in:

` du -sh * .??* | sort -h`

Er verschijnt een lijst van files en folders, gesorteerd op grootte, met
daarbij de hoeveelheid ruimte in MB (megabyte) die ingenomen wordt.

Een andere veel gebruikte utility is

` ncdu ~`

## Veel voorkomende schuldigen aan het opvullen van je quota.

Globaal gezien is het meestal caching, dit is bijna waardeloos, omdat caching
bedoeld is voor lokale schijven en niet je netwerk-homedirectory.

### Python PIP

Alle cache commando's van pip [kun je hier vinden](https://pip.pypa.io/en/stable/cli/pip_cache/).
Lang verhaal kort: gebruik `pip cache purge` om de cache te wissen.

### Firefox

Op de commandoregel kun je het volgende commando gebruiken om de firefox cache te legen:

`rm -rf ~/.cache/mozilla/firefox/*`. Daarna moet je firefox herstarten.

Als je wilt voorkomen dat firefox teveel cacheruimte inneemt, ga naar de `about:config`
instellingen (via de adresbalk) en zoek voor `browser.cache.disk.capacity`. Je zou ook naar andere
`browser.cache.disk.*` instellingen kunnen kijken.

## Mail

De mail quota en het huidige gebruik kunnen eenvoudig bekeken worden via
de [Roundcube webmail service](http://roundcube.science.ru.nl). In de
linkeronderhoek staat het percentage dat van de quota in gebruik is.
Daarnaast bij de map-acties kan een lijst van mappen getoond worden,
waarbij een optie is om de map-grootte te laten zien.
