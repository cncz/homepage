---
author: polman
date: '2023-09-13T17:07:00Z'
keywords: []
lang: en
tags:
- storage
aliases:
- quota-bekijken
title: Disk quota
wiki_id: '26'
---
Your personal quota only determine the amount of disk space your own
data may occupy and the number of files and folders you may have on your
Windows `U:\` disk or Linux home directory. Quotas may be active on other
disks as well, however not all disks have quotas. The mail servers also use
quotas to limit the disk space of each user’s mailbox.

## Windows

On Windows desktops where the home directory is available as U: or H:
disk right click on the disk, then choose `Properties` to view the
amounts of used and available personal disk space in kbytes.

For a graphical view of your disk usage you can use `WinDirStat`.
On managed systems this utility is installed at `S:\windirstat` .

## Linux

On Linux systems use one the following two commands to view the personal quota
and current usage:

`quota`

The output will show lines like:

```
Disk quotas for user login (uid 1000): 
     Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
homeX.science.ru.nl:/disk/login
                4899120* 4864000 5120000   7days    2880  1900000 2000000        
```

-   blocks : The amount of disk space in kKB (kilobytes) currently in
    use.\
    quota : The maximum amount of disk space available for you.\
    On Linux systems you may exceed your quota temporarily (up to 7
    days). For Windows however this is an absolute maximum.
-   limit : The absolute maximum amount of disk space you can occupy
    (see above).\
    timeleft : The time left to get below your quota.\
    If you wait too long (i.e. if timeleft shows EXPIRED), you cannot
    write files (new or old) until you’ve cleaned up to get under quota
    again. There is a risk of losing data if you try to write when the
    quota grace period has been exceeded.
-   files : The number of files currently owned by you.\
    quota : The maximum number of files and folders you can have.\
    On Linux systems you may exceed your file quota temporarily (upto 7
    days). Again for Windows this is an absolute maximum.
-   limit : The absolute maximum number of files and folders you can
    have.\
    timeleft : The time left to get below your file quota.\
    If you wait too long (i.e. if timeleft shows EXPIRED) you cannot
    create files anymore until you’ve cleaned up to get under quota
    again.


`df `

If `quota` gives no output, you have a homedirectory on ZFS. The ZFS filesystem doesn't have a
softquota, nor a timeleft concept. To see the size of your homedirectory use `df`, the output
will look like:

~~~ console
df -h ~
Filesystem                                                           Size  Used Avail Use% Mounted on
f6ea112e23d448e498d4374805f60174.home.science.ru.nl:/export/mylogin  5.0G  3.6G  1.5G  71% /home/mylogin
~~~

## How to determine what your disk space is used for

On a Linux login server go to your home directory

` cd`\
` pwd`

The `pwd` command will show something like `/home/mylogin`.

Now type the following command:

` du -sh * .??* | sort -h`

This will produce a list of the files and folders in your home
directory, each preceeded by its size in MB (megabytes), sorted by size.

Another command to sum up disk usage is

` ncdu ~`

## Frequent culprits for eating your quota

In general it is caching, this is semi-useless, because caching is meant to be done on local storage, not your network homedirectory.

### Python PIP

In general [look here for pip cache commands](https://pip.pypa.io/en/stable/cli/pip_cache/). TLDR: use `pip cache purge` to clean up.

### Firefox

From the commandline use this command to clean the firefox cache:
`rm -rf ~/.cache/mozilla/firefox/*`. Then restart firefox.

To reduce the amount of cache size used by firefox, go the the `about:config`
settings (via the address bar) and search for `browser.cache.disk.capacity` and
you may want to look at other `browser.cache.disk.*` settings as well.


### Anaconda


## Mail

The [Roundcube webmail service](http://roundcube.science.ru.nl) offers a
convenient way to view your mail quota and current use. The lower left
corner shows the percentage of quota in us. Next to that, clicking
Folder options, lists all folders. Clicking on a folder gives the option
to Click to get the folder size.
