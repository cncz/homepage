---
author: miek
date: '2024-01-12T15:12:45Z'
keywords: [ zfs ]
lang: en
title: ZFS Volumes
tags:
- storage
- zfs
cover:
  image: img/2023/storage.jpg
---

C&CZ heeft een opslagoptie ontwikkeld gebaseerd op ZFS. Deze pagina behandelt de technische details
en welke mogelijkheden er zijn. Dit om je te helpen beslissen of dit iets voor jouw organisatie is.
Elke data opslag die we uitdelen noemen we een "volume". Voor deze ZFS volumes geldt:

Grootte:
: De volume grootte kan zo groot zijn als nodig; de enige imitatie is hoeveel disken we in een
machine kunnen stoppen. Nu (2024+) kunnen we machines kopen die ongeveer 140-200 TB aan data kunnen
bevatten.

Snapshots:
: Dankzij ZFS hebben we nu [snapshots](../zfs-share-snapshot/), zodat je zelf bestanden terug kunt
halen.

Backups:
: Elk volume kan wordt gebackupped naar lokale S3 storage. Dit is voor een eventuele *disaster
recovery* en zal hopelijk alleen nodig zijn in zeldzame gevallen.

[Elk van deze volumes zijn zichtbaar in DHZ onder het kopje "Storage"](https://dhz.science.ru.nl) als je er toegang tot hebt.
[Hoe een volume te bestellen kun je hier
lezen](https://cncz.science.ru.nl/nl/howto/storage/#bestellen).

## Toegang

Op alle machines waar je kunt inloggen heb je toegang, via Samba en/of NFS, tot deze volumes.

Op elke volume worden twee, door jouw gekozen, groepen geconfigureerd

1. een lees-schrijf groep
2. een alleen-lezen groep

Als je eigenaar bent van deze groepen bepaal jezelf wie waar bij kan door middel van groep's
lidmaatschap in [DHZ](https://dhz.science.ru.nl). In de nabije toekomst willen ook andere
eigenschappen van deze volumes in DHZ zetten, zodat je bijvoorbeeld de grote van een volume zelf kunt
aanpassen.

## Extra Opties

Er zijn nog een aantal andere eigenschappen die aan of uitgezet kunnen worden. Het hangt af van de
gebruikers of deze _ook_ in DHZ komen. Voorlopig moet je hiervoor no contact met C&CZ opnemen.

* Synchroniseren van alle files en directories naar de originele lees-schrijf/alleen-lezen groepen,
  zodat eventuele "gekke" file permissies en eigenaarschap weer goed wordt gezet.
* Zet het gehele volume (tijdelijk) op alleen-lezen.
* Aan- of uitzetten van de backup.

## Migratie van Bestaande Volumes

Er zijn geen plannen om de bestaande volumes over te zetten. Als jouw organisatie wel zoiets wilt,
neem dan gerust contact op met C\&CZ.
