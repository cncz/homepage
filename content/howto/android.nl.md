---
author: petervc
date: '2017-10-05T15:43:27Z'
keywords: []
lang: nl
tags: []
title: Android
wiki_id: '949'
---
## Netwerk

### Wi-Fi Eduroam

Het Eduroam draadloze netwerk is in alle gebouwen op de campus
beschikbaar en ook op veel andere universiteiten in de wereld, zie
[Eduroam.nl](http://www.eduroam.nl).

-   Browse naar <https://www.ru.nl/services/campusfaciliteiten-gebouwen/ict/wifi> voor alle informatie en
    configuratie-handleidingen.

-   Als er om gebruikersnaam en wachtwoord wordt gevraagd: dit kan een van de volgende twee combinaties zijn:

| Gebruikersnaam             | Wachtwoord         |
| -------------------------- | ------------------ |
| u-nummer@ru.nl             | RU-wachtwoord (voor [Radboudnet](http://www.radboudnet.nl) website) |
| sciencelogin@science.ru.nl | Science-wachtwoord (voor [Doe-Het-Zelf](http://dhz.science.ru.nl) website) |

Controleer evt. het IP-adres door naar www.whatsmyip.org te gaan, hier
zal staan:

-   IP-adres: een adres uit de reeks 145.116.128.0 - 145.116.191.255

## VPN

Om netwerkschijven te benaderen is het meestal voldoende om met het RU campusnetwerk verbonden te zijn, bedraad of draadloos via Eduroam. Enkele netwerkschijven zijn extra beveiligd, deze zijn alleen vanaf bepaalde werkplekken te benaderen of via een VPN connectie.

Zie [Vpn](/nl/howto/vpn/)

## Toegang tot een netwerkschijf

Zie de [pagina over netwerkschijven](/nl/howto/netwerkschijf/) voor
benodigde apps en de naamgeving.

## Mail

De meeste mail apps hebben dezelfde basis, waarmee mail van andere
gebruikers in zgn. ‘shared folders’ niet gelezen kan worden. Als men dat
wil, kan men **Maildroid** gebruiken.

### Science mail

De meeste mail apps kunnen als volgt ingesteld worden:

-   Open de mail app
-   Kies *Voeg mailaccount toe…*

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Naam \| *Voornaam Achternaam* \|- \| Adres \|
*V.Achternaam*\@science.ru.nl \|- \| Wachtwoord \| *het wachtwoord dat
hoort bij de Science loginnaam* \|}\
\* Kies *Volgende*een aantal keer.

Controleer of het werkt:

-   Open de mail app

### Exchange (ISC)

-   Open **Instellingen**
-   Kies *E-mail, contacten, agenda’s*
-   Kies *Voeg account toe…*
-   Kies *Microsoft Exchange*
-   Vul in:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| E-mail \| *V.Achternaam*\@fnwi.ru.nl \|- \| Domein \| RU \|- \|
Gebruikersnaam \| *U-nummer* \|- \| Wachtwoord \| *het wachtwoord bij
het U-nummer (hetzelfde als van Intranet, FLEX)* \|- \| Beschrijving \|
*naar eigen inzicht, b.v.* “Exchange” \|}\
\* Kies *Volgende* een aantal keer. Controleer of het werkt:

-   Open de mail app
