---
author: miek
date: '2024-01-12T15:12:45Z'
keywords: [ zfs ]
lang: en
title: ZFS Volumes
tags:
- storage
- zfs
cover:
  image: img/2023/storage.jpg
---

A new storage option has been developed by C&CZ. This page will show the technical details and
properties of this storage, which will hopefully help you make a decision if this is something you
want. Any storage we handout we call a "volume". For these ZFS volumes:

Size
: Storage size can be as large as you want, but smaller than the amount we can put in a single machine.
Currently (2024+) we can buy machines with roughly 140-200 TB storage.

Snapshots
: On this we enable [snapshots](../zfs-share-snapshot/), so that you can retrieve (deleted) files your self.

Backup
: Each volume will be back upped to local S3 storage. This should be thought of as
disaster recovery, and restoring from the cloud should (hopefully) be rarely needed.

[All of these volumes are visible in DIY in the "Storage" section](https://dhz.science.ru.nl) if you have access to them.
[How to order such  storage can be seen here.](https://cncz.science.ru.nl/nl/howto/storage/#bestellen)

## Access

Each volume can be access via Samba and NFS from machines where you can log in.

On a volume we configure two groups (of your choice):

1. a read-write group
2. a read-only group

If you are the owner of those groups you can change who can access what by removing are adding user
into those groups in [DIY](https://dhz.science.ru.nl). In the (near) future we also want to expose
volume properties to DIY, so that group owners can e.g. increase the volume size.

Users of the volume can (naturally) change files they own and add/remove permissions.

## Optional Features

The are a bunch of knobs we turn for each of these volumes. Depending on user demand this will also
find a place in DIY, for now you'll need to contact C&CZ.

* Sync all directories and files to the original read-write/read-only groups and remove any added
  groups and/or access.
* Temporary set the volume to read-only.
* Enable/disable backup.

## Migration of Existing Volumes

There are currently no plans to migrate/consolidate exiting volumes. If your organization wants to
discuss such a migration please contact C\&CZ.
