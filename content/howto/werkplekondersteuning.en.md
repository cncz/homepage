---
author: sioo
date: '2025-02-20T15:29:55Z'
keywords: []
lang: en
tags:
- overcncz
title: Operations, Helpdesk, User support
wiki_id: '128'
---
## location

Operations/helpdesk is located in room HG00.051.

## Type of help available

-   User support for students and staff (General IT questions)
-   [Installation and maintenance of hardware available for students, especially the computer labs and public workstations](/en/howto/terminalkamers/)
-   General support of computer equipment in research departments
-   Assistance for, or advice with respect to,
    -   [PC installation and software](/en/howto/installeren-werkplek/)
    -   [repairing PC's and laptops](/en/howto/reparaties/)
