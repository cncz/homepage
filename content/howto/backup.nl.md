---
author: petervc
date: '2020-12-17T12:27:26Z'
keywords: []
lang: nl
tags:
title: Backup
wiki_id: '18'
---
Als er per ongeluk één of meerdere bestanden verloren gaan uit een
*home-directory* (U:-schijf) of een netwerkschijf of een andere plaats
van een C&CZ-server, dan is de kans groot dat ze teruggehaald kunnen
worden. Voorwaarde voor het aanwezig zijn in de backup is dat het
bestand aanwezig was tijdens een (nachtelijke) backup.

Er zijn diverse onafhankelijke backup schema’s in gebruik

-   Dagelijkse backup naar online storage.
-   Maandelijkse backup naar tape.
-   Jaarlijkse backup naar tape.

# Dagelijkse backup

Van alle partities (m.u.v. tijdelijke diskruimte zoals /tmp, /var/tmp,
/scratch e.d.) wordt na elke werkdag een backup op online storage gemaakt.
Vanwege de tijd die het kost om een backup te maken, zal dit vaak een
zogenaamde *incrementele dump* zijn, d.w.z. dat alleen alle
veranderingen t.o.v. de laatste backups worden weggeschreven. De backups
worden elke werkdag na werktijd gestart. Deze backups worden ca. vier
weken bewaard. Omdat ze online op disken staan, zijn ze sneller
terug te halen dan wanneer ze op tape zouden staan.

# Maandelijkse en jaarlijkse backup naar tape

Ieder weekeinde wordt een deel van de disk partities volledig naar een
set LTO6-tapes geschreven. Elke partitie komt eens in de 4 weken aan de
beurt. Deze maandelijkse backup tapes worden een heel jaar bewaard. De
jaarlijkse backups worden rond de kerstvakantie (eind december en begin
januari) op LTO-7 gemaakt en worden tenminste vijf jaar bewaard.

Als het terug te zetten bestand al meer dan vier weken niet meer op het
systeem aanwezig was, dan moeten de maandelijkse of jaarlijkse tapes
geraadpleegd worden. De recentste maandelijkse tapes tapes worden i.v.m.
calamiteiten ook extern bewaard.

# Backup server

De backups worden uitgevoerd m.b.v. de [Amanda](http://www.amanda.org/)
open source network backupsoftware. De in 2013 vernieuwde server is een
[Dell PowerEdge
R720xd](http://www.dell.com/nl/bedrijven/p/poweredge-r720/pd) met 44 TB
schijfruimte, die gebruikt wordt om snelle restores vanaf disk mogelijk
te maken. De backups worden weggeschreven op
[LTO-6](http://en.wikipedia.org/wiki/Linear_Tape-Open)-tapes in een
[Dell TL2000](http://www.dell.com/nl/bedrijven/p/powervault-tl2000/pd)
tapelibrary met 2 LTO6 drives en 24 slots. De nieuwe voorziening leest
en schrijft LTO-6, maar kan ook de hiervoor gebruikte
[LTO-4](http://en.wikipedia.org/wiki/Linear_Tape-Open#Generations) tapes
lezen.

# Prijs

Het complete backupschema (Daily plus Monthly plus Yearly backups) kost
intern FNWI 155 €/TB jaar. Per schema is dit:

\- Daily: 70 €/TB jaar

\- Monthly: 60 €/TB jaar

\- Yearly: 25 €/TB jaar

De prijs van het backuppen is het grootste deel van de [prijs van C&CZ
diskruimte](/nl/howto/storage/).

# Andere Backup opties

We hebben in sommige gevallen andere manieren van backup in gebruik.

- Restic backup
- ZFS snapshots
- Zrepl gerepliceerde ZFS snapshots op een tweede machine

Mogelijk gaan we in de toekomst backups maken naar de *cloud* met bijvoorbeeld
[kopia](https://kopia.io)

De kosten hiervan zijn nog niet bekend, omdat wat we tot nu toe gebruiken
hiervan van en naar hardware van de afdelingen zelf is of het gaat om algemene
FNWI data (gitlab).
