---
author: arranc
date: '2022-09-12T06:31:56Z'
keywords: []
lang: en
tags: []
title: FoxIt PDF Reader
wiki_id: '674'
---
The program [FoxIt PDF
Reader](http://www.foxitsoftware.com/pdf/rd_intro.php) can be used as an
alternative to Adobe Reader, it is a free program to view or print PDF
files.

One reason to sometimes not use Adobe Reader is that Adobe Reader 8
produces PostScript that HP4250/HP4350 printers can’t handle, not
even after a firmware upgrade. Therefore C&CZ switched to using the
(slower) PCL-driver for almost all HP4250/HP4350 printers.
