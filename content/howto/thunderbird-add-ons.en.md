---
author: jverdurm
date: '2008-04-09T22:21:43Z'
keywords: []
lang: en
tags:
- email
title: Thunderbird Add-ons
wiki_id: '662'
---
-   abcTajpu
    -   Copy Sent to Current
    -   Folderpane Tools
    -   GMailUI
    -   Lightning
    -   Provider for Google Calendar
    -   New Mail Icon
    -   Nostalgy
    -   Quicktext
    -   Signature Switch
    -   ThunderBrowse
    -   Xnote
    -   MailClassifier
    -   Tag bar
    -   Priority Switcher
    -   [MailTweak](http://journal.mozdev.org/mailtweak.html)
    -   Mail Redirect
    -   View Headers Toggle Button
    -   Display Mail User Agent Extension
    -   FolderFlags

More add-ons can be found at [addons.mozilla.org](https://addons.mozilla.org/en-US/thunderbird/).
