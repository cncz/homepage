---
title: Email
author: bram
date: 2022-10-14
keywords:
  - email
tags:
  - medewerkers
  - studenten
  - email
aliases:
  - email-authsmtp
cover:
  image: img/2023/science-email.png
ShowToc: true
TocOpen: true
---

{{< notice tip >}}

# Webmail

Gebruik onze webmailer als je snel vanuit je browser toegang wilt hebben tot je email:

## https://roundcube.science.ru.nl/

{{< /notice >}}

# Mailinstellingen

Bij het instellen van de meeste mailprogramma's, volstaat het om alleen je mailadres in te vullen. Zo ziet je Science mailadres eruit:

> `I.Achternaam@science.ru.nl`

Alle technische instellingen worden vervolgens automatisch voor je ingevuld.
Mocht dat niet lukken, of als het nodig is om eea zelf in te vullen, gebruik dan de instellingen van onderstaande tabel:

## Binnenkomende mail

| item           | instelling           | toelichting                                                                                    |
| -------------- | -------------------- | ---------------------------------------------------------------------------------------------- |
| mailprotocol   | `IMAP`               | Mailprotocol waarbij je mail in principe op de server bewaard blijft.                          |
| servernaam     | `post.science.ru.nl` | Server voor het lezen van mail. Hier staan je mail-mappen.                                     |
| poort          | `993`                |                                                                                                |
| versleuteling  | `SSL/TLS`            | Deze versleuteling wordt gebruikt bij de communicatie van je email-programma en de mailserver. |
| gebruikersnaam | `scienceloginnaam`   | Gebruik hiervoor je [Science account](/nl/howto/login).                                        |

## Uitgaande mail

| item           | instelling           | toelichting                                             |
| -------------- | -------------------- | ------------------------------------------------------- |
| servernaam     | `smtp.science.ru.nl` | Deze server wordt gebruikt bij het versturen van email. |
| poort          | `587`                |                                                         |
| versleuteling  | `STARTTLS`           |                                                         |
| gebruikersnaam | `scienceloginnaam`   | Gebruik hiervoor je [Science account](/nl/howto/login). |

## Adresboek

| item            | instelling           |
| --------------- | -------------------- |
| adresboekserver | `ldap.science.ru.nl` |
| poort           | 389                  |
| basedn          | `DN: o=addressbook`  |

# Science mail eigenschapen

> Lees op de [DHZ pagina](/nl/howto/dhz) wat je zoal voor je mail kunt instellen.

- C&CZ gebruikt open protocollen voor de mail. Dat betekent dat je mail kunt checken met een programma naar je eigen voorkeur.
- Op elk mailbericht wordt bij binnenkomst een eenvoudig antivirus en antispam-filter toegepast.
- Mailboxen kunnen tussen gebruikers [gedeeld](#mailboxen-delen) worden.
- Ook [labels](/nl/howto/thunderbird-tags/) voor mail kunnen met andere gebruikers gedeeld worden.
- Met behulp van [Sieve](/nl/howto/email-sieve/) kunnen mailberichten al op de server gefilterd worden.
- Er zijn twee mogelijkheden om mailadreslijsten op te zetten: [mailadreslijsten](#adreslijsten) en [mailman](#Mailman-mail-lijsten).
- Het automatisch doorsturen van mail door medewerkers en studenten naar externe (niet-RU) adressen is sinds augustus 2023 niet meer toegestaan.
- De opslagruimte voor je mail is standaard `5 GB`. Dit is op [verzoek](/nl/howto/contact) uit te breiden.

{{< notice tip >}}

De mailmap "verzonden" wordt vaak over het hoofd gezien bij het opruimen van email. Zoek hierin naar de mails met de grootste bijlagen.

{{< /notice >}}

## Mailboxen delen

Dit kan eenvoudig ingesteld worden in [Roundcube webmail](http://roundcube.science.ru.nl), linksonder bij het tandwiel-icon, via `Mapopties` en dubbelklikken op een map, maar ook in een mailprogramma als Kmail. Hier moet de `scienceloginnaam` ingevuld worden. Degenen die de map mogen gebruiken, moeten zich nog wel abonneren op die map.
Wanneer een gebruiker een mail leest, wordt die niet automatisch voor alle gebruikers gemarkeerd als gelezen. Dit kan echter bij `postmaster` voor betreffende mailbox aangevraagd worden.

## Adreslijsten

Er zijn eenvoudige lijsten van mail-adressen mogelijk, die men aan kan
vragen bij [Postmaster](/nl/howto/contact) en die daarna door de eigenaren zelf bijgehouden
kan worden op [de Doe-Het-Zelf website](/nl/howto/dhz),
Voor lijsten met externa adressen worden deze niet meer aanbevolen vanwege antispam-maatregelen bij externe organisaties.

## Mailman mail lijsten

C&CZ beheert een mail lijstserver gebaseerd op [Mailman](https://list.org/). Een mailman lijst wordt geadviseerd voor lijsten met ook externe adressen. Zie [de mailman pagina](/nl/howto/mailman-lists) voor meer informatie.

## Alternatieve mailinstellingen

- Naast het `IMAP`-protocol is het ook mogelijk om het `POP3`-protocol te gebruiken. Als je nog nooit van het `POP3` protocol hebt gehoord, gebruik het niet! Als je het toch wilt gebruiken: het `POP3`-poortnummer is `995`.
- Vanuit [een paar netwerken](../../news/2023-12-12-changes-in-smtp-policy/#science-smtp-vanaf-bepaalde-netwerken-alleen-nog-maar-geauthenticeerd) van het Huygensgebouw is het niet verplicht om je inloggegevens te gebruiken voor het versturen van mail. Gebruik dan poort `25` van de smtp server, waarbij betreffende mail wordt gecontroleerd op spam en virussen voor versturing.

# `@ru.nl` mail

{{< notice tip >}}

Voor bijna alle info over @ru.nl mail en agenda, zie [de centrale RU website](https://www.ru.nl/services/campusfaciliteiten-gebouwen/ict/e-mail-en-agenda).


{{< /notice >}}

Elke student of medewerker van de Radboud Universiteit heeft ook een [centraal `@ru.nl` mail-adres](http://www.ru.nl/ict/medewerkers/mail-agenda/) in de vorm:

> `Voornaam.Achternaam@ru.nl`