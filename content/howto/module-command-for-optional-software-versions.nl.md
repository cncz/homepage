---
title: Module commando voor optionele software versies
author: petervc
date: 2023-08-25
keywords: []
tags: []
ShowToc: false
TocOpen: false
---

Op door C&CZ beheerde Ubuntu machines is het "module" commando beschikbaar. Dit maakt het eenvoudig om specifieke versies van beschikbare software te gebruiken.

Om te zien welke modules beschikbaar zijn:

``` sh
module avail
```

en om Matlab-R2023a te gebruiken:

``` sh
module add Matlab-R2023a`
```

Zie voor meer info [de engelstalige Environment Modules website](https://modules.readthedocs.io/en/v5.3.1/module.html).
