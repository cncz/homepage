---
author: petervc
date: '2020-08-14T08:36:14Z'
keywords: []
lang: nl
tags: []
title: Laptop pool
wiki_id: '892'
---
## Uitleen laptop-pool

### Algemene opmerkingen

FNWI heeft een uitleen laptop-pool van 60 [standaard-RU
laptops](/nl/howto/terminalkamers/). Qua gebruik zijn deze laptops
vrijwel identiek aan de pc’s in de
[pc-cursusruimtes](/nl/howto/terminalkamers/), alleen worden ze niet via
Huisvesting & Logistiek gereserveerd.

Reserveren gaat via de [Library of
Science](https://www.ru.nl/over-ons/de-campus/gebouwen-en-ruimtes/huygensgebouw/library-of-science). Gebruik voor
FNWI-onderwijsdoeleinden heeft voorrang.

## Gebruik

### Hoe log je in onder Windows?

De laptops zijn te gebruiken met:

-   ofwel de [Science loginnaam](/nl/howto/login/) en het bijbehorende
    wachtwoord
-   ofwel het RU-account: RU\\s123456 en het bijbehorende wachtwoord

### Waar moet je zijn met vragen?

Voor vragen over de uitleen of reserveringen kan men zich wenden tot de
[Library of Science](https://www.ru.nl/over-ons/de-campus/gebouwen-en-ruimtes/huygensgebouw/library-of-science). Voor
technische vragen kan men zich wenden tot [C&CZ](/nl/howto/contact/).

## Uitleenprocedure

Er is een [laptop leenovereenkomst](/nl/howto/laptop-leenovereenkomst/),
waarin de lener tekent dat hij/zij zich zal houden aan het
[uitleenreglement van FNWI voor het uitlenen van
laptops](/nl/howto/laptop-uitleenreglement/).

### Trolley

In incidentele gevallen kan voor onderwijsdoeleinden een complete
[trolley](https://www.onderwijsmagazijn.nl/product/laptop-trolley-laptopkar-smartcharge-it-laptrolley-24-horizontaal/3595282/index.html)
met maximaal 20 laptops geleend worden. De trolley is door C&CZ
aangepast, zodat de laptops in de trolley via kabels met het netwerk
verbonden kunnen zijn, waardoor er automatisch updates gedaan kunnen
worden.

