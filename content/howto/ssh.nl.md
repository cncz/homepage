---
author: petervc
date: '2022-12-21T13:20:00Z'
keywords: []
lang: nl
tags:
- software
title: SSH
wiki_id: '155'
---
## SSH Secure Shell

SSH wordt gebruikt om een beveiligde (niet-afluisterbare)
terminal-verbinding te maken met een server. Alle C&CZ [Linux
loginservers](/nl/howto/hardware-servers/) zijn via SSH te benaderen.

**NB: sinds december 2022 kunnen niet meer alle science logins op de
loginservers inloggen**, mocht je problemen ondervinden, neem [contact op met
postmaster](/nl/howto/contact/).

### Aangeraden ssh client software

-   Windows:
    -   [MobaXterm](http://mobaxterm.mobatek.net/). Uit de MobaXterm
        website: “MobaXterm is een verbeterde terminal voor Windows met
        een X11 server, een SSH client met tabbladen en bevat een aantal
        andere netwerktools voor remote computing (VNC, RDP, telnet,
        rlogin). MobaXterm brengt alle essentiële Unix-commando’s naar
        het Windows bureaublad, in een enkele exe-bestand die werkt uit
        de doos.” Ook de OpenGL-ondersteuning kan een reden zijn om
        MobaXterm te gaan gebruiken. Bij professioneel gebruik dient men
        een [licentie voor de Professional
        Edition](http://mobaxterm.mobatek.net/download.html) te
        overwegen. MobaXterm is ook beschikbaar op de
        [S-schijf](/nl/howto/s-schijf/).
    -   [Mosh](https://mosh.org/) (mobile Shell) , speciaal bij roaming
        en niet-stabiele verbindingen.
    -   [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html).
    -   De OpenSSH client van [Cygwin](https://www.cygwin.com/).
-   Linux: ssh is standaard geïnstalleerd. Als dat niet zo is,
    installeer dan het pakket openssh-client. Installeer
    [Mosh](https://mosh.org/) (mobile Shell) bij niet-stabiele
    verbindingen.
-   OS X: ssh is standaard geïnstalleerd op de Mac. Voor
    grafische/X11-functionaliteit kan men
    [XQuartz](http://xquartz.macosforge.org) installeren. Installeer
    [Mosh](https://mosh.org/) (mobile Shell) bij niet-stabiele
    verbindingen.
-   Android: [JuiceSSH](https://juicessh.com/) of
    [ConnectBot](https://connectbot.org/). Installeer
    [Mosh](https://mosh.org/) (mobile Shell) bij niet-stabiele
    verbindingen.

### Aangeraden file transfer clients

-   Windows: [MobaXterm](http://mobaxterm.mobatek.net/) of
    [WinSCP](https://winscp.net)
-   Linux: scp
-   OS X en Windows: [Cyberduck](https://cyberduck.io/)

### SSH keys

`ssh` kan authenticatie doen via login en wachtwoord, maar je kunt als alternatief ook autheticeren met ssh keys. Ssh keys zijn openbaar/geheim *sleutel paren* die je kunt genereren met het `ssh-keygen` commando onder linux. Lees de manpage `man ssh-keygen` om meer hierover te leten. Je kunt een wachtwoord zetten om de private key te versleutelen, zodat niemand de sleutel kan kopieren en jou daarmee *nadoen*.

Stappen:

- genereer ssh keys: bijvoorbeeld `ssh-keygen -t ed25519` (vaak enter drukken, of lees goed en voer een wachtwoord in als je dat wilt. Het sleutelpaar wordt in `~/.ssh/` gezet op linux machines.
- kopieer de publieke sleutel naar de server (gebruik `ssh-copy-id <server>`), De publieke sleutel wordt toegevoegd aan `~/.ssh/authorized_keys` (let op de rechten op deze file en directory)

Als je een wachtwoord gezet hebt op je sleutels krijg je telkens wanneer de ze gebruikt een vraag om je wachtwoord te geven, tenzij je `ssh-agent` hebt ingericht om de sleutel in geheugen te houden voor een bepaalde tijd.
Bij ssh vanuit scripts kun je geen wachtwoord op je sleutels zetten, want dan kun je geen wachtwoord intypen.

Het is mogelijk om sleutels met verschillende namen te genereren, zodat je die voor verschillende toepassingen kunt gebruiken.

### SSH tips en instellingen

Om waarschuwingen te voorkomen over mogelijk veranderde ssh keys en ook
meldingen als ‘unknown host’ als je een ssh-verbinding met een host in
het science.ru.nl domein maakt, hebben we de publieke sleutel van al
onze servers getekend. Als je de volgende regels toevoegt aan het
bestand ‘config’ in de .ssh directory in je (lokale) home directory (het
kan zijn dat dit file nog niet bestaat)

~~~
CanonicalDomains science.ru.nl
CanonicalizeFallbackLocal no
CanonicalizeHostname yes
~~~

en de volgende regel aan .ssh/known\_hosts

~~~
@cert-authority *.science.ru.nl ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHpJveyOrLKFRDsbiW/29OadbCbkmUaIXnWbhVwtytbpftAc7Stj2RYa8yBmgfdm82T/UBVu1tLbeeCYQI8UlCvbAALMx+I60ux+iEGVdDBgIOjeu6LuY12pksVlXy/nKc59+m3AdMXfGHA8cI/O8eFosQLJ+dck7SBcvTT4lPhEcSQxg==
~~~

dan zullen door C&CZ getekende ssh keys van science.ru.nl hosts
automatisch geaccepteerd worden. De wijziging van het ‘config’ bestand
zorgt ervoor dat

`ssh lilo8`

automatisch overeenkomt met lilo8.science.ru.nl (dus korte hostnaam
volstaat) en de regel in known\_hosts zorgt ervoor dat ssh alleen voor
hostnamen die overeenkomen met \*.science.ru.nl zal controleren of de
publieke sleutel van de host getekend is door C&CZ en als dat inderdaad
het geval is de sleutel zonder meer accepteren. Als je het config
bestand niet wilt wijzigen, dan moet je altijd de volledige hostnaam
gebruiken lilo5.science.ru.nl want anders komt de regel in known\_hosts
niet overeen.

Ssh kan gebruikt worden voor

-   port forwarding op een andere host
-   proxying van bv web verkeer
-   bijna volledige VPN functionaleit

Zie [dit
artikel](http://blogs.perl.org/users/smylers/2011/08/ssh-productivity-tips.html)
voor enkele geweldige tips hoe je de ssh client kunt gebruiken en
instellen.

### Verbindingsproblemen voorkomen

Als je ervaart dat de ssh-verbinding zo nu en dan niet meer reageert,
gebruik dan de volgende ssh-client instellingen. Zet onderstaande regels
in het configuratiebestand .ssh/config (of /etc/ssh/ssh\_config):

~~~
TCPKeepAlive no
ServerAliveInterval 60
ServerAliveCountMax 10
~~~
