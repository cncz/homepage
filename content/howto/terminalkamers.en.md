---
author: bram
date: '2022-09-19T16:12:50Z'
keywords: []
lang: en
tags:
- hardware
- onderwijs
- studenten
- medewerkers
aliases:
- terminalrooms
title: Terminal-rooms
wiki_id: '52'
---
C&CZ manages a large number of machines that anybody with an
[Science-account](/en/howto/login/) can use. Students can also
login to the PC’s in the computer labs, the study area and the library
with their S-number and RU-password, in the RU-domain on MS-Windows. All
computer labs are dual boot: during startup of the PC one can choose to
boot Linux (Ubuntu) or Windows.

### Available computer labs

The Science Faculty provides the following computer labs:

-   TK023, (HG00.023)\
    41 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (AIO, Core i5-12500,
    3.0GHz, 16GB, SSD, 24" widescreen LCD monitor, sound), dual boot with
    Windows and Linux (Ubuntu).\
    The teacher-PC is connected to a beamer, which makes it possible to
    project the computer screen on the wall.
-   TK029, (HG00.029)\
    61 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (AIO, Core i5-12500,
    3.0GHz, 16GB, SSD, 24" widescreen LCD monitor, sound), dual boot with
    Windows and Linux (Ubuntu).\
    The teacher-PC is connected to a beamer, which makes it possible to
    project the computer screen on the wall.
-   TK075, (HG00.075)\
    65 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (AIO, Core i5-12500,
    3.0GHz, 16GB, SSD, 24" widescreen LCD monitor, sound), dual boot with
    Windows and Linux (Ubuntu).\
    A PC is connected to a beamer, which makes it possible to project
    the computer screen on the wall.
-   TK206, (HG00.206)\
    37 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (AIO, Core i5-12500,
    3.0GHz, 16GB, SSD, 24" widescreen LCD monitor, sound), dual boot with
    Windows and Linux (Ubuntu).\
    A PC is connected to a beamer, which makes it possible to project
    the computer screen on the wall.
-   TK625, (HG00.625)\
    43 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (AIO, Core i5-12500,
    3.0GHz, 16GB, SSD, 24" widescreen LCD monitor, sound), dual boot with
    Windows and Linux (Ubuntu).\
    A PC is connected to a beamer, which makes it possible to project
    the computer screen on the wall.

### Study Area & Library of Science

Most of the computers at the Study Area
(HG00.201) and the Library of Science (HG00.011) are Windows-only and
managed by the [ILS](/en/howto/contact/#ils-helpdesk).
That makes all Windows computers in all RU Study Areas and Library locations
uniform.
General information about the Library of Science (such as opening hours
and reservation of a study place) can be found at the [Library of Science website](https://www.ru.nl/library/library/library-locations/library-science/).
In addition to these Windows-only computers, eight Linux-only computers,
managed by C&CZ, are available in the front of the Study area
(HG00.201).

### Laptops

The faculty has a [laptop pool](/en/howto/laptop-pool/):

-   Library of Science, (HG00.11) 60 [Managed PC’s](/en/howto/windows-beheerde-werkplek/) (HP EliteBook 850 G6 -
    Core i7-8665U, 16GB, 512B SSD, 15.6" Full HD) with Windows 10.

### Teacher PC’s with video projector in college rooms

In the following college rooms a teacher-PC is present, connected to a
video projector.

-   HG00.062
-   HG00.065
-   HG00.068
-   HG00.071
-   HG00.086
-   HG00.303
-   HG00.304
-   HG00.307
-   HG00.308
-   HG00.310

### Reservation

Just like college rooms or colloquium rooms, computer labs can be
reserved for courses. One should consider the follwing:

-   Reservation of the computer lab.

To make a reservation, get in contact with *Zaalreservering*, department
HL (Housing & Logistics), room HG00.040,
{{< figure src="/img/old/telephone.gif" title="telefoon" >}} 52010.
N.B.: This is only for the reservation of the computer lab.

-   Admittance to computer labs is arranged by an [admittance procedure](/en/howto/medewerkers-sleutel/).
-   All students in a course must have an
    [Account](/en/howto/login/).
-   Installed software. Besides MS Windows with MS Office or Linux with
    Open Office, a lot of extra software is always available on the PC’s
    in the computer labs. See: [Windows
    applications](/en/howto/windows-beheerde-werkplek/), [Linux
    applications](/en/howto/unix/).
-   Additional software for courses **must** be installed by the system
    management of C&CZ.\
    It is **not** possible to get Administrative- or Power User
    privileges for installing additional software or running problematic
    programs.
-   Especially new software for the computer labs should be requested at
    the **earliest** possible moment (at least two weeks in advance).\
    This is needed because system management must figure out if the
    software can run using ordinary user privileges, to package the
    software and finally to distribute the package to the PC’s in the
    requested computer lab(s). It has happened that certain software
    could not be used because of security and maintainability reasons,
    an alternative software package had to be found, packaged, installed
    and used during the course.

If a computer lab is not reserved for a course, anybody with an
[account](/en/howto/login/) can use any of the PC’s available.

### Behaviour in the labs

It is not allowed to smoke, eat or drink in the computer labs, this is
to protect the equipment. Do not cause annoyances to other users, like
disturbing sounds, long term screen lock, …). Also there is a priority
to use a PC: highest priority has the course that has reserved the
computer lab, next are employees or students working on courses, essays
and reports.

### Advertising

The login screens of the computer lab PCs accommodate 7 advertisements
of 400x280 pixels, next to the official announcements. New
advertisements can be submitted to inlogschermen\@science.ru.nl. The
current advertisements can be viewed [here](/en/howto/loginschermen/) as
well.

### Opening hours

An [opening/closing procedure](/en/howto/sleutelprocedure-terminalkamers/) is available.
