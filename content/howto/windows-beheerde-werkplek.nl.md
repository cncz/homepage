---
author: bram
date: '2022-09-19T16:32:11Z'
keywords: []
lang: nl
tags:
- hardware
- software
- contactpersonen
title: Windows beheerde werkplek
wiki_id: '34'
---
## Algemeen

De ‘Beheerde Werkplek’ (BW-PC) is een standaard Windows machine die
voorzien wordt van standaard software. De installatie van de PC verloopt
volledig geautomatiseerd zonder dat een gebruiker iets moet invoeren.
Wanneer een dergelijke BW-PC wordt ingericht heeft een medewerker van
C&CZ circa 5 minuten nodig om de installatie te starten en initiële
gegevens in te voeren. Daarna verloopt de rest van de Windows en Office
installatie automatisch, dat is na ongeveer 1 uur gereed. Na een reboot
wordt vervolgens automatisch de rest van de software geïnstalleerd. Wat
die rest van de software precies is, hangt o.a. af van de groep waar de
PC deel van uitmaakt. Zie onder voor een lijst.

## Mogelijkheden

Omdat binnen FNWI de behoeften van medewerkers en afdelingen zeer divers
zijn, zijn er ook diverse typen BW-PCs:

### Volledig beheerde werkplek

Hierbij gaat het om Windows machines welke volledig beheerd worden door
C&CZ. Zowel het besturingssysteem, software, updates en instellingen
worden centraal geregeld. De gebruiker heeft hier geen omkijken naar.
Een gebruiker van een dergelijke PC krijgt *User*- en geen
*Administrator*- of *Power User*-privileges. Het is dan (meestal - er
zijn uitzonderingen) niet mogelijk om zelf software te installeren of te
verwijderen, drivers te updaten of systeem instellingen te wijzigen. In
principe worden alle bestanden en instellingen van gebruikers opgeslagen
op een netwerkschijf die regelmatig gebackupped wordt. De harddisk in de
PC wordt niet gebruikt voor opslag van gebruikersdata maar slechts voor
het besturingssysteem en de standaard software.\
Mochten er problemen met een PC optreden dan kan men in dit geval
gemakkelijk uitwijken naar een andere BW-PC.

Voor deze machines kan C&CZ goede garanties geven voor het functioneren
van het besturingssysteem en geïnstalleerde software.

Problemen met het besturingssysteem of software zal C&CZ trachten op te
lossen. Ook is C&CZ in staat om op dergelijke PC’s op afstand hulp
(*Remote Assistance*) te bieden via de helpdesk of systeembeheer. In
geval dat deze problemen niet binnen een acceptabele tijd op te lossen
zijn kan de machine in ca. 1 tot 2 uur geherinstalleerd worden.

### Afdelingscontactpersoon beheerde werkplek

Ook hier gaat het om standaard Windows machines die beheerd worden door
C&CZ. Het besturingssysteem, software, updates en instellingen worden
centraal geregeld. De gebruiker heeft hier geen omkijken naar. Een
gebruiker op dit soort PC’s krijgt *User*- en geen *Administator*- of
*Power User* privileges. Het is derhalve (meestal - er zijn
uitzonderingen) niet mogelijk om zelf software te installeren of te
verwijderen, drivers te updaten of systeem instellingen te wijzigen. En
ook hier worden in principe alle bestanden en instellingen van
gebruikers opgeslagen op een netwerkschijf die regelmatig gebackupped
wordt.\
Omdat binnen de afdeling echter behoefte bestaat aan meer flexibiliteit,
eigen software, vreemde hardware of andere systeem instellingen krijgt
de afdelingscontactpersoon een extra account op de PC met
*Administrator* privileges zodat hij software kan installeren of
wijzigingen aan de systemen kan uitvoeren ten behoeve van medewerkers
binnen zijn afdeling.

Voor deze machines kan C&CZ goede garanties geven voor het functioneren
van het besturingssysteem en geïnstalleerde software.\
C&CZ zal in dit geval afspraken maken met de afdelingscontactpersoon.
Het is daarbij uitdrukkelijk niet de bedoeling om de eindgebruikers
*Administrator* of *Power User* privileges te geven. Ook moet de
afdelingscontactpersoon bereid zijn om vragen over besturingssysteem en
software van collega’s te beantwoorden. In geval van problemen wordt
verwacht dat hij of zij eerst kijkt of het probleem lokaal is op te
lossen alvorens C&CZ in te schakelen. Tenslotte kan de
afdelingscontactpersoon bij C&CZ vragen om afdelingseigen- of licentie
software te *packagen* en te installeren op bepaalde BW-PC’s binnen de
afdeling.

Problemen met het besturingssysteem of software zal C&CZ trachten op te
lossen. Ook is C&CZ in staat om op dergelijke PC’s op afstand hulp
(*Remote Assistence*) te bieden via de helpdesk of systeembeheer.\
In tegenstelling tot de bovenstaande Volledig beheerde werkplek kan op
dit soort machines wel belangrijke data op de interne harddisk terecht
komen. In dat geval is het niet vanzelfsprekend dat de machine
geherinstalleerd mag worden en wil men liever zelf de problemen oplossen
of de data redden. De hulp die C&CZ hierbij kan bieden zal beperkt
zijn.\
In geval dat deze problemen niet binnen een acceptabele tijd op te
lossen zijn kan de machine in ca. 1 tot 2 uur geherinstalleerd worden.
Na de standaard installatie moet de afdelingscontactpersoon de
installatie van eigen software, aparte hardware of systeeminstellingen
afronden.

### Gebruikers beheerde werkplek

Ook hier gaat het om een standaard Windows machine welke mede beheerd
wordt door C&CZ. Het besturingssysteem, software, updates en
instellingen worden in principe centraal geregeld. De gebruiker heeft
ook hier normaal gesproken geen omkijken naar. Omdat de gebruiker echter
behoefte heeft aan meer flexibiliteit, eigen software, vreemde hardware
of andere systeem instellingen krijgt hij een extra account op de PC met
*Administrator* privileges zodat hij software kan installeren en/of
wijzigingen aan het systeem kan uitvoeren.\
C&CZ raadt aan om uw normale werkzaamheden op een dergelijke PC te
verrichten als een normale gebruiker met *User* privileges en het
account met de *Administrator* privileges uitsluitend te gebruiken voor
software installatie of het aanpassen van systeem instellingen. Ook
raden we aan om netwerkschijven te gebruiken voor het opslaan van
bestanden en instellingen zodat er regelmatig bestanden gebackupped
worden.\
De verleiding is echter vaak groot om toch het lokale administrator
account te gebruiken, de risico’s voor de PC en bestanden zijn in dat
geval ook groter.

Voor deze machines kan C&CZ geen garanties geven voor het functioneren
van het besturingssysteem en geïnstalleerde software. De gebruiker kan
namelijk allerlei acties uitvoeren, met het lokale administrator
account, die het functioneren van de PC beinvloeden.

Problemen met het besturingssysteem of software zal C&CZ trachten op te
lossen. De hoeveelheid tijd die C&CZ aan problemen bij dit soort PC’s
kan besteden is uiteraard beperkt. Ook is C&CZ meestal in staat om op
dergelijke PC’s op afstand hulp (*Remote Assistence*) te bieden via de
helpdesk of systeembeheer.\
Juist in tegenstelling tot de bovenstaande Volledig beheerde werkplek
kan op dit soort machines wel data op de interne harddisk terecht komen.
In dat geval is het niet vanzelfsprekend dat de machine geherinstalleerd
mag worden en wil men liever zelf de problemen oplossen of de data
redden.\
In geval dat deze problemen niet binnen een acceptabele tijd op te
lossen zijn kan de machine in ca. 1 tot 2 uur geherinstalleerd worden.
Na de standaard installatie moet de gebruiker de installatie van eigen
software, aparte hardware of systeeminstellingen afronden.

### Geïnstalleerde werkplek

Voorlopige oplossing:\
Men kan een standaard Windows machine op identieke wijze vanuit het
netwerk installeren. Na afloop van de installatie moet het
*Administrator* wachtwoord van de PC gewijzigd worden in een wachtwoord
dat de lokale gebruiker kent. Er wordt een kleine, initiële set aan
applicaties toegevoegd. Deze installatie zal op non standaard PC’s
mogelijk niet alle drivers voor de hardware installeren, evenals
software die de leverancier met de PC heeft meegeleverd/had
voorgeïnstalleerd. Het betreft in dit geval een zogenaamde unattended
installatie. Men kan geen keuzes maken hoe Windows te installeren (bv.
partitioneren - de harde schijf wordt volledig gebruikt als **`C:\`**
schijf en geformateeerd) of welke onderdelen van Windows (bv. Outlook
Express) al dan niet worden toegevoegd. De lokale gebruiker dient
naderhand zelf ontbrekende drivers en extra software te installeren.\
Geavanceerde bijzonderheden:\
Eventueel kan men vervolgens de PC uit het domein trekken (svp ons
meedelen als u dat doet, dan kunnen wij Active Directory aanpassen). Ook
is het mogelijk om de PC aan de *unmanaged* OU toe te voegen zodat de PC
geen *policies* uit het domein krijgt.

## Hardware eisen

De minimale eisen voor een nieuwe installatie van een Windows BW-PC
zijn:

-   Eigendom van de Radboud Universiteit (i.v.m. softwarelicenties)
-   Niet ouder dan 5 a 6 jaar
-   Een netwerkkaart of onboard netwerk interface met PXE
-   Processor 64-bit
-   8 GB geheugen
-   Windows10: 500 GB harddisk
-   Geen additionele insteekkaarten voor netwerk, video, audio, etc.

De standaard RU-PC en standaard RU laptop die aan de bovenstaande eisen
voldoet, is geschikt.

## Randapparatuur

Het aansluiten van randapparatuur op een BW-PC kan op problemen stuiten.
Vooral als de nieuwe hardware vereist dat er bepaalde software en/of
drivers geïnstalleerd moeten worden, heeft men als normale gebruiker
onvoldoende rechten om dit te doen. In dit geval zou men kunnen
uitwijken naar de *Afdelingscontactpersoon beheerde werkplek* of een
*Gebruikers beheerde werkplek* (met de bijbehoordende consequenties).

## Software

Er is een [lijst van Windows-software](/nl/howto/microsoft-windows/) die
geïnstalleerd is of kan worden op door C&CZ beheerde PC’s.
