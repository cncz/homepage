---
author: miek
date: '2023-07-05T15:12:45Z'
keywords: [ zfs ]
lang: en
title: Snapshots with ZFS
tags:
- storage
cover:
  image: img/2023/storage.jpg
---

Network shares with [ZFS](https://openzfs.org/wiki/Main_Page) as file system have the ability (which
C&CZ switches on by default) to make *snapshots*.

A *snapshot* is a read-only copy of the share, made at a specific point in time. Such
snapshots allow you to find previous versions of your files. On most shares, snapshots go back
45 days. After that period they are removed.

{{< notice tip >}}
If you remove a file, you still have the ability to retrieve - up to 45 days - different versions.
After that time period the file is _really_ gone from the share.

A snapshot is *not* an alternative folder to store your data.
{{< /notice >}}

In the sections below you can read how to access ZFS snapshots in Linux and Windows.

## Linux

In your share (or home directory) you'll see a `.zfs` with a `snapshot` directory. Listing those
contents:

~~~ txt
# ls -l .zfs/snapshot/
total 22
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230630_043047_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230630_203047_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230701_043046_000
drwxr-xr-x 2 root root 4 Apr 17 12:59 cncz_20230701_163047_000
~~~

Each directory here has a prefix 'cncz_' and then the date and time of when the snapshot was made.
Within each of these directories you find a copy of your share as it was at the date and time.

## Windows

When right clicking on a folder and then selecting "Properties", you'll see the following:

{{< figure src="/img/2023/zfs-snapshot-1.png" >}}

Then click on the tab "Previous Versions" and it shows an overview of the (snapshots) version of
the folder:

{{< figure src="/img/2023/zfs-snapshot-2.png" >}}

You can then browse the older versions or restore them.
