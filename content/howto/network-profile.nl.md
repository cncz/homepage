---
author: petervc
date: '2015-11-25T14:12:31Z'
keywords: []
lang: nl
tags:
- studenten
title: Network profile
wiki_id: '630'
---
# Handleiding voor de BW-PC voor studenten

## Hoe log ik sneller in en raak ik mijn data niet kwijt?

### Inleiding

Regelmatig klagen studenten erover dat het in- en uitloggen op de PC’s
in de PC-cursuszalen zo lang duurt. Deze handleiding legt uit hoe dat
komt en wat je eraan kunt doen.

### Hoe werken de C&CZ: Profiel en Home-schijf (H:\\)?

Om op alle verschillende PC’s je eigen gebruikersinstellingen (je
“profiel”) te kunnen hebben, gebruikt C&CZ drie centrale computers, de
zogenaamde “Domain Controllers”. Je profiel bestaat uit allerlei
persoonsgebonden dingen zoals je bureaublad-achtergrond, snelkoppelingen
op je desktop en allerlei gebruikers-specifieke instellingen van
programma’s (b.v.: laatst geopende documenten in Word, laatst getypte
commando’s in matlab, etc.). Je kunt het vinden in "C:’‘/Documents and
Settings/`<inlognaam>`/". Dit hele profiel wordt bij het inloggen
vanaf de Domain Controller naar je lokale computer gekopieerd omdat
Windows deze instellingen vaak snel nodig heeft, om je programma’s zo
snel mogelijk te laten lopen. Als je uitlogt, worden alle wijzigingen
weer teruggekopiëerd naar de Domain Controller, zodat alles ook weer
klopt als je op een andere PC inlogt. Naast je profielruimte (30 MB) heb
je ook nog een Home-disk (Home-schijf, H:\\, H-schijf, ’Mijn
Documenten’) voor je grote bestanden, verslagen, plaatjes, artikelen,
etc. Deze is meestal 200 MB of meer groot. Je home-disk wordt \*niet\*
gekopiëerd bij het in- en uitloggen, maar staat op een centrale computer
bij C&CZ en is via het netwerk bereikbaar als (H:\\ ,
‘\\\\manus\\\<inlognaam\>’ of ‘\\\\pluri\\\<inlognaam\>’).

### Waarom gaat in- en uitloggen zo traag?

Hoe groter je profiel is, hoe meer er heen en weer gekopieerd moet
worden, en hoe langer deze stap van het in- en uitloggen dus duurt. Je
bureaublad valt ook onder je profiel, dus veel op je bureaublad betekent
langzaam inloggen. Omdat veel mensen veel op hun bureaublad hebben staan
(vaak MSN-setup, verslagen, opdrachten, etc.) moeten de Domain
Controllers veel te veel kopiëren en raken de Domain Controllers
overbelast. Dit verklaart ook gelijk waarom ProQuota je waarschuwt als
je veel op je desktop hebt staan: ProQuota is bedoeld om iedereen
gelukkig te maken door de profielen klein, en dus het inloggen snel te
houden.

### Hoe maak ik het sneller?

De Domain Controllers zijn eigenlijk bedoeld om een paar-honderd
kilobyte aan snelkoppelingen en bestandsverwijzinkjes te kopiëren, geen
tientallen megabytes aan bestanden. Zorg dus dat je op je desktop niets
anders dan snelkoppelingen staan, en maak af en toe je profiel schoon.

Een stappenplan om je profiel in te krimpen:

1.  Verplaats alle bestanden, mappen, verslagen, artikelen, PDF’jes,
    installaties, etc. van je desktop naar je H:\\ schijf (Mijn
    Documenten kan ook, dat is hetzelfde)

1.  Schoon je profiel op. Een aantal directories worden al uitgesloten
    (hoef je dus niet op te schonen):

:\* C:\\Documents and Settings\\`<loginnaam>`\\Local Settings

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Sun

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Macromedia

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Allen Institute

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\OriginLab\\Origin75E/User Files/OCTemp

-   Deze tellen ook niet mee voor ProQuota.

    -   Browser Cache:
        -   Internet Explorer: Extra -\> Internet-opties -\> Tab: General -\> “Bestanden wissen”\
            Klik “Instellingen” (naast ‘bestanden wissen’) en zet de
            schuif op 3 MB
        -   Firefox: Menu Extra -\> Opties -\> Tab:
            Geavanceerd -\> Tab: Netwerk
            -\> “Nu wissen”\
            Buffergrootte 3 MB

:\* Ruim de tijdelijke bestanden van Acrobat Reader op, verwijder

C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Adobe

Dit zou al een heleboel moeten schelen. Het wordt niet aangeraden om
zomaar dingen weg te halen, niet alle programma’s kunnen er even goed
tegen als je hun tijdelijke bestanden verwijdert.\
(Laat met name C:\\Documents and
Settings\\`<loginnaam>`\\Application Data\\Microsoft met rust!)

Log NETJES uit. Het uitloggen kan langer duren, omdat je profiel veel
gewijzigd is, laat het rustig gebeuren want juist op dit moment wordt je
profiel op de Domain Controllers opgeschoond, zodat er de volgende keer
weinig gekopiëerd wordt.
