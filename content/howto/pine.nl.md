---
author: petervc
date: '2014-11-14T09:31:06Z'
keywords: []
lang: nl
tags:
- email
title: Pine
wiki_id: '507'
---
### Pine

Gebruik ‘S’ voor SETUP en daarna ‘C’ voor Config en vul in:

      smtp-server    = smtp.science.ru.nl/user=...
      inbox-path     = {post.science.ru.nl/ssl/user=...}INBOX

waarbij … de loginnaam is.

Om de andere IMAP folders te zien, gebruik ‘S’ voor SETUP en daarna ‘C’
voor Config en pas aan:

      [ Folder Preferences ]
                [X]  combined-folder-display
                [X]  expanded-view-of-folders
                [X]  quell-empty-directories

Ook nog binnen SETUP: kies ‘L’ voor collectionLists en maak met ‘A’ twee
nieuwe collection lists:

      Nickname  : Personal folders
      Server    : post.science.ru.nl/ssl/user=...
      Path      : INBOX.
      View      :

 (afsluiten met control-X)
