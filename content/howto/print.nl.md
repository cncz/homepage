---
author: wim
date: '2021-05-10T19:13:27Z'
keywords: []
lang: nl
title: Printen
aliases:
- copiers
- kamerbreed
- km
- peage
- printbudget
- printers-en-printen
ShowToc: true
TocOpen: true
wiki_id: '47'
---
# Printen
> Je kan overal op de campus printen, scannen en kopiëren - onafhankelijk van de ruimte of het gebouw waar je studeert of werkt. Je stuurt je printopdracht niet naar een bepaalde printer, maar zet je de printopdracht klaar in een wachtrij. Daarna kun je je printopdracht bij iedere printer ophalen. [Lees meer...](https://www.ru.nl/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/printen-kopieren-en-scannen/printen)

## Afdrukken vanaf eigen laptop
Mocht printen op zelf beheerde apparaten niet lukken, dan is een alternatief het uploaden van documenten naar <https://peage.ru.nl>. Andere mogelijkheden worden [hier](../printing-workarounds) beschreven.

## Inbinden, snijden, nieten en boekjes
In de [Libary of Science](http://www.ru.nl/ubn/bibliotheek/locaties/library-of-science/) kan
men smeltmapjes van diverse diktes kopen om werkstukken e.d. in te
binden in het lamineerapparaat. Ook is daar een snijmachine, een
23-gaatjes perforator en diverse nietmachines. Alle Konica Minolta
printers kunnen automatisch nieten.

In de printer bij HG00.002 zitten een gaatjesmaker en een rug-nieter die kan
vouwen. Hiermee is het mogelijk om complete boekjes te produceren. Er
kunnen 2 of 4 gaatjes worden geponst. Maximaal 20 vel kan in het midden
(op de rug) geniet worden en gevouwen, waardoor van A3-vellen een
A4-boekje gemaakt kan worden, of van A4-vellen een A5-boekje.

Bij C&CZ is ook een snijmachine, een 23-gaatjes perforator en een
grote nietmachine aanwezig.

# Posters printen
Als je posters wilt laten drukken, bieden we een reeks materialen om uit te kiezen:

| Materiaal  | Beschrijving                                                                                                                                     | Maximale breedte | A0 kosten |
|------------|--------------------------------------------------------------------------------------------------------------------------------------------------|-----------------:|----------:|
| Fotopapier | HP Everyday Pigment Ink Satin Photo                                                                                                              |            91 cm |   € 30,00 |
| Doek       | Canvasafdrukken vertonen niet zo snel scheuren als papieren afdrukken, kunnen zelfs zorgvuldig worden opgevouwen en meegenomen in de handbagage. |            91 cm |   € 40,00 |


{{< notice info >}}
Om het proces soepel te laten verlopen, vragen we je de printopdracht in pdf-formaat aan te leveren via e-mail aan <helpdesk@science.ru.nl>. Geef bij het indienen van de printopdracht het voorkeursmateriaal en de kostenplaats/verbijzondering door.
{{< /notice >}}

# 3D-printen
C&CZ heeft een 3D-printservice voor FNWI-medewerkers en -studenten.

De kosten voor deze service bedragen € 0,15 per gram.

Als je geïnteresseerd bent in, of vragen hebt over het gebruik van onze 3D-printservice, [neem contact met ons op](../contact).

{{< rawhtml >}}
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height=1800> 
{{< /rawhtml >}}
