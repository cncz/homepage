---
author: sioo
date: '2023-05-08T10:55:31Z'
keywords: []
lang: en
tags:
- hardware
title: Hardware servers
wiki_id: '51'
aliases:
- lilo
---
## Servers and Workstations

C&CZ manages ca. 900 Linux workstations and servers, and ca. 900 servers
and workstations with MS-Windows, primarily in the domain
`science.ru.nl`. Dual boot Windows/Linux machines are counted twice. The
ca. 350 servers are distributed over three datacenters, ca. 150 have a
fast network connection (25 Gb/s).

### Workstations

C&CZ manages (for departments) workstations with Ubuntu Linux or
MS-Windows. There are also workstations in the PC-rooms for general use
by anybody with a [Science login](/en/howto/login/), but also
by all RU employees and students and guests with a temporary login.
Almost all those machines are dual boot, the user can choose to boot
Ubuntu Linux or MS-Windows.

### Linux login servers

A few [secure shell](/en/howto/ssh/) servers are available for Students
and Staff of the Faculty (everyone with a [Science
account](/en/howto/login/)). You can use it just as you like, *as long
as you do not cause problems for other users*.

The primary login server can be reached with the following address:

`lilo.science.ru.nl`

At the moment, lilo points to a machine called lilo8 (Ubuntu 22.04).

The ssh fingerprints for lilo7 and lilo8 are:

` 256  SHA256:si3g2elo5m6TShx3PjX0+vF50pZ8NK/iXz/ESB+ZeP0  lilo7 (ECDSA)`\
` 256  SHA256:wHypnsKnhOMcagDli8xSxp2AnRYXAIfs+EVNB1/LdiA  lilo7 (ED25519)`

` 256  SHA256:yVihq2mnLULOCBFdvO2RWBps33hdE6PmaIGeBXX44wY  lilo8 (ECDSA)`\
` 256  SHA256:gsvoEP5djMC5GsXJJUYktYq3TjRtNQPKo0/psULHBYc  lilo8 (ED25519)`\
`3072  SHA256:gFrExKYRunrQQAkZit5mlLQrRXPaO3vU3GegM0/rP+o  lilo8 (RSA)`

You can test this with `ssh-keyscan host | ssh-keygen -lf -`.

To avoid the following ssh warning everytime we replace hardware or
change the alias for lilo or stitch

`@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`\
`@       WARNING: POSSIBLE DNS SPOOFING DETECTED!          @`\
`@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@`\
`The ECDSA host key for lilo.science.ru.nl has changed`

you can insert the following line into the file .ssh/known\_hosts

`@cert-authority *.science.ru.nl ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHpJveyOrLKFRDsbiW/29OadbCbkmUaIXnWbhVwtytbpftAc7Stj2RYa8yBmgfdm82T/UBVu1tLbeeCYQI8UlCvbAALMx+I60ux+iEGVdDBgIOjeu6LuY12pksVlXy/nKc59+m3AdMXfGHA8cI/O8eFosQLJ+dck7SBcvTT4lPhEcSQxg==`

### Compute servers/clusters

C&CZ manages a number of compute clusters, with compute nodes that
departments have purchased. In a few cases, departments have created a
separate cluster by also purchasing their own head node (master). The
largest `cn`-cluster also has a number of nodes purchased by C&CZ, so
FNWI departments may use these after approval by C&CZ. For departments
of FNWI the housing and management of clusternodes is free of charge.
The `cn`-cluster is heterogeneous. All nodes have
[SLURM](/en/howto/slurm/) as cluster software. As of February 2022, the
cn-cluster totalled 123 nodes with 4700 cores and 30 TB RAM. Several
clusternodes have a GPU with CUDA cores.

The Astrophysics coma cluster with 1048 cores, 8780 GB RAM, 600 TB
fileserver storage, GPU’s is a separate cluster. A separate cluster is
also managed for the RU department [Centre for Language
Studies](http://www.ru.nl/cls/).

Sometimes outside of opening hours also the pcs in the [computer
labs](/en/howto/sleutelprocedure-terminalkamers/) are used as a compute
cluster.

A [primitive overview of
clusternodes](https://sysadmin.science.ru.nl/clusternodes/) is available, until
we can fix something better to replace the old cricket overview.

### Mail

On [DIY](http://diy.science.ru.nl/), you can see your mail addresses, mail aliases as well as your
forwarding settings. For information on how to use the email service and optionally, conffigure
your email software, cunsult our [Email howto](../email).

### WWW servers

There are several WWW-servers with [websites](/en/howto/website/) and web
applications. A description of the [Apache webserver
setup](/en/howto/apachesetup/) is available.

### GitLab  servers

The [GitLab](/en/howto/gitlab/) service provides a source code
collaboration platform, git repository manager, issue tracker and code
reviewer.

The ssh fingerprints for gitlab.science.ru.nl:

` 256 SHA256:BSSXi19WgCSpZu5AQkUTtwm+5zAMioQJpFE3oxBrIMQ root@gitlab (ECDSA)`\
` 256 SHA256:7G/4WD/tUE8H0k9MkBznlcx+ZWgUxnn3KazKm1XQwRk root@gitlab (ED25519)`\
`2048 SHA256:NgZ9HciXlwG5JNPSFbem7bBbDbhqaNAO7JHag8rwi/I root@gitlab (RSA)`

gitlab.pep.cs.ru.nl fingerprints:

` 256 SHA256:pBFiBobAQTozB0xeeMCp3Du2PFtX7YIXw6XRFJSAGH0 root@gitlab (ECDSA)`\
` 256 SHA256:iWfwGvGZ/hdFImBzzG5Bu/y5gqhUkLOdSTYmzl1+k2w root@gitlab (ED25519)`\
`2048 SHA256:lWWFUfhwODG0bBuQJUR8ymlVfqpNHrXk0xbFAoDpTIg root@gitlab (RSA)`

### Printer server

One server is used to print jobs. See the [printer
webpage](/en/howto/hardware-printers/) to see which server is advised in
which situation.

### Disk servers

These servers share different types of files, e.g. home-directories,
application-software and disks for departments and groups. All modern
servers only have redundant disks, sometimes by using “mirroring”,
sometimes by use of a “RAID-array”. That means that loss of only 1 disk
will not cause any problem for users.

For more information, e.g. about [renting disc
space](/en/howto/diskruimte/) for departments, see our [disc
space](/en/howto/diskruimte/) page.

### FTP server

To make the transfer of large files to others simple, there is an FTP
service. It can be accessed through [the original FTP
protocol](ftp://ftp.science.ru.nl/pub) as well as through [the web HTTPS
protocol](https://ftp.science.ru.nl). Anyone with a Science-account who
acquired a directory on this server [through
postmaster](/en/howto/contact/), can copy files to that directory in
several ways:

-   by logging in to a login server like `lilo` in
    `/vol/ftp/pub/`*yourdirectory*.
-   by attaching the network drive: `\\ftp.science.ru.nl\ftp`

By using an unreadable upper directory containing a secret directory,
one can even transfer somewhat sensitive data.

### License servers

For several software packages there is a license server, that allows use
of the software on all pc’s, while making sure that the total number of
licenses uses is not more than what has been bought. Most of these
license servers use
[FlexNet](http://www.flexerasoftware.com/producer/products/software-monetization/flexnet-licensing/)
by Flexera.

### Backup servers

Almost all disks that are managed by C&CZ, are being
[backupped](/en/howto/backup/) regularly, in order to be able to restore
data in case of small or large calamities.
