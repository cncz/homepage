---
author: mroesner
date: '2022-08-31T06:38:28Z'
keywords: []
lang: en
tags: []
title: JupyterHub
wiki_id: '1091'
---
[JupyterHub](https://jupyterhub.readthedocs.io) is a multi-user Hub that
spawns, manages, and proxies multiple instances of the single-user
Jupyter notebook server. It can offer notebook servers to a class of
students, a scientific research project, or a high-performance computing
group.

Please [contact C&CZ](/en/howto/contact/) if you plan to use or are
interested in using the **JupyterHub** for your course or project.

## Login

The **JupyterHub** is available via <https://jupyterhub22.science.ru.nl>
and you can login with your **Science** username / password combination.

**Note**: If you are not on campus, you need to use
[VPN](/en/howto/vpn/).

## Assignment Handling

For the creating and grading of assignments within the JupyterHub you
can use the [NBGrader](/en/howto/nbgrader/) extension. It allows
instructors to easily create Jupyter notebook-based assignments that
include both coding exercises and written free-responses. NBGrader also
provides a streamlined interface for quickly grading completed
assignments via an auto-grading functionality.

Please [contact C&CZ](/en/howto/contact/) if you like to use NBGrader for
one of your courses.

## File Exchange

For the distribution of files you can use the
[NBGitPuller](https://jupyterhub.github.io/nbgitpuller/) extension. This
allows to distribute content from a git repository to a JupyterHub user
via a simple link.

### NBGitPuller Usage

#### Creating the NBGitPuller Download Link

To pull the content of a git repository to JupyterHub’s local directory
we first need to create a link via
<https://jupyterhub.github.io/nbgitpuller/link?hub=https://jupyterhub22.science.ru.nl>.
[ADD SCREENSHOT]

We have to fill a few fields to be able to generate the link:

-   JupyterHub URL: <https://jupyterhub22.science.ru.nl/>
-   Git Repository URL: URL of your publicly available git repository
-   File to open: The first file to open; not particularly important

Afterwards, you can copy the generated link and share it among the
students.

#### Using the NBGitPuller Download Link

For students to pull the content of the repository successfully, they
have to execute the following steps in order:

1.  login to the JupyterHub
2.  spawn a new session
3.  click on the link (from above) in the same browser.

Once done successfully in this order, students will be able to pull the
content of the repository to their home directories.

## Available Programming Languages

The default kernel (ipykernel) allows to use Python. Many other
languages such as Julia, C++, or Fortran, in addition to Python, may be
used in the notebooks as well.
[Here](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels) you can
find a list of possible kernels / programming languages. Please [contact
C&CZ](/en/howto/contact/) if you like to use one of them.
