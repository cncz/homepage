---
author: petervc
date: '2022-11-24T17:03:10Z'
keywords: []
noage: true
lang: en
title: Login
aliases:
- account
- gastlogin
- login-aanvragen
- dhz
wiki_id: '163'
ShowToc: true
TocOpen: true
---
# Science login
Every employee or student of the Science faculty receives a personal *Science login*. This allows you to use services such
 as [Eduroam](https://www.ru.nl/en/staff/services/services-and-facilities/ict/wifi), [storage](../storage),
 [computer lab PCs](../terminalkamers), [JupyterHub](../jupyterhub) and [GitLab+Mattermost](../gitlab).

> A Science login usually resembles your name and does not contain periods, capital letters or an at sign (@). For
> example `johndoe`.

{{< notice info >}}

All settings related to your Science login, like setting your password and managing groups, mail aliases, mail forwarding and vacation message can be managed in [DIY](https://diy.science.ru.nl).

{{< /notice  >}}

## Students
After registration, students automatically receive a login based on
the information from [Osiris](https://www.ru.nl/en/students/services/studying/view-study-progress-in-osiris). Once the
Science login is created you will receive an email with further instructions. This email is being sent
to the email address registered in Osiris.

The email address that comes with your Science login ends with `@student.science.ru.nl`. Email sent to that address will be
automatically forwarded to your `@ru.nl` address. This can be adjusted on [DIY](https://diy.science.ru.nl).

{{< notice info >}}

If you want to keep the Science login while you are no longer studying here, C&CZ will only do this on
request from the contact person of your study programme. Inquiries should be directed to the
[Student Information Point (STIP)](https://www.ru.nl/courseguides/science/practical-information/faculty-science-general-information/student-information-point-stip/)
of the Education Centre.

{{< /notice  >}}

## Employees, guests and trainees

New employees, trainees and guests receive a Science login at the request of the contact person of the department. A
Science login usually includes an email address that ends with `@science.ru.nl`. Incoming email can be read from our
[email facility](../email), or can be forwarded via [DIY](https://diy.science.ru.nl) to, for example, the `@ru.nl` address. For interns who
are also students here, it is not necessary to request a Science login.

{{< notice info >}}

Please contact the contact person of your department if you want to keep your Science login while you no longer work
here.

{{< /notice  >}}

### Request a Science login

Only contacts can request Science logins. Mail the following information to [Postmaster](../contact):

|                           |                                                                                                                                                                     |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **login name**            | Suggested login name. For example: `ajakobsen`, `pieterb` or `janjanssen`.                                                                                          |
| **email address**         | Suggeste email address. Usually something like `Initials.Lastname@science.ru.nl`.                                                                                   |
| **full name**             | Full name of the new employee, guest or trainee.                                                                                                                    |
| **U- or S-number**        | In case of an employee or student. With this account you can also log in to [DIY](https://diy.science.ru.nl) to set a (new) password for your Science login.        |
| **requester login name**  | As a requester, you can [manage the Science login](/en/news/2022-11-15_manage-science-accounts-in-dhz) in [DIY](https://diy.science.ru.nl).                         |
| private email address     | Only needed when the U- or S-numer is unknown. The private email address to which an initial password will be sent. This email contains instructions for setting a new password, but no mention of the login name. |

## Check date / end date

For employees with a contract and enrolled students the check/end date is automatically postponed. A month before the check date / end date is reached, the contact will be notified about the expiration of the login. If the contact doesn't extend the check date / end date, the user will be
notified two weeks later. When the check/end date is reached, the login is disabled. A month later the login will be removed.

## Guests
If you want to give one or more guests temporary access to, for example, the
[wireless network](/en/howto/network-wireless/) or to the PCs in the [computer labs](/en/howto/terminalrooms/), then
there are several options:

|                           |                      |                                                                                                                                                                                                                                                                          |
|---------------------------|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| WiFi guest access         | for one day          | Login to <https://portal.ru.nl> and check the instructions for *Wifi Access Guests*                                                                                                                                                                                      |
| Wifi for groups           | for one or more days | Apply for [Eduroam Visitor Access](https://eva.eduroam.nl/inloggen) at the [Library of Science](https://www.ru.nl/library/library/library-locations/library-science/) or at the [C&CZ helpdesk](../contact).                                                             |
| Science login             | one day              | Science logins for today are available at the [Library of Science](https://www.ru.nl/library/library/library-locations/library-science/).                                                                                                                                |
| Science login             | some days            | Please request a personal guest login as [described above](#request-a-science-login).                                                                                                                                                                                    |
| GitLab/Mattermost login   | temporary            | Please request a Science login as [described above](#request-a-science-login) with limited access to [GitLab](../gitlab#external-users)/Mattermost.                                                                                                                      |
| Science logins for groups | some days            | Science logins for groups can be requested by mail at [Postmaster](../contact). State the number of logins and until when they must remain valid. If the PCs in the [computer labs](../terminalkamers) will also be used, please also state which software is required. |
