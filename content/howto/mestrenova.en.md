---
author: lsummere
date: '2021-04-26T15:31:55Z'
keywords: []
lang: en
tags:
- software
title: MestreNova
wiki_id: '1060'
---
Mnova is a multivendor software suite designed for processing data from
NMR, LC-MS, GC-MS, and Electronic & Vibrational Spectroscopic techniques
(UV-VIS, FT-IR etc.).

Mnova is available for installation on Windows, Linux, and MacOS
machines. Employees and students from the Faculty of Science can install
mestreNova under the university site licence on their personal computer.

Currently the faculty holds a site license for MNova 15.
Installation & license files can be found on the [install
share](/en/howto/install-share/) in `\install\science\MestreNova`.

After installing MestreNova open the application and drag the license
files in the workspace. If the computer running the application is
connected to the university network (Eduroam, [VPN](/en/howto/vpn/))
MNova will validate the license for a three month period. When this
license expires it can be renewd by connecting to the university
network.
