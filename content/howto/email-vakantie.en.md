---
author: jverdurm
date: '2008-04-09T22:19:36Z'
keywords: []
lang: en
tags:
- email
title: Email vakantie
wiki_id: '15'
---
## Automatically respond to incoming email

The settings to automatically respond to incoming email can be changed
at the [Do-It-Yourself website](http://dhz.science.ru.nl).

If a (vacation) message is sent for incoming email, each sender of a
mail that is directly sent to you (no BCC:), gets only once a mail with
the text you have specified.

If one does not want to send automatic replies anymore, the (vacation)
message is saved for later use.
