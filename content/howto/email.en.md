---
title: Email
author: bram
date: 2022-10-14
keywords:
  - email
tags:
  - medewerkers
  - studenten
  - email
aliases:
  - email-authsmtp
cover:
  image: img/2023/science-email.png
ShowToc: true
TocOpen: true
---

{{< notice tip >}}

# Webmail

For quick access to your email, use our webmailer:

## https://roundcube.science.ru.nl/

{{< /notice >}}

# Email settings

When setting up your e-mail programs, it is sufficient to only enter your e-mail address. This is what your Science email address looks like:

> `I.Lastname@science.ru.nl`

All technical settings are then automatically filled in for you. If that does not work, or if it is necessary to fill in the details yourself, use the settings in the table below:

## Incoming email

| item         | setting              | description                                                                             |
| ------------ | -------------------- | --------------------------------------------------------------------------------------- |
| mailprotocol | `IMAP`               | With this protocol, your mail is stored on the mail server.                             |
| server name  | `post.science.ru.nl` | The server for reading your email. This is where your mail folders live.                |
| port         | `993`                |                                                                                         |
| encryption   | `SSL/TLS`            | This encryption is used in the communication of your email program and the mail server. |
| user name    | `scienceloginname`   | Use your [Science account](/en/howto/login).                                            |

## Outgoing email

| item       | setting              | description                                  |
| ---------- | -------------------- | -------------------------------------------- |
| servername | `smtp.science.ru.nl` | This server is used for sending email.       |
| port       | `587`                |                                              |
| encryption | `STARTTLS`           |                                              |
| user name  | `scienceloginnaam`   | Use your [Science account](/en/howto/login). |

## Address book

| item               | setting              |
| ------------------ | -------------------- |
| addressbook server | `ldap.science.ru.nl` |
| port               | 389                  |
| basedn             | `DN: o=addressbook`  |

# Science mail features

> Consult the [DHZ page](/en/howto/dhz) for settings regarding your email.

- C&CZ uses open protocols for email. This means that you can check mail with a program of your own preference.
- A simple antivirus- and antispam-filter is applied to every mail message upon arrival.
- Mailboxes can be [shared](#sharing-mailboxes) between users.
- Also [labels](/en/howto/thunderbird-tags/) for mail can be shared with other users.
- Using [Sieve](/en/howto/email-sieve/) mail messages can be filtered on the server.
- There are two options for setting up mailing lists: [address-lists](#address-lists) and [mailman](#mailman-mailing-lists).
  Address-lists containing external addresses are not recommended anymore, because of antispam measures taken at the external organisation.
- Automatically forwarding mail by employees and students to external addresses is not allowed for security reasons since August 2023.
- The storage space for your mail is `5 GB` by default. This can be expanded on [request](/en/howto/contact).

{{< notice tip >}}

The "sent" mail folder is often overlooked when cleaning up email. Search this folder for the mails with the largest attachments.

{{< /notice >}}

## Address lists

Mail sent to mailing lists is sent to its members. Lists can be managed through [DIY](/en/howto/dhz). Need a new list? [Contact Postmaster](/en/howto/contact).
Address-lists containing external addresses are not recommended anymore, because of antispam measures taken at the external organisation.

## Mailman mailing lists

C&CZ manages a mailing listserver based on [Mailman](https://list.org/). A mailman list is advised for lists containing external addresses. See [the mailman page](/en/howto/mailman-lists) for more info.

## Sharing mailboxes

This can easily be set in [Roundcube webmail](http://roundcube.science.ru.nl), at the bottom left near the gear icon, via 'Folder options' and double-clicking on a folder, but also in an email program such as Kmail. Please use `scienceloginnames` only.
The person who is allowed to use the folder often still has to subscribe to that folder.
When a user reads an email, it is not automatically marked as read for all users. You can ask `postmaster` to alter this setting on the specific mailbox.

## Alternative mail settings

- Besides the `IMAP` protocol, the `POP3` protocol is supported. If you don't know what `POP3` is, do not use it ;-). If you insist on using POP, the service listens on port `995`.
- The recommended way of sending mail is using authenticated SMTP. However, from [some networks](../../news/2023-12-12-changes-in-smtp-policy/#science-smtp-authentication-requirement-from-specific-networks) within the Huygens building network, it is possible to deliver unauthenticated mail. In that case, mail should be delivered on port `25` after which it will be checked for virusses and/or spam.

# `@ru.nl` mail

{{< notice tip >}}

For almost all info on your @ru.nl mail and calendar, see [the central RU website](https://www.ru.nl/en/services/campus-facilities-buildings/ict/email-and-calendar).


{{< /notice >}}

Every student and employee of the Radboud University should have a [@ru.nl email address](http://www.ru.nl/ict/medewerkers/mail-agenda/) in the format:

> Firstname.Lastname@ru.nl