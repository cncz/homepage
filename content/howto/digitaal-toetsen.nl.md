---
author: petervc
date: '2019-08-16T15:56:14Z'
keywords: []
lang: nl
tags:
- studenten
- docenten
- onderwijs
title: Digitaal toetsen
wiki_id: '1004'
---
## RU digitaal toetsen en Chromebooks

Om digitaal toetsen op grote schaal aan te kunnen bieden, heeft de RU
ca. 1000 [Chromebooks](https://en.wikipedia.org/wiki/Chromebook)
aangeschaft. Ook is gekozen voor de gebruikersvriendelijke toetssoftware
[Cirrus Assessment](http://www.cirrusassessment.com/nl/). In de toekomst
zal de facultaire digitale toetsomgeving (TAO, zie onder) alleen nodig
zijn voor toetsen die Cirrus niet aankan. Ook is het draadloze netwerk
van het Gymnasion/RSC geschikt gemaakt om dit grote aantal Chromebooks
van een goed draadloos netwerk te voorzien. Meer informatie hierover is
te vinden op de [RU-pagina over digitaal
toetsen](https://www.ru.nl/docenten/virtuele-map/onderwijs/toetsen-en-beoordelen/digitaal-toetsen/).

Op Chromebooks kunnen alleen webgebaseerde applicaties worden
aangeboden, in een browser. In juni 2018 is succesvol een toets
afgenomen bij meer dan 300 studenten Informatica, met gebruik van een
Windows-applicatie (een moderne programmeeromgeving) die via [Amazon
Appstream 2.0](https://aws.amazon.com/appstream2/) in een apart tabblad
van de Safe Exam Browser op deze Chromebooks werkte.

## Facultaire digitale toetsomgeving: TAO

Er is een digitale toetsomgeving beschikbaar, gebaseerd op het open
source product [TAO Testing](http://www.taotesting.com/), die voor zowel
summatief als formatief toetsen gebruikt kan worden. Hoewel TAO een
groot aantal vraagtypes kent, wordt slechts een selectie hiervan
ondersteund bij de operationele omgeving van FNWI:

-   [extendedTextInteraction](https://userguide.taotesting.com/3.2/interactions/extended-text-interaction.html)
-   [choiceInteraction](https://userguide.taotesting.com/3.2/interactions/choice-interaction.html)
-   [hotspotInteraction](https://userguide.taotesting.com/3.2/interactions/hotspot-interaction.html)
-   [associateInteraction](https://userguide.taotesting.com/3.2/interactions/associate-interaction.html)
-   [matchInteraction](https://userguide.taotesting.com/3.2/interactions/match-interaction.html)
-   [orderInteraction](https://userguide.taotesting.com/3.2/interactions/order-interaction.html)

De TAO-omgeving bevat zelf b.v. geen mogelijkheid om wiskundige of
chemische formules in te voeren in de antwoorden. Zulke formules kunnen
op redelijke schaal wel gebruikt worden in de toetsvragen. Het is wel
mogelijk om in TAO computer broncode in te voeren maar alleen in een
vrij tekstveld, niet in een vorm die in de buurt komt van de
programmeeromgevingen die we tegenwoordig gewend zijn. In dit soort
gevallen kan er een programmeeromgeving, naast de browser waarin TAO
wordt gebruikt, worden gedraaid. Er zijn toetsen afgenomen waarbij naast
de toetssoftware ook Windows Paint, Microsoft Word,
[Clean](https://clean.cs.ru.nl/Clean) en [Virtual
NMR](http://www.virtualnmr.org) zijn gebruikt.

## Onderdelen

De facultaire toetsomgeving bestaat uit vijf delen, allemaal
web-gebaseerd:

1.  Een “database” omgeving waarin docenten vragen kunnen samenstellen
    en hun verzameling toetsvragen kunnen beheren.
2.  Een “authoring” omgeving waarin docenten tentamens kunnen
    samenstellen met de vragen uit de database.
3.  Een “testing” (toets)omgeving waarin tentamens worden afgenomen. The
    toetsen vinden plaats op de PC’s in de [pc
    onderwijsruimtes](/nl/howto/terminalkamers/) die door C&CZ worden
    beheerd, met [Safe Exam Browser](https://www.safeexambrowser.org/).
4.  Een “grading” omgeving waarin de antwoorden kunnen worden
    beoordeeld.
5.  Een “review” omgeving waarmee deelnemers inzage in de gemaakte toets
    kunnen krijgen.

Al deze omgevingen bestaan en zijn operationeel.

## Voor- en nadelen van digitaal toetsen

De voordelen zijn groot.

1.  Correctie en beoordeling, vooral voor een groot aantal deelnemers,
    kan sneller worden uitgevoerd.
    1.  Antwoorden op open vragen worden ingetypt i.p.v. geschreven en
        zijn daardoor veel beter leesbaar.
    2.  Multiple choice vragen kunnen automatisch worden beoordeeld.
2.  Naarmate de database met toetsvragen groeit, wordt het eenvoudiger
    voor docenten om toetsen samen te stellen.
3.  Voor de meeste studenten is typen en klikken handiger dan schrijven.
4.  Voor Informatica-cursussen, vooral op het gebied van programmeren,
    is de mogelijkheid om code te typen en verbeteren een enorme
    verbetering t.o.v. het schrijven van code op papier.
5.  De cursus-enquete (Alice) is geïntegreerd aan het einde van de
    digitale toets, waardoor de response op de enquête enorm is
    gestegen.

Er zijn ook nadelen:

1.  Digitaal toetsen introduceert nieuwe fraude-risico’s. Het kan lastig
    zijn om deze allemaal uit te sluiten.
2.  Digitaal toetsen legt beslag op de beschikbare PC’s voor studenten.
    Dit probleem neemt toe naarmate het gebruik van digitaal toetsen
    toeneemt. Zie [Chromebooks](/nl/howto/digitaal-toetsen/).
3.  De PC’s in de computerruimtes zijn niet beschermd tegen
    stroomuitval. Bij een langdurige uitval kan een digitaal tentamen
    niet doorgaan.
4.  Op dit moment is het niet eenvoudig mogelijk om te werken met
    wiskundige of chemische formules, reactievergelijkingen, diagrammen,
    of welke andere vorm van input anders dan gewone tekst.

