---
author: petervc
date: '2017-10-30T09:09:35Z'
keywords: []
lang: en
tags: []
title: IPad
wiki_id: '857'
---
## General remarks

## Usage

### Initial Setup

Sorry, only the Dutch version of this text is available.

### Managing the iPad

Sorry, only the Dutch version of this text is available.

### Installing and updating apps

### Wi-Fi Eduroam

The Eduroam wireless network is available in all campus buildings. It is
also available in other universities, see
[Eduroam.nl](http://www.eduroam.nl).

Browse to <http://www.ru.nl/wireless> for all info and configuration
manuals.

-   When asked for username and password, this can be one of the
    following combinations:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| \| Username \| Password \|- \| choice 1 \| *u-number\@ru.nl* \|
*RU-password (as is used for Radboudnet)* \|- \| choice 2 \|
*sciencelogin*\@science.ru.nl \| *Science-password (as is used on the
[Do-It-Yourself site](http://dhz.science.ru.nl)* \|}\
You can check the network settings (blue arrow), it will show:

-   IP address: an address in the range 145.116.128.0 - 145.116.191.255
-   Search domain: ru.nl

## Mail and Calendar Configuration

Sorry, only the Dutch version of this text is available.

### Science mail

Sorry, only the Dutch version of this text is available.

### Google mail and calendar

Sorry, only the Dutch version of this text is available.

### Google calendar (CalDAV)

Sorry, only the Dutch version of this text is available.

### Exchange (RU mail)

Sorry, only the Dutch version of this text is available.

## VPN Configuration

See the [ISC VPN](http://www.ru.nl/ict-uk/staff/working-off-campus/vpn/)
page.

## Access to a Network Share

### C&CZ Samba shares

This setup works in principle for all so-called “Samba” shares, that are
also known as “smb” or “CIFS” network shares.

Off-campus you always need a [VPN](/en/howto/vpn/) connection to connect
to a share, on campus this is only necessary for a few more secured
shares.

The iPad does not natively support Samba, so an app is needed. The
example uses **FileBrowser**. An alternative is **GoodReader**.

The FileBrowser app can show MS Office documents, but not edit them. If
you want to edit these files, nstall the app **Documents To Go**.
FileBrowser can also show PDF documents.\
\* Determine the name of the fileserver. Some cases often encountered:

-   -   *Science home-directory*: log in to [the Do-It-Yourself
        website](https://diy.science.ru.nl) and look in the homepage
        behind “Home disk at”- This is something like:
        \\\\pile.science.ru.nl\\*mylogin*
    -   *Backupped network disk* (sometimes called “volletje”) or a
        *web-server directory*: on your own pc find out the network path
        of this network share
        -   Through “My Computer” in MS-Windows, right-click the network
            disk and look at the properties: the network path looks like
            \\\\*servername*.science.ru.nl\\*sharenaam*, with servername
            matching sharenaam-srv.
        -   Through the “Finder” in MacOS X, click the network disk and
            look at the properties with Command-I: the network path
            looks like <smb://>*servername*.science.ru.nl/*sharename*,
            with servername matching sharenaam-srv.

\* Install the app **FileBrowser**

-   Open **FileBrowser**
-   Choose *Menu*
-   Choose *+* to add a new network server
-   Fill in:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| Address \| \\\\*servername*.science.ru.nl (Windows notation) **or**
<smb://>*servername*.science.ru.nl (Mac notation) \|- \| colspan=“2” \|
Click *Show More Settings* \|- \| Display Name \| ‘’your own choosing,
e.g.’’ “Home” \|- \| User Name \| Choose *Edit…* and fill in the
*science loginnaam* \|- \| Password \| Safe is *On demand*, which lets
you fill in the password each time, You can also choose *Edit…* and fill
in the science password \|- \| colspan=“2”\| Click *Advanced Settings*
\|- \| Enable NetBIOS \| switched on - the warning can be ignored if
it’s a C&CZ-managed fileserver, click *OK* \|- \| Compatibility Mode \|
switched on - the warning can be ignored if it’s a C&CZ-managed
fileserver, click *OK* \|- \| Auto List Shares \| switched on (default)
if more than 1 network share is needed from this server, otherwise
switched off. \|- \| SMB Pipelining \| switched off \|- \| SMB Port
Number \| 445 (default) \|}\
\* Choose *Save*, FileBrowser will check whether it can reach the
server.

-   A pop-up will appear if the password has not been entered before.
    -   Fill in the password of the *science account* and press
        *Connect*.
-   If *Auto List Shares*:
    -   *switched on*: all shares of this server that can be accessed
        are shown.
    -   *switched off*: choose *Add Share*, fill in the *sharename* and
        choose *Add*.
-   Navigation on the network disk:
    -   Choose a directory (folder) to enter it;
    -   Choose a document to open it, FileBrowser uses an internal
        viewer (Stratosferix) to show documents;
    -   Tap the document to show the menu below:
        -   Choose the “export” option to open the document in a
            different app, e.g. in Documents To Go;
        -   Choose *+* to bookmark the current document;
        -   Choose the “bookmarks” option to show all bookmarks, this
            makes it easy to switch between documenten.
