---
author: bram
date: '2023-01-11T09:28:26Z'
keywords: []
lang: nl
tags: []
title: Thuis Werken - FNWI
wiki_id: '1061'
---

Zie ook de [tips voor thuiswerken](https://www.ru.nl/ict/medewerkers/thuiswerken/) van het centrale
ICT Service Centrum.

## Email

Gebruik [webmail of een lokale e-mail client](/nl/howto/email).

## Bibliotheek

Zie: Hoe krijg je toegang tot [de universiteitsbibliotheek (als je buiten de campus bent)](https://www.ru.nl/ubn/diensten/studie/toegang-digitale-bibliotheek/).

## BASS

[BASS](/nl/howto/bass) kan alleen vanaf het campusnetwerk gebruikt worden, maar dus ook
via [VPN](/nl/howto/vpn) vanaf een willekeurige plaats op het Internet. Een klein deel
van de FNWI-subneten zijn uitgesloten, deze bevatten servers die vanaf het Internet toegankelijk zijn.

## Bestanden op netwerkschijven

Er zijn twee manieren om bij je [netwerkbestanden](/nl/howto/diskruimte/)
te komen:

-   Met behulp van een [VPN verbinding](/nl/howto/vpn/) kun je direct
    toegang krijgen tot de [netwerkschijven zoals
    gebruikelijk](/nl/howto/diskruimte/)
-   Gebruik een [SCP (Secure Copy)
    client](/nl/howto/ssh#recommended-file-transfer-clients/) om te
    verbinden met een [SSH login server](/nl/howto/ssh/).

## Chat

Gebruik je [Science login](/nl/howto/login/) om in te loggen in
[Mattermost](https://mattermost.science.ru.nl). Je kunt hier zelf
kanalen aanmaken en samenwerken met je collega’s. Kijk [hier voor de
gebruikershandleiding](https://docs.mattermost.com/help/getting-started/welcome-to-mattermost.html).

## Meetings

- Video conferencing vanuit de browser: <https://jitsivm.science.ru.nl>
- ~~Video conferencing + notes + presenatie delen: BigBlueButton~~ *ivm veiligheids issues gestopt*
- Microsoft teams
- Conference calls: <https://conferencecall.nl>
- [Zoom](https://www.ru.nl/ict/medewerkers/online-vergaderen-chatten/zoom-veelgestelde-vragen/) gratis of via een licentie van de Universiteit voor grote groepen.

## Linux Login Server / Cluster

Zie [SSH](/nl/howto/ssh/). Van daaruit kun je door met ssh naar het
[Science cluster](/nl/howto/hardware-servers).

## Code collaboration

Gebruik je [Science login](/nl/howto/login/) om op [GitLab](https://gitlab.science.ru.nl) in te loggen.

## Remote Desktop naar je werkplek (laatste redmiddel!)
In sommige gevallen is er geen alternatief voor het werken op je werkplek-pc.

Als je dit echt nodig hebt, stuur de volgende informatie door aan [Postmaster](/nl/howto/contact):
|           |                                                                                                                                                |
| --------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| contact   | wie onderhoudt de pc? Jij, C&CZ of ISC                                                                                                         |
| hostnaam  | De hostnaam of of ip-adres van de pc. Soms helpt aanvullende informatie zoals het kamernummer of de netwerkaansluiting om de machine te vinden |
| loginnaam | Je [science loginnaam](/nl/howto/login) waarmee je op de pc inlogt.                                                                             |
| reden     | De reden waarom je het nodig hebt (misschien kennen we alternatieven waarvan je niet had gedacht)                                              |

Vervolgens, om Remote Desktop vanuit huis te gebruiken:
- Stel [VPN](/nl/howto/vpn/) thuis in
- Zet je Windows Computer op de faculteit aan voor [Remote Desktop](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-allow-access)
- Verbind met remote desktop naar je `"werkpleknaam".science.ru.nl`


## Help!

Je kunt ons op de gebruikelijke [bereiken](/nl/howto/contact).
