---
author: wim
date: '2011-01-06T10:00:41Z'
keywords: []
lang: nl
tags: []
title: Rdesktop
wiki_id: '812'
---
Als u linux machine of een mac met rdesktop gebruikt is waarschijnlijk
het onderstaande een mogelijke oplossing:

`cd ~`\
`mv .rdesktop .rdesktop.opzij`\
`mkdir .rdesktop`\
`chmod 500 .rdesktop`
