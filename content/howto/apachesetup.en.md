---
author: bram
date: '2019-09-23T09:01:32Z'
keywords: []
lang: en
tags: []
title: ApacheSetup
wiki_id: '687'
---
### Apache Setup

In order to improve the security of websites, we have switched to a new
setup for our webservers. Until recently we had just one apache httpd
running on a webserver. Different websites were served through the
apache virtualhosts mechanism. The main advantage of running just one
httpd is that all websites share the pool of httpd processes. The main
disadvantages are inter-website security and shared log files.

The new setup that is chosen doesn’t have these disadvantages. Instead
of virtual hosts files that are read by just one apache server, we now
use a separate apache http for each website. This means that the files
in the htdocs directory no longer have to be readable for every user on
the webserver. Furthermore every website now gets its own IP address,
which means that every website needing https can get its own
certificate.

The new directory structure looks like (we use *website* as an example
name):

-   **/var/www1/website**

top directory which contains all files w.r.t. this website

-   **/var/www1/website/live/htdocs**

The document root for the website. The standard situation for the
web-docs directory is that it is readable and writable by the unix group
which only contains the users maintaining the website. Next to these
users, only the user that runs the website-specific httpd can only read
(not write) the files.

-   **/var/www1/website/test/htdocs**

The document root for a test version of the website, e.g. to test a new
version of the website before it goes *live*. This test-version is
available via port 82, use as URL: <http://website.science.ru.nl:82/>
and port 4432 for https, use as URL:
<http://website.science.ru.nl:4432/> (note that the real site uses port
80 for http and port 443 for https). Readable and writable by the same
users that can read and/or write the htdocs directory.

-   **/var/www1/website/live/logs**

The directory containing the log-files of the httpd for this website,
the most important ones are access\_log and error\_log. Note that we
strongly advise that the website owners themselves check the errors in
error\_log and take action to remedy errors.

-   **/var/www1/website/test/logs**

Log-directory for the test-version of the website

-   **/var/www1/website/live/writable**

Preferably the only directory in which the httpd for this website can
write, the standard setup has a ‘sessions’ subdirectory that is used by
php as a session\_save\_path directory. If the website needs a writable
directory, please make it in this directory.

-   **/var/www1/website/live/cgi-bin**

The cgi-bin directory for the website.

-   **/var/www1/website/test/cgi-bin**

And of course a cgi-bin directory for the test-version.

Avoid the usage of absolute paths in web pages, this makes it much more
troublesome to move the website, because in that case the web pages need
to be changed!

The network drive (for Windows) is
\\\\website.science.ru.nl\\wwwwebsite, for off-campus usage this can
only be done through [vpn](/en/howto/vpn/) or easier and faster with
WinSCP to lilo.science.ru.nl in the directory /www/website.
