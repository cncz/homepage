---
author: petervc
date: '2014-11-14T11:32:29Z'
keywords: []
lang: en
tags:
- medewerkers
- studenten
- software
title: S-schijf
wiki_id: '509'
---
## S-disk

On [pc’s managed by C&CZ](/en/howto/windows-beheerde-werkplek/) the S-disk (a
[network share](/en/howto/netwerkschijf/)) is available by default. On
this share a large number of programs is available that can be run
directly form this share (no local installation needed). These programs
can also be used on pc’s not managed by C&CZ.

Attach the [network share](/en/howto/netwerkschijf/) as
**\\\\software-srv.science.ru.nl\\software** using Drive letter S
because some software might depend on the driveletter S.
