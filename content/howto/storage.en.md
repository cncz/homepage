---
author: bram
date: "2023-01-12T12:36:19Z"
keywords: []
lang: en
tags:
  - storage
title: Data storage
wiki_id: "48"
aliases:
  - diskruimte
cover:
  image: img/2023/storage.jpg
ShowToc: true
TocOpen: true
---

# Introduction

In our Faculty, different types of data storage are offered:

| Storage class                         | Description                                                                                                                                        | Risk for data loss |
| ------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| [Home directories](#home-directories) | Small (several GB), reliable, backed up storage for individuals                                                                                    | low                |
| [Network shares](#network-shares)     | Larger (1-200 TB), reliable, backed up storage for individuals or groups                                                                           | low                |
| [Local storage](#local-storage)       | Not backed up storage on desktop computers and cluster nodes, likely to be lost in case of a hardware problem or when the machine gets reinstalled | high               |

# Home directories

Your Science login comes with a home directory of 5GB at no costs. This is a safe place to store you work related documents. Home directories are stored
on reliable hardware and [backupped](../backup) automatically. If you need more than 5GB of storage, and you can't [clean up some data](../quota/#frequent-culprits-for-eating-your-quota), it can be enlarged on [request](../contact).

## Home directory paths

{{< notice info >}}
The location of your homedirectory can viewed in [DIY](../dhz).
{{< /notice >}}

On C&CZ managed systems, this is where you can find your home directory:

| C&CZ systems      | Path                                   |
| :---------------- | :------------------------------------- |
| Microsoft Windows | `U:` a.k.a. the U-drive                |
| Linux             | `/home/yourscienceloginname` a.k.a `~` |

From other systems like your home PC, you can reach your home directory through the paths below:
| Own system | Path | Instructions |
| ----------------- | ------------------------------------------------ | --------------------------------------------------------- |
| Microsoft Windows | `\\home?.science.ru.nl\yourscienceloginname` | [connect network share](../mount-a-network-share#windows) |
| macOS | `smb://home?.science.ru.nl/yourscienceloginname` | [connect network share](../mount-a-network-share#macos) |
| Linux | `smb://home?.science.ru.nl/yourscienceloginname` | [connect network share](../mount-a-network-share#linux) |

In case your account has been created recently (see [DIY](../dhz)):
| Eigen systeem | Path | Instructions |
| ----------------- | ---------------------------------------------------- | --------------------------------------------------------- |
| Microsoft Windows | `\\<hash>.home.science.ru.nl\yourscienceloginnaam` | [connect network share](../mount-a-network-share#windows) |
| macOS | `smb://<hash>.home.science.ru.nl/yourscienceloginnaam` | [connect network share](../mount-a-network-share#macos) |
| Linux | `smb://<hash>.home.science.ru.nl/yourscienceloginnaam` | [connect network share](../mount-a-network-share#linux) |

{{< notice info >}}
A [VPN](../vpn) connection is required when accessing these paths from outside of the RU network.
{{< /notice >}}

## Access rights

By default, home directories are only accessible to the owners themselves. However, it is possible
to change the access rights (ie. `chmod o+rx ~`).

# Network shares

For larger storage needs and the ability to share files in your group, there are network shares.
Network shares are available as both _Samba[^samba]_ and _NFS[^nfs]_ share. As mentioned in the
storage overview, network shares are backed up and hosted on reliable servers. The costs involved
with network shares depend on the share size and the backup options.

[^samba]: Windows file sharing implementation by the [Samba project](https://www.samba.org/)

[^nfs]: Network file system, https://en.wikipedia.org/wiki/Network_File_System

## Network share costs

One TB[^what_is_a_tb] (1000 GB) costs € 36 per _year_. This includes a _mandatory_ backup. (Roughly
a backup doubles the amount of data). A share without a backup is possible, but this does not
reduce the cost. Only when the storage costs exceed € 1000 per year will your department be charged.

Currently (2024+) the largest volume size is limited to the amount of disks that fit in a server,
which is roughtly 200 TB.

All new shares are using [ZFS](../zfs-storage) and have local [snapshots](../zfs-share-snapshot/).

We recommend renting storage instead of buying your own hardware, to prevent paying large upfront
sums of money and have continued storage available after a storage server reached end-of-life.

[^what_is_a_tb]: 1TB is `1.000.000.000.000` bytes, 1 TiB = `1.099.511.627.776` bytes

## Network share paths

On C&CZ managed systems, this is where you can find a network share. Using `sharename` as an example:
| C&CZ systems | Path |
| :---------------- | :---------------------------------------- |
| Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename` |
| Linux | `/vol/sharename` |

For self managed systems, you can access the following network paths:
| Own system | Path | Instructions |
| :---------------- | :-------------------------------------------- | --------------------------------------------------------- |
| Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename` | [connect network share](../mount-a-network-share#windows) |
| macOS | `smb://sharename-srv.science.ru.nl/sharename` | [connect network share](../mount-a-network-share#macos) |
| Linux | `smb://sharename-srv.science.ru.nl/sharename` | [connect network share](../mount-a-network-share#linux) |

{{< notice info >}}
A [VPN](../vpn) connection is required when accessing these paths from outside of the RU network.
{{< /notice >}}

## Access rights

The ability to access files on these folders can be limited to a
group of logins. Groups can be managed by the group owners on [DIY](/en/howto/dhz/).

## Special shares

Some services are built on these specific shares:
| Share name | Purpose | Documentation |
| ---------- | ------------------------------- | --------------------------------- |
| `dfs` | Virtual tree structure with links to (many) network drives | [DFS](#dfs) |
| `temp` | Temporary storage of files | [Temp share](../temp-share) |
| `cursus` | Software for courses | [T-schijf](../t-schijf) |
| `software` | General (Windows) software | [S-schijf](../s-schijf) |
| `install` | Installer programs for software | [Install share](../install-share) |

### DFS

For groups with many network drives, a DFS drive is available. If your group uses this, you can access all your group's network drives by connecting just one drive. Whether you can view the underlying drives depends on the permissions you have. The path to connect to the DFS share is:

```console
\\dfs.science.ru.nl\dfs
```

Or, one level deeper to directly see the tree of your group:

```console
\\dfs.science.ru.nl\dfs\{group directory}
```

## Ordering

When ordering a network share, please [send us](../contact) the following details:

| Detail                | Description                                                                                                                  |
| --------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| login name            | Your [Science account](../login)                                                                                             |
| share name            | How the share can be accessed over the network, see [paths](#network-share-paths)                                            |
| size                  | size of the partition, see [costs](#network-share-costs)                                                                     |
| charge account        | a.k.a. _kostenplaats_ or project code                                                                                        |
| unix group (optional) | The group that is granted access to the share. If not specified, a new group will be created with the same name as the share |

# Local storage

Local storage refers to disk space in or attached to systems like desktop pc's and cluster nodes.
Usually, local storage consists of single disks. In case of a disk failure, your data is likely te
be lost. Local disks are usually called `/scratch` to remind you of the temporary nature of the
storage. Local storage is **_not backed up_**. Advantages of local storage are low costs and fast
disk access.

{{< notice info >}}
On shared systems, like login servers and cluster nodes, make sure to create a personal directory in
`/scratch` first. Ie. `mkdir /scratch/$USER`. And don't forget to let your software actually write
to that directory instead of the default and much smaller `/tmp`.
{{< /notice >}}
