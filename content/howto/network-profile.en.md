---
author: petervc
date: '2015-11-25T14:12:31Z'
keywords: []
lang: en
tags:
- studenten
title: Network profile
wiki_id: '630'
---
# Manual for the BW-PC for students

## How to log in faster and not lose data.

### Introduction

Regularly students complain about the time it takes to log into and out
from PC’s in the computer labs. This manual explains the reason behind
it and what you can do about it yourself.

### Working with the C&CZ Profile and Home-disk (H:\\)

In order to have your own settings (your “profile”), C&CZ uses three
central computers, the so-called “Domain Controllers”. Your profile
consists of all kind of user-specific stuff like desktop background,
shortcuts on your desktop and all kinds of user-specific settings of
programs (e.g.: recently opened documents in Word, recently typed
commands in Matlab etc.). You can find these settings in "C:’‘/Documents
and Settings/`<inlognaam>`/". All of this profile is copied when
you login, from the Domain Controller to your local computer because
Windows needs these settings often and fast to run your prograns fast.
When you logout, all changes are copied back to the Domain Controller,
in order to have an up-to-date copy that is needed when you login to a
different PC. Next to your profile disk space (30 MB) every user also
has a Home-disk (home-directory, H:\\, ’My Documenst’) for large
documents, reports, pictures etc. This drive is usually at least 200 MB
large. This home-directory is \*not\* being copied when you log in or
out, it is part of a central C&CZ server that is reachable through the
network as (H:\\ , ‘\\\\manus\\\<inlognaam\>’ of
‘\\\\pluri\\\<inlognaam\>’).

### Why is logging in and out so slow?

If your profile is larger, more data has to be copied, so it takes
longer. Your desktop is part of your profile, so if you keep lots of
data on your desktop you will log in and out slowly. Because a lot of
users have far too much things on their desktop (often MSN-setup,
reports etc.), the Domain Controllers have to copy too much and can’t
handle the load easily. This explains why ProQuota gives a warning if
you have too much things on your desktop: ProQuota is meant to keep
everybody happy by keeping profiles small and logging in fast.

### How do I make it faster?

The Domain Controllers are originally meant to copy just a few hundred
kilobyte with shortcuts and other settings, not tens of megabytes of
files. So only keep shortcuts on your desktop and clean your profile on
a regular basis.

Steps to shrink your
profile:

1.  Move all files, directories, etc. from your desktop to your H:\\
    disk (My Documents is fine too, that is the same disk)

1.  Clean up your profile. Some directories are excluded (you do not
    have to clean them):

:\* C:\\Documents and Settings\\`<loginnaam>`\\Local Settings

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Sun

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Macromedia

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Allen Institute

:\* C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\OriginLab\\Origin75E/User Files/OCTemp

-   These do not count for
        ProQuota too.

    -   Browser Cache:
        -   Internet Explorer: Extra -\> Internet
            Options -\> Tab: General -\> “Remove
            files”\
            Click “Settings”(next to ‘remove files’)
            and drag the slider to 3 MB
        -   Firefox: Menu Extra -\> Options -\> Tab:
            Advanced -\> Tab: Network
            -\> “Clean Now”\
            Buffer size 3 MB

:\* Clean the temporary files of Acrobat Reader, remove

C:\\Documents and Settings\\`<loginnaam>`\\Application
Data\\Adobe

This should really make a difference. It is not advised to remove random
files, not all programs handle the removal of temporary files well.\
(Please leave C:\\Documents and
Settings\\`<loginnaam>`\\Application Data\\Microsoft alone!)

Log out NICELY. Logging out can take a long time, if your profile has
changed a lot. Please let this happen, because at this moment the
profile on the Domain Controllers is cleaned, which makes logging in the
next time faster.
