---
author: lsummere
date: '2022-05-19T12:05:09Z'
keywords: []
lang: en
tags:
- software
- labs
title: Labservant
wiki_id: '1062'
---
# General

# Account

*The procedure below only works for employees with an U-number. For
access with an E-number account please contact
labservant\@science.ru.nl*\
</br>
All employees and guests who need to access to labservant can obtain an
account using the following steps:

1.  Request labservant access via the secretariat of your department,
    they can grand labservant access via RBS.
2.  After labservant access is granted in RBS, login to labservant using
    your RU account (https://labservant.science.ru.nl)
3.  During your first logon an account is created based on your personal
    data in RBS
4.  After a successful login (you should see the Labservant home
    screen), the lab-manager of your department can grant you the
    authorizations needed.

When the message “Logged in but no verified check” is displayed after logon, try to login again using a capital letter in your user name (U-number). If the message persists the most probable cause is that labservant access was not granted in RBS by your department’s secretariat.

# Lab-managers

Each organisational unit or cluster of units has one or more labservant
lab-managers who can grant labservant authorizations to department
members and can manage the department’s storage locations and budget
numbers. The labmanager is the first in line to contact regarding any
labservant questions or issues.

{{< include file="content/howto/labservant-managers.html" >}}
