---
author: bram
date: '2022-09-19T16:23:09Z'
keywords: []
lang: en
tags:
- hardware
title: Disposal
wiki_id: '948'
---
## Disposal

-   Safe disposal of IT equipment and data carriers

-   The FNWI Housing & Logistics (HL) department manages a contract
	with an external specialized company for the secure
	destruction/shredding of data carriers on location. The secure
	metal container that is used as a temporary cache, can be found
	at C&CZ as of September 2022.  Employees and students of the
	Faculty of Science can put data carriers with sensitive data
	in the container themselves at C&CZ. The C&CZ helpdesk can help
	recognizing and removing data carriers from desktops, laptops etc.


-   Broken monitors and other electronics can be discarded at the
    ‘Logistiek Centrum’ of Housing & Logistics:

-   <https://www.ru.nl/science/about-the-faculty/organisation/supporting-services/housing-logistics/>

    You can find the unsafe containers by taking the elevator to floor -2 of
    the Huygens building, then go all the way right (south), and then go
    all the way left (east).

