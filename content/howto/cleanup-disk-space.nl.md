---
author: bram
date: '2023-11-24T12:17:00Z'
keywords: []
lang: nl
tags:
- storage
aliases:
ShowToc: true
TocOpen: true
title: Diskruimte vrijmaken
---
Je hebt mogelijk een e-mail ontvangen dat je persoonlijke of groepsnetwerkschijf bijna vol is.
Dit artikel beschrijft enkele tools om grote bestanden/mappen te identificeren en schijfruimte op te schonen.

## Grote bestanden/mappen vinden met WinDirStat (grafisch, Windows)

![windirstat screenshot](/img/2023/windirstat.png)

[WinDirStat](https://windirstat.net) is een opruimtool voor Windows die een visuele weergave van het schijfgebruik biedt. Het helpt je snel te identificeren en te begrijpen welke bestanden en mappen de meeste ruimte in beslag nemen. Het programma biedt gedetailleerde informatie over elk bestand, inclusief het type, de laatste wijzigingsdatum en het bestandspad. Naast visualisatie bevat WinDirStat ingebouwde opruimopties, zodat je bestanden rechtstreeks binnen het programma kunt verwijderen of verplaatsen.

{{< notice info >}}
Op door C&CZ beheerde Windows-systemen is WinDirStat geïnstalleerd in `S:\windirstat`.
{{< /notice >}}


## Grote bestanden en mappen vinden met Ncdu (tekstgebaseerd, Linux)

![ncdu screenshot](/img/2023/ncdu.png)

[Ncdu](https://code.blicky.net/yorhel/ncdu/) is een command-line-programma voor Linux dat een eenvoudige en snelle manier biedt om schijfgebruik te analyseren. Het presenteert een duidelijke en interactieve interface, waarmee gebruikers door mappen kunnen navigeren, bestandsgroottes kunnen bekijken en ruimteconsumerende bestanden kunnen identificeren. In tegenstelling tot grafische tools is ncdu lichtgewicht en werkt het efficiënt in terminalomgevingen, waardoor het een praktische keuze is voor gebruikers die de voorkeur geven aan de terminal.

Om je home directory te analyseren:

``` console
ssh lilo.science.ru.nl
~$ ncdu
```

Grebruik voor een netwerkschijf iets als:

``` console
~$ ncdu /vol/mijnnetwerkschijf
```

## Andere nuttige commando's, Linux

### Quota

Voor bestandssystemen met quota, gebruik het `quota` commando om het huidige quota gebruik weer te geven:
Bijvoorbeeld:

``` console
bram@lilo8:/home/bram$ quota -s
Disk quotas for user bram (uid 8451):
     Filesystem   space   quota   limit   grace   files   quota   limit   grace
home1.science.ru.nl:/VGsda01/bram
                 37244K   4750M   5000M             906   1900k   2000k
```

{{< notice info >}}
De [nieuwe ZFS home volumes](/nl/howto/zfs-storage/) hebben geen *soft* (zacht) quota, alleen de
harde grens die gelijk is aan de grootte van je homedirectory. Als het `quota` commando niet werkt,
gebruik dan `df`, zoals hieronder uitgelegd.
{{< /notice >}}

### df

`df` staat voor: disk free. Voor disk-gebaseerde bestandsysstemen, rapporteert het de totale groote en het gebruik.
Voor de statistieken van de disk waarop je homedirectory staat, gebruik:

``` console
bram@lilo8:/home/bram$ df -h .
Filesystem                         Size  Used Avail Use% Mounted on
home1.science.ru.nl:/VGsda01/bram  197G   91G  102G  48% /home/bram
```

{{< notice info >}}
Hoewel schijfruimte verwijst naar de totale opslagcapaciteit die beschikbaar is op een bestandssysteem, vertegenwoordigen inodes de gegevensstructuren die informatie opslaan over bestanden en mappen. Zelfs als er voldoende schijfruimte is, kan het opraken van inodes optreden, wat het maken van extra bestanden of mappen kan voorkomen. Schijfrapportagehulpprogramma's zoals `df` en `du` (hieronder besproken) hebben de optie `--inodes` om inodes te rapporteren.
{{< /notice >}}

### du
De schijfruimte van bestanden of directorystructuren kan worden bepaald met `du`. Net als `df` heeft het een `-h` vlag voor menselijk leesbare eenheden. Het converteert bytes naar iets dat je daadwerkelijk kunt begrijpen. Bijvoorbeeld, om de schijfruimte van je `src`-directory te vinden:

``` console
~$ du -h -s ~/src
14G	/home/bram/src
```

Ik gebruik hier ook de -s vlag om de output samen te vatten voor elk argument (`~/src` in dit geval).

### Dubbele bestanden zoeken (en verwijderen): Fdupes

`fdupes` doorzoekt het opgegeven pad naar duplicate bestanden. Dergelijke bestanden worden gevonden door bestandsgroottes en MD5-handtekeningen te vergelijken, gevolgd door een byte-voor-byte vergelijking. Fdupes heeft opties om duplicaatbestanden weer te geven of te verwijderen. Zie `man fdupes`.

{{< notice warning >}}
Pas op met fdupes in combinatie met een gesnapshot filestysteem zoals ZFS. Zorg ervoor dat je de `.snapshots` directory niet meeneemt als een van de argumenten.
{{< /notice >}}


## Cache
Een veelvoorkomende map waarin applicaties tijdelijke bestanden opslaan, is de `~/.cache` map in je home directory. Bestanden daar verwijderen is over het algemeen een veilige actie. Als er veel bestanden in je cache directory staan, zal ncdu ze weergeven.
