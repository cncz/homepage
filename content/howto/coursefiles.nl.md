---
author: petervc
date: '2020-06-23T16:22:15Z'
keywords: []
lang: nl
tags:
- onderwijs
- docenten
- software
- docenten
- software
title: CourseFiles
wiki_id: '1014'
---
How can I access my course file?

The course files is an network disk that can be attached on campus.
Off-campus, VPN must be used to attach this network disk. More info on
[how to attach a network disk](/nl/howto/netwerkschijf/) and on [using
VPN](/nl/howto/vpn/) is available. Linux users have the alternative to
login with ‘ssh’ on the Science loginserver lilo, followed by a cd to
/vol/cursusdossiers.

Windows:

At ‘File Explorer’, choose the option ‘Map Network Drive’, and type in
<file://cursusdossiers-srv.science.ru.nl/cursusdossiers> and pick your
drive letter of choice. By doing so, you will link up the network disk
to your local computer. The next options are also possible to get a more
limited set of courses:

  ------------------------------- ------------------------------------------------------
  General                         <file://cursusdossiers-srv.science.ru.nl/cdAlgemeen>

  Biology                         <file://cursusdossiers-srv.science.ru.nl/cdBiow>

  Computing Science and           <file://cursusdossiers-srv.science.ru.nl/cdOii>
  Information Science             

  Isis                            <file://cursusdossiers-srv.science.ru.nl/cdIsis>

  Molecular Life Sciences         <file://cursusdossiers-srv.science.ru.nl/cdOmw>

  Mathematics and Physics &       <file://cursusdossiers-srv.science.ru.nl/cdWinst>
  Astronomy                       
  ------------------------------- ------------------------------------------------------

Or

  -------------------------------- ---------------------------------------------------
  Courses with the codes NWI-Bx    <file://cursusdossiers-srv.science.ru.nl/cdbiow>
  and -TWM:                        

  Courses with the codes NWI-fxxx  <file://cursusdossiers-srv.science.ru.nl/cdisis>
  and -GCSE:                       

  Courses with the code NWI-Ixxx:  <file://cursusdossiers-srv.science.ru.nl/cdoii>

  Courses with the codes NWI-MOL   <file://cursusdossiers-srv.science.ru.nl/cdomw>
  and -SM:                         

  Courses with the code NWI-Nx:    <file://cursusdossiers-srv.science.ru.nl/cdwinst>
  -------------------------------- ---------------------------------------------------
