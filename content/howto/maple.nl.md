---
author: petervc
date: '2021-12-05T23:25:59Z'
keywords: []
lang: nl
tags:
- software
title: Maple
wiki_id: '149'
---
[Maple](http://www.maplesoft.com/products/Maple/) van Waterloo
Maple’s [Maplesoft](http://www.maplesoft.com/) is een interactief
computer-algebra systeem, waarvoor C&CZ een licentie afgesloten heeft .

De licentie van Maple is in 2023 een 4-gelijktijdige-gebruikers licentie, in 2024 voor 2 gelijktijdige gebruikers.
