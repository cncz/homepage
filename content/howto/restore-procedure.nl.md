---
author: wim
date: '2022-08-11T10:53:32Z'
keywords: []
lang: nl
tags:
- medewerkers
- studenten
title: Restore procedure
wiki_id: '19'
---
Met de onderstaande informatie in een email naar
postmaster\@science.ru.nl kan men bij C&CZ een restore-verzoek indienen.

Omdat het in principe niet mogelijk is om te verifiëren of degene die
een mailtje stuurt ook daadwerkelijk is wie hij of zij beweert te zijn,
zal een aanvraag altijd eerst gecontroleerd worden en dus langer duren.

De snelste manier om een restore aanvraag in te dienen is daarom na het
sturen van een mailtje naar postmaster\@science.ru.nl nog even te bellen
of langs te lopen. Ook voor C&CZ is dat handig, want C&CZ kan namelijk
meestal heel eenvoudig bestanden uit de snapshot van die ochtend
terugzetten, indien men niet een dag wacht met aanvragen. Gebruik voor
de mail de velden hieronder als voorbeeld.

Vul de velden zo volledig en nauwkeurig mogelijk in.

------------------------------------------------------------------------

~~~ txt
naam:
loginnaam:
machine:
datum aanmaak:
tijd aanmaak:
datum laatste wijziging:
tijd laatste wijziging:
datum verlies:
tijd verlies:
oorzaak verlies:
urgentie:
~~~

Vul hier de volledige bestandsnamen in, één per regel.
