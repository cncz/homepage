---
author: petervc
date: '2017-10-05T15:43:27Z'
keywords: []
lang: en
tags: []
title: Android
wiki_id: '949'
---
## Network

### Wi-Fi Eduroam

The Eduroam wireless network is available in all campus buildings. It is
also available in other universities, see
[Eduroam.nl](http://www.eduroam.nl).

Browse to <https://www.ru.nl/services/campusfaciliteiten-gebouwen/ict/wifi> for all info and configuration
manuals.

-   When asked for username and password, this can be one of the following combinations:

| Username                   | Password           |
| -------------------------- | ------------------ |
| u-nummer@ru.nl             | RU-password (eg for [Radboudnet](http://www.radboudnet.nl) website)           |
| sciencelogin@science.ru.nl | Science-password (eg for [Do-It-Youself](http://diy.science.ru.nl) website) |

You can check the IP address by visiting www.whatsmyip.org it will show:

-   IP address: an address in the range 145.116.128.0 - 145.116.191.255

## VPN

To access network disks, it is usually sufficient to be
connected to the RU campus network, wired or wireless via Eduroam. Some network shares are stored in an extra secure location, these can only be accessed from certain workstations or through a VPN connection.

See [Vpn](/en/howto/vpn/)

## Access to a network share

See the [page on network disks](/en/howto/netwerkschijf/) for the apps
needed and the naming of network disks.

## Mail

Sorry, only the Dutch version of this text is available.

### Science mail

Sorry, only the Dutch version of this text is available.

### Exchange (ISC)

Sorry, only the Dutch version of this text is available.
