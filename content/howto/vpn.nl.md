---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: [ vpn ]
lang: nl
tags: [ vpn ]
title: VPN
wiki_id: '71'
ShowToc: true
TocOpen: true
cover:
  image: img/2022/vpn.png
---
# VPN

Een VPN maakt het mogelijk om een veilige verbinding te maken met internet of met het campus netwerk vanaf
waar dan ook.
De werkplek thuis (of ergens anders op het internet) wordt in het laatste geval daarmee
gezien als onderdeel van het campusnetwerk. Op deze manier kan men
toegang krijgen tot faciliteiten die alleen vanaf het campusnetwerk
toegankelijk zijn. Men kan hierbij denken aan [het aankoppelen van
netwerkschijven](/nl/howto/mount-a-network-share) of toegang tot speciale servers.

## EduVPN

De VPN voor RU-medewerkers en -studenten is [EduVPN](https://www.ru.nl/medewerkers/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/buiten-de-campus-werken/waarom-vpn-gebruiken/vpn-gebruiken),
De [eduVPN client software is verkrijgbaar voor Windows, macOS, Android, iOS and Linux](https://www.eduvpn.org/client-apps/).
Je hebt een [RU-account](http://www.ru.nl/wachtwoord) nodig om gebruik te maken van eduVPN.

Alle Science diensten die te gebruiken zijn via een Science VPN zijn ook te gebruiken via eduVPN.

## Universiteitsbibliotheek
Voor het gebruik van de [bibliotheek](http://www.ru.nl/ub) is VPN niet nodig,
want de UB gebruikt een proxy website *UB Off-Campus*, die na inloggen
met [RU-account en RU-wachtwoord](http://www.ru.nl/wachtwoord) toegang
vanuit het hele Internet mogelijk maakt.
