---
author: bram
date: '2022-11-02T10:08:04+01:00'
keywords: []
lang: nl
tags: ['faq']
title: Contact
wiki_id: '816'
weight: 1000
aliases:
- helpdesk
---
Voor alle ICT-gerelateerde vragen en problemen bij FNWI kun je contact
opnemen met C&CZ.

## Helpdesk & [Systeembeheer](../systeemontwikkeling)
| telefoon                            | kamer      | mail                     |
|-------------------------------------|------------|--------------------------|
| [+31 24 36 20000](tel:+31243620000) | `HG00.051` | helpdesk@science.ru.nl   |

## Bezoekadres

Wij zijn gevestigd in het voorste deel van vleugel 5, begane grond van het Huygensgebouw (kamers `HG00.051`, `HG00.509` en `HG00.545`). De helpdesk is beschikbaar tijdens kantoortijden (van 8:30 tot 17:00). Op de afdeling zijn dan ook de medewerkers aanwezig.

> **Volledig adres**:
> Huygensgebouw, HG00.051, Heyendaalseweg 135, 6525 AJ Nijmegen
>
> **Postadres**:
> Radboud Universiteit Nijmegen, Faculteit NWI, C&CZ, Postbus 9010, 6500 GL Nijmegen

# Andere helpdesks

## ILS Helpdesk

Bij de ILS helpdesk kun je terecht met vragen over, oa:
- ILS beheerde werkplekken
- Printen
- `@ru.nl` mailadressen
- Bureau en mobiele telefoons
- Netwerkaansluitingen

| telefoon                            | email             | web                    |
|-------------------------------------|-------------------|------------------------|
| [+31 24 36 22222](tel:+31243622222) | icthelpdesk@ru.nl | https://www.ru.nl/ict/ |

Studenten met vragen over het Radboud-account of `@ru.nl` adressen kunnen het
beste direct contact opnemen met [Studentenzaken](https://www.ru.nl/afdelingen/academic-affairs-afdelingen/student-affairs).
