---
author: petervc
date: '2022-09-09T11:01:58Z'
keywords: []
lang: nl
tags:
- software
title: Microsoft windows
wiki_id: '160'
---
## Windows software voor medewerkers en studenten van FNWI

### Om zelf te installeren op een zelfbeheerde Windows PC

-   [Install-share](/nl/howto/install-share/): een netwerkschijf met
    installeerbare software.
-   Het [ILS](http://www.ru.nl/ils) heeft een
    [pagina over software](https://www.ru.nl/medewerkers/services/campusfaciliteiten-gebouwen/ict/software)
     en houdt een [lijst van RU-softwarelicenties](https://gosoftware.hosting.ru.nl/) bij.

-   Surfspot.nl: Allerlei licentie software (die ook thuis gebruik mag
    worden) is bij [Surfspot](http://www.surfspot.nl/) te koop wanneer
    men met RU-account en
    [RU-wachtwoord](http://www.ru.nl/wachtwoord) inlogt. Afdelingen
    kunnen deze software via [Inkoop](http://www.ru.nl/inkoop/)
    bestellen. Meestal betreft het software waarvoor
    [SURFdiensten](http://www.surfdiensten.nl/) voor
    onderwijs-instellingen een
    [SURFlicentie-overeenkomst](http://www.surfdiensten.nl/info/licenties/index.html)
    afgesloten heeft.
    -   De upgrade van Windows 10 naar de Education versie met o.a.
        BitLocker versleuteling is erg aan te raden.
-   [ChemDraw / ChemBioOffice](/nl/howto/chembiooffice/)
    moet men direct installeren vanaf de site van de leverancier.
-   C&CZ leent aan studenten en medewerkers van de FNWI DVD/CDs uit van
    allerlei [Microsoft Windows](/nl/howto/microsoft-windows/),
    [Unix/Linux](/nl/howto/unix/) of [Apple MacOS](/nl/howto/macintosh/)
    FNWI- of RU-licentie-software.
-   Software voor de RU Concern systemen ([BASS-Finlog, HRM](/nl/howto/bass/), …).

### Beschikbare software op door C&CZ beheerde Windows-computers

Het is niet mogelijk om exact aan te geven welke software op een door
C&CZ beheerde Windows-computer geïnstalleerd wordt. Dit hangt in sterke
mate af van de functie van de PC en de afdeling die eigenaar van de PC
is. Licentiesoftware waarvoor geen campuslicentie bestaat is vaak maar
op een deel van de PC’s geïnstalleerd.

Op de Windows PC’s in de [PC-onderwijszalen](/nl/howto/terminalkamers/) is
een [T-schijf](/nl/howto/t-schijf/) van het netwerk aangekoppeld met
cursussoftware.
