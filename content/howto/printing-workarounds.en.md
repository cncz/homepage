---
author: harcok
date: '2022-05-25T13:44:50Z'
keywords: []
lang: en
title: Printing workarounds
aliases:
wiki_id: '855'
---
{{< notice info >}}
this document needs to be updated
{{< /notice >}}
## General

Péage is the print/scan/copy system of Radboud University for staff and
students. As of March 2019 the new server is `print.hosting.ru.nl` and
the print queue “FollowMe”. The Library of Science has [passes for
guests](http://www.ru.nl/facilitairbedrijf/printen/gastenpas/). General
information can be found on the [Péage
website](https://www.ru.nl/fb/english/print/printing-campus/).

On this page, C&CZ provides information specific for staff and students
of FNWI.

## Terminology

-   MFP: multifunctional printer (that can print, copy and scan)

## PrintNightmare problem workaround

-   Since ca. September 22, 2021, Peage doesn’t work anymore on self
    managed/BYOD Windows/macOS/Linux computers. That day the ISC
    installed the patches for [the security vulnerablility
    PrintNightmare](https://borncity.com/win/2021/09/22/windows-printnightmare-patch-stand-und-workarounds-22-sept-2021/)
    on the print servers. The ISC and C&CZ thereafter introduced a
    workaround for their managed computers. A workaround for *all*
    computers is using the web upload on [the Peage
    website](https://peage.ru.nl). Since October 28, 2021, uploaded jobs were
    printed in color. Since January 1, 2024 uploaded jobs were printed in black and white (B&W) and double-sided.

    -   Self managed Windows pcs can also use the C&CZ workaround. This
        workaround is based on an old printer installation method of the
        Windows 98/XP age. This is not a real solution. If Microsoft
        solves the ‘PrintNightmare’ problems, this printer installation
        should be removed. We do not guarantee that this workaround
        works correctly. Below you’ll find steps to introduce the
        workaround. The C&CZ helpdesk can assist, when you find this
        troublesome.

    -   It is important that your RU credentials have been entered in
        the Credential Manager:

        `  Internet or network address: print.hosting.ru.nl`\
        `  User name: [Givenname.Surname@ru.nl]`\
        `  Password: [your RU password]`

    -   If you entered this correctly, then Windows Explorer will show a
        list of printers when you type \\\\print.hosting.ru.nl\\ in the
        address bar.

1. Download the FollowMe driver from [the Konica-Minolta download
centre](https://www.konicaminolta.eu/eu-en/support/download-centre).

Choose Product Type: A3 MultiFunctional Colour and Product: C658.

To be compatible with the drivers installed by the ISC, choose Version
11.1.2.0 Jul 26, 2018 PCL6.

At the time of writing this workaround, this resulted in [a long URL at
dl.konicaminolta.eu](https://dl.konicaminolta.eu/en?tx_kmanacondaimport_downloadproxy%5BfileId%5D=c9bde5fb7d1de8e581c797666d089ed2&tx_kmanacondaimport_downloadproxy%5BdocumentId%5D=1719&tx_kmanacondaimport_downloadproxy%5Bsystem%5D=KonicaMinolta&tx_kmanacondaimport_downloadproxy%5Blanguage%5D=EN&type=1558521685).
Save the driver zipfile in a known location and unzip it.

2. Go from ‘Start’ -\> ‘Settings’ -\> ‘Devices’, to ‘Printers &
scanners’ and start ‘Add a printer or scanner’. An option appears ‘The
printer that I want isn’t listed’, select that option..

3. A window appears ‘Find a printer by other options’. Select ‘Add a
local printer or network printer with manual settings’. Click ‘next’.
‘Choose a printer port’ appears, leave this at ‘Use an existing port:
LPT1 (Printer Port)’ for the time being. Click ‘next’.

4. A window appears ‘Install the printer driver’, select ‘Have Disk…’
and browse to the unzipped driver. Select the only ‘INF’ file and click
the ‘Open’ button and ‘OK’. From the different models, choose \`KONIKA
MINOLTA C658SeriesPCL’ and click ‘Next’.

5. A window ‘Type a printer name’ appears with already filled-in ‘KONICA
MINOLTA C658SeriesPCL’. Click ‘Next’. A request for the Administrator
password appears. After entering that, the driver is installed.

6. A window ‘Printer Sharing’ appears, pick the default ‘Do not share
this printer’ and choose ‘Next’ and ‘Finish’.

7. We are going to change the installed printer. In ‘Settings’ -\>
‘Devices’ -\> ‘Printers & scanners’: select the installed printer and
click ‘Manage’. After that, click ‘Printer properties’.

8. A new window ‘KONICA MINOLTA C658SeriesPCL Properties’ appears. In
the tab ‘Ports’ choose ‘Add Port…’, select ‘Local Port’ and after that
‘New Port…’. In the window ‘Port Name’ with the ‘Enter a port name:’
field, fill in ‘\\\\print.hosting.ru.nl\\FollowMe’. Cllik ‘OK’, ‘Close’
and ‘Close’.

## Known problems

-   An E-number user can’t print. RBS-management can solve
    that:

`   Nummer Kamer Naam                              Afdeling      Email `\
`    52246 HG02.047 Ir. J.J. Zijlstra, (Jan-Jetze) Facbureau NW  J.Zijlstra@science.ru.nl `

-   Remote Downlevel Document: When printing from Linux, the MFP shows “Remote
    Downlevel Document” instead of the name of the printed file.
-   Evince print
    in A3 format. Remove the file
    **\~/.config/evince/print-settings**.
-   Okular on Ubuntu 18.04 lacks Advanced
    Options to print eg in color - Ubuntu problem.
-   **lpq** problem.
    Update default printer. Edit the file
    **\~/.cups/lpoptions**.
-   In the Linux Document Viewer you need to print twice to
    get one output.
-   Please remove printers
    that are no longer part of the print system from your computer.
-   [`cups-2.2.7` en `smbclient-4.8.1` hebben een API discrepantie
    waardoor printen niet werkt op Linux (gezien op Arch Linux). Een
    oplossing is om [`/usr/lib/cups/backend/smb` te
    patchen](https://twitter.com/mrngm/status/993492483786117121) (of
    `smbclient` te updaten).][`cups-2.2.7` and `smbclient-4.8.1`
    conflict in their command-line API which breaks printing on Linux
    (seen on Arch Linux). A solution is to [patch
    `/usr/lib/cups/backend/smb`](https://twitter.com/mrngm/status/993492483786117121)
    (or to update `smbclient`).]

## Printing with Péage

### Login with campuscard/ru-card

Before you can use your campuscard/ru-card to login onto the printer you
first have to link the card to your u-account:

-   Keep your to the front side of the printer at the place where the
    card sticker is.
-   When the printer recognizes the card it will popup a new window with
    which you can login.
-   Login (only once) with your U, E, S of F number and RU-password

The next time you place your campuscard/ru-card before the printer you
are immediately logged in.

### User groups

Péage has 1 printserver and 1 printqueue for FollowMe printing:

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  printserver              printqueue    UNC - Windows style                                          URI - IPP style
  ---------------------------------------------- ----------------------------------- ------------------------------------------------------------ ---------------------------------------------------------------
  **print.hosting.ru.nl**   **FollowMe**   **\\\\print.hosting.ru.nl\\FollowMe**   **<smb://print.hosting.ru.nl/FollowMe>**


  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

For smaller printers without display/keyboard, the direct printqueue of
the printer must be used:

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  printserver              printqueue   UNC - Windows style                                         URI - IPP style
  ---------------------------------------------- ---------------------------------- ----------------------------------------------------------- --------------------------------------------------------------
  **print.hosting.ru.nl**   **KM-xxxx**   **\\\\print.hosting.ru.nl\\KM-xxxx**   **<smb://print.hosting.ru.nl/KM-xxxx>**


  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Péage portal: Printing with Péage on any Internet device

With the [Péage portal](https://peage.ru.nl) you can print from
smartphone, tablet and every Internet-device connected to the university
network to [Peage](/en/howto/peage/), through e.g. web upload. A web
upload always prints double-sided.

### Printing with Péage on a C&CZ managed Windows machine

The printer “FollowMeScience” is listed at the server
“print.science.ru.nl”. To let this work, your U- or s-number must be
registered at C&CZ. You can check this on the [Do-It-Yourself
site](https://diy.science.ru.nl).

### Printing with Péage on a self managed Windows 7/8/8.1/10 machine

The instructions below are valid if you logged in with your Science
account or a personal account. If you logged in with RU\\U-number or
RU\\s-number, go directly to step 4. N.B.: your settings are not saved
then.

To attach the printer, you need your RU-account (u/f/e/s-number and
RU-password, not the Peage PIN code).

-   Step 1\
    Select **Start**, in the
    **Help / Search** area enter
    **Credential Manager** and launch the
    application found.

-   Step 2\
    The Windows Vault appears. Click on **Add a Windows
    credential** next to the header
    **Windows Credentials**. Enter
    credential data: Internet or network address:
    **print.hosting.ru.nl** User name:
    **ru\\** followed by your RU-account,
    eg. **ru\\u123456** (staff) or
    **ru\\s123456** (students) Password:
    your matching RU-password (not your Peage PIN code)

-   Finally click <strong>OK</strong> and close the <strong>Credential
        Manager</strong>.

-   Step 3\
    Press the **“windows”** key and
    **“r”** at the same time. Fill out
    **\\\\print.hosting.ru.nl** in the
    **open** bar and press
    **enter** or click
    **ok**

-   Step 4\
    Double-click the **FollowMe**
    printer. It will install automatically when the printer que pops up,
    the installation is complete.

-   Step 5\
    Continue with [Printing a job at the
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

### Printing with Péage on a Mac OS X machine

The ISC has made an installation tool available to add Péage printers:
See [Mac OS X
instructions](https://www.ru.nl/fb/english/print/printing/mac-os/).

#### Manual installation

You need your RU-account (u/f/e/s-number and RU-password, not your Peage
PIN code) to send a print job to the printer.

-   Step 1\
    Select **System Preferences**, select
    **Printers & Scanners**

-   Step 2\
    Click on the **+** (plus) sign below
    the list of printers. See if you have an item
    **Advanced** in the menu bar.
    Whenever this item is not present: Right click in this menu bar and
    choose **Customize Toolbar…**. Drag
    the item **Advanced** into the menu
    bar. Click on **Done** and, again,
    click on the item **Advanced** in the
    menu bar.

-   Step 3\
    Enter printer data: Type: **Windows printer via
    spoolss** Device: **Another
    Device** URL:
    **<smb://print.hosting.ru.nl/FollowMe>**
    Name: Free to type text field, eg.
    **Peage** (this will be the name of
    the printer in the printer list) Location: Free to type text field,
    eg. **Péage Konica Minolta printers**
    Use: Change **Choose a Driver…** to
    the printer driver, eg. **Generic Postscript
    Printer**. Remark: if you want to staple your
    prints, then you need for printing on MacOS to install a specific
    driver for your specific printer. Unfortunately Konica Minolta
    doesn’t provide a generic driver for all its printers for MacOS as
    it does for Windows. If you need stapling only in special occasions
    then you could ask a colleague with a Windows computer to print it
    for you stapled. A specific driver for your specific printer can be
    downloaded from the Konica Minolta website. You also have to
    configure the specific hardware addons to your specific printer. You
    can do this in the “printer&scanner” preferences, then select your
    specific printer left, and then on the right side click the “options
    supply” button, and go to the Options tab. In this tab set the
    specific “paper source unit”, “finisher” and “punch unit”. Ask a
    colleague with a Windows computer to look in its printer
    configuration to find out which specific “paper source unit”,
    “finisher” and “punch unit” your printer has. Finally click
    **Add**.

-   A new window will appear with an option <strong>Duplex Printing
        Unit</strong>. Mark the checkbox as active and click
        <strong>OK</strong>.

-   Step 4\
    Now is the time to setup your printing preferences. Click
    **Options & Supplies…** and next
    **Options**. Our advice: select
    **black-and-white printing** and
    **Duplex Printing Unit** as your
    printing defaults.

-   Step 5\
    The setup to use your RU-account and RU-password (not your Peage PIN
    code) is explained next. Find some document you wish to print and
    send your first print job to this new Péage printer.

-   If your printer is shown in the <strong>Dock</strong> you will
        notice a <strong>1</strong> on top of the printer icon.
        Click on the printer icon to view the print queue.
        You could also click on the printer in <strong>Printers &
        Scanners</strong> in <strong>System Preferences</strong>.
        The status will be that the print job has been send, but after a
        while the status will change to <strong>Hold for
        Authentication</strong>.
        Select the print job in the print queue and click on the button
        <strong>Resume</strong>.
        A dialog window appears requesting your <strong>Name</strong> and
        <strong>Password</strong>.
        Enter <strong>ru\\</strong>, followed by your RU-account, eg.
        <strong>ru\\u123456</strong>, and the matching RU-password (not your
        Peage PIN code).

-   If the printer icon is missing from the <strong>Dock</strong>, the
        dialog window requesting <stron>Name</strong> and
        <strong>Password</strong> will popup.
        Login with your RU-account (<strong>ru\\u123456</strong>) and
        RU-password (not your Peage PIN code).
        Select if desired <strong>Remember this password in my
        keychain</strong>, so your RU-account and RU-password will be
        remembered, authentication for print jobs is no more needed.
        Clicking <strong>OK</strong> will, after a second (depending on the
        size of your print job), remove the print job from the queue. The
        job is then received by the print server and remains
        in its print queue for final printing.

-   Step 6\
    Continue with [Printing a job at the
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

### Printing with Péage on a C&CZ managed Linux machine

The printer “FollowMe” is listed. Command line printing with
`lpr -PFollowMe` is possible. To let this work, your U- or s-number must
be registered at C&CZ. You can check this on the [Do-It-Yourself
site](https://diy.science.ru.nl).

### Printing with Péage on a self managed Linux machine

You need your RU-account (u/f/e/s-number and RU-password, not your Peage
PIN code) to send a print job to the printer.\
Below a short description how to install the printer on Ubuntu 18.04
(Bionic Beaver). This procedure might work for other recent Linux
systems using the Gnome Desktop. Make sure that the packages

-   **smbclient**
-   **python3-smbc**
-   **dbus-x11**

are installed. If not, install them using your package manager.

-   Step 1\
    If you are using a C&CZ managed Linux computer, the printer is
    already installed. Go to step 6. Via
    **Settings** visit
    **Printers**. Or start the tool
    **system-config-printer** tool from
    the commandline with the command: `system-config-printer`

-   Step 2\
    Click below the Printer list on **Additional Printer
    Settings…** and next within the pop-up app
    **Printers
    -   localhost** on the
        **Add** button. There might
        appear a window **Authenticate**,
        provide the requested root password.

-   Step 3\
    A window **New Printer** emerges,
    select **Network Printer** and then
    **Windows Printer via SAMBA**. On the
    right side you can provide required data: SMB Printer:
    **<smb://print.hosting.ru.nl/FollowMe>**
    Authentication: **Prompt user if authentication is
    required**. If this option is not available then
    restart the installation with the system-config-printer tool instead
    which has this option!

-   Click on <strong>Forward</strong>.

-   Step 4\
    In the next window you have to choose a driver: Select
    **Provide PPD file**. Also select the
    [FollowMe.ppd](/downloads/2022/FollowMe.ppd)
    (Postscript Printer Description) file, which has to be downloaded
    first via right-clicking the link. Click on
    **Forward**. You should see a window
    **Installable Options** with settings
    you can change. If using the correct PPD file no adaptations are
    needed. Click on **Forward**.

-   Step 5\
    Provide the requested data: Printer Name: Peage *(use the name of
    the print queue, see [User
    group](#.5bonderscheid_gebruikers.5d.5buser_groups.5d))*
    Description: KONICA MINOLTA C759SeriesPS/P Location: Virtual
    printqueue / available at all Peage KM printers

-   Click on <strong>Apply</strong>.
        There might appear a window <strong>Authenticate</strong>
        At <strong>Username</strong> type: first the domain prefix
        <strong>ru\\</strong>, directly followed by your u/s/e/f-number,
        e.g. <strong>ru\\u123456</strong>, and at <strong>Password</strong>
        your RU-password (not your Peage PIN code).
        Select <strong>Cancel</strong> at the question <strong>Would you
        like to print a test page?</strong>.

-   Step 6\
    First stop the cups server before editing the config:

`   sudo /etc/init.d/cups stop`

-   Edit the file /etc/cups/printers.conf and change for this printer's
    configuration the line

    `AuthInfoRequired none`

    to

    `AuthInfoRequired username,password`

-   restart the cups server:

    `sudo /etc/init.d/cups start`

-   Step 7\
    Your printer has been installed and is ready to use. It is adviced
    to first use firefox to make a test print to initialize and store
    your credentials safely in the gnome keyring. Use firefox because
    not all linux programs ask for authentication correctly, but will
    print alright once the credentials are stored correcty in the
    keyring. As soon as you use the printer, a window will pop up
    showing the message **Authentication is required to
    print …** and also asking for your credentials. It
    can happen that your print job gets paused requesting
    authentication. In that case open the print queue and right click on
    your print job. In the upcoming context menu select the authenticate
    option. It might be a good idea to keep the print queue applet open
    and to check for print jobs requesting authentication. At
    **Username** type first: the domain
    prefix **ru\\**, directly followed by
    your u/s/e/f-number,
    e.g. **ru\\u123456**, and at
    **Password** your RU-password (not
    your Peage PIN code).

-   Credentials are stored in the gnome-keyring which can be viewed or
        modified with the tool seahorse. Before using seahorse you probably
        need to install it first with your package manager.

-   Step 8\
    Continue with [Printing a job at the
    MFP](#.5bafdrukken_van_een_printjob_bij_de_mfp.5d.5bprinting_a_job_at_the_mfp.5d)

#### Fix for wrong credentials in keyring

Credentials are stored in the gnome-keyring which can be viewed or
modified with the tool seahorse. Before using seahorse you probably need
to install it first with your package manager.

method 1: Update your password in the gnome-keyring:

-   start the program: "Seahorse" also called "Passwords and Keys"
    at the left sidebar select "Logins" under the "Passwords" section.
    at the right select the entry with label
    "<ipp://localhost:631/printers/FollowMe>"
    double click this entry to open a popup window
    in the popup window edit your ru-password
    close the popup window by pressing the X on the window title bar
    before it closes the window you are asked: "Save changes for this
    item?
    answer by pressing "OK", and the windows close
    close the "Seahorse/Passwords and Keys" program
    DONE

method 2: However sometimes updating your new password in the
gnome-keyring doesn’t solve the authentication problem. In that case
follow the steps below:

-   Delete your current keyrings folder in \~/.local/share/keyrings
        **Logout, and immediately login again.** This ensures that the
        keyring server is restarted, which neatly creates the
        keyrings/folder again. Check that a new keyrings folder has been
        created.
        Start Firefox and print a page.
        Now you should get an authentication dialog with a checkbox
        "Remember password". Enter your ru credentials and check the
        checkbox to store your credentials in the gnome keyring.
        From now on you should be able to print without needing to enter
        your credentials, because they will taken from your keyring which is
        made accessible when you logged in with your machine's password.

### Printing with Péage using smbclient (on Linux)

Assuming your U-number is u123456 and you want to print the file
“something.pdf”:

-   Step 1\
    smbclient -U “ru\\u123456” //print.hosting.ru.nl/FollowMe -c “print
    something.pdf” Enter RU-password at the prompt and go to a printer
    to follow the usual procedure.

### Printing with Péage using CUPS (on Linux)

If printing using the smbclient command works, a regular print queue can
be set up with CUPS. Usually this requires root access.

-   Step 1\
    With a browser open: <https://localhost:631> and select “Adding
    Printers and Classes”, “Add Printer” [Username: root, Password:
    `<rootpassword>`“, Other Network Printers:”Windows Printer
    via SAMBA“,”Continue".

-   Step 2\
    Enter connection:
    <smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe>, “Continue” We
    could enter the actual \_USER and \_PASSWORD, but is more secure to
    keep the strings “\_USER” and “\_PASSWORD”, and set them later (see
    below).

-   Step 3\
    Provide a PPD File: [Browse], upload the ppd file
    [FollowMe.ppd](https://wiki.cncz.science.ru.nl/images/c/cd/FollowMe.ppd),
    and click “Add Printer”. After this, “lpq -P FollowMe” should show
    “FollowMe is ready”.

-   Step 4\
    CUPS will store the information from step 3 into
    /etc/cups/printers.conf There should be details of the printer, and
    also these lines: AuthInfoRequired username,password DeviceURI
    <smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe> The actual
    \_USER and \_PASSWORD can be set, and the AuthInfoRequired can be
    set: AuthInfoRequired none After restarting cups (“service cups
    restart”) printing should work. To make this the default add “export
    PRINTER=FollowMe” to \~/.bashrc and print with lpr, or using, e.g.,
    okular or evince. The default setting is to print in grayscale. If
    you want to print in color, change the settings. With lpr, use
    `-o SelectColor=Color`. To set this option permanently use:
    `"/usr/bin/lpoptions -o     SelectColor=Color"`. This will store the
    setting in `/etc/cups/lpoptions`.

-   Step 5\
    After a reboot, AuthInfoRequired in /etc/cups/printers.conf may be
    reset to “username,password”. Also, you may not like a password in
    plain ASCII. This can be fixed by changing the backend: the “smb” in
    printer.conf refers to /usr/lib/cups/backend/smb, which usually is a
    link to /usr/bin/smbspool. Change /usr/lib/cups/backend/smb into a
    bash script: \#!/bin/bash export AuthInfoRequired none export
    DEVICE\_URI=“<smb://_USER:_PASSWORD@print.hosting.ru.nl/FollowMe>”
    /usr/bin/smbspool “\$@” You can now use the actual RU-username and
    password, or create a script that has a more secure way of obtaining
    the password

### Printing a job at the MFP

-   Step 1\
    Log in with your RU-account, without the
    **ru\\** prefix, eg.
    **u123456** and RU-password or your 6
    digit Péage PIN on one of the Konica Minolta MFP’s. All MFPs have a
    card reader, you can also just swipe the coupled card (campus card
    or other). There is a [manual how to couple a
    card](https://www.ru.nl/facilitairbedrijf/printen-nieuwe-mfp/uitloggen-printer/inloggen-campuskaart/).
-   Step 2\
    Choose **Print**, select the print
    job(s). Pressing **Details** next to
    the print job will show you the costs of this print job.
-   Step 3\
    Press the blue button to print your document(s).
-   Step 4\
    Log out

## Generate PIN for Péage

-   Step 1\
    Visit the site <https://peage.ru.nl/>. This site is worldwide
    available.
-   Step 2\
    Select **Peage Portal**.
-   Step 3\
    Your are now forwarded to <https://safeq.ru.nl/>.\
    Log in with your RU-account,
    **without** the
    **ru\\** prefix, eg.
    **u123456** and RU-password.
-   Step 4\
    Click the link “Generate PIN” which will generate a 6 digit pincode
    for you.
-   Step 5\
    Log out.

## Report, status and history budget Péage

-   Step 1\
    Visit the site <https://peage.ru.nl/>. This site is worldwide
    available.
-   Step 2\
    Select **Peage Portal**.
-   Step 3\
    Your are now forwarded to <https://safeq.ru.nl/>.\
    Log in with your RU-account,
    **without** the
    **ru\\** prefix, eg.
    **u123456** and RU-password.
-   Step 4\
    In the top of the page click the tab “Reports” which will show you
    the reports page.
-   Step 5\
    Log out.
