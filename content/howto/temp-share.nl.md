---
title: Temp share
author: bram
date: 2023-01-13
keywords: []
tags:
- storage
cover:
  image: img/2023/temp-share.png
ShowToc: true
TocOpen: true
---
## Gedeelde tijdelijke schijfruimte

Het komt wel eens voor dat je een of meer grote bestanden naar iemand binnen de faculteit wilt sturen, mail is
daarvoor ongeschikt. Om dit makkelijker te maken is er de netwerkschijf `temp` beschikbaar. Hier kun je tijdelijk
grote bestanden plaatsen. Deze kunnen dan door iemand anders van deze locatie gekopiëerd worden. 

| C&CZ systemen     | Pad                             | Instructies                                         |
| ----------------- | ------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\temp-srv.science.ru.nl\temp` | [schijf koppelen](../mount-a-network-share#windows) |
| Linux             | `/vol/temp`                     |                                                     |

| Eigen Systeem     | Pad                                 | Instructies                                         |
| ----------------- | ----------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\temp-srv.science.ru.nl\temp`     | [schijf koppelen](../mount-a-network-share#windows) |
| macOS             | `smb://temp-srv.science.ru.nl/temp` | [schijf koppelen](../mount-a-network-share#macos)   |
| Linux             | `smb://temp-srv.science.ru.nl/temp` | [schijf koppelen](../mount-a-network-share#linux)   |


{{< notice warning >}}

Bedenk dat dit nadrukkelijk bedoeld is voor tijdelijke opslag, er worden geen backups van gemaakt en van de disk worden dagelijks alle bestanden ouder dan 3 weken verwijderd.

{{< /notice >}}

## Persoonlijke tijdelijke opslag
De netwerkschijfnaam voor het tijdelijk opslaan van persoonlijke bestanden is `onlyme`. Deze schijf is alleen beschikbaar
als een Windows netwerkschijf:

| C&CZ systemen     | Pad                                 | Instructies                                         |
| ----------------- | ----------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\onlyme-srv.science.ru.nl\onlyme` | [schijf koppelen](../mount-a-network-share#windows) |

{{< notice info >}}
Op door C&CZ beheerde Linux systemen is de `onlyme`-netwerkschijf niet beschikbaar. Voor het tijdelijk opslaan van
prive-data op `/vol/share` wordt je geacht zelf de juiste permissies in te stellen
{{< /notice >}}

| Eigen systeem           | Pad                                     | Instructies                                         |
| ----------------- | --------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\onlyme-srv.science.ru.nl\onlyme`     | [schijf koppelen](../mount-a-network-share#windows) |
| macOS             | `smb://onlyme-srv.science.ru.nl/onlyme` | [schijf koppelen](../mount-a-network-share#macos)   |
| Linux             | `smb://onlyme-srv.science.ru.nl/onlyme` | [schijf koppelen](../mount-a-network-share#linux)   |

## Tijdstempels
Files in de `temp`-share worden automatisch opgeruimd op basis van de hun tijdstempels (timestamps). Sommige
kopieerprogramma’s (zoals rsync) behouden de originele tijdstempels en dan worden mogelijk de oudere bestanden
door de temp-schijf verwijderd. Werk daarom na het kopieren de tijdstempels bij, bijvoorbeeld met het volgende
commando:

```
find /vol/temp/$USERNAME -exec touch {} +
```

# Alternatief
Voor het versturen van bestanden tot 1TB is ook [SURFfilesender](https://www.surffilesender.nl/) geschikt.
> Met SURFfilesender verstuur je vertrouwd grote bestanden, zoals onderzoeksdata. De bestanden zijn in Nederland opgeslagen. Versleuteling biedt extra veiligheid.