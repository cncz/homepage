---
author: bram
date: '2022-09-19T16:30:58Z'
keywords: []
lang: en
tags:
- netwerk
title: Netwerk draadloos
wiki_id: '86'
---
# Wireless network

The [wireless network](https://www.ru.nl/ict-uk/general/wifi/) that we
advise to use, is [eduroam](http://www.eduroam.org/). Authentication is
done in one of the following ways:

-   Firstname.Lastname\@ru.nl and RU password 
-   Science-loginname\@science.ru.nl and Science password

Employees of Science-departments can request [Eduroam Visitor Access
accounts](https://eva.eduroam.nl/inloggen), that give visitors access to
a separate part of the wireless network. through
[C&CZ-Operations](/en/howto/werkplekondersteuning/), tel. 20000. Every
employee of Radboud University can provide a guest with access for the
rest of the day, by providing the guest a day code (keyword). That code
can be found on the [the RU portal](https://portal.ru.nl). For more
information, see the [ILS
website](http://www.ru.nl/ict-uk/staff/wifi/wifi-visitors/). Guests can
also get access through the Library of Science.
