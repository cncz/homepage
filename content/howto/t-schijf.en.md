---
author: petervc
date: '2014-11-14T11:44:26Z'
keywords: []
lang: en
tags:
- software
title: T-schijf
wiki_id: '533'
---
## T-disk

On [pc’s in computer labs](/en/howto/windows-beheerde-werkplek/) which are managed by
C&CZ, not only the [S-disk](/en/howto/s-schijf/) is available but also
the T-disk (a [network share](/en/howto/netwerkschijf/)). On this network
share, a large number of programs for a large number of courses is
available that can be run directly from this share (no local
installation on the pc necessary). Lecturers can manage the course
software themselves. These programs may also be used on pc’s that are
not managed by C&CZ.

[[netwerkschijf\|Attach the network share] as
**\\\\cursus-srv.science.ru.nl\\cursus** using Drive letter T because
some software depends on this letter to be T.

Check “Reconnect at logon” for your convenience.
