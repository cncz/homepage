---
author: Postmaster
date: '2016-09-21T08:20:41Z'
keywords: []
lang: en
tags:
- medewerkers
title: Medewerkers latexbrief
wiki_id: '82'
---
## RU Corporate Identity Information

Starting September 1, 2004, a new corporate identity has been introduced
in which the name and logo of the university changed, but also the
layout of all printed material. For MS-Word, templates and manuals have
been centrally developed. Information can be found
[elsewhere](https://www.ru.nl/en/services/campus-facilities-buildings/communication-and-promotion/design/corporate-identity). For LaTeX C&CZ developed a
new letter layout.

### RU letter layout for LaTeX

The new version supports an English and a colored version of the logo.

On the C&CZ Unix systems one can find in
/usr/local/share/texmf/tex/latex/rubrief/ on linux machines the
following files:

`RUlogo.eps                postscript version of the logo`\
`RUlogo.jpg                jpg  version of the logo for pdflatex`\
`fnwi.tex                  example file for address information`\
`rubrief.cls               the documentclass file for the rubrief letter layout`\
`RU_voorbeeld.tex          an example letter for the new corporate identity`

On Windows PC’s it is also possible to use the newest version of LaTeX
including the rubrief style, by attaching the
[S-disk](/en/howto/s-schijf/).

Then
you’ll find the above mentioned files in the folder:
S:\\texlive\\texmf-local\\tex\\latex\\rubrief.

Copy `RU_voorbeeld.tex` and `fnwi.tex` to a place wher you can write
them. The rubrief style is an adapted version of the `letter` class and
can be used like that. Look at the example to see which macro’s exist to
supply address and sender information. The logo can be printed in Dutch
as well as in English and one can choose a color version of the logo
(option color). Please note that you’ll have to use `dvips` yourself
before sending the dvi-file to a Postscript printer. Finally, it is also
possible to make a pdf version of a letter with the command pdflatex,
this will then immediately contain the logo.
