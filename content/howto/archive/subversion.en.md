---
author: sioo
date: '2016-06-01T07:34:28Z'
keywords: [ subversion ]
lang: en
tags:
- backup
title: Subversion
wiki_id: '689'
---

{{< notice info >}}
svn.science.ru.nl has been turned off!

[GitLab](/en/howto/gitlab/) is the successor of our Subversion service!
{{< /notice >}}

## Migrating from Subversion to Gitlab

We have automated the migration from
[Subversion](https://en.wikipedia.org/wiki/Apache_Subversion) to GitLab.
Contact postmaster\@science.ru.nl if you wish to have clone access to
your migrated repository.

## Manual migration from Subversion to Gitlab

Inspired by [this short
manual](https://gist.github.com/leftclickben/322b7a3042cbe97ed2af), an
SVN to git conversion could boil down to something like:

``` bash
cd /tmp/
git svn clone --no-metadata -A users.txt https://svn.science.ru.nl/repos/myproject
cd myproject
git remote add gitlab git@gitlab.science.ru.nl:yourscienceloginname/myproject.git
git push --set-upstream gitlab master
```

Git URLs (https and ssh) are shown on the top of the project page in
gitlab. The generic repository URL for the [science SVN
server](/en/howto/subversion/) is:

[`https://svn.science.ru.nl/repos/{repository}`](https://svn.science.ru.nl/repos/%7Brepository%7D)

And for CS:

[`https://svn.cs.ru.nl/repos/{repository}`](https://svn.cs.ru.nl/repos/%7Brepository%7D)

You can also see this for public svn repositories on
<https://svn.science.ru.nl> (viewing private repositories requires
authentication).

## Migrating to a private SVN repository

It is possible to continue to use subversion for your personal
repository. In that case, you C&CZ can help you move your repository off
the svn server to your home directory. From that point, you can access
the repository via one of the [login
servers](/en/howto/hardware-servers#linux-.5bloginservers.5d.5blogin-servers.5d/).

## Old Subversion wiki page

The old subversion wiki page is archived
[here](/en/howto/oldsubversionpage/)
