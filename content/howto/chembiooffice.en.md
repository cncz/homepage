---
author: polman
date: '2022-04-05T11:52:47Z'
keywords: []
lang: en
tags:
- software
title: ChemDraw Prime / ChemBioOffice
wiki_id: '690'
---
Starting December 2022 the ChemOffice+ license is being replaced by a ChemDraw Prime campus license.

ChemDraw Prime is a complete “entry-level” drawing program that provides all the tools you need to quickly create chemically intelligent, publication-ready chemical structures and reactions, laboratory notes and experiment write-ups. As well as a full set of chemical structure essentials such as rings, bonds, chains, atoms and functional groups, ChemDraw Prime includes chemical and lab equipment templates and handy TLC and Gel Electrophoresis Plate drawing tools.

ChemDraw Prime includes the following features:

- Analyze and check structures
- Structure clean-up
- Expand and contract labels
- Create and use nicknames
- Tetrahedral and geometric stereochemistry, including absolute and relative
- Multicenter attachment points for haptic and other pi bonds
- Chemical polymer tools
- Calculate properties including pKa, LogP, LogS and tPSA and hotlink to structure
- Read and write all common chemical and graphics files
- Read JCamp and Galactic spectra files
- Fragmentation tools
- Special “copy/paste as” command for CDX, CDXML, molfile, SMILES, InChI and InChIKey (copy only)
- In-place OLE editing of ChemDraw objects

ChemDraw is actively used in multiple chemical research groups within the Science Faculty and in the curriculum of the molecular sciences programs. Because of the user-friendliness and intuitive use of the software it is highly appropriate for student use. 

Everyone with an e-mail account ending in @science.ru.nl, @ru.nl or @student.ru.nl is eligible to download the software and install it 3 times maximum.

The installation files for ChemDraw Prime (Windows and Mac-OS) are available on the Revvity Signals webpage:

**New users**

New users first need to register at Revvity Signals via

<https://connect.revvitysignals.com/sitesubscription/>

- Search Radboud University
- **Important** Choose the **upper**  Register button. 
- Fill out your name, the address of the faculty and your Radboud email-address. And click “Submit”
- After submitting the data you will receive a verification email on the supplied address.
- In the email click “Create Account” 
- On the “Sign Up” tab create a new account, use your Radboud email-address (the one you used to register) and a self chosen password, and click “Sign Up”.
- After a short while you will be re-routed to the “List Entitlements”, which shows are active licenses.
- Write down or copy the 'Activation code'(you will need it during the install) and click the “Download Now” link in the last column of the ChemDraw Prime entry.
- Download the installation files for your operating system.

**Existing  users**

Excisting Revvity Signals users can login via https://revvitysignals.flexnetoperations.com/.

- Click 'List Entitlements' in the menu on the left-hand side of the page.
- Write down or copy the 'Activation code'(you will need it during the install) and click the “Download Now” link in the last column of the ChemDraw Prime entry.
- Download the installation files for your operating system.


After Activation ChemDraw-prime should work until April 5, 2026.
