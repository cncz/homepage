---
author: bram
date: '2023-01-16T15:12:45Z'
keywords: []
lang: en
title: Mounting a network share
tags:
- storage
aliases:
- netwerkschijf
wiki_id: '685'
cover:
  image: img/2023/leaf.jpg
ShowToc: true
TocOpen: true
---
# What path to mount?
The [storage page](../storage) describes the paths for your [home directory](../storage#home-directory-paths) and [network shares](../storage#network-share-paths). Once you know what network share to mount, follow the steps for your operating system below.

{{< notice info >}}
A [VPN](../vpn) connection is required when accessing these paths from outside of the RU network.
{{< /notice >}}

> If you have trouble connecting to a network share after following the steps below, do not hessitate to [contact us](../contact). We'll be happy to assist you!

# Windows

To assign a drive letter to a network share:

- Open file explorer and go to _This PC_
- In the ribbon select _Computer_ -> _Map network drive_ -> _Map network drive_
- Choose a drive letter and fill in the _network share path_, which starts with `\\`
- Select _Reconnect at sign-in_
- Select _Connect using different credentials_
- Click _Finish_
- Fill in your [Science loginname](../login) and your password
- Select _Remember my credentials_ and click _Ok_

> The network drive should now be visible in Explorer. If not? [Contact us](../contact)!

# macOS

To connect to a network share on a macOS system:

- Open Finder
- In Finder Under the _Go_ menu, choose _Connect to Server_ 
- Or use the _Command + K_ keyboard short cut to get ther in one click
- Specify the share path, which starts with `smb://...`, and click _Connect_
- Use your [Science account](../login) and your password to authenticate

> The network drive should now be available in Finder. If not? [Contact us](../contact)!

# Linux

To mount a network share on a Ubuntu Linux system:

- Open the file manager _Files_
- On the left side, click _+Other Locations_
- In the bottom bar, at _Connect to Server_, enter the network share path, which starts with `smb://...`
- Click the _Connect_ button
- A dialog pops up that asks for authentication
- For _Connect as_, select _Registered User_
- Fill in your [Science account](../login) and your password
- (Leave the suggested field _Domain_ unchanged, it's value actually does not matter)
- Choose weather or not your system should remember your password
- Hit hit the _Connect_ button to finalize

> The network drive should now be available in 'Files'. If not? [Contact us](../contact)!

# Mobile devices

Mobile devices have no built-in or standard way of accessing network shares. Various apps fill in that gap.
These are apps that support accessing network shares:

| Android                                                                                                                                                                                               | Description  |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------ |
| [Astro File Browser](https://play.google.com/store/apps/details?id=com.metago.astro) + [Astro SMB module](https://play.google.com/store/apps/details?id=com.metago.astro.smb)                         | File manager |
| [SolidExplorer](https://play.google.com/store/apps/details?id=pl.solidexplorer2)                                                                                                                      | File manager |
| [SyncMe Wireless](https://play.google.com/store/apps/details?id=com.bv.wifisync)                                                                                                                      | Sync app     |
| [FolderSync Lite](https://play.google.com/store/apps/details?id=dk.tacit.android.foldersync.lite) or [FolderSync Pro](https://play.google.com/store/apps/details?id=dk.tacit.android.foldersync.full) | Sync app     |

| iOS                                                                                   | Description                                |
| ------------------------------------------------------------------------------------- | ------------------------------------------ |
| [FileBrowser](https://apps.apple.com/nl/app/filebrowser-document-manager/id364738545) | File manager with built-in document viewer |
| [GoodReader](https://apps.apple.com/nl/app/goodreader-pdf-editor-viewer/id777310222)  | Primarily for PDF documents                |
