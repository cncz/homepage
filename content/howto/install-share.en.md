---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
noage: true
lang: en
tags:
- software
title: Install share
wiki_id: '811'
---
Most licensed software that can be freely used by students and staff of the Science faculty can be found in the `install` network share.

You can connect the install share in MS Windows using the path:

```
\\install-srv.science.ru.nl\install
```

{{< notice info >}}
Consult [this page](../storage/#network-share-paths) on how to connect to a network share.
{{< /notice >}}

It is often possible to install this software directly from the install
share. Usually a license key is needed. Install info and licenses
can be found on the [Science licenses pages](https://cncz.pages.science.ru.nl/licenses/). If you need more info, please contact
[postmaster](../contact).

Every software package has its own license requirements. Sometimes it
can only be used on university owned computers, sometimes it can even
be used by students at home.
