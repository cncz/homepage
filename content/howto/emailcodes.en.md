---
author: bram
date: "2022-09-23T07:16:08Z"
keywords: []
lang: en
tags:
  - email
title: EmailCodes
wiki_id: "926"
---

## Email codes overview

### ALICE-1

This is an email in which students are requested to participate in the
digital course evaluation.

- Recipients? All students that are registered in OSIRIS for the exam
  of the corresponding course.
- When? Typically on the day of the exam (around 17:00).

### ALICE-2

This is an email reminder in which students are requested once again to
participate in the digital course evaluation.

- Recipients? All students that are registered in OSIRIS for the exam
  of the corresponding course.
- When? Five days after the ALICE-1 email, so typically five days
  after the exam.

### ALICE-3

This is an email in which the feedback of the course are sent to the
students.

- Recipients? All students that were invited to participate in the survey.
- When? The feedback of courses of a given quarter are sent at the end
  of the following quarter.

### ALICE-4

This is an email in which the feedback of the course is sent to the
lecturer/course coordinator.

- Recipients? The lecturer/course coordinator of the course.
- When? The feedback of courses of a given quarter are sent at the end
  of the following quarter.

### CUDO-1

This is an email by which the course coordinator is notified that the
student course evaluation is completed (and can be found in the course
dossier) and that subsequently the lecturer’s course evaluation form can
be submitted to the course dossier.

- Recipients? The course coordinator.
- When? When the student course evaluation is completed, typically two
  weeks after the exam.

### CUDO-2

This is an email by which the course coordinator is reminded that the
student course evaluation is completed (and can be found in the course
dossier) and that subsequently the lecturer’s course evaluation form can
be submitted to the course dossier.

- Recipients? The course coordinator.
- When? Two weeks after the student course evaluation is completed,
  typically four weeks after the exam.

### CUDO-3

This is an email sent to the programme committee (“OLC”) members listing
courses that should be ready for evaluation in the week after.

- Recipients? The secretary/chairman of a Programme Committee (“OLC”).
- When? Typically five weeks after the exam.

### PEERFEEDBACK-1

This is an email sent to all students who participate in a [PeerFeedback](https://peerfeedback.edutools.science.ru.nl)
evaluation which just opened.

- Recipients? All students who are registered in the BrightSpace group for which the evaluation has been enabled.
- When? Within five minutes after the opening of the evaluation.

### PEERFEEDBACK-2

This is a reminder to all students who have an uncompleted evaluation
in [PeerFeedback](https://peerfeedback.edutools.science.ru.nl).
For each evaluation, the instructor can choose how often a reminder is sent.
We send a maximum of one email per day, so if necessary, we bundle the reminders for multiple open evaluations in one
email.

- Recipients? Studens who have an uncompleted evaluation in PeerFeedback.
- When? Every day around 06:00.

### REMA-1

This is an email sent after a new incident has been reported through
LimeSurvey Incident Form.

- Recipients? Employees of the Department of Occupational Health and
  Safety and Environmental Service (AMD) of the faculty where the
  incident took place.
- When? Immediately after a form has been submitted.

### ROUNDCUBE-1

- Recipients? Users of the [Roundcube](http://roundcube.science.ru.nl)
  webmail service.
- When? If pre-existing Sieve filters are present on the mail server
  when changing mail filters in Roundcube.
- Why? Pre-existing filters will remain active, but are not shown in
  Roundcube.
- What to do? Remove the pre-existing Sieve filters and configure
  everything with Roundcube. If this makes no sense to you, [contact
  C&CZ!](/en/howto/contact/).

### DIY-1

This mail will be sent if the check date of a [Science login](../login) has been
reached.

- Recipients? IT contact persons.
- When? Monday 10am. Only in case the check date of a Science login
  has been reached.

On the "My accounts" page of [DIY](http://diy.science.ru.nl), you can see for
which Science logins you are the IT contact person.

### DIY-2

This mail will be sent if the check date of your [Science login](../login) has been
reached.

- Recipients? Owner of the account.
- When? At 10:00 on the day that the check date has been reached.

### LOGIN-1

This mail will be sent if a [Science login](../login) has been created.

- Recipient? The person for whom a Science login has been created.
- When? Each day at 7pm.

### LOGIN-3

This mail will be sent if a [Science login](../login) is about to expire.

- Recipient: The designated contact person for the [Science Login] set to expire.
- When? Every monday (if applicable)

### NEWLOGIN-1

This mail will be sent to the requester of [Science logins](../login).

- Recipients? Requesters of Science logins.
- When? As soon as the Science login is created.

### WELCOME-1

This mail will be sent to new [Science logins](../login).

- Recipients? Users of Science logins.
- When? One hour after the Science login is created.

### PASSWORDRESET-1

This mail will be sent to give you a new initial password for your
[Science login](../login).

- Recipient? The person who requested a password reset.
- When? Directly after the password has been reset by a system
  administrator.

### PASSWORDRESET-2

This email is being sent to you as the contact person following the submission of a password reset request for one of the [Science logins](../login) managed by you.

- To whom? Requester of a password reset.
- When? Immediately after the password has been modified by a system administrator.

### VOLUME-1

This email is sent to the requester of a network drive.

- To whom? The owners and or members of the group linked to the [network drive](../storage/#network-shares).
- When? As soon as the drive is created, migrated or upon request.

{{< rawhtml >}}
<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" height=1800>
{{< /rawhtml >}}
