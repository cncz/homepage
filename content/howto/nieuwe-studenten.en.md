---
author: bram
date: '2022-09-19T13:25:45Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
title: New students
wiki_id: '164'
---
## C&CZ Information for new students and employees

### Two logins

To get access to computer resources, one usually needs a username and a
password. As a student or employee of the Faculty of Science, one gets 2
logins: a RU-number and a Science-name. Below you can read for which
services each of them can be used. The Faculty of Science tries to make
faculty services accessible with the RU-number, but at this moment this
is not yet possible for all services.

#### RU number (U123456 / s1234567)

-   Students: At the start of a study at Radboud University, all
    students receive a letter with their student number (something like
    *s1234567*) as a username and a **RU password**. A student number
    cannot be changed. Read about the RU password, where it can be used
    and how it can be changed on the [RU
    password](https://www.ru.nl/ict-uk/general/password/) web page.

Students with questions about their RU-account or student.ru.nl mailbox,
please contact [Student
Affairs](http://www.ru.nl/studenten/actueel/contactadressen/) directly.

-   Employees: A new employee gets a personnel number (U-number,
    something like *U123456*) from [the personnel
    department](http://www.ru.nl/fnwi/pz/), that cannot be changed. One
    can change this RU-password through the [RU IdM
    website](http://www.ru.nl/wachtwoord), where one can also read where
    it can be used. For employees of the Faculty of Science,
    [C&CZ](/en/howto/contact/) can help in case of problems and reset the
    password.

#### Science login (isurname)

Because the Faculty of Science since long has had faculty IT services,
that could only be used by faculty staff and students, and often it is
impossoble to use the RU-number for the service, every staff member and
student is also entitled to a **Science** login name, that is based on
initials, given name and/or surname, something like *jsmith*. This login
name cannot (easily) be changed. The Science password can be changed on
the [Do It Yourself website](http://diy.science.ru.nl/), that also
explains which passwords are allowed. In general the rules are a bit
less strict than the newer RU password rules. E.g., a RU password must
be at least 8 characters long, a Science password at least 6. When using
a Science-login, students should follow the [login usage
rules](/en/howto/login/).

The two logins, RU number or Science name, can thus easily be
distinguished. The passwords are (initially) different, of course you
can choose to make them the same. Below a list of the most important
services you can use with these logins.

### Which login for which services?

PC’s in [computer labs and study area](/en/howto/terminalkamers/) can be
used with both logins: domain B-FAC with Science loginname
(B-FAC\\jdevries) or domain RU with student/personnel number
(RU\\s1234567). If on campus a PC lists B-FAC as a possible login
domain, then one can login with the Science account. Examples are the
[PC’s for all
RU-students](http://www.ru.nl/studenten/voorzieningen/pc-voorzieningen_0/).

-   With the faculty **Science** login you can use:
    -   The [Do It Yourself website](http://diy.science.ru.nl/) to
        change your password and other settings of your Science account.
    -   Science [network disk space](/en/howto/diskruimte/).
    -   Linux [login-servers](/en/howto/hardware-servers/).
    -   Mail: [Science mail](/en/tags/email). The corresponding email
        address ends in **science.ru.nl**, for example:
        J.Smith\@student.science.ru.nl. Extra email addresses of the
        form John.Smith\@student.science.ru.nl can be requested. Faculty
        mailings will use this address. It is possible, if needed, to
        automatically forward this mail to a different email address
        through the [Do It Yourself website](http://diy.science.ru.nl/).
-   With the **RU** login, you get access to [some 100 RU
    applications](http://www.ru.nl/idmuk/faq/frequently_asked/#CanIusetheRUaccount),
    the most important ones are:
    -   Printers: all
        [Peage](http://www.ru.nl/fb/english/print/printing-peage-0/)
        printers.
    -   The RU [Portal](http://portal.ru.nl).
    -   The RU [Brightspace](https://brightspace.ru.nl/) electronic
        learning environment.
    -   The RU mailservice, with [@ru.nl and/or @student.ru.nl email (RU
        Exchange Outlook Web App email)](http://mail.ru.nl/) The
        corresponding email address is John.Smith\@ru.nl or
        J.Smith\@student.ru.nl. University mailings will use this
        address. If you do not want to read your mail on this mail
        server, then have it automatically forwarded to a different
        email address. See the [RU Mail
        manual](http://www.ru.nl/ict-uk/staff/mail-calendar/user-guides/forwarding-your-mail/)
        on how to do this.
    -   Wi-fi: the [Eduroam](http://www.eduroam.org/) wireless networks
        with username `U-number@ru.nl`.
    -   Software: [SURFspot](http://www.surfspot.nl/), for relatively
        cheap software, often also for home use.
    -   RU Intranet: [RadboudNet](http://www.radboudnet.nl).
    -   [BASS](/en/howto/bass/), the RU financial/logistic system
        (BASS-FinLog) and personnel system (BASS-HRM).
