---
author: petervc
date: '2021-02-12T13:40:08Z'
keywords: []
lang: nl
tags:
- hardware
title: WacomOne
wiki_id: '1068'
---
## Tips for Wacom One from teacher Sebastiaan Terwijn

A few tips for the use of the Wacom One pen display.

To connect the device, you need a computer with both a USB and an HDMI
connection. If your computer does not have these, you will need an extra
converter cable, for example HDMI to DisplayPort or mini-DisplayPort. My
Thinkpad has the latter.

The Wacom One is significantly larger than a standard iPad (13 inch vs
10.2 inch), which is a big plus in case you are using this as an
alternative for a blackboard.

This is not a standalone device: it is a display, it needs to be
connected to a computer to work. The Wacom comes with a pen. (This in
contrast to an iPad, for which an extra pen costs € 135.) The pen has no
battery, and does not need charging.

You will also need software. Wacom suggests to use Bamboo, which can be
downloaded for free, even if you don’t have a Wacom display. Both Apple
and Microsoft also have their own software, for example Microsoft Ink.
The latter works well for me, but has the disadvantage that you only
have one page/whiteboard, so you cannot do proofs that take multiple
pages, or flip back and forth between pages/whiteboards. Bamboo does
have multiple pages, but at first it had the drawback that it was
\*slow\*. For example, it would miss all dots/points/umlauts that I
would write at normal speed.

In the Wacom Desk, you can set the pen pressure, but this does not make
much difference for how fast you can write. However, you can also set
the double click speed, and this \*does\* make a difference. This is
also mentioned at the Wacom troubleshoot page, so apparently this is a
common problem. Setting the double click speed to zero (off) makes a
huge difference. I can now write almost at a normal speed.

In principle, you can use all drawing software, such as MS Paint, but I
don’t know of any other writing applications. If you know of one that is
simple, fast, and has multiple pages, please let me know.

The [Openboard](https://openboard.ch/index.en.html) application for
macOS/Windows/Ubuntu is nice open-source “white-board” software to use
in combination with the Wacom pen display. It provides an intuitive user
interface and the option to use multiple pages and switch between them
fast. Under Windows, the application has a tendency to start on your
main screen and not on the pen display. To make the application switch
screen you can press windows key + shift + left or right arrow.

## Calibration button unavailable on macOS 11.2

If you experience problems with the calibration button not being
available after being asleep, there a few things you can try:

-   “Try adding the driver to your Login Items. It might help but as I
    mentioned before, something in your system is preventing the driver
    from starting properly.” [Wacom Forum
    link](http://forum.wacom.eu/viewtopic.php?f=5&t=4228)
-   “If I manually deactivate and again activate the wacom tablet driver
    in the system preferences/security/input monitoring, then the
    calibration works again.” (Nadine Hauptmann)

## External Wacom One pen display on Linux

The Wacom One pen display is well supported on modern Linux
distributions, it seems to be supported in Ubuntu 20.04 and probably
also Ubuntu 18.04 LTS.

Some things you may run into though…

#### Stylus mapping doesn’t match the Wacom One screen

This is probably trivial to someone who knows how X11 works, but for
non-expert users it can be a bit tricky to figure out.

The information below is probably old and now has been integrated in the
plasma system settings, if you go to System Settings -\> Input Devices
-\> Graphic Tablet Go to the “tablet” tab and select the button “Map
tablet area to screen”. In the upper left of the pop-up window, you can
toggle which screen the tablet area is mapped to.

Copied from: [KDE
forum](https://forum.kde.org/viewtopic.php?f=139&t=125532#) (I’m not
sure if this is true for Gnome as well, or on Wayland systems)

`   $ xinput  # get the IDs for all relevant pieces of my tablet.`\
`   ⎡ Virtual core pointer                       id=2   [master pointer  (3)]`\
`   ⎜   ↳ Virtual core XTEST pointer                 id=4   [slave  pointer  (2)]`\
`   [...]`\
`   ⎜   ↳ Wacom Bamboo 16FG 4x5 Pen stylus           id=17   [slave  pointer  (2)]`\
`   ⎜   ↳ Wacom Bamboo 16FG 4x5 Pen eraser           id=20   [slave  pointer  (2)]`\
`   ⎜   ↳ Wacom Bamboo 16FG 4x5 Pad pad              id=21   [slave  pointer  (2)]`\
`   ⎜   ↳ Wacom Bamboo 16FG 4x5 Finger               id=22   [slave  pointer  (2)]`\
`   ⎣ Virtual core keyboard                      id=3   [master keyboard (2)]`\
`       ↳ Virtual core XTEST keyboard                id=5   [slave  keyboard (3)]`\
`       ↳ Power Button                               id=6   [slave  keyboard (3)]`\
`       ↳ Power Button                               id=7   [slave  keyboard (3)]`\
`       ↳ Microsoft Microsoft® Nano Transceiver v2.1   id=13   [slave  keyboard (3)]`\
`   [...]`\
`   $ xrandr  # get the names of my displays`\
`   Screen 0: minimum 8 x 8, current 3840 x 1200, maximum 16384 x 16384`\
`   DVI-I-0 disconnected (normal left inverted right x axis y axis)`\
`   [...]`\
`   DVI-I-3 connected 1920x1200+1920+0 (normal left inverted right x axis y axis) 546mm x 352mm`\
`      1920x1200     59.95*+`\
`      1920x1080     60.00 `\
`      1680x1050     59.95 `\
`   [...]`\
`   DP-1 connected 1920x1200+0+0 (normal left inverted right x axis y axis) 546mm x 352mm`\
`      1920x1200     59.95*+`\
`      1920x1080     60.00    59.99    59.94    50.00    60.05    60.00    50.04 `\
`      1680x1050     59.95 `\
`   [...]`\
`   $ xinput map-to-output 17 DVI-I-3`\
`   $ xinput map-to-output 20 DVI-I-3`\
`   $ xinput map-to-output 21 DVI-I-3`\
`   $ xinput map-to-output 22 DVI-I-3`
