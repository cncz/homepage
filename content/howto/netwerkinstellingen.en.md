---
author: bram
date: '2022-09-19T16:33:11Z'
keywords: []
lang: en
tags:
- netwerk
title: NetwerkInstellingen
wiki_id: '1089'
---
## Network configuration

Almost all systems connected to the network will by default use
**DHCP**. This will configure the following settings:

-   IP address and network mask
-   Default Gateway for the subnet of the IP address
-   Nameservers to resolve names to IP addresses.

The IP address, network mask and default gateway are hard to figure out
without DHCP. In specific cases it may be necessary to configure these
settings manually, like when the hardware doesn’t support DHCP.

**NB** The **nameserver** settings for manually configured machines are:

-   **131.174.30.40**
-   **131.174.16.131**

The use of external nameservers, like *8.8.8.8* or *9.9.9.9* is
discouraged, because these servers are not managed by the University and
therefore do not fall under the security policies of the RU.
