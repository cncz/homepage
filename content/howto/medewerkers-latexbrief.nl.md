---
author: Postmaster
date: '2016-09-21T08:20:41Z'
keywords: []
lang: nl
tags:
- medewerkers
title: Medewerkers latexbrief
wiki_id: '82'
---
## RU Huisstijl Informatie

M.i.v. 1 september 2004 is de nieuwe huisstijl ingevoerd waarbij de naam
en logo van de universiteit veranderd zijn maar ook de lay-out van alle
drukwerk. Voor Word-toepassingen zijn centraal, in opdracht van de
Eenheid Communicatie, sjablonen en handleidingen ontwikkeld. Info is
[elders](https://www.ru.nl/services/campusfaciliteiten-gebouwen/communicatie-en-promotie/vormgeving-en-opmaak/huisstijl) te vinden. Voor
het gebruik van LaTeX is door C&CZ een nieuwe briefstijl ontwikkeld.

### RU Briefstijl voor LaTeX

De nieuwe versie ondersteunt een Engelse en een gekleurde versie van het
logo.

Op de C&CZ Unix systemen staan in
/usr/local/share/texmf/tex/latex/rubrief/ op linux machines onder andere
de volgende bestanden:

`RUlogo.eps                postscript versie van het logo`\
`RUlogo.jpg                jpg  versie van het logo voor pdflatex`\
`fnwi.tex                  voorbeeldbestand voor afzendergegevens`\
`rubrief.cls               het documentclass bestand voor de rubrief huisstijl`\
`RU_voorbeeld.tex          een voorbeeld brief voor de nieuwe huisstijl`

Ook op windows pc’s is het mogelijk om de nieuwste LaTeX versie te
gebruiken inclusief de rubrief stijl, door de
[S-schijf](/nl/howto/s-schijf/) aan te koppelen.

Dan vind je de bovenbeschreven bestanden in de directory
S:\\texlive\\texmf-local\\tex\\latex\\rubrief.

Kopieer `RU_voorbeeld.tex` en `fnwi.tex` naar een voor jezelf
schrijfbare directory. De rubrief stijl is een aangepaste versie van de
`letter` class en is grotendeels ook als zodanig te gebruiken. Bekijk
het voorbeeld om te zien welke macro’s er zijn om adres- en
afzendergegevens vast te leggen. Het is mogelijk het logo in het Engels
danwel in het Nederlands af te drukken en men kan kiezen voor een
kleurenversie van het logo (optie color). Denk eraan dat als je ook het
logo wilt afdrukken dat je zelf `dvips` moet gebruiken voordat je het
dvi-bestand naar een Postscript printer stuurt. Tenslotte is het ook
mogelijk direct een pdf versie van een brief te maken met behulp van het
commando pdflatex, deze bevat dan al direct het logo.
