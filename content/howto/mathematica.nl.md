---
author: petervc
date: '2021-12-05T23:34:06Z'
keywords: []
lang: nl
tags:
- software
title: Mathematica
wiki_id: '150'
---
[Mathematica](http://www.wolfram.com/products/mathematica/) is een
wiskundig softwarepakket van [Wolfram Research](http://www.wolfram.com/).

C&CZ beheert de netwerk-licentie voor het 
gelijktijdig gebruik van Mathematica binnen de RU.

De licentie voor een klein aantal (ca. 20) gelijktijdige gebruikers,
wordt door C&CZ betaald en mag door staf en studenten van FNWI gebruikt
worden.  Tot eind 2023 gold tijdelijk een ongelimiteerde licentie voor
alle RU-medewerkers en -studenten.

Gebruikers/afdelingen van andere delen van de campus kunnen Mathematica gebruiken tegen een jaarlijkse vergoeding
van de kostem.

Bij [C&CZ systeembeheer](/nl/howto/systeemontwikkeling/) zijn de
installatie- en licentie-informatie te krijgen. 

Het gebruik van Mathematica via de license server wordt gelogd.

Op de door C&CZ beheerde Linux PCs is Mathematica in /vol/mathematica te
vinden. Op de MS-Windows PCs is Mathematica op de S-schijf (software)
beschikbaar.
