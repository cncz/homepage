---
author: miek
date: '2025-03-10T12:12:45Z'
keywords: [ dyi ]
lang: en
title: DIY
tags:
- dyi
- docs
#cover:
#  image: diy.jpg
---

The [DIY](https://diy.science.ru.nl) server that C&CZ maintains allows people to control certain
properties of their science account and change/view their used services.

The pages here contain extra documentation for certain features. We strive to keep DIY intuitive and
lean. Some things might require more explanation, instead of trying to fit this into the DIY website
we explain those here.

## SSH Public Keys {#pubkey}

Ever since [CPK 1383](https://cncz.science.ru.nl/nl/cpk/1383/), SSH key usage needs "go through us",
to prevent phisher to update those keys as well. Currently two uses for these keys are defined:

* `login`: this key allows you to login via SSH.
* `superchown`: this key allows usage of [superchown](https://cncz.science.ru.nl/en/news/2024-04-09_superchown/).
