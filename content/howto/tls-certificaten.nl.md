---
author: remcoa
date: '2022-10-06T15:13:58Z'
keywords: []
lang: nl
tags:
- internet
title: TLS Certificaten
wiki_id: '68'
---
# TLS certificaten

Iedere service die beschikbaar is via TLS (https), moet een TLS
certificaat hebben, zo ook elke webserver met versleutelde of
“beveiligde” inhoud. Een TLS
(Transport Layer Security, zie [RFC 8446](https://www.rfc-editor.org/rfc/rfc8446))
certificaat is een
elektronisch getekende garantie dat een bepaalde server daadwerkelijk de
server is die deze claimt te zijn. Ze worden hoofdzakelijk (maar niet
alleen) gebruikt om webpagina’s te geven via een versleutelde
verbinding. Een certificaat is getekend door een Certificate Authority
(CA), deze verzekert de integriteit van het certificaat.

Een aantal Certificate Authorities wordt standaard vertrouwd door TLS
clients (inclusief web-browsers), o.a. Let's Encrypt, Verisign, Thawte en Terena. Dat
wil zeggen dat certificaten door een van deze getekend zonder meer
vertrouwd worden zonder tussenkomst van de gebruiker. Alle C&CZ servers
en webapplicaties zijn voorzien van door Terena (via SURFdiensten)
getekende certificaten.

# Certificaten verkrijgen

Omdat een TLS Certificaat gebruikt wordt als **bewijs** dat de website
of server valide is, kan niet zomaar iedereen een gesigneerd certificaat
aanvragen voor een willekeurige domeinnaam. De Certificate Authority
controleert dat de aanvrager de rechtmatige eigenaar is van de
domeinnaam waarvoor het certificaat wordt aangevraagd. Domeinnamen die
via C&CZ worden [geregistreerd](/nl/howto/domeinnaam-registratie/) komen
op naam van de Radboud Universiteit te staan. Voor zulke domeinnamen kan
C&CZ ook een TLS Certificaat aanvragen.

