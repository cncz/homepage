---
author: bram
date: '2022-11-02T10:08:04+01:00'
keywords: ['contact']
lang: en
tags: ['faq']
title: Contact
wiki_id: '816'
weight: 1000
aliases:
- helpdesk
---
For all ICT related questions and problems within FNWI please contact
C&CZ.

## Helpdesk & [Sysadmin](../systeemontwikkeling)
| phone                               | room       | mail                     |
|-------------------------------------|------------|--------------------------|
| [+31 24 36 20000](tel:+31243620000) | `HG00.051` | helpdesk@science.ru.nl   |

## Visiting address

We are located on in the front part of wing 5, ground floor of the Huygens Building (rooms `HG00.051`, `HG00.509` and `HG00.545`). The helpdesk is available during office hours (8:30 - 17:00).
During that time the office is staffed.

> Full address:
> Huygens Building, HG00.051, Heyendaalseweg 135, 6525 AJ  Nijmegen
>
> Postal address:
> Radboud University Nijmegen, Faculty of Science, C&CZ, P.O. Box 9010, 6500  GL Nijmegen

The page about [how to reach the campus](https://www.ru.nl/en/contact/getting-here) also lists all parkings.

# Other helpdesks

## ILS Helpdesk

Contact the ILS Helpdesk for questions about:
- ILS managed PC's
- [Printing](https://www.ru.nl/facilitairbedrijf/printen/printen/)
- `@ru.nl` mail addresses
- Desk and cell phones
- Network outlets

| phone                               | mail              | web                       |
|-------------------------------------|-------------------|---------------------------|
| [+31 24 36 22222](tel:+31243622222) | icthelpdesk@ru.nl | https://www.ru.nl/ict-uk/ |

Students with questions about their Radboud-account or `@ru.nl` address, please
contact [Student Affairs](https://www.ru.nl/en/departments/academic-affairs-departments/student-affairs) directly.
