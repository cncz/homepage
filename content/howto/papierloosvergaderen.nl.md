---
author: petervc
date: '2015-06-24T12:57:18Z'
keywords: []
lang: nl
tags: []
title: PapierloosVergaderen
wiki_id: '988'
---
## Papierloos vergaderen met Android tablets

De aanleiding om deze pagina te schrijven, is dat de
[FGV](http://www.ru.nl/fnwi/over_de_faculteit/organisatie/facultaire_0/)
per 1 september 2015 papierloos wil gaan vergaderen. Voor de eerste
instellingen van een Android-telefoon of -tablet, zie de
[Android](/nl/howto/android/) pagina. Hieronder worden de specifieke
onderwerpen voor het papierloos vergaderen beschreven, met als voorbeeld
de FGV: een app voor het synchroniseren van de vergaderstukken naar de
tablet en een voor het annoteren van PDF-bestanden. Als laatste wordt
beschreven hoe de geannoteerde bestanden gesynchroniseerd kunnen worden
naar een gebackupte schijf.

### Eerste benodigdheden voor de FGV

-   [Android](/nl/howto/android/) tablet of telefoon, die met een netwerk
    zoals Eduroam verbonden is.
-   [Science-login](/nl/howto/login/), dat door het Faculteitsbureau lid
    is gemaakt van de groep “fgv”.
-   Science [VPN](/nl/howto/vpn/) ingesteld en verbonden.

### Synchroniseren van en naar een netwerkschijf

-   Om je FGV-werk te kunnen doen is het nodig dat je zelf over een
    eigen set FGV-stukken beschikt, die op je tablet worden geplaatst en
    waarop je (op digitale wijze, zie verderop) aantekeningen kunt
    maken. Daarnaast is belangrijk dat er dagelijks een reservekopie van
    deze set stukken (incl. je aantekeningen) wordt gemaakt. Dat kan
    door je set stukken (incl. je annotaties) van je tablet naar je
    home-directory te kopiëren. Voor deze beide synchronisaties kun je
    eenzelfde app gebruiken. Een alternatief zou zin om je geannoteerde
    stukken naar jezelf te mailen. FolderSync (gratis met reclame:
    FolderSync Lite) is een app om van en naar een netwerkschijf te
    synchroniseren. Er bestaan ook alternatieve apps, bijv. SyncMe
    Wireless, maar FolderSync werkt naar behoren. De stappen worden
    hieronder verder uitgelegd.

-   Start FolderSync, druk onderaan op “Create new Sync”, geef het een
    naam en klik op “Volgende”.

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-32-29-foldersync-create-new-sync.png/)

-   Selecteer SMB/CIFS als Accounttype:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-32-49-foldersync-account-type.png/)

-   Geef er een naam aan, vul de gegevens bij Server en bij Inlog in en
    klik op Opslaan:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-49-02-foldersync-smb-account.png/)

-   Klik in het hoofdscherm op “Mappenparen”:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-49-46-foldersync-create-new-sync.png/)

-   Klik rechtsonder op de groene “+” om een mappenpaar toe te voegen:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-50-01-mappenpaar-toevoegen.png/)

-   Kies de bronmap die je naar je tablet wil synchroniseren en de
    locale map waar de bestanden op moeten terechtkomen:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-58-38-foldersync-synchronisatie-mappen.png/)

-   Kies bij “Planning” hoe vaak er gesynchroniseerd moet worden en druk
    op Opslaan:

[500px](/nl/howto/bestand:screenshot-2015-05-12-16-59-55-foldersync-synchronisatie-instellen.png/)

-   Door te klikken op “Sync” wordt er handmatig gesynchroniseerd:

[500px](/nl/howto/bestand:screenshot-2015-05-12-17-00-41-foldersync-gesynchroniseerd.png/)

-   Het maken van een mappenpaar om naar de homedirectory te
    synchroniseren gaat identiek. Zoek vooraf op [DHZ](/nl/howto/dhz/) de
    locatie van de homedirectory op. Als daar staat

`               Eigen schijf op     :   \\bundle.science.ru.nl\yourloginname`

dan moet als Server address bij het nieuwe account ingevuld worden:
<smb://bundle.science.ru.nl/yourloginname>

### PDF-bestanden annoteren

-   Een app voor annotatie van de PDF-bestanden: **Foxit MobilePDF**. Er
    zijn alternatieve apps, maar Foxit werkt naar behoren. Sla
    gewijzigde bestanden onder een andere naam op, zodat ze niet
    overschreven worden door de volgende synchronisatie.

[500px](/nl/howto/bestand:foxit-save-as.png/)
