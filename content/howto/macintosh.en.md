---
author: petervc
date: '2022-09-09T10:58:09Z'
keywords: []
lang: en
tags:
- software
title: Macintosh
wiki_id: '146'
---
## OS X software

### To install on your own Apple Mac computer

-   [Install-share](/en/howto/install-share/): a network drive with
    installable software.
-   The [ILS](http://www.ru.nl/ils) maintains a
    [list](http://gosoftware.hosting.ru.nl/) of RU licensed software.
-   Surfspot.nl: All kinds of licensed software (which can also be used
    at home) can be bought at [Surfspot](http://www.surfspot.nl/) when
    one logs in with employee/student number and
    [RU-wachtwoord](http://www.ru.nl/wachtwoord). Departments can order
    these software packages via
    [BASS](https://www.ru.nl/cif/cfa/bass-finlog/). The
    [ILS](http://www.ru.nl/ils) coordinates most RU-wide software
    licenses, mainly through [SURFdiensten](http://www.surfdiensten.nl/)
    and [SLIM](http://www.slim.nl/).
-   F-Secure: License codes for the [F-Secure](http://www.f-secure.com)
    security software can be found on
    [Radboudnet](http://www.radboudnet.nl/fsecure).
-   The [ChemBioOffice](/en/howto/chembiooffice/) software must be
    installed directly from the site of the supplier.
