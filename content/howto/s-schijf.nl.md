---
author: petervc
date: '2014-11-14T11:32:29Z'
keywords: []
lang: nl
tags:
- medewerkers
- studenten
- software
title: S-schijf
wiki_id: '509'
---
## S-schijf

Op door [C&CZ beheerde pc’s](/nl/howto/windows-beheerde-werkplek/) is standaard een
[Netwerkschijf](/nl/howto/netwerkschijf/) gekoppeld als S-schijf. Daarop
staan een groot aantal programma’s die vanaf die netwerkschijf gestart
kunnen worden. Ook op niet door C&CZ beheerde pc’s kan men hiervan
gebruikmaken.

Koppel de [netwerkschijf](/nl/howto/netwerkschijf/) aan als
**\\\\software-srv.science.ru.nl\\software**, gebruik Station: S omdat
sommige software misschien alleen werkt als de drive letter S is.
