---
title: High Performance Computing
author: sioo
date: 2024-11-05
keywords: []
tags: []
cover:
  image: img/2024/high-performance-computing.png
ShowToc: false
TocOpen: false
---
At the faculty of science, C&CZ manages a number of compute clusters and we
have our own computer room with a few dozen racks for server hardware and also some racks
in the *Forum* computer room.

## Financial model

The faculty in general doesn't own (m)any compute nodes, departments purchase their own servers (we can help configure these), which we install and manage during their lifetime.
All clusternodes can be integrated in our [SLURM](/en/howto/slurm/) cluster management.

For departments of FNWI, housing and power is "free", because we have currently no way to determine what to charge for it. This may change in the future as our monitoring equipment improves and policies change to include charging for power.

We also manage and host a separate cluster for the RU department [Centre for Language Studies](http://www.ru.nl/cls/). External customers have custom financial agreements.

Sometimes outside of opening hours also the pcs in the [computer labs](/en/howto/sleutelprocedure-terminalkamers/) are used as a compute cluster.

A [primitive overview of clusternodes](https://sysadmin.science.ru.nl/clusternodes/) is available, until we can fix something better to replace the old cricket overview.

## Other options

[Surf Sara](https://projectescape.eu/partners/surfsara-netherlands)

...

