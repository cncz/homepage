---
author: bram
date: '2022-09-19T16:32:11Z'
keywords: []
lang: en
tags:
- hardware
- software
- contactpersonen
title: Windows beheerde werkplek
wiki_id: '34'
---
## General

The ‘Managed PC’ is a standard Windows machine which gets installed with
standard software. The installation of the PC runs fully automated
without any user interaction. When such a ‘Managed PC’ gets installed an
employee of C&CZ needs approximately 5 minutes to start the installation
and to enter some initial data. Thereafter the installation runs fully
automated and will be finished after approximately 1 hour. After a
reboot the rest of the software is automatically installed. What that
rest of the software exactly is, depends also on the group to which the
PC belongs. For a list see below.

## Capabilities

Because the needs differ for employees and departments within the
Faculty of Science, there are different kind of ‘Managed PCs’:

### Fully managed PC

These are Windows machines that are completely managed by C&CZ. The user
doesn’t have to worry about operating system or other software, updates
and settings, these are managed by C&CZ. The user has *User*-rights, but
no *Power User* or *Administrator* rights. It is (almost always, there
are exceptions) impossible for the user to install software, update
drivers or change system settings. The idea is that all user files and
settings are saved on a network disk that is backupped regularly. The
computer hard disk isn’t used for savinf user data, but only for the
operating system and the standard software.\
If there are problems with a PC, one can easily move to a different
“Managed PC”.

For these machines C&CZ can give very good assurance for correctly
functioning operating system and installed software.

C&CZ will try to fix any problem with the operating system or software.
C&CZ helpdesk or sysadmins can also offer “Remote Assistance”. When
these problems can’t be solved in an acceptable timeframe, the machine
can be completely re-installed in about 1 to 2 hours.

### Department contact managed PC

In a department that needs more flexibility, own software, “strange”
hardware or different system settings, the IT-support person of the
department can get an extra account with “Administrator” rights on the
departmental “Managed PCs”. This makes it possible for that person to
install software and change system settings for the employees of his
department.

For these machines C&CZ can give good assurance for correctly
functioning operating system and installed software. C&CZ will make an
agreement with the departmental IT-support person.

C&CZ will try to fix problems with the operating system or software,
that the departmental contact can’t fix. C&CZ helpdesk or sysadmins can
also offer “Remote Assistance”. When these problems can’t be solved in
an acceptable timeframe, the machine can be completely re-installed in
about 1 to 2 hours. C&CZ can “package” departmental software on request
of the departmental IT-support person, to make it easy to install this
software on several departmental PCs.

This construction makes it possible to have important data on the
non-backed-up hard disk. Therefore it is not immediately clear that the
machine can be reinstalled in case of a problem that cannot be solved
within a reasonable timeframe.

### User managed PC

Again, this is a standard Windows machine which is also managed by C&CZ.
The operating system, software, updates and settings are basically
arranged centrally. The user normally does not have to worry about this
either. However, because the user needs more flexibility, his own
software, foreign hardware or other system settings, he gets an extra
account on the PC with Administrator privileges so that he can install
software and/or make changes to the system. C&CZ recommends that you
carry out your normal work on such a PC as a normal user with User
privileges and only use the account with the Administrator privileges
for software installation or adjusting system settings. We also
recommend using network drives to store files and settings, so that
files are backed up regularly. However, the temptation is often to still
use the local administrator account, then the risks for the PC and files
are larger in that case.

For these machines C&CZ can not guarantee the functioning of the
operating system and installed software. The user can perform all kinds
of actions with the local administrator account, which influence the
functioning of the PC.

C&CZ can try to solve problems with the operating system or software.
The amount of time that C&CZ can spend on problems with this kind of PCs
is of course limited. C&CZ is also usually able to provide remote
assistance on such PCs via the helpdesk or system management. It is
precisely in contrast to the fully managed workplace above that data can
be transferred to the internal hard disk on these types of machines. In
that case, it is not self-evident that the machine can be easily
reinstalled and the user/admin would prefer to solve the problems
themselves in order to save the data. In case these problems can not be
solved within an acceptable time, the machine can be reinstalled in
approx. 1 to 2 hours. After the standard installation, the user must
complete the installation of his own software, separate hardware or
system settings.

### Installed PC

Preliminary solution:\
One can install a standard Windows machine from the network in an
identical way. At the end of the installation, the ’’ Administrator ’’
password of the PC must be changed to a password that the local user
knows. A small, initial set of applications is added. This installation
may not install all drivers for the hardware on non-standard PCs, as
well as software that the supplier has supplied / pre-installed with the
PC. In this case it concerns a so-called unattended installation. One
can not make choices about how to install Windows (e.g. partitioning -
the hard disk is fully used as **`C:\`** disk and formatted) or which
parts of Windows (eg Outlook Express) may or may not be added. The local
user has to install missing drivers and additional software.\
Advanced details:\
If necessary, you can then pull the PC out of the AD-domain (please let
us know if you do that, then we can adjust Active Directory). It is also
possible to add the PC to the ’’ unmanaged ’’ OU so that the PC does not
get ’’ policies ’’ from the domain.

## Hardware requirements

The minimal requirements for a new installation of a Windows “Managed
PC” are:

-   Owned by Radboud University (because of software licenses)
-   Not older than 5 or 6 years
-   A network card or onboard network interface with PXE
-   Processor 64-bit
-   8 GB memory
-   Windows10: 500 GB harddisk
-   No additional expansion cards for network, graphics, audio, etc.

The standard RU PC and standard RU laptop that has the above properties,
suffices.

## Peripheral hardware

Connecting peripheral hardware to a ‘Managed PC’ can run into problems.
Especially if the new hardware requires installation of special software
or drivers. As a ‘unprivileged’ user one does not have the rights to
install such software. In this case a solution could be to change the PC
into a ‘Department managed PC’ or a ‘User managed PC’ (this has impact
on support available from C&CZ).

## Software

There is a [list of Windows software](/en/howto/microsoft-windows/) that
is or can be installed on C&CZ managed PCs.
