---
author: sioo
date: '2022-09-13T09:27:00Z'
keywords: []
lang: nl
tags:
- email
title: Ldap adresboek
wiki_id: '707'
---
### FNWI adresboek in Outlook/Thunderbird

Let op dat dit alleen over VPN of binnen de RU werkt

## Outlook

Indien je een Engelstalige Outlook gebruikt bekijk dan de Engelse versie
van deze pagina

1.  In Outlook kies Extra-\>E-mailaccounts
2.  Vervolgens selecteer “Een nieuwe adreslijst of een nieuw adresboek
    toevoegen” en klik op “Volgende”
3.  Selecteer “Internetadreslijstservice (LDAP)” en klik op “Volgende”
4.  Vul als servernaam in: ldap.science.ru.nl en klik op “Meer
    instellingen”
5.  Selecteer het Tabblad Zoeken en vul in als Zoekbasis: o=addressbook
6.  en klik op “OK”, vervolgens op “Volgende” en tenslotte op
    “Voltooien”
7.  Sluit Outlook af en start Outlook opnieuw op.

Je kunt nu in dit adresboek zoeken maar de handigste methode is om als
je een nieuwe mail maakt een deel van de naam in het “Aan:” veld in te
vullen en dan “Control-K” te typen. Er verschijnt dan een lijst van
namen die matchen met de ingevulde tekst.

Jammer genoeg stopt Outlook met het doorzoeken van adresboeken zodra er
een hit is in een adresboek. Dat betekent dat als LDAP het tweede
adresboek is, en je hebt een contact die matcht in je lokale adresboek
dan zie je geen resultaten uit het LDAP adresboek. In dat geval zul je
als nog expliciet moeten zoeken in het LDAP adresboek, je kunt de
gevonden resultaten overnemen in je persoonlijke adresboek zodat bij een
volgende zoekactie deze persoon gevonden wordt in je eigen adresboek.

## Thunderbird

Indien je een Engelstalige Thunderbird gebruikt bekijk dan de Engelse
versie van deze pagina

1.  In Thunderbird kies Extra-\>Adresboek
2.  Vervolgens kies in het menu van het Adresboek venster Bestand -\>
    Nieuw -\> Ldap-directory …
3.  Kies een Naam: bijvoorbeeld Science
4.  Hostnaam: ldap.science.ru.nl
5.  Basis DN: o=addressbook
6.  Eventueel kun je in het Geavanceerd tabblad het aantal getoonde
    resultaten groter of kleiner maken (default is 100)
7.  Sluit af door “OK” te klikken
8.  Om Automatische adresaanvulling aan te zetten voor het LDAP
    adresboek, kies in het menu Extra “Opties” het “Opstellen” tabblad
9.  Daarbinnen vervolgens het tabblad Addressering en vink de
    Directoryserver aan (Kies Science in het drop down menu)

en vervolgens zal als je een mail adres begint te typen in een Aan: veld
een lijst te voorschijn komen met alle matches uit je eigen adresboek EN
het LDAP adresboek.

Als je meerdere LDAP servers wilt raadplegen dan is er de Thunderbird
plugin [Multi LDAP
Enabler](https://addons.mozilla.org/nl/thunderbird/addon/5684) die
automatische aanvulling mogelijk maakt uit meerdere LDAP servers
tegelijkertijd.

## OsX 10.7

Open je adresboek. Bij voorkeuren (Cmd+,) ga naar Accounts en klik op
het plusje rechtsonder.

1.  Bij Account type, kies voor: LDAP
2.  Server address: ldap.science.ru.nl
3.  Klik op Continue.

1.  Description: vrije keuze (kies bijvoorbeeld Science)
2.  Search base: o=addressbook
3.  Scope: Subtree, Authentication: None.
4.  Klik op Create.

Bij View-Groups (Cmd+3) zie je nu je nieuwe directory.
