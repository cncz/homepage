---
author: bram
date: '2021-12-03T16:33:36Z'
keywords: []
tags: [ gitlab ]
title: GitLab
wiki_id: '945'
cover:
  image: img/2021/gitlab.png
---
## About GitLab
> "GitLab is an incredibly powerful open source code collaboration
> platform, git repository manager, issue tracker and code reviewer."
> Quoted from the [GitLab website](https://about.gitlab.com/features/)

## Logging in
{{< notice info >}}

The URL for GitLab is: <http://gitlab.science.ru.nl>

{{< /notice >}}

## Science login
Although anyone with a [Science Login](../login) can login to
GitLab, only people that have actually logged in to GitLab will be
visible for other GitLab users. Keep that in mind when adding people to
projects or groups.

## External users
If you wish to add external users to your GitLab project, please request a 
[Science login](../login#request-a-science-login) with access rights limited to GitLab.

A *GitLab-only* Science login is registered as a so called *external user* in GitLab. As an external user, you can't
 create projects, but you can be assigned to an existing project. Consult
 [GitLabs documentation](https://docs.gitlab.com/ee/user/admin_area/external_users.html) about external users for more
 information.

## Git and managing large files

As of July 18th 2016, LFS (Large File Storage) support is enabled on the
GitLab server. Git LFS allows you to manage version control on large
files without storing them in the git repository. Please refer to the
[Git LFS project website](https://git-lfs.github.com/) for more
information.

## Documentation

- [Git documentation](http://git-scm.com/documentation)
- [General GitLab documentation](http://doc.gitlab.com/)

## Changes
- 2023-03-07 [Science logins](../login) for [external GitLab users](#external-users).
- 2022-10-17 enabled the feature-flags for the [Debian API](https://docs.gitlab.com/ee/user/packages/debian_repository/#enable-the-debian-api).
- 2021-12-03 Enabled [GitLab pages](/nl/news/2021-12-07_gitlab-pages-installed).
- 2021-10-21 enabled the Docker Container Registry service. See the [gitlab
 documentation](https://gitlab.science.ru.nl/help/user/packages/container_registry/index.md) for more info.
- 2020-06-26 gitlab.pep.cs.ru.nl moved to a new Ubuntu 20.04 server. These are the ssh fingerprints for `gitlab.pep.cs.ru.nl`:

``` text
256 SHA256:bVYa+loFqV0Hdv3sPukp40zf1LdQd4PNBOSJa6oRnyE root@gitlab9 (ECDSA)
256 SHA256:j4pDiX2SSKGHjoZ1OQXgrKpIvLxrCnckZCtYWapHFcM root@gitlab9 (ED25519)
3072 SHA256:QIdVcGdaEhyYXcUVtc2FIa1R9F/1KUekR+7IV23c0LI root@gitlab9 (RSA)
```

- 2019-01-25 GitLab updated to version 11.7.0 and moved to a new Ubuntu 18.04 server. You might encounter an SSH prompt when pushing or pulling code. The current ssh fingerprints for `gitlab.science.ru.nl` are:

``` text
SHA256:7G/4WD/tUE8H0k9MkBznlcx+ZWgUxnn3KazKm1XQwRk root@gitlab (ED25519)
SHA256:NgZ9HciXlwG5JNPSFbem7bBbDbhqaNAO7JHag8rwi/I root@gitlab (RSA)
SHA256:BSSXi19WgCSpZu5AQkUTtwm+5zAMioQJpFE3oxBrIMQ root@gitlab (ECDSA)
```
- 2016-09-13 Configured reply by email. You can now simply reply by email to GitLab mail notifications.
- 2016-07-18 Setup [GitLab LFS](/en/howto/gitlab#git-and-managing-large-files/) (Large File Storage).

## Maintenance window

Updates to GitLab are usually done on Friday mornings. During the upgrade, GitLab may be unavailable for a while.
