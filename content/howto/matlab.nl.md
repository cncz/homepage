---
author: bram
date: '2022-09-19T16:29:14Z'
keywords: []
lang: nl
tags:
- software
title: Matlab
wiki_id: '151'
---
### Matlab

Matlab, een "matrix laboratory" voor interactieve wetenschappelijke
berekeningen van [The Mathworks Inc.](http://www.mathworks.com/) is een
geïntegreerd pakket, het combineert numerieke berekeningen,
data-visualisatie en een programmeertaal.

### Licentie

Een aantal afdelingen binnen de RU betaalt mee aan de door C&CZ
afgesloten netwerklicentie voor het gebruik van Matlab. Als men
meebetaalt aan de licentie, kan men software en installatiesleutels van
Matlab voor Linux, Mac of MS-Windows, ook voor oudere versies van
Matlab) krijgen van [C&CZ systeembeheer](/nl/howto/systeemontwikkeling/).

Deze netwerklicentie met veel toolboxen kan alleen door computers die
aan het Internet zitten gebruikt worden. Voor standalone gebruik kan men
proberen een gratis alternatief als [Scilab](http://www.scilab.org) of
[Octave](http://www.gnu.org/software/octave/) te gebruiken. Een licentie
voor gemiddeld 1 gelijktijdige Matlab-gebruiker kost EUR 241,- ex BTW
aanschaf. Het gemiddelde over een jaar wordt hierbij bepaald door van
elke week het maximum aantal gelijktijdige gebruikers te tellen. Deze
maxima worden dan over het jaar gemiddeld. Onderhoud kost afhankelijk
van het aantal gebruikers, ca EUR 50,- per jaar per licentie.

Als een afdeling structureel gebruik maakt van Matlab, maar nog niet
meebetaalt, kan men contact opnemen met [Astrid
Linssen](/nl/howto/cncz-medewerkers/). Het gebruik wordt op
de license server gelogd. Achteraf wordt opgeteld wat het gemiddelde
piekgebruik per week van elke groep is geweest, dit hoort overeen te
stemmen met het aantal aangeschafte licenties. Dus zal tijdens het
hoogste piekgebruik een afdeling meer licenties gebruiken dan men
aangeschaft heeft, maar dat is door de gedeelde netwerklicentie geen
probleem, omdat niet alle afdelingen op dezelfde tijd hun piekgebruik
hebben.

Op de door C&CZ beheerde machines met MS-Windows of Linux is Matlab
beschikbaar.

### Integrating Matlab in Python

Het is mogelijk Matlab te gebruiken vanuit python, zie
[matlab-engine-for-python](https://nl.mathworks.com/help/matlab/matlab-engine-for-python.html).
Op alle Linux clients en servers is dit al voorbereid voor Matlab R2020a
en R2019b. Zie /opt/matlab-\*/Matlab-Python.Readme voor details.

### Online Matlab cursus

Als uw afdeling een Matlab-licentie heeft en u wilt Matlab interactief
leren met de Matlab Academy (Mathworks Training Services), ga dan naar
[de Matlab Academy](https://matlabacademy.mathworks.com/) en maak een
account aan met een e-mailadres dat eindigt op: @science.ru.nl,
@donders.ru.nl, @pwo.ru.nl, @let.ru.nl, @fm.ru.nl, @ai.ru.nl,
@socsci.ru.nl of @ru.nl en gebruik de Activation Key die
[C&CZ](/nl/howto/contact/) kan verstrekken.

## Bekende problemen

### Toolkit problemen na upgrade naar nieuwe versie

Een toolkit lijkt te ontbreken, maar de oorzaak is een
Matlab zoekpad dat niet bijgewerkt is voor de nieuwe versie. Zie
[Mathworks
help1](http://www.mathworks.com/help/techdoc/matlab_env/br7ppws-1.html#br7poxu-10)
en [Mathworks
help2](http://www.mathworks.com/help/techdoc/matlab_env/br7ppws-1.html#br8p4gj-1).
Een snelle fix, in Matlab is:

`restoredefaultpath; matlabrc`

### Firewalls

-   Als men achter een firewall zit, moeten 2 poorten naar de server
    opengezet worden: 1 waar de server op luistert en 1 return
    portnumber. C&CZ kan die portnumbers vertellen, ze kunnen wijzigen
    bij upgrades.
