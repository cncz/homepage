---
author: bram
date: "2023-01-12T12:36:19Z"
keywords: []
lang: nl
tags:
  - storage
title: Data opslag
wiki_id: "48"
aliases:
  - diskruimte
cover:
  image: img/2023/storage.jpg
ShowToc: true
TocOpen: true
---

# Introductie

In onze faculteit worden verschillende soorten dataopslag aangeboden:

| Storage klasse                       | Beschrijving                                                                                                                                                           | Risico op dataverlies |
| ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| [Home directories](#homedirectories) | Klein (enkele GB), betrouwbaar, gebackedupte opslag voor individuen                                                                                                    | laag                  |
| [Netwerk schijven](#netwerkschijven) | Groter (1-200 TB), betrouwbaar,gebackedupte opslag voor individuen en groepen,                                                                                         | laag                  |
| [Lokale storage](#local-storage)     | Opslag zonder back-up op desktopcomputers en clusternodes, die waarschijnlijk verloren gaat bij een hardwareprobleem of wanneer de machine opnieuw wordt geïnstalleerd | hoog                  |

# Homedirectories

Bij je [Science account](../login) hoort standaard een homedirectory van 5GB. Hiervoor worden geen kosten in rekening gebracht. Dit is een veilige plek om je werkgerelateerde documenten op te slaan. Homedirectories staan op degelijke hardware,
en worden automatisch [gebackupped](../backup). Als je meer opslagruimte nodig hebt, en je kunt niet [opschonen](../quota/#veel-voorkomende-schuldigen-aan-het-opvullen-van-je-quota), dan kunnen we je homedirectory op [verzoek](../contact) vergroten.

## Homedirectory paden

{{< notice info >}}
De locatie van je homedirectory kan bekeken worden op [DHZ](../dhz).
{{< /notice >}}

Op door C&CZ beheerde systemen is je homedirectory als volgt te vinden:

| C&CZ systemen     | Pad                                |
| :---------------- | :--------------------------------- |
| Microsoft Windows | `U:` ook wel de `U-schijf` genoemd |
| Linux             | `/home/jescienceloginnaam` of `~`  |

Vanaf andere systemen is je homedirectory middels de volgende paden te vinden:
| Eigen systeem | Pad | Instructies |
| ----------------- | ---------------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\home?.science.ru.nl\jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#windows) |
| macOS | `smb://home?.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#macos) |
| Linux | `smb://home?.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#linux) |

Indien je account recent aangemaakt is ziet het er anders uit (zie [DHZ](../dhz)):
| Eigen systeem | Pad | Instructies |
| ----------------- | ---------------------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\<hash>.home.science.ru.nl\jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#windows) |
| macOS | `smb://<hash>.home.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#macos) |
| Linux | `smb://<hash>.home.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#linux) |

{{< notice info >}}
Voor toegang tot deze paden van buiten het RU-netwerk is een [VPN](../vpn) verbinding vereist.
{{< /notice >}}

## Toegangsrechten

Homedirectories zijn standaard alleen toegankelijk voor de eigenaren zelf. Het is echter mogelijk
om de toegangsrechten te laten wijzigen (bijvoorbeeld `chmod o+rx ~`).

# Netwerkschijven

Voor grotere opslagbehoeften en de mogelijkheid om bestanden in je groep te
delen, zijn er netwerkschijven.

Netwerkschijven zijn beschikbaar als _Samba[^samba]_- en _NFS[^nfs]-share_.
Zoals vermeld in het overzicht, worden netwerkschijven geback-upt en gehost op
betrouwbare servers. De kosten van netwerkschijven zijn afhankelijk van de
schijfgrootte en de [backup-opties](../backup).

[^nfs]: Network file system, https://nl.wikipedia.org/wiki/Network_File_System

[^samba]: Windows file sharing implementatie door het [Samba project](https://www.samba.org/)

## Kosten van een netwerkschijf

Één TB[^what_is_a_tb] (1000 GB) kost € 36 per _jaar_. Hier is de verplichte backup bij ingegrepen.
(We rekenenen dat een backup de hoeveelheid data verdubbelt). Een netwerkschijf zonder backup is
mogelijk, maar is niet goedkoper. Pas als de kosten boven de € 1000 per jaar uitkomen zal er een
rekening worden opgemaakt.

Het grootste volume dat we nu (2024+) kunnen aanbieden is gelimiteerd tot de hoeveelheid disken die
in een server passen. Nu is dat ongeveer 200 TB.

Alle nieuwe network schijven gebruiken [ZFS](../zfs-storage) en hebben lokale [snapshots](../zfs-share-snapshot/).

[^what_is_a_tb]: 1TB is `1.000.000.000.000` bytes, 1 TiB = `1.099.511.627.776` bytes

We raden aan om capaciteit te huren in plaasts van eigen servers te kopen. Dit om te voorkomen dat
er ineens veel geld moet worden besteed aan hardware en er voor te zorgen dat er ook na de server's
end-of-life gebruikt kan worden gemaakt van de netwerkschijven.

## Paden voor netwerkschijven

Op door C&CZ beheerde systemen zijn netwerkschijven als volgt te vinden. Met `sharename` als voorbeeld:
| C&CZ systemen | Pad | Instructies |
| :---------------- | :---------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename` | [schijf koppelen](../mount-a-network-share#windows) |
| Linux | `/vol/sharename` | |

Voor andere systemen zijn netwerkschijven als volgt te vinden:
| Eigen systeem | Pad | Instructies |
| :---------------- | :-------------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename` | [schijf koppelen](../mount-a-network-share#windows) |
| macOS | `smb://sharename-srv.science.ru.nl/sharename` | [schijf koppelen](../mount-a-network-share#macos) |
| Linux | `smb://sharename-srv.science.ru.nl/sharename` | [schijf koppelen](../mount-a-network-share#linux) |

{{< notice info >}}
Voor toegang tot deze paden van buiten het RU-netwerk is een [VPN](../vpn) verbinding vereist.
{{< /notice >}}

## Toegangsrechten

Toegangsrechten voor netwerkschijven is op basis van groepen.
Groepen kunnen worden beheerd door de groepseigenaren op [DHZ](/en/howto/dhz/).

## Speciale netwerkschijven

Een aantal algemene diensten maakt gebruik van netwerkschijven:
| Schijf | Doel | Documentatie |
| ---------- | --------------------------------- | --------------------------------- |
| `dfs` | Virtuele boomstructuur met koppelingen naar (veel) netwerkschijven | [DFS](#dfs) |
| `temp` | Tijdelijke opslag van bestanden | [Temp share](../temp-share) |
| `cursus` | Software voor cursussen | [T-schijf](../t-schijf) |
| `software` | Algemene (Windows) software | [S-schijf](../s-schijf) |
| `install` | Installatiebestanden van software | [Install share](../install-share) |

### DFS

Voor groepen met veel netwerkschijven is er een DFS-schijf beschikbaar. Als je groep hiervan gebruikmaakt, kun je door slechts één schijf aan te koppelen toegang krijgen tot alle netwerkschijven van je groep. Of je de onderliggende schijven daadwerkelijk kunt inzien, hangt af van de rechten die je hebt. Het pad om de DFS-share te koppelen is:

```console
\\dfs.science.ru.nl\dfs
```

Of één niveau dieper om direkt de boomstructuur van je groep te zien:

```console
\\dfs.science.ru.nl\dfs\{groepsmap}
```

## Bestellen

Bij het bestellen van een netwerkschijf, graag de volgende details [aan ons doorgeven](../contact):

| Detail                 | Beschrijving                                                                                                                             |
| ---------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| loginnaam              | Je [Science account](../login)                                                                                                           |
| schijfnaam             | Waarmee de schijf over het netwerk gevonden kan worden, zie [paths](#paden-voor-networkschijven)                                         |
| grootte                | grootte van de partitie, zie [kosten](#kosten-van-een-netwerkschijf)                                                                     |
| kostenplaats           | _kostenplaats_ of project code                                                                                                           |
| unix groep (optioneel) | De groep die toegang tot de schijf moet hebben. Indien niet opgegeven, wordt er een nieuwe groep gemaakt met dezelfde naam als de schijf |

# Lokale opslag

Lokale opslag verwijst naar schijfruimte in of gekoppeld aan systemen zoals desktop-pc's en
clusternodes. Meestal bestaat lokale opslag uit enkel uitgevoerde schijven. Bij een defect aan een
schijf is de kans groot dat er gegevens verloren gaan. Lokale schijven worden gewoonlijk `/scratch`
genoemd om je te herinneren aan de tijdelijke aard van de opslag. Lokale wordt **_niet
gebackupped_**. Voordelen van lokale opslag zijn lage kosten en snelle schijftoegang.

{{< notice info >}}
Op gedeelde systemen, zoals loginservers en clusternodes, is het aan te raden eerst een persoonlijke
map te maken in `/scratch`. D.w.z. `mkdir /scratch/$USER`. En vergeet niet om je software
daadwerkelijk naar die map te laten schrijven in plaats van naar de standaard en veel kleinere
`/tmp`.
{{< /notice >}}

```

```
