---
author: petervc
date: '2017-03-03T10:51:40Z'
keywords: []
lang: nl
tags: []
title: Netwerk mutaties
wiki_id: '95'
---
## Netwerk mutaties

Door de gebouwen van de B-faculteit lopen allerlei soorten kabels voor
dataverkeer. De belangrijkste zijn de glasfiber- en UTP-kabels
(Unshielded Twisted Pair) voor het netwerk. De bekabeling, switches en
outlets worden beheerd door C&CZ; wijzigingen mogen alleen door of in
opdracht van C&CZ worden uitgevoerd.

Mutaties (toevoegen, wijzigen, verplaatsen, verwijderen) van PC’s die
geen services (WWW-service, disken uitdelen buiten de afdeling, etc.)
aanbieden, hoeven niet bij C&CZ te worden aangevraagd. Zulke PC’s
krijgen geen vast IP-nummer.

Alle mutaties (toevoegen, wijzigen, verplaatsen, verwijderen) van
apparaten met een vast IP-nummer aan het netwerk op de B-faculteit
moeten altijd vooraf bij C&CZ
[**Netwerkbeheer**](/nl/howto/overcncz-wieiswie-netwerk/) worden
aangevraagd. Aanvraag via WWW is mogelijk via onderstaand formulier.

Omdat het in principe niet mogelijk is om te zien of degene die via een
WWW formulier zoals dit een mailtje stuurt ook daadwerkelijk is wie hij
of zij beweert te zijn, zal een aanvraag via dit formulier altijd eerst
gecontroleerd worden. De snelste manier om een ethernet-mutatie aan te
vragen is door rechtstreeks een mailtje te sturen naar
<ethergids@science.ru.nl> met onderstaand formulier als voorbeeld.

------------------------------------------------------------------------

  -------------------- --------------
    **soort mutatie:** nieuw
                       wijziging
                       verwijdering
  -------------------- --------------

|                   \| \|
| —————-: \| \|
| **ingangsdatum:** \| \|

|                     \| \|
| ——————: \| \|
| **contactpersoon:** \| \|
|       **telefoon:** \| \|
|         **e-mail:** \| \|

|                     \| \|
| ——————: \| \|
| **gebouw/vleugel:** \| \|
|    **kamernummer:** \| \|
| **hardware-adres:** \| \|
|      **node-naam:** \| \|
|           **merk:** \| \|
|           **type:** \| \|
|    **serienummer:** \| \|

  ---------------- -------------------
    **categorie:** PC
                   Workstation
                   Router/Gateway
                   anders, namelijk:
  ---------------- -------------------

  ----------------------- --------------------
    **operating system:** MS-WINDOWS versie:
                          Linux versie:
                          anders, namelijk:
  ----------------------- --------------------

|               \| \|
| ————: \| \|
| **afzender:** \| \|

|                  \| \|
| —————: \| \|
| **Opmerkingen:** \| \|
