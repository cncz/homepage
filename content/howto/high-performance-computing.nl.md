---
title: High Performance Computing
author: sioo
date: 2024-11-05
keywords: []
tags: []
cover:
  image: img/2024/high-performance-computing.png
ShowToc: false
TocOpen: false
---

Binnen FNWI beheert C&CZ een aantal rekenclusters, en we hebben een zaal met enkele tientallen rekken voor servers in Huygens en ook een paar in het Forum


## Financieel model

De faculteit heeft zelf in principe geen of weinig rekenservers. De hardware servers worden door afdelingen gekocht en bij ons gebracht voor het beheer in de rekenclusters.

In een enkel geval heeft een afdeling
(Astrofysica) een apart cluster gemaakt door ook hun eigen headnode
(master) aan te schaffen.

Het grootste `cn`-cluster heef ook een klein
aantal door C&CZ aangeschafte nodes, waar FNWI-afdelingen na overleg met
C&CZ gebruik van kunnen maken. Voor afdelingen van FNWI is de housing en
het beheer van clusternodes "gratis". In de toekomst zou dit kunnen veranderen
als we betere monitoring spullen hebben en beleid veranderd zodat we ook stroom
in rekening kunnen brengen.

Het `cn`-cluster is heterogeen. Alle
nodes gebruiken [SLURM](/nl/howto/slurm/) als clustersoftware, een deel
draait nog ubuntu 18.04, een deel ubuntu 20.04. Per februari 2022 telde
het cn-cluster 123 nodes met 4700 cores en 30 TB RAM. Diverse
clusternodes hebben een GPU met CUDA cores.

We hebben ook een apart cluster voor
de RU-afdeling [Centre for Language Studies](http://www.ru.nl/cls/)
in beheer. Voor niet FNWI afdelingen die hun cluster bij ons willen laten
beheren gelden aparte financiele afspraken.

Soms worden ook de PC’s in de
[terminalkamers](/nl/howto/sleutelprocedure-terminalkamers/) buiten de
openingstijden gebruikt als rekencluster.

Er is een [primitief overzicht van
clusternodes](https://sysadmin.science.ru.nl/clusternodes/) beschikbaar, tot we
een betere manier hebben gemaakt om het oude cricket te vervangen.

## andere opties

[Surf Sara](https://projectescape.eu/partners/surfsara-netherlands)

...

