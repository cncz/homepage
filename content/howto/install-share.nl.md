---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
noage: true
lang: nl
tags:
- software
title: Install share
wiki_id: '811'
---
Veel licentiesoftware die door studenten en medewerkers van FNWI gebruikt
mag worden is te vinden in de `install` network share.

Je kunt de netwerk share koppelen in windows via het pad:

```
\\install-srv.science.ru.nl\install
```

{{< notice info >}}
Zie [hier](../storage/#paden-voor-netwerkschijven) hoe je een netwerkschijf, zoals de install share kunt koppelen.
{{< /notice >}}

Meestal kan men deze software direct vanaf de install share installeren.
Vaak is een licentiecode nodig. Als de installatie-instructies op de [Science licentiepagina's](https://cncz.pages.science.ru.nl/licenses/) niet voldoede zijn, benader dan [postmaster](../contact).

Elk softwarepakket heeft eigen licentievoorwaarden. Soms is gebruik
alleen op RU-computers toegestaan, soms ook thuisgebruik door studenten.
