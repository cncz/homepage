---
author: petervc
date: '2012-08-17T14:35:48Z'
keywords: []
lang: en
tags:
- storingen
- software
title: Software probleem op pc
wiki_id: '623'
---
In case you have questions about system software or applications on your
PC, and it is not easy to explain them by phone or e-mail to the
helpdesk, you may demonstrate your problem at C&CZ on one of the two
PC’s which are set-up for that purpose.