---
author: remcoa
date: '2022-10-06T15:13:58Z'
keywords: []
lang: en
tags:
- internet
title: TLS Certificaten
wiki_id: '68'
---
# TLS certificates

Any service accessible via TLS (https) must have an TLS certificate.
This includes any web server with encrypted or “secure” content. An TLS
(Transport Layer Security, see [RFC 8446](https://www.rfc-editor.org/rfc/rfc8446))
certificate is a signed electronic guarantee that
a particular server is the server it claims to be. Certificates are used
primarily (but not exclusively) for providing web pages via an encrypted
connection. A certificate is signed by a Certificate Authority (CA)
which ensures the integrity of the certificate.

A few Certificate Authorities such as Let's Encrypt, Verisign, Thawte, and Terena are
automatically trusted by TLS clients (including web browsers), so
certificates signed by these companies are validated without user
confirmation. All C&CZ certificates of servers and web applications are
signed by Terena (through SURFdiensten).

# Obtaining a certificate

Because TLS certificates are used as **proof** of the validity of the
web site or server, it is not possible to acquire a signed TLS
certificate for just any domain name. The Certificate Authorities check
if the person or organisation requesting a certificate is indeed the
owner of the domain name for which the certificate is requested. Domain
names [registered](/en/howto/domeinnaam-registratie/) through C&CZ are
owned by the Radboud University. Therefore C&CZ can also request TLS
Certificates for these domain names.
