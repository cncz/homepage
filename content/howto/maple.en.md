---
author: petervc
date: '2021-12-05T23:25:59Z'
keywords: []
lang: en
tags:
- software
title: Maple
wiki_id: '149'
---
[Maple](http://www.maplesoft.com/products/Maple/) by Waterloo
Maple’s [Maplesoft](http://www.maplesoft.com/) is an interactive
computer-algebra system, for which C&CZ has acquired a license.

The license of Maple is a 4-user concurrent license in 2023 and a 2-user in 2024.
