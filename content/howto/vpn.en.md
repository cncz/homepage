---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: [ vpn ]
lang: en
tags: [ vpn ]
title: VPN
wiki_id: '71'
ShowToc: true
TocOpen: true
cover:
  image: img/2022/vpn.png
---
# VPN

A VPN makes it possible to get secure access to the internet or to the campus network from anywhere.
The computer at home (or anywhere on the internet) in the latter case becomes part of the
campus network. In this way you can get access to services that are
normally only accessible from computers on campus. The most common of
such services are [connecting to disk shares](/en/howto/diskruimte/) or
to special servers.

## EduVPN

The VPN of choice for Radboud employees and students is [eduVPN](https://www.ru.nl/en/staff/services/services-and-facilities/ict/working-off-campus/why-should-you-use-eduvpn/using-eduvpn).
The [eduVPN client software is available for Windows, macOS, Android, iOS and Linux](https://www.eduvpn.org/client-apps/).
You need a [RU-account](http://www.ru.nl/idmuk/) to use eduVPN.

## Library
For the use of the [University library](http://www.ru.nl/ub) one does
not need VPN, because the library has a proxy website *UB Off-Campus*, that can be used
from anywhere on the Internet after logging in with your RU account.
