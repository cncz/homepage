---
author: bram
date: '2023-01-11T09:28:26Z'
keywords: []
lang: en
tags: []
title: "Work from Home - FNWI"
wiki_id: '1061'
---

Also see the [working from home](https://www.ru.nl/ict-uk/staff/working-from-home/) page provided by the central IT Service Centre.

## Email

User [webmail or your local e-mail client](/en/howto/email).

## Library

How to get [access to the University Library (when you are off-campus)](https://www.ru.nl/library/services/study/access-digital-library/).

## BASS

[BASS](/en/howto/bass) can only be accessed directly from the campus network or through [VPN](/en/howto/vpn) from any Internet location. Note however that from a small part of the subnets at the Faculty of Science, direct access to BASS is not possible. These subnets are the ones with servers that can be accessed from the Internet.

## Files on network discs

There are two ways to access your [network files](/en/howto/diskruimte/):
- Establish a [VPN connection](/en/howto/vpn/) and access [network shares like you would normally do](/en/howto/diskruimte/)
- Use an [SCP client](/en/howto/ssh#recommended-file-transfer-clients/) to connect to an [SSH login server](/en/howto/ssh/).

## Chat

Use your [Science login](/en/howto/login/) to login to
[Mattermost](https://mattermost.science.ru.nl). You can define your own
channels and communicate with your colleagues. Check [this page for the
user
manual](https://docs.mattermost.com/help/getting-started/welcome-to-mattermost.html).

## Meetings

- Video conferencing from the browser: <https://jitsivm.science.ru.nl>
- ~~Video conferencing + notes + presentation sharing: BigBlueButton~~ *stopped due to security issues*
- Microsoft teams
- Conference calls: <https://conferencecall.nl>
- [Zoom](https://www.ru.nl/ict/medewerkers/online-vergaderen-chatten/zoom-veelgestelde-vragen/) either free or you can request a license from the University for larger meetings.

## Linux Login Server / Cluster

See [SSH](/en/howto/ssh/).

From there, login to the [Science cluster](/en/howto/hardware-servers).

## Code collaboration

Use your [Science login](/en/howto/login/) to login to [GitLab](https://gitlab.science.ru.nl).

## Help!

You can ask for help in the [usual way](/en/howto/contact).

## Remote Desktop (last resort!)

In some cases, there is no alternative to working on your work PC.
However this should be a last resort and not to be used all the time. It
puts the most strain on the VPN service and it requires the PC to have a
permanent IP address, which is not the common situation.

If you really need this, please send e-mail to postmaster\@science.ru.nl
with the following information:

-   who is maintaining the PC? You, C&CZ or ISC
-   hostname or ip address of your PC\
    Sometimes additional information like room number and outlet number
    helps to resolve ambiguities
-   the username you use to login to the PC (in case PC is managed by
    C&CZ)
-   reason why you need it (we may know alternatives you didn’t think
    of)

In order to use Remote Desktop you need to:
- Setup [VPN](/en/howto/vpn/) at home
- Enable your Windows Computer at the faculty for [Remote Desktop](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-allow-access)
- Connect with remote desktop to `"yourworkpc".science.ru.nl`
