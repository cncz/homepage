---
author: jzelenka
date: '2021-12-13T09:44:20Z'
keywords: []
lang: en
tags: []
title: Gaussian
wiki_id: '1076'
---
## Prerequisities

-   Science (CNCZ) account.
-   Being member of the qm, qmorgchem or qmteaching and gaussian groups
    (ask Jan for access).
-   [Vpn](/en/howto/vpn/) (when working from home).
-   SSH client (linux usually has [OpenSSH](https://www.openssh.com/),
    windows - for instance [MobaXterm](https://mobaxterm.mobatek.net/)
    or [Putty](https://www.putty.org/)).
-   X11 client (linux usually has [OpenSSH](https://www.openssh.com/),
    windows - for instance [MobaXterm](https://mobaxterm.mobatek.net/)
-   (optional) [WinSCP](https://winscp.net/eng/download.php)

## Video guides

[GUIDE](https://youtu.be/krr8xk7LCCA) how to setup environment for
Gaussian calculations in windows.

## Setting-up the environment (to be done once)

To submit calculations easily you need to set-up the environment first.

### Automated script

You must be within university network or connected through VPN. connect
through SSH to lilo.science.ru.nl (in putty that is the hostname). Your
username is your science login. After logging-in, execute
**/vol/qmorgchem/scripts/gaussify** script (cnorris is the username in
this example):

`cnorris@lilo7:~$ /vol/qmorgchem/scripts/gaussify`

If you’re more experienced user, you can setup the things on your own.

### Explanation of the automated script

The bash script sets-up few things on your behalf. What it does is:

#### \~/.profile file editation

to see your \~/.profile content, execute:

`cnorris@lilo7:~$ cat ~/.profile`

you should see something like this:

`export PATH=$PATH:/vol/qmorgchem/scripts`\
`module load Gaussian-16`\
`umask 027`

This file stores some of your server settings, the script:

-   Adds (**[export](https://linux.die.net/man/1/bash)** PATH=… line)
    /vol/qmorgchem/scripts in your \$PATH variable - by doing this you
    can easily execute scripts located in /vol/qmorgchem/scripts folder
    such as the important **G16** script.
-   Automatically loads Gaussian-16 module
    (**[module](https://linux.die.net/man/1/module)** load… line) which
    enables you to use commands like **cubegen** and similar.
-   Changes the **[umask](https://linux.die.net/man/2/umask)** to 027 -
    that means that files newly created from the console can be readed
    by people from qmorgchem, but **cannot be changed** by them. If you
    want to keep the files changeable by other people from qmorgchem,
    then remove this line from your \~/.profile file.

#### Folders and shortcuts creation

Script creates your folder in qmorgchem disk. If you are member of the
qmorgchem group it creates your folder also on the qmorgchem-old disk.
It creates shortcuts (named symlinks in linux) to those folders within
your \$HOME directory by **ln -s** commands.

To see those folder changes, execute:

`cnorris@lilo7:/vol$ ls -lh ~`

You should then see something like this:

`total 24K `\
`drwxr-s--- 2 cnorris cnorris 4.0K Apr  1 13:37 '$RECYCLE.BIN'`\
`-rw-r----- 1 cnorris cnorris  402 Apr  1 13:37  desktop.ini`\
`lrwxrwxrwx 1 cnorris cnorris   28 Apr  1 13:37  old -> /vol/qmorgchem-old/cnorris/`\
`lrwxrwxrwx 1 cnorris cnorris   15 Apr  1 13:37  others -> /vol/qmorgchem/`\
`lrwxrwxrwx 1 cnorris cnorris   24 Apr  1 13:37  running -> /vol/qmorgchem/cnorris/`

ls is a command which lists directory content, \~ is a shorthand for
your \$HOME directory (/home/cnorris in case of cnorris user), -lh are
arguments: l stands for “long” and h stands for “human-readable.” You
clearly see that shortcut named “old” points to your folder at
qmorgchem-old disk and “running” points to your folder at qmorgchem
disk. Please use qmorgchem-old for inactive projects and do not store
**.chk** files there.

## Checking calculations, submitting jobs, checking queues

### Queues

Each computational job must be submitted to a queue. Currently we have
two queues - **orgfast** and **orglong.**

-   **orgfast** - Queue should be used for standard jobs (e.g.,
    coptimization of geometries, harmonic frequency calculations,
    excited state spectra calculations). Please use **max 4 cpus** and
    **maxi 12 GB of ram** in this queue.
-   **orglong** - Queue for more demanding jobs (typically days or weeks
    of computational time). This queue has a lower priority; jobs will
    be running only if the given number of processors is free and not
    requested in the orgfast queue. Please use **max 12 cpus** and **max
    32 GB of ram** in this queue.

### Submitting jobs, checking queues

There are currently two queues available on the cluster: **orgfast** for
small and quick jobs and **orglong** for bigger and more-demanding jobs.
We are currently using [Slurm](/en/howto/slurm/) as our job queuing
manager.

**To submit a job and doing anything with the running jobs you MUST be
on cnlogin22**

**Basic Commands**:

-   **G16** *$job\_filename* *$queue* - for instance **G16
    co2capcture.gjf orgfast** - submits a calculation
-   **squeue** - checks queue
-   **scancel \$jobid** - cancels a job
-   **scontrol show jobid \$jobid** - show details of a job with job
    number \$jobid (good for getting a full name)

Once calculation is running a **.log** file will appear in the folder
where is the input file.

#### Putty

Connect to cnlogin22.science.ru.nl - details are the same as for the
abovementioned for lilo.science.ru.nl. Change directory to your active
directory where computations are being made (cd running - this brings
you through shortcut to your folder on the qmorgchem disk).

To submit a job, type:

`cnorris@cnlogin22:~$ G16 $job_filename $queue`

command where \$job\_filename is filename of your Gaussian input and
\$queue is either orgfast or orglong.

#### WinSCP

connect through SFTP to cnlogin22.science.ru.nl and then open folder
running and then type:

`cnorris@cnlogin22:~$ G16 $job_filename $queue`

command where \$job\_filename is filename of your Gaussian input and
\$queue is either orgfast or orglong.

For clarity, see the pictures below:

Winscp1.png\|opening console Winscp2.png\|logging in
Winscp3.png\|submitting

### Checking calculations

once calculations are finished or you want to take a look, you can
either use **molden** (its installed on the cluster - you can run it
when you have mobaxterm on and you are connected on lilo) or you can connect
to the disks and check the files from your windows molecule viewer.

### Connecting to disks - windows

Open file explorer and into navigation bar type:

`\\qmorgchem-srv.science.ru.nl\qmorgchem\`

for qmorgchem-old, the link is:

`\\qmorgchem-old-srv.science.ru.nl\qmorgchem-old\`

### Connecting to disks - Linux

create some file in your homedir - for example lilofs:

`mkdir ~/lilofs`

install sshfs if you do not have it yet and then:

`sshfs lilo.science.ru.nl:/ ~/lilofs`

the disks are then located in \~/lilofs/vol , I will leave creating
appropriate symlinks as a homework excercise - its analogous to commands
mentioned above.

## Standard operation procedure, tips

-   If the orglong queue is full and you have quick jobs, run them at
    orgfast
-   Pay attention when setting-up memory allocation, use of excessive
    memory prevents other jobs from running. Memory is shared. Do not
    use memory that you do not need.
-   Do calculations in optimization steps
-   reading force constants (RCFC) speeds-up the initial optimization
    step.

#### optimization steps

This section is an example of the optimization steps. Basis sets and
levels of theory are case-specific and thus may vary.

-   semiempirical (if applicable, AM1, PM6)
-   cheap DFT (pure DFT, fe: B97D/sto-3g, M06L/midix)
-   expensive DFT (hybrid, fe: B3Lyp)

## Issues

### Open

#### Failing calculations after submission

**error note:** *galloc: could not allocate memory.* For now there is an
issue that sometimes calculation fail while submitting a job. If this
happens to you, please record a job number and send it to Jan. Do not
delete .error, .out and .log files of the failed job, they are important
for debugging this issue.

## Disclaimer

If you have any issue to report please write Jan.

This guide is a draft, if you have any suggestions how to improve the
readability, understandability or any idea how to make it better, please
do not be shy and write Jan.
