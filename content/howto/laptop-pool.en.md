---
author: petervc
date: '2020-08-14T08:36:14Z'
keywords: []
lang: en
tags: []
title: Laptop pool
wiki_id: '892'
---
## Lending laptop pool

### General remarks

The Faculty of Science has a pool of 60 RU-standard laptops that can be
lent by employees and students of the Faculty. These are [standard-RU
laptops](/en/howto/terminalkamers/). They are almost identical in use to
the pc’s in the [computer labs](/en/howto/terminalkamers/). The main
difference is that reservation is not through Housing & Logistics.

The reservation of the laptops is administered by the [Library of
Science](https://www.ru.nl/en/about-us/the-campus/buildings-and-spaces/huygens-building/library-of-science). Use for FNWI education has
precedence.

## Usage

### How to log in with Windows?

You can log on to the laptops with:

-   either with a [Science loginname](/en/howto/login/) and the
    corresponding password
-   or a RU account: RU\\s123456 and the corresponding password

### Any questions?

For reservations, please contact the [Library of
Science](https://www.ru.nl/en/about-us/the-campus/buildings-and-spaces/huygens-building/library-of-science). For technical questions,
please contact [C&CZ](/en/howto/contact/) directly.

## Lending procedure

The borrower promises by signing the [laptop lending
agreement](/en/howto/laptop-leenovereenkomst/) to agree with the [lending
regulations of the Faculty of Science for lending
laptops](/en/howto/laptop-uitleenreglement/) to students and employees.

### Trolley

In some special cases, a complete
[trolley](https://www.onderwijsmagazijn.nl/product/laptop-trolley-laptopkar-smartcharge-it-laptrolley-24-horizontaal/3595282/index.html)
with maximum 20 laptops can be lent for Faculty education. The trolley
has been modified by C&CZ in such a way, that the laptops are connected
by UTP-cable to the network, in order to have automatic updates working.

