---
title: Gebruik eduVPN in plaats van Science VPNs VPNsec/OpenVPN
author: petervc
date: 2024-02-23
tags:
- medewerkers
- studenten
cover:
  image: img/2024/eduVPN.jpg
---
Voor RU-medewerkers en -studenten heeft de [SURF dienst eduVPN](https://www.surf.nl/diensten/eduvpn) veel voordelen t.o.v.
de oudere Science VPNs: VPNsec en OpenVPN. Het is erg makkelijk om
[eduVPN te installeren op Windows/macOS/Android/iOS/Linux](https://www.ru.nl/medewerkers/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/buiten-de-campus-werken/waarom-vpn-gebruiken/vpn-gebruiken)
en eduVPN geeft toegang tot alle Science diensten die ook te gebruiken zijn via de Science VPNsec/OpenVPN.
Daarom staat eduVPN bovenaan op [onze VPN pagina](/nl/howto/vpn/) als de aan te raden VPN.

We adviseren gebruikers van de Science VPNs om [eduVPN te gaan gebruiken](https://www.ru.nl/medewerkers/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/buiten-de-campus-werken/waarom-vpn-gebruiken/vpn-gebruiken).

