---
author: wim
date: 2010-05-21 09:52:00
title: 'End of life of Solaris servers: Solo and Solost'
---
The Solaris login servers **solo** and
**solost** will be phased out on July 1, 2010.
Please switch over to the repacement Linux servers
**lilo** and **stitch**. In
case of problems Postmaster can help. The phasing out of all Solaris
servers marks the end of the system administration for this operating
system. Because there are plans to switch to a different Linux
distribution for servers and/or workstations, the number of supported
operating systems will be constant in the near future.
