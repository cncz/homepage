---
author: polman
date: 2014-10-28 13:38:00
tags:
- studenten
- medewerkers
- docenten
title: Maple license terminated January 1, 2015
---
Because [Maple](/en/howto/maple/) is no longer used in the Faculty of
Science education and the use has decreased, C&CZ intends to terminate
the Maple license per January 1, 2015. Should this lead to problems,
please contact [C&CZ](/en/howto/contact/).
