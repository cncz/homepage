---
author: petervc
date: 2013-01-03 14:27:00
tags:
- studenten
- medewerkers
title: MS Office 2013 op Install-schijf
---
De nieuwste versie van [Microsoft Office](http://office.microsoft.com),
2013, is voor MS-Windows op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan gebruik op RU-computers toe. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Eigen DVD’s kunnen
ook op [Surfspot](http://www.surfspot.nl) besteld worden.
