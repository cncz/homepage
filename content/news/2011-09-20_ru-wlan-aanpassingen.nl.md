---
author: mkup
date: 2011-09-20 15:51:00
tags:
- studenten
- medewerkers
- docenten
title: Ru-wlan aanpassingen
---
Sinds kort is het ook mogelijk om op het draadloze netwerk
[ru-wlan](http://www.ru.nl/draadloos) in te loggen met een Science
account, door als username in te vullen
*loginnaam*`@science.ru.nl`. Per 3 oktober gaat
het [UCI](http://www.ru.nl/uci) direct peer-to-peer verkeer tussen
wireless clients op ru-wlan afzetten, in eerste instantie buiten het
Huygensgebouw. Dit komt de beveiliging van het netwerk ten goede.
