---
title: Lilo en stitch worden omgewisseld op 2 oktober. Lilo8 wordt lilo
author: petervc
date: 2023-09-17 23:03:00
tags:
- medewerkers
- studenten
cover:
  image: img/2023/lilo8.jpg
---
Op 2 oktober krijgt de nieuwste loginserver lilo8, die Ubuntu 22.04 draait net als de pc's in de TK's,
de naam/alias lilo.science.ru.nl en wordt daarmee dan de standaard loginserver.

Lilo7, met Ubuntu 20.04, krijgt dan de alias 'stitch' als de alternatieve loginserver.

Lees, om waarschuwingen over veranderde vingerafdrukken te voorkomen, 
[de pagina over servers](/nl/howto/hardware-servers/) en/of  [de SSH pagina](/nl/howto/ssh/).

