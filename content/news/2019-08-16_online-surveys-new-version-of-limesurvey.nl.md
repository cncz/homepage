---
author: remcoa
date: 2019-08-16 13:49:00
tags:
- medewerkers
- studenten
title: 'Online enquetes: Nieuwe versie LimeSurvey'
---
Er is een nieuwe versie van [LimeSurvey](/nl/howto/limesurvey/)
geïnstalleerd, samen met nieuwe, moderne templates. Meer informatie over
[LimeSurvey](/nl/howto/limesurvey/)…
