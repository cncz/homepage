---
author: petervc
date: 2017-12-21 14:53:00
tags:
- studenten
- medewerkers
- docenten
- dns
title: 'Donation to free and open source software: ISC, FSF and OpenBSD'
---
For the majority of the services C&CZ uses [free
software](https://en.wikipedia.org/wiki/Free_software) and [open source
software](https://en.wikipedia.org/wiki/Open-source_software).
Therefore, some time ago the idea emerged that the C&CZ employees would
vote each year which projects would receive a donation of C&CZ. This
year the [Internet Systems Consortium](https://www.isc.org/),
[OpenBSD](https://www.openbsd.org/) and the [Free Software Foundation
(FSF)](https://en.wikipedia.org/wiki/Free_Software_Foundation) were
chosen. The Internet Systems Consortium develops a.o.
[BIND](https://www.isc.org/downloads/bind/) (DNS/nameserver) and [ISC
DHCP](https://www.isc.org/downloads/dhcp/), both basic services of the
network and the Internet. OpenBSD develops, next to
[OpenBSD](https://www.openbsd.org/) itself, also OpenSSH and
[LibreSSL](https://www.libressl.org/). The FSF supports GNU/Linux, which
is used on almost all servers maintained by C&CZ and on many hundreds of
workstations, including the [computer labs](/en/howto/terminalkamers/).
