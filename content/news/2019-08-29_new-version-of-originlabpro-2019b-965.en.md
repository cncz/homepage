---
author: petervc
date: 2019-08-29 15:14:00
tags:
- medewerkers
- studenten
title: New version of OriginLabPro (2019b, 9.65)
---
A new version (2019b, 9.65) of [OriginLabPro](/en/howto/originlab/),
software for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. See [the website of
OriginLab](https://www.originlab.com/2019b) for all info on the new
version.The license server supports this new version. Departments that
take part in the license can request installation and license info from
C&CZ, also for standalone use. Installation on C&CZ managed PCs still is
being planned. Departments that want to start using OriginLab should
contact [C&CZ](/en/howto/contact/).
