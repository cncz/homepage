---
title: New computer lab pcs in summer break
author: petervc
date: 2023-05-16
tags:
- medewerkers
- studenten
cover:
  image: img/2023/Dell-AiO-W10-U22.jpg
---
During the summer break all pcs in the [computer labs](https://cncz.science.ru.nl/en/howto/terminalkamers/) will be replaced, because they are between four and seven years old. 
The new pcs will again be dual-boot, with Windows10 22H2 and Ubuntu 22.04.
Lecturers who want to test their Linux course software, can do so on the Linux loginserver lilo8, that has Ubuntu 22.04.
The new pcs are [Dell Optiplex 7400 All-In-One](https://www.dell.com/nl-nl/shop/desktops-en-werkstations/optiplex-7400-all-in-one/spd/optiplex-7400-aio/).

For employees with pcs with older versions of Ubuntu, this might be a good time to upgrade to Ubuntu 22.04 too. Please [contact C&CZ](/en/howto/contact) to schedule the upgrade.

The only computer lab that will not be upgraded is TK053 (HG02.053), this room will no longer be used as a computer lab.
