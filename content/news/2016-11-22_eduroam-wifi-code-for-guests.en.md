---
author: petervc
date: 2016-11-22 10:40:00
tags:
- medewerkers
- docenten
title: Eduroam wifi code for guests
---
On the [RU portal](https://portal.ru.nl), RU staff can find a day code
and phone number, with which they can give ther guests
[wifi](/en/howto/netwerk-draadloos/) access. Guests then use a separate
part of the Eduroam wireless network. For more information, see the [ISC
website](http://www.ru.nl/ict-uk/staff/wifi/wifi-visitors/).
