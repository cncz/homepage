---
author: visser
date: 2020-05-18 13:51:00
tags:
- studenten
- medewerkers
- docenten
title: Mailquota verhoogd naar 5 GB
---
De Science mail[quota](/nl/howto/quota-bekijken/) zijn naar 5 GB
opgehoogd voor iedereen die een lager quotum had. Wie meer quota nodig
heeft, kan dat altijd aanvragen via een mail naar postmaster. De Science
mail wordt elke avond [gebackupt](/nl/howto/backup/).
