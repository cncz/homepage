---
author: bram
date: 2011-09-28 17:16:00
tags:
- studenten
- medewerkers
- docenten
title: 'Versioning of text and source code: Subversion hosting'
---
Are you developing software? Do you write code? You’ve probably created
some zip files of working versions to fall back to. Sounds familiar? You
can now use the [Subversion service of C&CZ](/en/howto/subversion/).
[Subversion](http://subversion.apache.org/) supports you with versioning
of source code files. Using Subversion, progress becomes visible and
there’s always an ‘undo button’. Ultimately, it’s a nice way to manage
any kind of collaborative work where multiple individuals are editing
the same files. The Subversion service is available for people with a
Science account. Repository access is also possible for people from
outside the faculty.
