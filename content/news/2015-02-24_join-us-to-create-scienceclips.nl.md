---
author: caspar
date: 2015-02-24 16:02:00
tags:
- medewerkers
- studenten
title: Maak samen met ons ScienceClips
---
Wij zijn op zoek naar docenten die graag willen experimenteren met de
toepassing van video in hun onderwijs. Op de
[ScienceClips](http://clips.science.ru.nl/) website (Engels) is
aanvullende informatie te vinden en hebben we een aantal voorbeelden
verzameld. Bent u geïnteresseerd in het maken van een ScienceClip voor
uw vak of wilt u meedoen met het pilot project met een eigen
concept/idee? Neem dan alstublieft contact op met Caspar Terheggen,
C&CZ.
