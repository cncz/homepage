---
author: caspar
date: 2013-03-25 16:25:00
tags:
- medewerkers
- docenten
title: Electronisch declareren met i-Expense, ook vanaf thuis/Internet
---
Met de invoering van het electronisch
declareren via
i-Expense, een Oracle module in BASS-Finlog - het financiële systeem van
de universiteit, is het aantal BASS-gebruikers ineens fors toegenomen.
Voor de nieuwe gebruikers is het van belang te weten of zij rechtstreeks
toegang tot BASS hebben of dat zij via de Port Forwarder dienen te
werken. Sinds kort is toegang vanaf thuis/Internet mogelijk indien
gebruik gemaakt wordt van [VPN](/nl/howto/vpn/). Nadere informatie is te
vinden op de pagina over [BASS](/nl/howto/bass/) en in een eerder
nieuwsartikel
over het gebruik van BASS in combinatie met de RU firewall.

Inloggen in BASS zal niet vanaf elke werkplek automatisch goed gaan
omdat de aansluiting tussen infrastructuur en de verschillende
PC-systemen nog niet overal optimaal is. Mocht het inloggen onverhoopt
niet of lastig lukken, dan is voorlopig het indienen op papier nog zeer
zeker mogelijk, totdat de inlogbarrières zijn overwonnen. De centrale
organisatie werkt samen met C&CZ aan deze verbeteringen.
