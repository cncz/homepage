---
author: petervc
date: 2018-08-31 12:33:00
tags:
- medewerkers
- studenten
title: Eduroam CAT voor instellen Eduroam
---
Het [ISC](https://www.ru.nl/ict/) heeft het instellen van het [draadloze
netwerk](https://www.ru.nl/draadloos) eenvoudiger en veiliger gemaakt
door over te gaan op [Eduroam CAT](https://cat.eduroam.org). Hierbij
krijgen medewerkers en studenten van de Radboud universiteit een op de
RU campus toegesneden installatiebestand aangeboden. Eduroam CAT
ondersteunt op dit moment diverse versies van Windows, MacOS, OS X,
Linux, Chrome OS, Android en een generieke EAP configuratie. Om Eduroam
CAT te gebruiken is Internetconnectiviteit noodzakelijk, dat kan op de
campus het netwerk Eduroam-config zijn, maar dan moet men [de specifieke
RU-instructies](https://www.ru.nl/ict/medewerkers/wifi/eduroam-instellen/)
volgen.
