---
author: petervc
date: 2011-09-14 11:11:00
tags:
- studenten
- medewerkers
title: Adobe Creative Suite 5.5 Web/Design Premium
---
De nieuwste versie van de [Adobe Creative Suite Design/Web Premium
Bundel](http://www.adobe.com/products/creativesuite), 5.5, is
beschikbaar voor MS-Windows en Mac OS X. Zowel de Engelstalige als de
Nederlandstalige versie is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden en [te
leen](/nl/howto/microsoft-windows/). De licentievoorwaarden staan gebruik
op RU-computers toe en ook thuisgebruik door medewerkers. Eigen DVD’s
kunnen ook op [Surfspot](http://www.surfspot.nl) besteld worden.
