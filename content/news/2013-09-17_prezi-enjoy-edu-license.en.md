---
author: caspar
date: 2013-09-17 11:45:00
tags:
- medewerkers
- studenten
- docenten
title: Prezi Enjoy Edu license
---
[Prezi](/en/howto/prezi/) is a cloud based application to create and show
presentations. It is an alternative for powerpoint. For teachers and
students an extended license is available for free, the so called “Enjoy
Edu” license. This license offers more storage space, the right to make
presentations private, and the right to use your own logo instead of the
Prezi logo.
