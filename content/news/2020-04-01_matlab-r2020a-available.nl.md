---
author: petervc
date: 2020-04-01 14:58:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2020a beschikbaar
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2020a, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is R2020a binnenkort beschikbaar, een
oudere versie (/opt/matlab-R2019b/bin/matlab) is nog tijdelijk te
gebruiken. Op de door C&CZ beheerde Windows-machines zal Matlab tijdens
het semester niet van versie veranderen om versieafhankelijkheden bij
lopende colleges te voorkomen.
