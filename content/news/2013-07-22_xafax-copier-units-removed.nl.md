---
author: petervc
date: 2013-07-22 16:23:00
tags:
- studenten
- medewerkers
- docenten
title: Xafax-kopieerpalen verwijderd
---
Zoals [eerder aangekondigd](/nl/news/), zullen vanaf 23 juli 2013
de Xafax-kopieerpalen verwijderd worden. Korte tijd later zullen ook de
Ricoh MFP’s verwijderd worden. De nieuwe [Konica Minolta
MFP’s](/nl/howto/km/) zijn al enkele weken beschikbaar. FNWI-medewerkers
kunnen [van C&CZ](/nl/howto/contact/) een pincode krijgen om te kunnen
kopiëren op de nieuwe KM MFP’s. Daarom verwachten we geen problemen met
het verwijderen van de Xafax-palen. Studenten kunnen [kopiëren op de
Peage-MFP](http://www.ru.nl/ictservicecentrum/studenten/peage/kopieren/)
bij het restaurant, of ze kunnen scannen en daarna printen.
