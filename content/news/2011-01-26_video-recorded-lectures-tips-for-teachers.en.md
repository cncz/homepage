---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: 'Video recorded lectures: tips for teachers'
---
-   Please repeat questions of students, because the microphone is
    closer to you than to them.
-   Please start every sheet in your Powerpoint presentation with text,
    not with an image. This improves searching for words in the Flash
    application.
