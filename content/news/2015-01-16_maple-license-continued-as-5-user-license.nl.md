---
author: polman
date: 2015-01-16 15:39:00
tags:
- studenten
- medewerkers
- docenten
title: Maple licentie als 5-gebruikerslicentie voortgezet
---
De licentie van [Maple](/nl/howto/maple/) is voortgezet als een
5-gebruikers licentie. Hiervan worden 4 licenties betaald door de
FNWI-afdelingen [EHEF](http://www.ru.nl/ehef/nl/) en
[THEF](http://www.ru.nl/thef/). Doordat [C&CZ](/) ook 1 licentie
betaalt, kan elke FNWI-medewerker en -student incidenteel Maple
gebruiken. Belangstellenden die Maple vaker willen gebruiken, kunnen
zich melden bij [C&CZ](/nl/howto/contact/).
