---
author: petervc
date: 2017-10-12 15:35:00
tags:
- medewerkers
- docenten
title: 'SURFdrive: nu 250 GB per gebruiker'
---
Via [SURF
Nieuws](https://www.surf.nl/nieuws/2017/10/voortaan-250-gigabyte-opslag-op-surfdrive.html)
werden we erop geattendeerd dat sinds 9 oktober de opslagcapaciteit op
[SURFdrive](https://www.surfdrive.nl/) vergroot is van 100 GB naar 250
GB per gebruiker. SURFdrive is een Dropbox-achtige opslag van
werkgerelateerde bestanden, waarvan de beveiligingsclassificatie niet
kritisch of gevoelig hoort te zijn. Data zijn binnen de Nederlandse
grenzen opgeslagen, gebruikers blijven eigenaar van hun eigen data en
SURF zelf verstrekt geen informatie aan derden, waardoor SURFdrive als
veiliger dan Dropbox beschouwd kan worden.
