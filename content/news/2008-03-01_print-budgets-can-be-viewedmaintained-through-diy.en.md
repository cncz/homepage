---
author: petervc
date: 2008-03-01 00:46:00
title: Print budgets can be viewed/maintained through DIY
---
It is now possible to look at and maintain printbudgets through the
[Do-It-Yourself](https://dhz.science.ru.nl/index?Language=en)
selfservice website for Science logins. For more info see the [DHZ
page](/en/howto/dhz/).
