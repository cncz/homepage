---
title: Beheer Science accounts met DHZ
author: bram
date: 2022-11-15
tags: [ contactpersonen ]
cover:
  image: img/2022/dhz.png
---
Vanaf nu is het mogelijk om [Science accounts](../../howto/login) die onder jouw beheer vallen te beheren in [DHZ](https://dhz.science.ru.nl). Het overzicht van Science accounts waar je contactpersoon van bent, is te vinden in het menu "Mijn accounts".

![mijn accounts screenshot](/img/2022/mijn-accounts.png)

Voor elk van deze accounts, kun je doorklikken naar een scherm waar drie acties zijn uit te voeren:

- Doorschuiven van de controledatum. Deze datum wordt gebruikt om herinneringsmailtjes te versturen.
- Account blokkeren. Met een geblokkeerd account is het niet mogelijk om op diensten in te loggen (zoals mail, login servers, gitlab).
- Account verwijderen: Als je een account verwijdert, wordt het account per direct geblokkeerd en na een maand ook echt verwijderd.

Als de controledatum van een account verstreken is, wordt er een [herinneringsmail](../../howto/emailcodes/#diy-1) verstuurd met de suggestie om in DHZ aan te geven wat er met het account moet gebeuren. Het is in de meeste gevallen dus niet meer nodig om op deze mail een reply te sturen.
