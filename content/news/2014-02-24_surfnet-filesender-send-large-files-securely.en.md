---
author: petervc
date: 2014-02-24 13:59:00
tags:
- medewerkers
title: 'SURFnet FileSender: send large files securely'
---
Since February 24, securely transfering large files has become possible
for employees with a `@science.ru.nl` mail address with the experimental
version of [SURFnet FileSender](https://filesender.surfnet.nl). Files up
to 100GB can be sent to a maximum of 25 recipients. You can also send
others a “guest voucher” with which they can make use of this service.
The transfer of files is encrypted (https) to a server of SURFnet in the
Netherlands. FileSender is often more useful than alternatives because
of the maximum size or the [information
security](/en/howto/informatiebeveiliging/). The SURFnet FileSender
service is based on [the FileSender project](http://www.filesender.org),
in which SURFnet participates.
