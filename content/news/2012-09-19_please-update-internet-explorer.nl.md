---
author: petervc
date: 2012-09-19 12:11:00
tags:
- studenten
- medewerkers
title: Update svp Internet Explorer\!
---
Update 24 september: Microsoft heeft een [update voor Internet
Explorer](https://technet.microsoft.com/en-us/security/bulletin/ms12-063)
uitgebracht. Het wordt dringend geadviseerd die te instaleren voordat
men Internet Explorer gebruikt om het Internet op te gaan.

19 september: Vanwege een recent ontdekte [kwetsbaarheid in Microsoft
Internet Explorer (versies 6 t/m
9)](http://www.waarschuwingsdienst.nl/ID/WD-2012-083) wordt geadviseerd
de komende tijd niet met Internet Explorer het Internet op te gaan.
Bezoeken van niet-besmette websites in ru.nl kan natuurlijk nog wel.
Gebruik alternatieven als Google Chrome of Mozilla Firefox. Ook zonder
installatie kan men bv [Firefox
Portable](http://portableapps.com/apps/internet/firefox_portable)
gebruiken.
