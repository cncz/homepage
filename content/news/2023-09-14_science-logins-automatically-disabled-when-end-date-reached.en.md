---
title: Science login accounts automatically disabled when end date reached
author: petervc
date: 2023-09-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/account-disabled.jpg
---
In accordance with new RU policy, from November 1, Science login accounts will be disabled automatically when the end date is reached.
From that moment it is not possible to use that account for C&CZ services.

* One month **before** the end date, the responsible contact person will be notified.
* If the end date is only **two weeks** ahead, the account holder will be notified.
* One month **after** the end date, the account will be removed from C&CZ managed services like the Science mailservice and homedirectory.

It is advisable for contact persons to take a more pro-active approach to maintaining the end-dates of the accounts they are responsible for, to prevent accidental disabling of accounts.
Continuation of an account can also be a security risk, so end-dates should not be extended unreasonably long.
