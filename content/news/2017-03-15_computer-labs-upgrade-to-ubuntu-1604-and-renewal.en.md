---
author: john
date: 2017-03-15 18:35:00
tags:
- medewerkers
- docenten
- studenten
title: 'Computer labs: upgrade to Ubuntu 16.04 and renewal'
---
During the summer break all [computer labs](/en/howto/terminalkamers/)
will be (re)installed with Ubuntu 16.04 LTS. Since months ago one can
test course software using the Ubuntu 16.04
[loginserver](/en/howto/hardware-servers/) lilo5. All four year old PCs
in TK029 (HG00.029), TK206 (HG00.206), TK253 (HG02.253), library, study
landscape, project rooms and college rooms will be replaced by new ones,
(All-in-One, dual boot with Windows7 and Ubuntu 16.04 Linux).
