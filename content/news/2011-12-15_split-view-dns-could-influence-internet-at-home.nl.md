---
author: petervc
date: 2011-12-15 15:08:00
tags:
- studenten
- medewerkers
- docenten
- dns
title: Invoering split-view DNS kan gevolgen hebben voor Internet thuis
---
Wie thuis een `science.ru.nl` nameserver als `ns1.science.ru.nl`
ingesteld heeft, kan die DNS-servers nu beter uit de lijst van
DNS-servers halen. De reden is de invoering van [split-view
DNS](http://en.wikipedia.org/wiki/Split-horizon_DNS), waarbij interne
(RU op de campus) pc’s andere antwoorden kunnen krijgen dan externe
klanten. Dit is een aantal maanden geleden voor o.a. de DNS zones
`ru.nl` en `heyendaal.net` door het [UCI](http://www.ru.nl/uci)
ingevoerd in het kader van
[ICT-beveiliging](http://www.ru.nl/ict-beveiliging/).
