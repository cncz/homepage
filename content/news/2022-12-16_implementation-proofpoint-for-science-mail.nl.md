---
title: Implementatie Proofpoint voor Science mail
author: bbellink
date: 2022-12-16
tags:
- email
cover:
  image: img/2022/proofpoint.png
---
De laatste tijd zien we helaas een sterke toename van succesvolle [phishing](/nl/howto/know-about-phishing) pogingen in het Science domein, hier is vorige week al een bericht over verstuurd om hier aandacht voor te vragen.

Tegelijkertijd is er hard gewerkt, in samenwerking met ILS, om de centraal gebruikte anti-phishing tool van Proofpoint ook in te gaan zetten voor FNWI. De centrale RU mailomgeving wordt nu al enige tijd aangevuld met deze zeer effectieve anti-phishing maatregel.

Op het moment dat de science mail domeinen ook 'achter' deze anti phishing tool worden geplaatst zal er voor de gebruikers, die hier nog niet mee te maken hebben gehad, het één en ander gaan veranderen. Meer informatie hierover is te vinden op https://www.ru.nl/ict/algemeen/proofpoint/.

Een belangrijk verschil voor gebruikers die een niet door ILS beheerde werkplek gebruiken is dat er geen meldknop aanwezig is in het mailprogramma om zelf phishing te melden. Ontvang je een phishing mail die niet wordt geblokkeerd door Proofpoint, dan willen we je vragen om dit te melden bij [C&CZ](/nl/howto/contact).

Een ander gevolg is dat eventueel aanwezige links (URL’s) in de ontvangen mail herschreven worden en is er een webportal beschikbaar waarin door Proofpoint geblokkeerde mail is te zien die in quarantaine is geplaatst. In deze portal zijn veel opties beschikbaar zoals bijvoorbeeld het vrijgeven van mail en het toestaan of blokkeren van afzenders. De C&CZ helpdesk kan je hierbij ook ondersteunen als je hier vragen over hebt.

Er bestaat op dit moment geen manier voor individuele gebruikers om je voor Proofpoint af te melden met je RU of Science mail-adres.

We gaan ervan uit dat deze maatregel ertoe zal gaan leiden dat we minder kwetsbaar worden voor phishing van Science logins en hiermee de informatieveiligheid kunnen verbeteren. Als dit leidt tot vragen of problemen in de afhandeling van mail dan horen we dit natuurlijk graag.

Alvast bedankt voor je begrip.
