---
author: petervc
date: 2014-04-17 13:08:00
tags:
- studenten
- medewerkers
title: ArcGIS campuslicentie?
---
De [ArcGIS](http://www.esri.nl/producten/arcgis-platform) (Geografisch
Informatie Systeem) software suite wordt binnen de [Faculteit
Managementwetenschappen](http://www.ru.nl/fm/) steeds meer gebruikt,
waardoor het huidige aantal beschikbare licenties niet meer toereikend
is. Zij willen daarom graag weten of er belangstelling bestaat om deel
te nemen aan een campuslicentie ArcGIS. Een campuslicentie voor 3 jaar
kost het eerste jaar €5500 en de volgende jaren €8500/jaar. Interesse
graag [melden bij C&CZ](/nl/howto/contact/).
