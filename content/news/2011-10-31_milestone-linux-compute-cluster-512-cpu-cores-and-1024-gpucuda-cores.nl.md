---
author: petervc
date: 2011-10-31 15:05:00
tags:
- studenten
- medewerkers
title: 'Mijlpaal Linux Rekencluster: 512 CPU-cores en 1024 GPU/CUDA-cores'
---
Begin 2006 is gestart met een [facultair Linux
rekencluster](/nl/howto/hardware-servers/), door de aanschaf van
rackmountable servers met 4 cores door diverse afdelingen van FNWI. De
laatste toevoegingen aan het cluster, een machine met 48 CPU-cores en
een machine met 12 CPU-cores en 1024 CUDA GPU-cores in 2 [NVIDIA Tesla
M2090](http://en.wikipedia.org/wiki/Nvidia_Tesla) kaarten, hebben de
clustergrootte op deze mijlpaalgetallen gebracht. Om CUDA-cores te
gebruiken moet de broncode herschreven worden. De laatste versie van
[Matlab](/nl/howto/matlab/) heeft
[ondersteuning](http://www.mathworks.nl/company/newsletters/articles/gpu-programming-in-matlab.html?s_v1=32543176_1-AL7T9)
voor het gebruik van CUDA-cores. Net als op de werkplekken wordt ook op
deze servers Fedora Linux langzamerhand vervangen door [Ubuntu LTS
Linux](http://en.wikipedia.org/wiki/Ubuntu_%28operating_system%29).
