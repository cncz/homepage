---
author: petervc
date: 2013-01-03 14:21:00
tags:
- studenten
- medewerkers
title: SPSS 21 en AMOS op Install-schijf
---
De nieuwste versie van [IBM
SPSS](http://www-01.ibm.com/software/analytics/spss/products/statistics/),
21, is voor MS-Windows, Mac OS X en Linux op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. SURFdiensten
meldde ook dat het uitgebreid is met o.a. Amos, Modeller en
Bootstrapping. De licentievoorwaarden staan gebruik op RU-computers toe
en ook thuisgebruik door RU-medewerkers en RU-studenten. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Eigen DVD’s kunnen
ook op [Surfspot](http://www.surfspot.nl) besteld worden.
