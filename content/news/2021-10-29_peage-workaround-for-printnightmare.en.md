---
author: petervc
date: 2021-10-29 14:58:00
tags:
- medewerkers
- docenten
- studenten
title: 'Peage: workaround for PrintNightmare'
---
C&CZ has a workaround that one can try if on a self-managed/BYOD Windows
pc one has problems printing due to the [RU Peage PrintNightmare
issue](https://meldingen.ru.nl/detail.php?id=1167&lang=en&tg=0&f=0ⅈ).
This workaround can be found at the [C&CZ Peage
page](https://wiki.cncz.science.ru.nl/index.php?title=Peage&setlang=en#.5BPrintNightmare_probleem_workaround.5D.5BPrintNightmare_problem_workaround.5D).
