---
author: polman
date: 2012-11-28 16:52:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuwe TeX Live voor Windows beheerde werkplek
---
Op de S-schijf is [texlive2012](http://www.tug.org/texcollection)
geïnstalleerd, daarnaast is [texmaker](http://www.xm1math.net/texmaker/)
op de S-schijf toegevoegd, dit is een LaTeX editor, Om de paden naar de
onderliggende programma’s in texmaker goed te zetten kan eenmalig vanuit
texmaker (opstarten via S:\\texmaker\\texmaker.exe) uit het Options menu
-\> Settings File -\> “Replace the settings file by a new one” gekozen
worden om texmaker.ini te overschrijven, kies daartoe het bestand
S:\\texmaker\\texmaker.ini en klik OK. Dit gaat er vanuit dat het
bewaren van instellingen via het roaming profile aan staat, anders moet
dit iedere keer herhaald worden na inloggen, zie [Dhz](/nl/howto/dhz/).
