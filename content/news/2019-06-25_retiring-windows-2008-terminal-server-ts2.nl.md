---
author: wim
date: 2019-06-25 14:44:00
title: Opheffen Windows 2008 Terminalserver ts2
---
Half juli 2019 wordt de Windows 2008 Terminalserver ts2 uitgezet.
Vroeger was deze terminalserver nodig om BASS of Tracelab te gebruiken
voor niet-Windows gebruikers. Neem [contact op met
C&CZ](/nl/howto/contact/) als u hierdoor een probleem denkt te krijgen
