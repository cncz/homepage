---
author: petervc
date: 2019-08-27 13:08:00
tags:
- medewerkers
title: SLTN nieuwe leverancier servers en opslag
---
De [Europese aanbesteding van servers en
opslag](https://www.ru.nl/inkoop/aanbestedingen/lopende/) is door
leverancier [SLTN](https://www.sltn.nl/) gewonnen, het contract start
per 1 september 2019. De afgelopen jaren was [Centralpoint (Scholten
Awater)](https://www.centralpoint.nl/) leverancier van servers.
