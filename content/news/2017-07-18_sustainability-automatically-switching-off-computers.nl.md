---
author: visser
date: 2017-07-18 15:41:00
tags:
- medewerkers
title: 'Duurzaamheid: automatisch uitzetten pc''s'
---
Als een afdeling de door C&CZ beheerde pc’s die op dat moment niet in
gebruik zijn, na werktijd en/of lunchpauze uit wil laten schakelen, dan
kan C&CZ dat automatiseren. Neem hiervoor [contact op met
postmaster](/nl/howto/contact/). De pc’s in de [pc-cursuszalen,
studielandschap en bibliotheek](/nl/howto/terminalkamers/) worden al
jaren op deze manier na sluitingstijd uitgezet en ook meerdere keren per
dag als ze niet in gebruik zijn. Met dit mechanisme worden overigens ook
de Windows- en Ubuntu Linux-updates ’s nachts automatisch geïnstalleerd.
