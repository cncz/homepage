---
author: polman
date: 2014-09-19 17:57:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van Mathematica (10.0)
---
Er is een nieuwe versie (10.0) van [Mathematica](/nl/howto/mathematica/)
geïnstalleerd op alle door C&CZ beheerde Linux systemen. De
installatiebestanden voor Windows, Linux en Apple Mac zijn op de
[Install](/nl/howto/install-share/)-schijf te vinden. Afdelingen die
meebetalen aan de licentie kunnen bij C&CZ de licentie- en
installatie-informatie krijgen.
