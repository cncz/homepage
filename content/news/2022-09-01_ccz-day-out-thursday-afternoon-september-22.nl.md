---
author: petervc
date: 2022-09-01 13:30:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: donderdagmiddag 22 september'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor donderdag 22
september. Voor bereikbaarheid in geval van ernstige storingen wordt
gezorgd.
