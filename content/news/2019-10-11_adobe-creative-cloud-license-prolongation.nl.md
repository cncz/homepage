---
author: petervc
date: 2019-10-11 13:36:00
tags:
- studenten
- medewerkers
- docenten
title: Adobe Creative Cloud licentieverlenging
---
De Adobe [Creative Cloud
software](https://www.adobe.com/nl/creativecloud.html) (Acrobat,
Illustrator, Photoshop etc.) geeft bij opstarten de mededeling dat de
licentie op 29 november 2019 afloopt. Voor de door C&CZ beheerde pc’s
zal dit voor die tijd opgelost worden. Wie zelf een Windows pc of Mac
beheert, die eigendom van FNWI is en waarop de Adobe Creative Cloud
software geïnstalleerd is, kan zich wenden tot de [C&CZ
helpdesk](/nl/howto/contact/) om de licentie te laten verlengen. Zelf de
afloopdatum controleren kan met AdobeExpiryCheck op de
[Install-schijf](/nl/howto/install-share/).
