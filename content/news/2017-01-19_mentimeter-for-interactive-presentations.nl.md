---
author: fmelssen
date: 2017-01-19 18:18:00
tags:
- medewerkers
- studenten
title: Mentimeter voor interactieve presentaties
---
De RU heeft een licentie genomen voor het gebruik van
[Mentimeter](https://www.mentimeter.com/join/ru/) voor het studiejaar
2016-2017. Mogelijk zal de licentie daarna worden gecontinueerd.
Mentimeter is een tool om een presentatie interactief te maken:
aanwezigen kunnen anoniem via telefoon, laptop of tablet stemmen op
vragen en stellingen. Feedback kan onmiddellijk worden getoond.
Mentimeter lijkt op de oudere tool
[Shakespeak](https://www.shakespeak.com/), maar biedt meer
functionaliteit en is volledig browser-georiënteerd (werkt niet via een
Powerpoint plugin zoals Shakespeak). Iedereen met een mailadres binnen
het RU-domein kan zich [aanmelden](https://www.mentimeter.com/join/ru/).
