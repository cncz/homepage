---
title: New A0 poster printer, twice as fast
author: petervc
date: 2023-11-21
tags:
- medewerkers
- studenten
cover:
  image: img/2023/posterprinter.jpg
---
Because the old poster printer broke down, we ordered an HP Designjet Z6 44 inch postscript.
The new printer is twice as fast, so the speed of service will be even better.
See [the poster printer page](https://cncz.science.ru.nl/en/howto/print/#posters) for all details.
