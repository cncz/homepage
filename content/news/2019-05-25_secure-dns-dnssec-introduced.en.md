---
author: visser
date: 2019-05-25 23:22:00
tags:
- studenten
- medewerkers
- docenten
- dns
title: Secure DNS (DNSSEC) introduced
---
As a requisite for the RU project “Introduction Safe Email Standards”
project, that deals with the list of mandatory open standards of the
[Pas-Toe-of-Leg-Uit](https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68)
list, C&CZ introduced
[DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions)
for all mail domains within ru.nl administered by C&CZ (science.ru.nl,
cs.ru.nl, astro.ru.nl, math.ru.nl, …).
