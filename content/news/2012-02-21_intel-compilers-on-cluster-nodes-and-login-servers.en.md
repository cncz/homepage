---
author: polman
date: 2012-02-21 12:48:00
tags:
- medewerkers
title: Intel Compilers on cluster nodes and login servers
---
The [Intel Cluster Studio for
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/)
is available on [cluster nodes and login
servers](/en/howto/hardware-servers/) in `/opt/intel`. C&CZ bought two
licences for concurrent use of this compiler.
