---
author: remcoa
date: 2021-01-26 17:39:00
tags:
- medewerkers
- studenten
title: Einde van Science OpenID service
---
De Science [OpenID-service](https://openid.science.ru.nl/), die in 2007
ingevoerd is als authenticatiemethode voor de door C&CZ beheerde wiki’s,
zal per 1 maart 2021 gestopt worden. Alle wiki’s zullen omgezet worden
naar een modernere vorm van authenticatie via
[SAML](https://nl.wikipedia.org/wiki/Security_Assertion_Markup_Language).
