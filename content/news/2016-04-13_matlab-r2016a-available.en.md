---
author: petervc
date: 2016-04-13 17:38:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2016a available
---
The latest version of [Matlab](/en/howto/matlab/), R2016a, is available
for departments that have licenses. The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-disc. All
C&CZ-managed Linux machines will soon have this version installed, an
older version (/opt/matlab-R2015b/bin/matlab) is still available
temporarily. The C&CZ-managed Windows machines will not receive a new
version during the semester to prevent problems with version
dependencies in current lectures. The R2015b release was the last
version for 32-bit Windows. R2016a supports 64-bit Windows, but not
32-bit Windows.
