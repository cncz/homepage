---
author: petervc
date: 2012-11-19 13:33:00
tags:
- studenten
- medewerkers
- docenten
title: Mailproblemen na weggeven wachtwoord aan phishers
---
Horde webmail bleek wederom misbruikt te worden voor spam. Een naïeve
gebruiker had het Science-wachtwoord aan phishers/spammers gegeven,
waardoor dit mogelijk werd. Nadat eerst horde stopgezet is, is
vrijdagochtend vroeg de login van deze gebruiker afgezet en horde weer
herstart. Zaterdagochtend bleek deze kortdurende spam-outbreak toch
reden geweest te zijn voor de beheerders van hotmail.com om onze
uitgaande mailserver op hun zwarte lijst te zetten. Daarom hebben we
zaterdagochtend deze server een ander IP-adres gegeven.
