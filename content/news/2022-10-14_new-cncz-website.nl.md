---
title: Nieuwe C&CZ website
author: bram
date: 2022-10-14
tags:
- medewerkers
- studenten
cover:
  image: img/2022/cncz.png
---
De C&CZ website is in een heel nieuw jasje gestoken. De update maakt de website bruikbaar op smartphones. We zullen ook zorgen dat we via de website medewerkers en studenten kunnen informeren zelfs als alle RU-diensten down zijn. De inhoud van de oude site is overgezet. De oude website is voorlopig nog beschikbaar op https://old.cncz.science.ru.nl. Voor vragen en of opmerkingen, neem contact op met [Postmaster](/nl/howto/contact).
