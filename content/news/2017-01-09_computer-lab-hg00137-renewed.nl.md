---
author: john
date: 2017-01-09 11:15:00
tags:
- medewerkers
- docenten
- studenten
title: Terminalkamer HG00.137 vernieuwd
---
In de kerstvakantie zijn alle pc’s uit
[Terminalkamer](/nl/howto/terminalkamers/) TK137 (HG00.137) vervangen
door hetzelfde type PC’s als bij [de vorige
vervanging](/nl/news/). Dit zijn PC’s van het type [Dell OptiPlex
7440 AIO](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd) met
24-inch Full-HD scherm, een Core i5 6500 (3.2GHz,QC) processor, 1x8GB
geheugen, 512GB SSD harde schijf en vrij stille [Cherry Stream 3.0
toetsenborden](https://www.techpowerup.com/216759/cherry-announces-the-stream-3-0-keyboard).
Voor mogelijke toepassing bij digitale tentamens is ook gekozen voor de
[Dell Articulating
Stand](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd) als
voet.
