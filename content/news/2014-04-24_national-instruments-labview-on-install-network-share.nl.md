---
author: petervc
date: 2014-04-24 15:58:00
tags:
- software
cover:
  image: img/2014/labview.png
title: National Instruments LabVIEW op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, zijn alle DVD’s van de versie “Fall 2013” op de
[Install](/nl/howto/install-share/)-netwerkschijf gezet. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Wanneer de
2014-versie arriveert, zal die ook op de Install-schijf gezet worden.
