---
author: petervc
date: 2016-02-23 17:26:00
tags:
- medewerkers
- studenten
title: IPv6 pilot project
---
C&CZ and [ISC Networks](http://www.ru.nl/isc) jointly started a pilot
project with [IPv6](https://en.wikipedia.org/wiki/IPv6) networks early
2016. The aim of the project is to gain knowledge on and experience with
IPv6 in order to implement it successfully on all campus networks.
Currently there are two [dual-stack
Vlans](https://en.wikipedia.org/wiki/IPv6#Dual_IP_stack_implementation),
1 for clients and 1 for servers. Since this is a pilot, no guarantees
are given and IPv6 connectivity can be shut down for changes and
maintenance. One expected change is the moving of the SURFnet IPv6
connection from the Huygens building to the Forum (ISC building). Until
then, only outlets in the FNWI buildings can be connected to this
network. Joining the pilot is thus only possible at the moment through
C&CZ Networks, netmaster\@science.ru.nl, for outlets in FNWI buildings.
