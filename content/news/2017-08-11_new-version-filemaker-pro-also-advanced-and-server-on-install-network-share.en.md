---
author: petervc
date: 2017-08-11 15:16:00
tags:
- medewerkers
- docenten
title: New version Filemaker Pro, also Advanced and Server, on Install network share
---
A new versions of [Filemaker
Pro](http://www.filemaker.com/products/filemaker-pro/), [Filemaker Pro
Advanced](http://www.filemaker.com/nl/products/filemaker-pro-advanced/)
and of [Filemaker
Server](http://www.filemaker.com/products/filemaker-server/), 16, for
Windows (32- and 64-bit) and Mac is available on the
[Install](/en/howto/install-share/) network share. The license permits
the use on university computers. License codes can be obtained from C&CZ
helpdesk or postmaster. For home PCs, the software is available through
[Surfspot](http://www.surfspot.nl).
