---
title: "Superchown"
date: 2024-04-09T10:05:14+02:00
author: miek
cover:
  image: img/2024/superchown.png
---

We hebben een nieuwe service **superchown** uitgerold. Met deze service kun je, als eigenaar van een volume,
het eigenaarschap van bestanden in het volume veranderen. Normaal moet je daar
[onze hulp](../../howto/contact) voor inroepen, maar met **superchown** kun je dat nu
zelf.

Op dit moment werkt het als een Unix utility die geïnstalleerd is op de
[LILOs](/nl/howto/hardware-servers/#linux-login-servers). De *manual page* heeft alle details:

~~~
man superchown
~~~

Mocht je vragen hebben naar aanleiding van deze service, of je wilt **superchown** gaan gebruiken
[stuur dan ons een mailtje](../../howto/contact).
