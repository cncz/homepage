---
author: bbellink
date: 2020-09-23 23:15:00
tags:
- medewerkers
title: AWS Research Immersion Day for RU Tuesday Sep 29, 13:00-16:00
---
`13:00 – 13:05: Welcome`
`13:05 – 13:45: Research Computing on AWS (including short introductions on cost management, data privacy and sustainability)(various speakers)`
`13:45 – 14:25: Jupyter Notebooks on AWS (Nicolas Metallo)`
`14:25 – 14:30: Break` `14:30 – 15:45: HPC on AWS (John Segers)`
`15:45 – 16:00: Q&A and closing`

This is an online meeting, powered by Amazon Chime. Meeting info on
request. Background information:
<https://aws.amazon.com/government-education/research-and-technical-computing>.
