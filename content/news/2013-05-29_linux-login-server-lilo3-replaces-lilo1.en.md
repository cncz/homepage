---
author: petervc
date: 2013-05-29 14:32:00
title: Linux login server lilo3 replaces lilo1
---
A new Ubuntu 12.04 Linux login server `lilo3.science.ru.nl` is
available. Therefore the six year old login server `lilo1` will be
phased out soon. The name `lilo`, that always points to the
newest/fastest login server, will also be moved from the four year old
`lilo2` to the new `lilo3`. The new `lilo3` is a Dell PowerEdge R410
with 2 Xeon L5640 processors and 32 GB memory. Please remove the old
entry of lilo(.science.ru.nl) from your \~/.ssh/known\_hosts file. For
cautious people wanting to check the fingerprint of the public RSA key
before supplying their Science-password to the new server:
`3e:b9:89:4d:53:9b:8f:4a:97:b2:a1:c3:0d:74:34:2c`.
