---
author: mkup
date: 2012-02-28 14:44:00
tags:
- medewerkers
title: Network mini switch only to be used after consultation
---
The new network switches by default allow only two different computers
(MAC addresses) for each network outlet, plugging in a third one will
shut down the outlet for 15 minutes. If it is necessary to have more
computers on a network outlet, please consult [C&CZ network
management](/en/howto/netwerkbeheer/) beforehand.
