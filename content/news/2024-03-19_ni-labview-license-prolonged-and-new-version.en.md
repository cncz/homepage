---
title: NI LabVIEW license prolonged and new version
author: petervc
date: 2024-03-19
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/labview.png
---
The license for the [LabVIEW](/en/howto/labview/) from [NI](https://ni.com) has been prolonged until April 16, 2025.
This license is for the versions 2022Q3 and higher, that use a subscription model.

The new 2024Q1 software can be found on the [C&CZ Install disc](https://cncz.science.ru.nl/en/howto/install-share/). The license server is:

	labview.science.ru.nl:28000

In order to be able to check out a license, names of pc's and/of users have to be entered by C&CZ in the license server.
For remote pcs this DNS suffix has to be added to the IPv4 DNS configuration: science.ru.nl

C&CZ manages a FlexNet license server for LabVIEW 2022Q3 and higher, with a limited
number of licenses and a deadline in April for extension of the license.
Earlier versions of LabVIEW work standalone and unlimited in terms of time.

