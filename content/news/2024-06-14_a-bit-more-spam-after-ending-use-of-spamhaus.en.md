---
title: A bit more spam after ending use of Spamhaus
author: petervc
date: 2024-06-14
tags:
- mail
- medewerkers
- studenten
cover:
  image: img/2024/spamhaus.jpg
---
The Science mailservice used [Spamhaus](https://www.spamhaus.org) for
decades as one of the blocklists to filter spam in mail.

When we were notified that we would need a commercial license that would
cost ca. 5kEUR/y, we investigated how much more spam would come through
without the Spamhaus service. This showed that users would get more spam,
but most of it would be delivered in the Spam folder. Next to that, we expect that
the spam filtering of the also used Exchange Online Protection will improve.

Therefore we stopped using Spamhaus at the end of April 2024. If many
users complain about spam getting through, we can always reconsider
this decision.
