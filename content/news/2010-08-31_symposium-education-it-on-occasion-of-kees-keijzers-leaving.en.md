---
author: petervc
date: 2010-08-31 14:29:00
title: Symposium Education-IT on occasion of Kees Keijzers leaving
---
On occasion of Kees Keijzers leaving the Science Faculty, the Education
Center (OWC) and the Computer- en Communication Department (C&CZ)
organize a symposium in the Linnaeus building Friday, October 1 from
13:15 hours: “Simulation and copying in the lecture-room: education
innovation with IT”. The symposium shows products for education, that
are a results of close cooperation between lecturers and IT workers at
short distance, with feeling for the field of study.

For the programme and detailed information, see [the invitation (in
Dutch)](/download/old/symposium_onderwijs_-_ict%2c_afscheid_kees_keijzers.pdf).
