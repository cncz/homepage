---
author: bbellink
date: 2018-05-16 14:09:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Tuesday May 29'
---
The C&CZ yearly day out is scheduled for Tuesday May 29. C&CZ can be
reached in case of serious disruptions of services.
