---
title: New version of Mathematica 14.2.0
author: arnoudt
date: 2025-01-27
tags:
  - medewerkers
  - studenten
  - software
cover:
  image: img/2024/mathematica.png
---

A new version of [Mathematica](/en/howto/mathematica/) (14.2.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`.

When connected wired or wireless on campus or through [VPN](/en/howto/vpn/),
the software for Windows, Linux and macOS can be found on the
[C&CZ Install disc](https://install.science.ru.nl/science/Mathematica/).

During the installation you must specify:

     License server: mathematica.science.ru.nl
     License: L4601-6478

According to the [Mathematica Quick Revision
History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 14.2 introduces a powerful tool in Tabular, which provides a very streamlined and efficient way to handle tables of data laid out in rows and columns, with hundreds of other functions enhanced to make use of its special features. Additional new functions and improvements have been added to enhance neural nets and LLMs, work with game theory, improve GPU computation, and expand images and videos."
