---
title: Short survey regarding the services provided by C&CZ
author: petervc
date: 2023-12-13
draft: false
tags:
- medewerkers
- studenten
cover:
   image: img/2023/survey-cncz-en.png
---
We would like to draw your attention to [a short survey regarding the services provided by C&CZ](https://u1.survey.science.ru.nl/index.php/515387?lang=en).

We would really appreciate it if you would take a few minutes to provide us with feedback.
Based on the results, we will determine how we can further improve our services for the faculty in the coming years.

Thank you very much in advance for your response!

{{< notice note >}}
The survey has now been closed. Thank you very much for the valuable feedback!
{{< /notice >}}
