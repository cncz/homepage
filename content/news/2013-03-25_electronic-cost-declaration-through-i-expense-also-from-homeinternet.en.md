---
author: caspar
date: 2013-03-25 16:25:00
tags:
- medewerkers
- docenten
title: Electronic cost declaration through i-Expense, also from home/Internet
---
Since the introduction of i-Expense, the electronic cost
declaration module
in BASS-Finlog - the financial computer system of our university, the
number of BASS users has increased enormously. New users need to be
aware whether they can access BASS directly or only through the so
called Port Forwarder. Recently, access from home/Internet has been made
possible if one uses [VPN](/en/howto/vpn/). More information on this can
be found on the BASS page and in a previous news
article about using BASS in combination with the RU firewall.

Logging on to BASS may be cumbersome or impossible from certain
workstations because the BASS infrastructure does not support all kinds
of PC-systems seamlessly. Cost declarations on paper are allowed if
logging on to BASS fails for some reason, until the logon difficulties
are solved. The central organisation and C&CZ work together to this end.
