---
author: petervc
date: 2018-05-17 12:34:00
tags:
- medewerkers
- studenten
title: New version of OriginLabPro (2018b, 9.5)
---
A new version (2018b, 9.5) of [OriginLabPro](/en/howto/originlab/),
software for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. See [the website of
OriginLab](https://www.originlab.com/2018b) for all info on the new
version.The license server supports this new version. Departments that
take part in the license can request installation and license info from
C&CZ, also for standalone use. Installation on C&CZ managed PCs still is
being planned. Departments that want to start using OriginLab should
contact [C&CZ](/en/howto/contact/).
