---
author: polman
date: 2015-09-24 11:49:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginLab (2015, 9.2)
---
Er is een nieuwe versie (2015, 9.2) van
[OriginLab](/nl/howto/originlab/), software voor wetenschappelijke
grafieken en data-analyse,beschikbaar op de
[Install](/nl/howto/install-share/)-schijf. De licentieserver heeft een
update naar deze versie gehad. Afdelingen die meebetalen aan de licentie
kunnen bij C&CZ de licentie- en installatie-informatie krijgen, ook voor
standalone gebruik. Installatie op door C&CZ beheerde PC’s moet nog
gepland worden. Afdelingen die OriginLab willen gaan gebruiken, kunnen
zich melden bij [C&CZ](/nl/howto/contact/).
