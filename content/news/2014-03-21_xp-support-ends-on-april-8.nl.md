---
author: petervc
date: 2014-03-21 14:10:00
tags:
- medewerkers
- docenten
title: XP support stopt op 8 april
---
Zoals [eerder aangekondigd](/nl/news/) zal vanaf 8 april [Windows
XP niet meer door Microsoft
ondersteund](http://windows.microsoft.com/nl-nl/windows/products/lifecycle)
worden. C&CZ adviseert daarom met klem om voor die tijd overgestapt te
zijn op een ander besturingssysteem, indien dat mogelijk is.
XP-computers die geen upgrade kunnen krijgen, b.v. omdat ze aan
meetapparatuur hangen die van XP afhankelijk is, kunnen op een
afgeschermd deel van het netwerk geplaatst worden. Upgraden kan b.v.
naar een door C&CZ beheerde [Windows
7](/nl/howto/windows-beheerde-werkplek/) en/of Ubuntu 12.04 werkplek. Op
een beheerde Windows 7 werkplek kan een afdeling lokale beheersrechten
(localadmin) krijgen. Ook is het mogelijk om een PC of laptop alleen
door C&CZ te [laten installeren](/nl/howto/windows-beheerde-werkplek/) om
daarna het beheer zelf uit te voeren. Neem s.v.p. [contact op met de
C&CZ helpdesk](/nl/howto/contact/) indien u hiervan gebruik wilt maken.
N.b. Als een PC niet voldoet aan de
[hardware-eisen](/nl/howto/windows-beheerde-werkplek/), dan zal men
nieuwe hardware aan moeten schaffen, soms alleen geheugen, soms een
nieuwe PC.
