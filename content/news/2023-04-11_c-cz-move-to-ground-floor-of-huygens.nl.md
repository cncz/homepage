---
title: C&CZ verhuist naar begane grond Huygens
author: petervc
date: 2023-04-11
cover:
  image: img/2023/we-are-moving.png
---
In de week van 17 april gaat C&CZ verhuizen naar de nieuwe kantoorruimte in vleugel 5
op de begane grond van het Huygensgebouw. De C&CZ-helpdesk blijft een deur aan de centrale straat houden in `HG00.051`. Zie ook onze [contact-informatie](/nl/howto/contact).