---
author: petervc
date: 2008-03-28 15:02:00
title: SURFnet stops Usenet News service?
---
In the [RIO](http://www.ru.nl/rio)-meeting of March 27, the
[UCI](http://www.ru.nl/uci) announced that
[SURFnet](http://www.surfnet.nl) in the future would stop the [Surfnet
Usenet News
service](http://www.surfnet.nl/nl/diensten/cc/Pages/news.aspx) unless
universities paid ca 12 k€/year for it. If somebody thinks this service
is important for their work and that one cannot simply use an
alternative like [Google groups](http://groups.google.com), one should
contact C&CZ.
