---
author: petervc
date: 2010-12-20 17:12:00
title: MS Office Visio and Project 2010
---
MS Office [Visio](http://office.microsoft.com/en-us/visio/) and
[Project](http://www.microsoft.com/project/) 2010 are available for
computers owned by the Faculty of Science, in almost all versions: NL/UK
and 32/64. Only Visio 32 NL is missing at this moment. The CD’s are
available on the [install](/en/tags/software) disc.
