---
author: petervc
date: 2008-10-28 16:49:00
title: Alternatief voor Surfnet Usenet News service
---
Het UCI
[meldt](http://www.ru.nl/uci/actueel/nieuws/Usenet_via_surfspot/) een
alternatief voor de per 1 juli gestopte Surfnet Usenet News service: de
nieuwsgroepvoorziening van [Hitnews](http://www.hitnews.eu/), die via
[SurfSpot](http://www.surfspot.nl) voor 40 euro/jaar aan te schaffen is.
