---
author: mkup
date: 2013-11-26 11:40:00
tags:
- studenten
- medewerkers
- docenten
title: Ru-wlan wireless will be discontinued, switch to Eduroam in time
---
In all buildings of Radboud University the [Eduroam wireless
network](/en/howto/netwerk-draadloos/) is now available. The wireless
network ru-wlan will be discontinued on January 15, 2014. Please switch
to Eduroam as soon as possible. One can login with *U-* or
*S-number*\@ru.nl and RU-password or with
*science-username*\@science.ru.nl and Science password. Note: Laptops
with Windows 8 sometimes do not connect automatically to Eduroam. If
this is the case, choose “Microsoft: Protected EAP (PEAP)” at the prompt
“Choose network authentication” during configuration. The iPhone, iPad
and iPod cannot cope with multiple corporate networks, so one has to
‘Forget’ ru-wlan before configuring Eduroam. Wireless network
[configuration manuals](http://www.ru.nl/draadloos/) for various devices
and operating systems are available on the [IT
Servicecenter](http://www.ru.nl/isc) website.
