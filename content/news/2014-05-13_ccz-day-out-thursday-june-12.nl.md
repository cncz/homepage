---
author: petervc
date: 2014-05-13 13:03:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: donderdag 12 juni'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor donderdag 12 juni.
Voor bereikbaarheid in geval van ernstige storingen wordt gezorgd.
