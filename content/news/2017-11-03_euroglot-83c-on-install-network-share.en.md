---
author: petervc
date: 2017-11-03 11:37:00
tags:
- studenten
- medewerkers
title: Euroglot 8.3C on Install network share
---
The most recent version (8.3C) of the [Euroglot](http://www.euroglot.nl)
translation software, is available on the
[Install](/en/howto/install-share/) network share. License codes, valid
until January 1 2019, can be requested from [the
helpdesk](/en/howto/contact/). Home use is not permitted with this
license. For home use you can buy a private license relatively cheap at
[Surfspot](http://www.surfspot.nl).
