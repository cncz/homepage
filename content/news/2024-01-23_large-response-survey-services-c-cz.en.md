---
title: Large response survey services provided by C&CZ
author: petervc
date: 2024-01-23
tags:
- medewerkers
- studenten
cover:
  image: img/2023/survey-cncz-en.png
---
We are pleased that we received 330 completed surveys. Many of these
were very positive. Some contained suggestions for improvements or new
services. We'll analyze the results further in the next months.
