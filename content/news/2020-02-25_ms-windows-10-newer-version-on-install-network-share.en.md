---
author: petervc
date: 2020-02-25 11:31:00
tags:
- medewerkers
- docenten
title: 'MS Windows 10: newer version on Install network share'
---
A newer version of [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, is available on
the [Install](/en/howto/install-share/) network share. The license
permits upgrade to this version on university computers. License codes
can be requested from C&CZ helpdesk or postmaster. For privately owned
computers you can order this software relatively cheap at
[Surfspot](http://www.surfspot.nl). The added version is: 1909.
