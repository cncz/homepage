---
author: petervc
date: 2007-11-20 14:14:00
title: Acrobat Reader, nieuwe versie met problemen
---
Op de [beheerde werkplek](/nl/howto/windows-beheerde-werkplek/) PCs is versie 7 van
[Adobe](http://www.adobe.com) Reader vervangen door versie 8.1.1,
vanwege een
[beveiligingslek](http://www.adobe.com/support/security/bulletins/apsb07-18.html).
De volgende problemen zijn geconstateerd:

1.  Printen vanuit Adobe Reader naar een HP4250/HP4350 printer gaat
    fout. Volgens Adobe ligt dit aan de printer. Alternatief: print naar
    de dali printer in het studielandschap of naar de kleurenprinters
    (HP or Tektronix). De Ricoh multifunctionele copier/printer/scanners
    die binnenkort op allerlei plaatsen ingezet gaan worden ter
    vervanging van de Oce copiers hebben het probleem ook niet.
2.  Werkplekken met een licentie voor Adobe Professional hebben (nog?)
    geen licentie om te upgraden, maar gelijktijdige installatie van
    Adobe Professional 7 met Adobe Reader 8 werkt ook niet goed.
    Mogelijk komt er binnenkort een campuslicentie voor allerlei Adobe
    produkten.
