---
author: visser
date: 2019-05-25 23:59:00
tags:
- studenten
- medewerkers
- docenten
title: 'Secure Email: end of Squirrel webmail July 1'
---
Because [Squirrel webmail](https://squirrel.science.ru.nl) can’t handle
the [STARTTLS](https://en.wikipedia.org/wiki/Opportunistic_TLS) security
level that we want to achieve, Squirrel will be phased out July 1. Users
are advised to switch to a more modern [Science webmail
system](https://wiki.cncz.science.ru.nl/Categorie:Email).
