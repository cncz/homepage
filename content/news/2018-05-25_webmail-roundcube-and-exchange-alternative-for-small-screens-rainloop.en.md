---
author: polman
date: 2018-05-25 14:02:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube and Exchange alternative for small screens: Rainloop'
---
For small screens of smartphones, the webmail of
[Roundcube](https://roundcube.science.ru.nl) is not very well suited.
That is why there is an alternative now:
[Rainloop](https://rainloop.science.ru.nl). You can set a “threaded
view” in Rainloop, which is also handy for small screens. The Science
addressbook is available in Rainloop. One can also use Rainloop for the
RU Exchange mail. But Roundcube has many other advantages over Rainloop:
manage folders shared with other users, shared labels and the
administration of Sieve filters and out-of-office messages. For more
information see our [mail page](/en/tags/email/).
