---
author: polman
date: 2017-06-22 16:59:00
tags:
- software
cover:
  image: img/2017/labview.png
title: National Instruments LabVIEW Spring 2017 on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, the newly arrived
“Spring 2017” version has been copied to the
[Install](/en/howto/install-share/) network share. Install media can also
be borrowed. License codes can be obtained from C&CZ helpdesk or
postmaster.
