---
author: theon
date: 2008-09-03 17:20:00
title: Reporting repairs/problems through the helpdesk website
---
Since a few months several employees and students use the [C&CZ helpdesk
website](https://helpdesk.science.ru.nl/) to request repairs and report
other problems. One has to login on the website with the [Science login
name](/en/howto/login/). Advantages are that if the machine is known,
like with a [Managed Workplace PC](/en/howto/windows-beheerde-werkplek/),
one doesn’t need to fill in serial numbers and that one can follow the
actions for the repair through the website.
