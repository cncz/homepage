---
author: petervc
date: 2014-03-12 12:38:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2014a beschikbaar
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2014a, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op de door C&CZ
beheerde 64-bit Linux machines is R2014a binnenkort beschikbaar, een
oudere versie (/opt/matlab-R2013b/bin/matlab) is nog tijdelijk te
gebruiken. Op de door C&CZ beheerde Windows-machines zal Matlab tijdens
het semester niet van versie veranderen om versieafhankelijkheden bij
lopende colleges te voorkomen.
