---
title: "Superchown"
date: 2024-04-09T10:05:14+02:00
author: miek
cover:
  image: img/2024/superchown.png
---

We have enabled a service called **superchown**. This service allows you as a volume owner to change
the ownership of files in the volume. Previously, you would need to [send an email](../../howto/contact) to
make these changes, but with **superchown** you can do it yourself.

Currently this is implemented as a command line utility that is available on the
[LILOs](/en/howto/hardware-servers/#linux-login-servers). Its manual page has all the details:

~~~
man superchown
~~~

If you have further questions, or want to use **superchown**, please [send us an email](../../howto/contact).
