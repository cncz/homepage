---
author: wim
date: 2020-09-24 09:00:00
tags:
- medewerkers
- docenten
- studenten
title: Microsoft Windows 10 upgrade
---
Microsoft Windows 10 upgrade beschikbaar voor C&CZ beheerde
afdelings-pc’s in het Software Center.

We raden aan om de komende weken zelf een tijdstip te kiezen om de
upgrade uit te voeren. Eind oktober zullen wij deze update verplicht
uitrollen i.v.m. de lifecycle policy voor [Windows 10
producten](https://docs.microsoft.com/en-us/lifecycle/products/windows-10-enterprise-and-education).

Voor door C&CZ beheerde afdelings-pc’s kan men met de [C&CZ
helpdesk](/nl/howto/contact/) een afspraak maken in geval van problemen.
Ook voor andere pc’s kunnen zij hulp bieden.
