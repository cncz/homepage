---
author: fmelssen
date: 2017-02-22 00:13:00
tags:
- medewerkers
- studenten
title: New phone number 25225 (BLACK) for Blackboard support staff
---
Triggered by the arrival of the new employee Maarten de Meijer of the
Education Centre, who will support Blackboard together with Fred
Melssen, we decided to pick a new phone number for Blackboard support:
(+31 24 36) 25225, that can be answered by both of them. The internal
part of the number can be easily remembered, because it [can be
spelled](https://en.wikipedia.org/wiki/Phoneword) as BLACK.
