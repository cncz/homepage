---
author: polman
date: 2014-09-19 11:22:00
tags:
- medewerkers
- studenten
title: Rekenclusters stand van zaken
---
Met de aanschaf van o.a. een server met 3072 GB RAM door Informatica in
het kader van de [RRF](http://www.radboudresearchfacilities.nl), is het
door C&CZ beheerde cn-rekencluster gegroeid tot 1324 cores en 7914 GB
RAM. Het coma-cluster van Sterrenkunde, met 320 cores, 240 TB fileserver
storage, GPU’s en snelle
[Infiniband](http://nl.wikipedia.org/wiki/InfiniBand) interconnectie is
een apart cluster. Voor meer info, zie de
[cn-rekencluster](/nl/howto/hardware-servers/) pagina.
