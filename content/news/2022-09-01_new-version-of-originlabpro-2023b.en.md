---
author: petervc
date: 2023-10-10 14:16:00
tags:
- medewerkers
- studenten
title: New version of OriginLabPro (2023b)
cover:
  image: img/2023/originlab2023b.png
---
A new version (2023b) of [OriginLabPro](/en/howto/originlab/),
software for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. See [the website of
OriginLab](https://www.originlab.com/2023b) for all info on the new
version. The license server supports this new version.
There is a support contract until August 2026. Departments
within FNWI can request installation and license info from C&CZ, also
for standalone use. Installation on C&CZ managed PCs is being planned.
In case of questions contact [C&CZ](/en/howto/contact/).
