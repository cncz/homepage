---
author: petervc
date: 2014-02-20 17:39:00
tags:
- medewerkers
- studenten
title: Scanning on the Konica Minolta MFPs
---
Update: as of February 27, the settings of all Konica Minolta MFP
network switch ports have been changed. Please [report remaining
problems to C&CZ](/en/howto/contact/).

Scanning to e-mail sometimes doesn’t (didn’t?) work on the KM MFPs. It
can temporarily be resolved by switching the machine off and on again.
An alternative is scanning to a USB stick. Log in at the MFP with the
scan pin, put a document in the feeder or on the document glass, select
`Scan` and then plug the USB stick into the USB port on the right side
of the MFP, on the top side. After a few seconds the MFP recognizes the
USB stick and presents the choice “Save a document to external memory”.
Choose `OK` and press `Start`. See also the [RU
manual](http://www.ru.nl/publish/pages/687597/uitrol-mf-scan.pdf) on the
[RU-page about the
MFP’s](http://www.ru.nl/ictservicecentrum/medewerkers/werkplek-campus/uitrol/).
