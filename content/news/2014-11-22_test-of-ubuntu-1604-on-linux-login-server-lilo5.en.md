---
author: polman
date: 2014-11-22 17:16:00
tags:
- medewerkers
- studenten
title: Test of Ubuntu 16.04 on Linux login server lilo5
---
As a test of the new version of Ubuntu, 16.04 LTS, we have installed a
new login server `lilo5.science.ru.nl`. In a few months it will replace
the five year old login server `lilo3`. The name `lilo`, that always
points to the newest/fastest login server, will then be moved from the
two year old `lilo4` to the new `lilo5`. The new `lilo5` is a Dell
PowerEdge R430 with 2 Xeon E5 2630L v4 processors with 10 cores. Because
we enabled hyper-threading, it is showing 40 processors. It has 64 GB of
memory. For users who want to check the identity of this new server
before supplying their Science-password to the new server:
`ECDSA key fingerprint is 7d:08:27:61:ac:c8:f1:05:4f:c9:91:2a:83:e5:c3:4f`.
Please [report all problems](/en/howto/contact/) with this new version of
Ubuntu Linux.
