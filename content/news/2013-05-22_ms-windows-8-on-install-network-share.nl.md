---
author: petervc
date: 2013-05-22 11:13:00
tags:
- medewerkers
- docenten
title: MS Windows 8 op Install-schijf
---
De nieuwste versie van [Microsoft Windows](http://office.microsoft.com),
8, is op de [Install](/nl/howto/install-share/)-netwerkschijf te vinden.
De licentievoorwaarden staan gebruik op RU-computers toe. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Eigen DVD’s kunnen
ook op [Surfspot](http://www.surfspot.nl) besteld worden.
