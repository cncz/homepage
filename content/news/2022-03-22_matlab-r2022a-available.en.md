---
author: petervc
date: 2022-03-22 12:16:00
tags:
- software
cover:
  image: img/2022/matlab.png
title: Matlab R2022a available
---
The latest version of [Matlab](/en/howto/matlab/), R2022a, is available
for departments that have licenses. The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-disc. All
C&CZ-managed Linux machines have this version installed, an older
version (/opt/matlab-R2021a/bin/matlab) is still available temporarily.
The C&CZ-managed Windows machines will not receive a new version during
the semester to prevent problems with version dependencies in current
lectures.
