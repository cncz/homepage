---
author: mkup
date: 2012-03-13 16:37:00
tags:
- studenten
- medewerkers
title: Gebruik svp ru-wlan i.p.v. Science draadloos\!
---
Binnenkort zullen in het Huygensgebouw en de gebouwen eromheen, de
wireless access points vervangen worden door nieuwe exemplaren met een
nieuwe techniek. Hierbij komt het draadloze netwerk `Science` te
vervallen. Ga daarom s.v.p. vooraf over op gebruik van
[`ru-wlan`](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/wireless-ru/),
dat is zowel voor als na de overgang beschikbaar. Men kan inloggen op
ru-wlan met U-nummer en RU-wachtwoord, maar ook met
`science-username@science.ru.nl` en Science-wachtwoord. Na de overgang
zullen op drukke plaatsen access points met hogere bandbreedte
([wireless-N](http://nl.wikipedia.org/wiki/IEEE_802.11#802.11n))
neergehangen worden. Na de overgang is tevens het draadloze netwerk
[eduroam](http://www.eduroam.org/) beschikbaar, waarop men in kan loggen
met `U-nummer@ru.nl` en RU-wachtwoord, maar ook met
`science-username@science.ru.nl` en Science-wachtwoord. Voor de overgang
is Eduroam in de buurt van het Huygensgebouw alleen op het terras achter
het restaurant beschikbaar. Op termijn zal ook `ru-wlan` weer verdwijnen
en blijft alleen Eduroam over. Het is overigens de bedoeling om ook
buitenruimte als de centrale ingang en grasvelden achter het
Huygensgebouw te gaan ontsluiten.
