---
author: polman
date: 2008-12-23 10:20:00
title: 'Firefox: nieuwe versie met NoScript beveiliging'
---
`Op alle beheerde pc's is de nieuwste versie van Firefox geinstalleerd met`

[NoScript](http://noscript.net/) als plugin. Deze voorkomt in eerste
instantie de uitvoering van alle scripts op webpagina’s. Per website kan
vervolgens toestemming gegeven worden om scripts tijdelijk of altijd uit
te voeren. Dit is een uitstekend hulpmiddel om de risico’s van websites
te verkleinen.
