---
author: sommel
date: 2010-03-23 10:55:00
title: Linux cups server and printing using Kerberos tickets
---
To [print from
Linux](/en/howto/printers-en-printen#.5bafdrukken-op-beheerde-linux-werkplekken.5d.5bprinting-using-a-managed-linux-pc.5d/)
to a budgetted printer you had to type your account password for every
print job. Sometimes this was problematic if the application was not
started from a terminal window. Now there is a
[CUPS](http://www.cups.org/)-server `drukwerk.science.ru.nl` which
demands a valid [Kerberos](http://www.kerberos.org/) ticket, so you do
not need to type your password for every print job. Managed Linux PCs
running Fedora 8 and above already use this CUPS-server. A Kerberos
ticket granting ticket can be obtained by typing **kinit**.

We will try to extend this CUPS-server in such a way it can be used for
Windows
[IPP-printing](http://en.wikipedia.org/wiki/Internet_Printing_Protocol).
