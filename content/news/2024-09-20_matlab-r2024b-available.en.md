---
title: Matlab R2024b available
author: arnoudt
date: 2024-09-20
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/matlab.png
---
The latest version of [Matlab](/en/howto/matlab/), R2024b, is available.
The software and license codes
[are directly available for Science accounts](https://cncz.pages.science.ru.nl/licenses/Matlab/)
and can be requested by other entitled users by sending [mail to the helpdesk](https://cncz.science.ru.nl/en/howto/contact/).
The software can also be found on the [install](/en/tags/software)-share. All
C&CZ-managed Linux machines have this version installed, the older
version (/opt/matlab-R2024a/bin/matlab) will be available temporarily.
The C&CZ-managed Windows machines will not receive a new version during
the semester to prevent problems with version dependencies in current
lectures.
