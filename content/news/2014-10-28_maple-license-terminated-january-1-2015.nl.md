---
author: polman
date: 2014-10-28 13:38:00
tags:
- studenten
- medewerkers
- docenten
title: Maple licentie stopt per 1 januari 2015
---
Doordat [Maple](/nl/howto/maple/) niet meer in het FNWI-onderwijs
gebruikt wordt en het gebruik gedaald is, is C&CZ van plan de Maple
licentie te stoppen per 1 januari 2015. Mocht dit tot problemen leiden,
neem dan s.v.p. contact op met [C&CZ](/nl/howto/contact/).
