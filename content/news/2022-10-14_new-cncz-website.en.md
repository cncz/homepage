---
title: New C&CZ website
author: bram
date: 2022-10-14
tags:
- medewerkers
- studenten
cover:
  image: img/2022/cncz.png
---
We have given the C&CZ website a completely new look. The update makes the website usable on smartphones. We will also make it possible to inform staff and students through this website even when all RU services are down. The content of the old site has been transferred. The old website is still available at https://old.cncz.science.ru.nl for the time being. For questions or comments, please contact [Postmaster](/en/howto/contact).
