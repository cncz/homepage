---
author: wim
date: 2015-06-17 09:01:00
tags:
- medewerkers
- docenten
- studenten
title: 'Computer labs: upgrade to Ubuntu 14.04 and renewal HG00.075'
---
During the summer break all [computer labs](/en/howto/terminalkamers/)
will be (re)installed with Ubuntu 14.04 LTS. Since months ago one can
test course software using the Ubuntu 14.04
[loginserver](/en/howto/hardware-servers/) lilo4. All PCs in TK075
(HG00.075) will be replaced by new ones, type [HP EliteOne 800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, i5-4570S Core, 2.9GHz, 8GB, 23 inch widescreen LCD monitor,
sound), dual boot with Windows and Linux (Ubuntu).
