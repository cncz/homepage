---
author: pberens
date: 2010-10-11 14:35:00
tags:
- docenten
title: Beelden Symposium ICT en Onderwijs online
---
symposium ICT &
Onderwijs](http://podcast.science.ru.nl/Podcasts/flashplaylist.php?courseID=oib)
dat op 1 oktober 2010 werd gehouden, zijn een aanrader voor docenten die
geïnteresseerd zijn in onderwijsvernieuwing. De sprekers: Drs. H.P.A.M.
Geurts: Opening Prof. dr. F.P.J.T. Rutjes: Chemie onderwijs in silico
Prof. dr. S.E. Speller: Een STM voor iedereen (STM=Scanning Tunneling
Microscope) Dr. E.S. Pierson: Virtual Classroom Biologie: het
springplank effect Dr. T.F. Oostendorp: ECGSIM: je houdt je hart
(virtueel) vast! (ECGSIM = Electro CardioGram SIMulatie) Prof. dr. E.
Barendsen: Wiki-technologie en samenwerkend leren
