---
author: bbellink
date: 2021-09-28 13:02:00
tags:
- medewerkers
- docenten
- studenten
title: AVD FNWI nu onderdeel van AVS Radboud
---
Per 1 oktober zal [de AVD van
FNWI](https://www.ru.nl/fnwi/faculteit/organisatie/ondersteunende-diensten/audio-visuele-dienst/)
samengevoegd worden met [AVS van Campus &
Facilities](https://www.ru.nl/facilitairbedrijf/voorzieningen/mediatechniek/).
Deze reorganisatie is een gevolg van het verbeterprogramma facilitaire
zaken en vastgoed, met bundeling van soortgelijke diensten op de campus.
Met deze bundeling ontstaat een campusbrede AV dienstverlening die in
zijn geheel door [Campus &
Facilities](https://www.ru.nl/facilitairbedrijf/) zal worden
aangestuurd. Voor gebruikers van FNWI worden vooralsnog de bestaande
contactmogelijkheden (email, telefoon) en beschikbaarheid van lokale
ondersteuning gecontinueerd conform de bestaande situatie. Bij eventuele
veranderingen in de toekomst zal hierover nog nadere communicatie
plaatsvinden. De AVD FNWI was sinds januari 2018 onder C&CZ geplaatst.
