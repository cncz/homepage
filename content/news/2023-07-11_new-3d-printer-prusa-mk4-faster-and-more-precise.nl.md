---
title: Nieuwe 3D-printer Prusa MK4 - sneller en preciezer
author: petervc
date: 2023-07-11
tags:
- medewerkers
- studenten
cover:
  image: img/2023/MK4.jpg
---
De [3D printservice](https://cncz.science.ru.nl/nl/howto/print/#3d-printen) is verbeterd met de toevoeging van een [Prusa MK4](https://www.prusa3d.com/product/original-prusa-mk4-2/).
