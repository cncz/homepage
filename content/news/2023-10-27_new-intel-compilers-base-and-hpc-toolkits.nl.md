---
title: Nieuwe Intel compilers (oneAPI Base en HPC Toolkits)
author: petervc
date: 2023-10-27
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/intel-one-api.jpg
---
De nieuwste versies van de Intel-compilers, beschikbaar onder de naam [Intel oneAPI](https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html),
hebben geen licentie meer nodig, ze zijn vrij te gebruiken.
De 2023.2 versie is geïnstalleerd op door C&CZ beheerde Ubuntu systemen, clusternodes met Ubuntu 22.04 en loginservers. Wanneer ze beschikbaar zijn, zal het module commando

   module avail

onder andere “compiler/latest” vermelden. Onder andere de compilers Intel C (icc) en Fortran (ifort) kunnen na het typen van:

   module add compiler/latest 

worden gebruikt.
