---
author: bram
date: 2015-11-23 14:31:00
tags:
- medewerkers
title: Eduroam wifi SMS-code voor gasten via mailinglist
---
Het tonen op de informatieschermen van het Huygensgebouw van de
dagelijks wijzigende [Eduroam Visitor
Access](https://eva.eduroam.nl/inloggen) (eVA) SMS-code wordt op verzoek
van SURFnet gestopt. Met deze code konden gasten zonder hulp van anderen
verbinding maken met het gastendeel van het draadloze netwerk Eduroam.
C&CZ heeft een alternatief ontwikkeld: de code kan dagelijks per mail
aan bv. secretariaten van afdelingen van FNWI gemaild worden. Dit kan
aangevraagd worden via aan mail naar [postmaster](/nl/howto/contact/).
Medewerkers die wifi-toegang aan willen vragen voor (ook groepen van)
gasten, kunnen dat doen bij
[C&CZ-Werkplekondersteuning](/nl/howto/werkplekondersteuning/), tel.
20000. De SMS-codes zullen vanaf 1 december ook verkrijgbaar zijn bij de
[Library of
Science](http://www.ru.nl/ubn/bibliotheek/locaties/library-science/).
