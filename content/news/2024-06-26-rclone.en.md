---
title: "Access network shares via other network protocols"
date: 2024-06-26T15:33:06+02:00
tags:
- storage
author: miek
cover:
  image: img/2024/rclone.png
---

If you use [C&CZ storage](/en/howto/storage/#network-shares), you now have the new option
to access these via network protocols other than SMB (for Windows and macOS) and NFS (for Linux), namely:

- `sftp`: a secure variant of FTP
- `https`: a (read-only) view of the share for browsers
- `webdavs`: a (read-write) view of the share for browsers and webdav clients
- `s3`: a read/write view using the Simple Storage Service (S3)

Each of these may be active at the same time, and optionally can have an end date, so you can "open
up" a share for writes from another organisation which then automatically "closes" at the end date.
For `sftp`, authentication is done via an SSH key.
The other protocols are authenticated with a username and password. These credentials are not tied to any person and are easily revoked or re-generated if the need arises.

This service is based on [rclone](https://rclone.org/).

Please [contact us](/en/howto/contact/) if you want more information or would like to have this configured for your
shares.
