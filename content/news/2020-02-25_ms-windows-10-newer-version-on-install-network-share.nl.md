---
author: petervc
date: 2020-02-25 11:31:00
tags:
- medewerkers
- docenten
title: 'MS Windows 10: nieuwere versie op Install-schijf'
---
Een nieuwere versie van [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan upgrade naar deze versie op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden. De toegevoegde
versies is: 1909.
