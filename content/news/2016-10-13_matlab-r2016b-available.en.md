---
author: petervc
date: 2016-10-13 17:38:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2016b available
---
The latest version of [Matlab](/en/howto/matlab/), R2016b, is available
for departments that have licenses. The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-disc. All
C&CZ-managed Linux machines will soon have this version installed, an
older version (/opt/matlab-R2016a/bin/matlab) is still available
temporarily. The C&CZ-managed Windows machines will not receive a new
version during the semester to prevent problems with version
dependencies in current lectures.
