---
author: stefan
date: 2013-05-29 17:35:00
tags:
- medewerkers
- studenten
title: A0 photo printer operational again
---
C&CZ now offers the possibility to make prints of posters and such on
high quality photo paper upto 90 cm in width. The printer
“[Kamerbreed](/en/howto/kamerbreed/)” is the same printer that used to be
operated by the Graphic Design department. The price for an A0 print is
32 euros, smaller prints are possible too.
