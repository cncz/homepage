---
title: C&CZ move to ground floor of Huygens
author: petervc
date: 2023-04-11
cover:
  image: img/2023/we-are-moving.png
---
In the week of April 17, C&CZ will to move to the new offices in wing 5
on the ground floor of the Huygens building.  The C&CZ helpdesk will still
have an entrance on the main street at `HG00.051`. See also our [contact info](/en/howto/contact).