---
author: petervc
date: 2011-05-25 11:49:00
tags:
- studenten
- medewerkers
- docenten
title: IT Security Campaign at the Faculty of Science
---
In April, May and June, C&CZ, GDI and UCI carry out an IT security
campaign throughout Radboud University. During the campaign, extra
attention will be paid to matters such as security software, keeping all
software up-to-date, using Blackboard more secure, how to choose a safe
password, how to use USB-sticks securely and ‘phishing’. The campaign at
the Faculty of Science started on Monday, May 23. On the [RU IT security
site](http://www.ru.nl/ict-beveiliging) you can find much more
information about IT security. Everything under the motto: RU Secure!
