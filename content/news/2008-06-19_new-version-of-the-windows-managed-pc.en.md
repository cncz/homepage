---
author: wim
date: 2008-06-19 16:10:00
title: New version of the Windows Managed PC
---
There is a new installation method for the [Windows-XP Managed
PCs](/en/howto/windows-beheerde-werkplek/), that has been developed and
is being maintained by several IT-support groups in the RU tohether, in
the SITO (Samenwerkende IT-Ondersteuning) project. The main difference
is that more software is installed, and that it is less dependent on
network drives. Many PCs in the [Study
Landscape](/en/howto/terminalkamers/) and several departmental PCs have
been installed with the new method.
