---
author: bram
date: 2015-08-07 16:58:00
tags:
- medewerkers
- docenten
- studenten
title: ArcGIS 10.3.1 beschikbaar
---
Er is een campuslicentie voor [ArcGIS](http://www.arcgis.com/) 10.3.1
beschikbaar. ArcGIS wordt op dit moment op alle PC’s in de
[terminalkamers](/nl/howto/terminalkamers/) geïnstalleerd en kan op
verzoek op andere beheerde Windows 7 PC’s geïnstalleerd worden. Voor
thuisgebruik van ArcGIS door studenten en medewerkers zijn de
installatiebestanden op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. Voor
thuisgebruik is een [VPN verbinding](/nl/howto/vpn/) vereist om de
licentieserver te kunnen bereiken.
