---
author: bram
date: 2021-11-23 14:50:00
tags:
- studenten
- medewerkers
title: GitLab Docker Container Registry service aangezet
---
In de door C&CZ beheerde [GitLab service](/nl/howto/gitlab/) is de Docker
Container Registry service aangezet. Zie voor meer info de [GitLab
documentatie](https://gitlab.science.ru.nl/help/user/packages/container_registry/index.md).
