---
author: petervc
date: 2014-11-16 10:45:00
tags:
- studenten
- medewerkers
- docenten
title: End of SSLv3 protocol
---
Because of
[vulnerabilities](https://www.us-cert.gov/ncas/alerts/TA14-290A) in the
[SSLv3](http://nl.wikipedia.org/wiki/Secure_Sockets_Layer) security
protocol, C&CZ has switched off support for this protocol on all C&CZ
managed web servers. A secure connection will thus use the successor
protocol [TLS](http://en.wikipedia.org/wiki/Transport_Layer_Security).
Browsers like
[Chrome](http://venturebeat.com/2014/10/30/google-plans-to-disable-fallback-to-ssl-3-0-in-chrome-39-and-remove-ssl-3-0-completely-in-chrome-40/),
[Internet
Explorer](https://technet.microsoft.com/en-us/library/security/3009008.aspx)
and
[Firefox](https://blog.mozilla.org/security/2014/10/14/the-poodle-attack-and-the-end-of-ssl-3-0/)
will each sooner or later stop supporting SSLv3. There is information on
[how to manually adjust
settings](https://scotthelme.co.uk/sslv3-goes-to-the-dogs-poodle-kills-off-protocol/)
to eliminate the vulnerabilities immediately. This can however lead to
problems with websites that need SSLv3, such as [BASS](/en/howto/bass/).
P.S.: as of mid-December BASS can be used without SSLv3.
