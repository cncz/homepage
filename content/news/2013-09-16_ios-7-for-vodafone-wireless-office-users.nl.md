---
author: bertw
date: 2013-09-16 13:56:00
tags:
- medewerkers
- docenten
title: iOS 7 voor Vodafone Wireless Office gebruikers
---
Op 18 september introduceerde Apple iOS 7. De update naar iOS 7 heeft
als consequentie dat gebruikers van iPhones met een [Vodafone Wireless
Office](/nl/tags/telefonie) abonnement van de RU, de internet (APN)
settings van hun iPhone opnieuw handmatig moeten instellen. Dit doet u
onder Instellingen / algemeen / mobiel netwerk / mobiel datanetwerk /
Mobiele Data / APN, nadat de software op de iPhone is bijgewerkt naar
iOS 7. De internet settings (APN) zijn: office.vodafone.nl
(Gebruikersnaam: vodafone en Wachtwoord: vodafone).
