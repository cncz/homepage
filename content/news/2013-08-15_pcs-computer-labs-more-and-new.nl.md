---
author: john
date: 2013-08-15 16:48:00
tags:
- studenten
- medewerkers
- docenten
title: 'PC''s onderwijszalen: meer en nieuw'
---
Op de pagina over de [terminalkamers](/nl/howto/terminalkamers/) is te
lezen hoeveel en welke PC’s beschikbaar zijn in de onderwijszalen. De
belangrijkste veranderingen zijn:

-   De oude PC’s uit TK029, TK206, Studielandschap, Bibliotheek,
    Projectkamers en Flexwerkplekken zijn vervangen door nieuwe [HP
    Compaq Elite
    8300](http://www.pcmag.com/article2/0,2817,2406799,00.asp) PC’s van
    het type [All-in-one
    PC](http://en.wikipedia.org/wiki/All-in-One_PC#All-in-one).
-   In het studielandschap zijn extra PC’s geïnstalleerd.
-   Er is een nieuwe terminalkamer TK702 met 17 PC’s.
