---
author: petervc
date: 2017-11-24 12:14:00
tags:
- medewerkers
- studenten
title: Four KM MFPs temporarily relocated for IJSO2017
---
To accommodate the large printing and copying needs of
[IJSO2017](http://www.ijso2017.nl), 4 KM MFPs will be relocated in the
period November 29 - December 11. The MFPs chosen are the least used
ones:

`HG00.038    BizHubC554-40` `HG00.645    km-pr0926`
`HG01.108    km-pr0930` `HG00.084    km-pr0928`

Apologies for any inconvenience.
