---
author: polman
date: 2021-03-09 23:20:00
tags:
- medewerkers
- studenten
title: Site license for Gaussian 16 and GaussView 6
---
Organic Chemistry purchased a site license for the use of [Gaussian
16](https://gaussian.com/gaussian16/) and [GaussView
6](https://gaussian.com/gaussview6/) with the GMMX Module on Linux. The
software has been installed on the
[cn-cluster](https://wiki.cncz.science.ru.nl/index.php?title=Hardware_servers&setlang=en#.5BReken-.5D.5BCompute_.5Dservers.2Fclusters).
Departments that want to use this, can contact Prof. Jana Roithova or
[Postmaster](/en/howto/contact/).
