---
title: "CephFS retired"
date: 2024-04-15T10:05:14+02:00
author: miek
tags:
- medewerkers
- studenten
- ceph
cover:
  image: img/2024/ceph-retired.jpg
---

After the [storage downtime](https://cncz.science.ru.nl/en/cpk/1359/) of CephFS in our Ceph cluster, we decided
together with the users of this service to stop offering CephFS shares through SMB and NFS.

All data has been copied out of CephFS.

We have ended the CephFS service. We will only use the Ceph cluster to store our backups, using the S3 API.
