---
author: mkup
date: 2017-09-06 14:30:00
tags:
- studenten
- medewerkers
- docenten
title: Netwerkonderhoud op 19 september 04:00 - 05:30
---
Op dinsdagochtend 19 sept zal onderhoud plaatsvinden aan diverse
netwerkonderdelen van de RU. Tussen ca. 04:00-05:30 uur zal binnen FNWI
geen netwerkverkeer mogelijk zijn op het vaste en draadloze netwerk. Ook
de op het netwerk aangesloten IP-telefoons zullen dan niet werken. Denk
s.v.p. na of deze verstoring voor u gevolgen heeft en bereid u indien
nodig voor.
