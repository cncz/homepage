---
author: sioo
date: 2019-02-25 18:08:00
tags:
- medewerkers
- docenten
- studenten
title: Linux login server lilo4 uit
---
Met het aflopen van de ondersteuning van Ubuntu 14.04 LTS per 25 april
2019 wordt het tijd om te stoppen met ubuntu 14.04 op de linux login
server (lilo4). Lilo4 gaat daarom per 1 mei uit. Pas in de zomervakantie
wordt de default login server (lilo.science.ru.nl) doorverwezen naar
lilo6, die sinds mei 2018 al beschikbaar is met Ubuntu 18.04 LTS. De
huidige default met Ubuntu 16.04 LTS (lilo5) zal ook daarna nog
beschikbaar blijven, tot de ondersteuning daarvan afloopt in 2021. De
[signatures van de
C&CZ-loginservers](https://wiki.cncz.science.ru.nl/Hardware_servers#Linux_.5Bloginservers.5D.5Blogin_servers.5D)
kunnen gecontroleerd worden.
