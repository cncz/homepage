---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: Poor Blackboard performance? Use Firefox or IE8
---
The Grade Center can be slow, especially in courses in which lots of
students are enrolled. We’re working on installing Google Chrome.
