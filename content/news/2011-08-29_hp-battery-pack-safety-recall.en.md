---
author: caspar
date: 2011-08-29 16:58:00
tags:
- medewerkers
- studenten
title: HP Battery Pack Safety Recall
---
Earlier this year HP has issued a Notebook PC Battery Pack Replacement
Program notice regarding a number of notebook models which were shipped
with battery packs which have the potential to overheat, posing a fire
and burn hazard to consumers. The list of affected models also includes
a number of Compaq products. According to the HP notice the easiest way
to validate your battery is to visit the [HP Notebook PC Battery
Replacement Program
website](http://www.hp.com/support/BatteryReplacement). The Recall
notice states that if your battery is affected, HP will provide a free
replacement battery pack.
