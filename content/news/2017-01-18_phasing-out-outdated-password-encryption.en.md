---
author: wim
date: 2017-01-18 14:37:00
tags:
- medewerkers
- studenten
title: Phasing out outdated password encryption
---
For security reasons C&CZ will phase out an outdated, now considered
unsafe, encryption type of Science passwords. Accounts that haven’t
changed their passwords since September 1, 2014, will be affected. The
change will be carried out mid February 2017.

If you haven’t changed the password of your Science account for several
years, it is very likely that some services still use the old encryption
type. Examples of these services are passwords used to login to Linux
machines, Eduroam access by using your Science account or the Science
[VPN](/en/howto/vpn/) server. To continue using these services, the old
encryption has to be replaced. This can be done by resetting your
Science password at the [Do It Yourself
site](https://dhz.science.ru.nl). The menu item ‘password’ allows you to
set your password. This page also shows a remark about when your
password was last changed.
