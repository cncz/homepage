---
author: petervc
date: 2018-11-06 11:41:00
tags:
- medewerkers
- studenten
title: Endnote X9 beschikbaar
---
[Endnote](http://www.endnote.com/) versie X9 is beschikbaar voor
MS-Windows en Mac OS X. Het is te vinden op de
[install](http://www.cncz.science.ru.nl/software/installscience) netwerk
schijf. De licentie staat gebruik door medewerkers en studenten toe, ook
thuis. Binnenkort zal het ook te vinden zijn in het Software Center van
de [C&CZ beheerde werkplek met Windows
10](https://wiki.cncz.science.ru.nl/index.php?title=Windows_beheerde_werkplek&setlang=nl).
