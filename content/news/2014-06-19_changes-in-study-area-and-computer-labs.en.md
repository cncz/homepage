---
author: john
date: 2014-06-19 13:59:00
tags:
- medewerkers
- studenten
title: Changes in study area and computer labs
---
After a renovation during the summer break, 28 new All-in-one (AIO) PCs
will be added to the [study area](/en/howto/terminalkamers/). The
computer lab TK702, (HG02.702) will be vacated during this period.
