---
title: Iets meer spam na stoppen met Spamhaus
author: petervc
date: 2024-06-14
tags:
- medewerkers
- studenten
- mail
cover:
  image: img/2024/spamhaus.jpg
---
De Science mailservice gebruikte decennialang [Spamhaus](https://www.spamhaus.org)
als een van de blocklists om spam in e-mail te filteren.

Toen we te horen kregen dat we daarvoor een commerciële licentie nodig hadden
van ca. 5kEUR/j, hebben we onderzocht hoeveel meer spam er zou binnenkomen
zonder de Spamhaus-service. Hieruit bleek dat gebruikers meer spam zouden krijgen,
maar het meeste ervan zou in de map Spam terechtkomen. Daarnaast verwachten wij
dat de spamfiltering van het eveneens gebruikte Exchange Online Protection zal verbeteren.

Daarom zijn we eind april 2024 gestopt met het gebruik van Spamhaus. Als er veel gebruikers
klagen over spam in de inbox, dan kunnen we deze beslissing altijd heroverwegen.
