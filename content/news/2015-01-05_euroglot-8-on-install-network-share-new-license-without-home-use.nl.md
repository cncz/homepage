---
author: petervc
date: 2015-01-05 16:57:00
tags:
- studenten
- medewerkers
title: Euroglot 8 op Install-schijf, nieuwe licentie zonder thuisgebruik
---
De nieuwste versie (8) van de [Euroglot](http://www.euroglot.nl)
vertaalsoftware is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden.
Licentiecodes die geldig zijn tot 1 januari 2016 zijn via [de
helpdesk](/nl/howto/contact/) te verkrijgen. Thuisgebruik is volgens deze
licentie niet meer toegestaan. Voor thuisgebruik kan relatief goedkoop
een licentie gekocht worden bij [Surfspot](http://www.surfspot.nl).
