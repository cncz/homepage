---
author: petervc
date: 2011-05-31 14:37:00
tags:
- medewerkers
- docenten
title: Virus scanning software on Install share
---
During the IT security campaign, USB sticks with virus scanning software
and playing cards of [Cybersave
Yourself](http://www.surfnet.nl/en/Thema/cybersafe/Pages/Default.aspx)
have been distributed. The [virusscanner software of the
USB-stick](/en/howto/ict-beveiligingscampagne/) has been copied to the
[Install share](/en/howto/install-share/).
