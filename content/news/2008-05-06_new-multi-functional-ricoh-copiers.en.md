---
author: sommel
date: 2008-05-06 10:50:00
title: New multi-functional Ricoh copiers
---
All Oce copiers have been replaced by [Ricoh](/en/howto/ricoh/) machines
that can [print](/en/howto/printers-en-printen/) and
[scan](/en/tags/scanners/) too. The old printers *dali* and *oersted*
have been shut down and the printer *chagall* has been moved from
HG00.089 to HG00.201 (Studielandschap). Shortly the printer *picasso*
will also be shut down.
