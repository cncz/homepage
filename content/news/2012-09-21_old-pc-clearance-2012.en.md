---
author: stefan
date: 2012-09-21 15:21:00
tags:
- studenten
- medewerkers
title: Old pc clearance 2012
---
Update, September 27: *All PCs have gone.* Next year there will probably
be a PC clearance again.

From September 24, old PCs can be obtained from C&CZ. These PCs have
served about 5 years in the [computer labs](/en/howto/terminalkamers/)
and were replaced last summer. All hard drives have been removed.
Employees and students of the Faculty of Science interested in obtaining
one of these PCs can make an appointment by mailing the [C&CZ
helpdesk](/en/howto/helpdesk/).
