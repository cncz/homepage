---
author: petervc
date: 2023-10-10 14:16:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginLabPro (2023b)
cover:
  image: img/2023/originlab2023b.png
---
Er is een nieuwe versie (2023b) van
[OriginLabPro](/nl/howto/originlab/), software voor wetenschappelijke
grafieken en data-analyse,beschikbaar op de
[Install](/nl/howto/install-share/)-schijf. Zie [de website van
OriginLab](https://www.originlab.com/2023b) voor alle info over de
nieuwe versie. De licentieserver ondersteunt deze versie.
Er is een onderhoudscontract tot augustus 2026.  Afdelingen van
FNWI kunnen bij C&CZ de licentie- en installatie-informatie krijgen, ook
voor standalone gebruik. Installatie op door C&CZ beheerde PC’s wordt
gepland. Bij vragen, benader [C&CZ](/nl/howto/contact/).
