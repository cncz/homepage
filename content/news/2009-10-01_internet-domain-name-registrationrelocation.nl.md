---
author: caspar
date: 2009-10-01 16:58:00
title: Internet domeinnaam registratie/verhuizing
---
Recentelijk heeft C&CZ een partner-account bij domeinnaam registrar
[QDC](http://www.qdc.nl) verkregen, waarmee we een `.nl`, `.com`, `.org`
of `.eu` domeinnaam snel en eenvoudig kunnen aanmaken. Het (technisch)
beheer van de domeinnaam wordt door C&CZ gedaan (via onze eigen Internet
domain name servers). Hiermee wordt de Radboud Universiteit Nijmegen
houder van de domeinnaam in plaats van bv. een medewerker van een
afdeling. Het verhuizen van bestaande domeinnamen naar de registrar QDC
is ook mogelijk, maar vereist wat extra papierwerk, zie de
[domeinnaamregistratie pagina](/nl/howto/domeinnaam-registratie/) voor
meer informatie.
