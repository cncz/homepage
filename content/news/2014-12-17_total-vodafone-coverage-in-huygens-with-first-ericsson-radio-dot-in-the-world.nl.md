---
author: bertw
date: 2014-12-17 23:58:00
tags:
- medewerkers
- docenten
- studenten
title: Complete Vodafone-dekking in Huygensgebouw door eerste Ericsson Radio Dot ter
  wereld
---
Ook in de kelders en transportgangen van het Huygensgebouw is dekking op
het Vodafone netwerk, voor o.a. [VWO-mobiele
telefoons](/nl/tags/telefonie/) gerealiseerd. Dit werd mogelijk doordat
Ericsson en Vodafone in het Huygensgebouw een pilot wilden doen met [het
eerste operationele Radio Dot-systeem wereldwijd in de zakelijke
markt](https://www.ericsson.com/en/press-releases/2014/12/vodafone-and-ericsson-deploy-first-radio-dot-system-at-dutch-university). Dit is een aanvulling op
het in 2011 aangelegde [gedistribueerde
antennesysteem](/nl/howto/gsm-/-fax-/-dect/), dat nodig was omdat het
Huygensgebouw lijkt op een [kooi van
Faraday](http://nl.wikipedia.org/wiki/Kooi_van_Faraday). Sommige
oudgedienden denken hierbij terug aan juni 1987, toen bij FNWI door PTT,
Philips, SURF en de universiteit een pilot is uitgevoerd met de eerste
[PABX](http://nl.wikipedia.org/wiki/PABX) met veel dataverkeer in
Nederland.
