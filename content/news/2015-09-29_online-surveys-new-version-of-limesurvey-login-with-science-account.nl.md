---
author: remcoa
date: 2015-09-29 14:45:00
tags:
- medewerkers
- studenten
title: 'Online enquetes: Nieuwe versie LimeSurvey, login met Science accounts'
---
Er is een nieuwe versie van [LimeSurvey](/nl/howto/limesurvey/)
geïnstalleerd, samen met nieuwe, moderne templates. Het is niet langer
nodig om toegang aan te vragen, elke Science account kan nu inloggen.
Meer informatie over [LimeSurvey](/nl/howto/limesurvey/)…
