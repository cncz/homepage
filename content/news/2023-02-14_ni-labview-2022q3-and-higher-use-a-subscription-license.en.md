---
title: NI LabVIEW 2022Q3 and higher subscription license
author: petervc
date: 2023-02-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/labview.png
---
There is a new version of [LabVIEW](/en/howto/labview/) from
[NI.com](https://ni.com), version 2022Q3.

NI.com communicated that they will move to a
[subscription model for LabVIEW
licencing](https://www.ni.com/en/landing/subscription-software.html) starting with version 2022Q3.
C&CZ manages a FlexNet license server for LabVIEW 2022Q3, with a limited
number of licenses and a deadline in April for extension of the license.
Earlier versions of LabVIEW work standalone and unlimited in terms of time.

The software can be found on the [C&CZ Install disc](https://install.science.ru.nl/science/NI%20LabVIEW/). The license server is:

	labview.science.ru.nl:28000

In order to be able to check out a license, names of pc's and/of users
have to be entered by C&CZ in the license server.
For remote pcs this DNS suffix has to be added to
the IPv4 DNS configuration: science.ru.nl
