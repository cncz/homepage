---
author: stefan
date: 2017-09-04 12:33:00
tags:
- studenten
- medewerkers
- docenten
title: C&CZ 3D-printservice nu 4-kleuren
---
De lang geleden bestelde
[vierkleuren-uitbreiding](https://all3dp.com/prusa-i3-multi-material-upgrade/)
van de [Prusa i3
MK2](https://www.3dhubs.com/3d-printers/original-prusa-i3-mk2) 3D
Printer is eindelijk geleverd. Daardoor kunnen we nu objecten met vier
kleuren, of drie kleuren en oplosbaar steunmateriaal, printen. Zie voor
meer details de [Printers-pagina](/nl/howto/printers-en-printen/).
