---
author: polman
date: 2008-12-23 10:20:00
title: 'Firefox: new version with NoScript security'
---
`On all managed pc's the latest version of Firefox has been installed including`

the [NoScript](http://noscript.net/) plugin. NoScript initially forbids
execution of all scripts on a webpage. Per website one can decide to
allow scripts temporarily or permanent. This is an excellent tool to
minimize the risks of using websites.
