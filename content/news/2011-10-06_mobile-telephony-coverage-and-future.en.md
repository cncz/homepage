---
author: bertw
date: 2011-10-06 14:01:00
tags:
- studenten
- medewerkers
- docenten
title: 'Mobile telephony: coverage and future'
---
The coverage of the [Vodafone](http://www.vodafone.nl) mobile network in
the Huygens building has been improved considerably, it should be good
in the whole building now. We plan to connect the mobile networks of KPN
and T-Mobile to the newly installed [Distributed Antenna
System](http://en.wikipedia.org/wiki/Distributed_Antenna_System) as
well. The improved coverage is a necessary step to use [Vodafone
Wireless
Office](http://enterprise.vodafone.com/products_solutions/voice_roaming/wireless_office.jsp),
which allows phone calls to and from cell phones using the RU-internal
five-digit numbers. The future of enterprise telephony for Radboud
University consists of Voice over IP for desktop phones and Wireless
Office for cell phones.
