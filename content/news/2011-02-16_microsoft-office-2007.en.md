---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: Microsoft Office 2007
---
Currently the default office software on campus is Microsoft Office
2007. We tried to install this software on as many PC’s as possible. If
you still use an older version of Office then we would advise to install
Microsoft Office 2007.

If your PC is owned by the university than you are allowed to use the
campus license and you can borrow the software from our department or do
the installation directly from the install disk
(\\\\install-srv.science.ru.nl\\install).

For privately owned PCs you can buy the software at a reduced price at
[Surfspot](http://www.surfspot.nl).
