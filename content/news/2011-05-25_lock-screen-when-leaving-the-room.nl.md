---
author: petervc
date: 2011-05-25 17:44:00
tags:
- studenten
- medewerkers
- docenten
title: Kamer uit, beeldschermvergrendeling aan\!
---
Als uw computer onbeheerd achter blijft (geen screensaver met
wachtwoordbeveiliging), kan er met de data op uw pc geknoeid worden. Ook
kan iemand dan uit uw naam acties ondernemen die niet in de haak zijn.
In de campagneweken wordt extra gelet op onbeheerde, niet vergrendelde
pc’s op niet afgesloten kamers. In zo’n geval kunt u op uw monitor een
‘statische’ sticker vinden. De beeldschermvergrendeling van een Windows
pc kunt u aanzetten door tegelijkertijd de Windows- en de L-toets in te
drukken.
