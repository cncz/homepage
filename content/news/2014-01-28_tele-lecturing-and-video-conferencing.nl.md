---
author: caspar
date: 2014-01-28 12:53:00
tags:
- medewerkers
- studenten
title: Telecollege en videoconferencing
---
FNWI beschikt sinds kort over een zogenaamde
[telecollegezaal](/nl/howto/telecollege/): HG00.108. Hiermee is het
mogelijk om studenten die zich elders bevinden *virtueel* en
*interactief* te laten deelnemen aan een college in deze zaal. Andersom
is ook mogelijk. Daarnaast is in de grote vergaderzaal HG01.060 een
videoconferencing-faciliteit ingericht. Deze faciliteit dient tevens als
backup voor de telecollege-faciliteit. Voor hulp en advies kan men
terecht bij de [Audiovisuele
Dienst](http://www.ru.nl/fnwi/ihz/avd/audio-visuele-dienst/) van onze
faculteit.
