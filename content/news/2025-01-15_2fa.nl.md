---
title: Multifactor authenticatie voor Science accounts
author: miekg
date: 2025-01-27
tags:
  - medewerkers
  - studenten
cover:
  image: img/2025/2fa.png
---

[Eind vorig jaar](/nl/news/2024-12-11_science-services-vpn/) hebben we gezegd dat we begin 2025
terug komen met een update over hoe wij onze diensten veiliger gaan maken. Hieronder lees je onze
plannen hier voor.

In het kort: we gaan 2FA aan zetten op Science accounts, en op diensten die open staan naar het
internet 2FA _verplicht_ stellen.

Hoe we organisatorisch e.e.a. gaan inregelen is ook voor ons nog niet duidelijk. Moet er een
*vetting* procedure komen? Of mag je het zelf aanzetten in DHZ? Ideeën hierover zullen, na de
testperiode, naar boven komen.

Wil je graag <strike>als proefpersoon</strike> mee doen aan de test? Bijvoorbeeld als je
contact persoon voor een aantal accounts bent, of handig met SSH (geen vereiste)?
[Laat het ons dan weten](https://cncz.science.ru.nl/nl/howto/contact/). The test periode zal iets
van 6 weken duren, en starten zodra we genoeg aanmeldingen hebben.

# Achtergrond

We zijn begonnen met het implementeren van Time-based One-time Password (TOTP) als tweede factor in
de nieuwe [DHZ](https://cncz.science.ru.nl/nl/news/2024-12-09-new-dhz/). Dit willen we eerst
uitrollen naar een kleine deelverzameling van Science gebruikers om ervaring op te doen.

Deze TOTP werkt met een 6-cijferige code die je genereert met een authenticator-app (b.v. "Google
Authenticator" of "Microsoft Authenticator") op je telefoon. Dit is getest en we kunnen dit aan zetten in de nieuwe DHZ.

Hardware tokens, zoals Yubikeys, willen we in de toekomst ook gaan ondersteunen, maar heeft
vooralsnog geen prioriteit.

## 2FA op DHZ

Het idee (en zoals nu geïmplementeerd is) is dat, als je met TOTP ingelogd bent, je _meer_ mag op DHZ. Een van de eerste zaken die je
dan extra kan, is het beheer van SSH publieke sleutels, die nodig zijn om in te loggen [op de
LILOs](https://cncz.science.ru.nl/nl/cpk/1383/). Als je contactpersoon bent van andere Science
accounts, dan mag je _ook_ die publieke sleutels beheren.

In de toekomst zullen meer zaken in DHZ "achter 2FA" worden gezet, welke dat worden moet tijdens de
test periode duidelijk(er) worden.

## 2FA op RoundCube

Als een van de eerste services gaan we op RoundCube 2FA aanzetten, deze _functionaliteit_ wordt actief op ....
