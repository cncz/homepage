---
author: petervc
date: 2014-12-10 11:36:00
tags:
- medewerkers
- docenten
- studenten
title: Student.ru.nl and fnwi.ru.nl mail also accessible through Roundcube
---
During the weekend of November 12, 13 and 14, the
[ISC](http://www.ru.nl/isc) will move the mail of @student.ru.nl from
the old RU Share service to the new [RU Exchange
service](http://www.ru.nl/ict-uk/staff/mail-calendar/). This can be
accessed with a browser via [RU Exchange webmail](https://mail.ru.nl),
but also via [C&CZ Roundcube webmail](https://roundcube.science.ru.nl),
by choosing `mail.ru.nl` in the drop-down menu. Advantages of Roundcube
are that you can set multiple sender addresses (via
Settings-\>Identities) and that the Science LDAP addressbook is
available, which contains all Science staff and students. The RU
Exchange AD address book has very little FNWI scientific staff.
Furthermore Roundcube has a more powerful and modern interface
independent of the browser used. You can also leave out the extra `RU\`
before the U- or S-number when logging in. A disadvantage w.r.t. the
standard [Outlook Web Access](https://mail.ru.nl) might be that
Roundcube does not have calendar support, it is only an
[IMAP](http://en.wikipedia.org/wiki/Internet_Message_Access_Protocol)
mailclient.
