---
author: petervc
date: 2014-11-25 14:52:00
tags:
- studenten
- medewerkers
- docenten
title: 'Again phishing mails: "WAARSCHUWING" by "Admin"'
---
In the past few days, a dozen students and staff of the Faculty of
Science fell for a phishing email again. In broken Dutch the email
stated that the mailbox was almost full. The subject often was
“WAARSCHUWING” and the sender called himself “Admin”. The mail contained
a link to a counterfeit Science webmail page hosted by
<http://www.heliohost.org/> , a provider of free websites. On this
website everybody could read the login names and passwords of everybody
who reacted to the phishing email. C&CZ has blocked these logins and
asked the sysadmins of HelioHost to remove the website. Do not fall for
phishing emails, it causes inconvenience to yourself and others!
