---
author: wim
date: 2018-08-27 17:58:00
tags:
- medewerkers
- studenten
title: Windows10, Software Center and computer lab pcs
---
During the summer break all [pc’s in the computer
labs](/en/howto/terminalkamers/) got an upgrade from Windows7 to
Windows10. The 4 year old pcs in TK625/HG00.625 and
HG00.201/studylandscape/library have been replaced. In TK053/HG02.053
and HG00.201/studylandscape pcs have been added. With the Software
Center, users can install software from the catalog. All [remarks and
questions](/en/howto/contact/) are welcome.
