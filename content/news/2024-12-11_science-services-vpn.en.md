---
title: Science services behind VPN
author: miekg
date: 2024-12-11
tags:
- medewerkers
- studenten
cover:
  image: img/2024/.png
---

Due to a phishing attack targeting science accounts, and the subsequent abuse of RU mail servers to
sent out spam and phishing from valid science e-mail accounts, we were forced to employ a set of defensive measure as evident from
[cpk/1381](/en/cpk/1383/), [cpk/1382](/en/cpk/1382/), [cpk/1381](/en/cpk/1381/).

To prevent further abuse (and disruption) caused by compromised accounts, we have had to temporarily restrict access to some science services. These services include:

* Anything related to e-mail: Roundcube, authenticated SMTP, IMAP, and POP3.
* SSH access to the LILOs (see [cpk/1383](/en/cpk/1383/) for a workaround).
* [DIY](https://diy.science.ru.nl) as well as [the new replacement](/en/news/2024-12-09-new-dhz/).

Email services are now being reopened on a per science account basis. A set of approximately 1,000 accounts has regained normal access. To request access, please [contact us](/en/howto/contact/).

To continue using the other services, you will need to use the campus network or log in via [EduVPN](/nl/howto/vpn/#eduvpn).

At the beginning of 2025, we will provide an update on how we plan to make access to our services more secure.
