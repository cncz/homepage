---
author: wim
date: 2017-07-26 14:11:00
tags:
- medewerkers
- docenten
- studenten
title: Upgrade naar Ubuntu 16.04 voor terminalkamers en persoonlijke werkstations
---
In de zomervakantie worden alle pc’s uit de
[pc-onderwijszalen](/nl/howto/terminalkamers/) met Ubuntu 16.04 LTS
ge(her)installeerd. Cursus-software kan getest worden op de Ubuntu 16.04
[loginserver](/nl/howto/hardware-servers/) lilo5. Alle PC’s in HG00.029,
HG00.206, HG02.253, bibliotheek, studielandschap, projectkamers,
collegezalen en colloquiumkamers worden in deze periode vervangen door
nieuwe PC’s (All-in-One, dual boot met Windows7 en Ubuntu 16.04 Linux).

Indien u gebruikt maakt van zowel PC’s in de
[pc-onderwijszalen](/nl/howto/terminalkamers/) als een beheerde PC met
Linux op uw werkplek is het raadzaam te overwegen om uw werkplek PC ook
een upgrade naar Ubuntu 16.04 te laten geven. Gebruik maken van Ubuntu
12.04 of 14.04 in uw kantoor en Ubuntu 16.04 op een beheerde werkplek in
de [pc-onderwijszalen](/nl/howto/terminalkamers/) kan problemen
veroorzaken.
