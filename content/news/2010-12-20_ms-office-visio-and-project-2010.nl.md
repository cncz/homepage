---
author: petervc
date: 2010-12-20 17:12:00
title: MS Office Visio en Project 2010
---
MS Office [Visio](http://office.microsoft.com/en-us/visio/) en
[Project](http://www.microsoft.com/project/) 2010 zijn beschikbaar voor
computers die eigendom van de faculteit zijn, in vrijwel alle versies:
UK/NL en 32/64. Alleen Visio 32 NL ontbreekt op dit moment. De CD’s
staan op de [install](/nl/tags/software) schijf.
