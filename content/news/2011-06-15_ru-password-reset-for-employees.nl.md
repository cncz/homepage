---
author: pberens
date: 2011-06-15 14:19:00
tags:
- medewerkers
- docenten
title: RU wachtwoord reset voor medewerkers
---
RU-wachtwoord logt u in op RU-webapplicaties. Als u uw wachtwoord kwijt
bent, kunt u via C&CZ uw wachtwoord laten resetten. Breng uw
personeelspas of ander id mee. U krijgt dan een tijdelijk wachtwoord
waarmee u zichzelf een nieuw wachtwoord kunt geven in Identity Manager
op [idm.ru.nl](http://idm.ru.nl/). Maar wist u dat u zichzelf ook een
activatiecode kunt laten sms-en nadat u éénmalig twee vragen
geregistreerd heeft? Meer info op [www.ru.nl/idm](http://www.ru.nl/idm/)
en [idm.ru.nl](http://idm.ru.nl). We bevelen deze doe-het-zelf oplossing
van harte aan.
