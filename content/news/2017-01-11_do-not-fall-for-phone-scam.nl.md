---
author: petervc
date: 2017-01-11 15:03:00
tags:
- medewerkers
- studenten
title: Trap niet in telefonische oplichting
---
We kregen vandaag enkele berichten dat medewerkers onverwacht
telefonisch benaderd zijn door personen die hulp aanboden met de
computer of die vertelden dat ze konden meekijken met alles wat de
medewerker op de computer deed. \*Trap hier niet in, geef geen antwoord
op hun vragen en doe niet op de computer wat zij vragen.\* Bij twijfel
kunt u hen doorverwijzen naar [C&CZ](/nl/howto/contact/).
