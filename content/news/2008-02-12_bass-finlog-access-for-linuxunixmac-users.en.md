---
author: remcoa
date: 2008-02-12 16:15:00
title: BASS-Finlog access for Linux/Unix/Mac users
---
For Linux/Unix/Mac users it’s now possible to access BASS-Finlog, see
[BASS](/en/howto/bass/).
