---
author: stefan
date: 2018-02-20 14:50:00
tags:
- medewerkers
title: Safely dispose of old hardware
---
At the [Logistics
Center](https://www.radboudnet.nl/fnwi/fnwi/ihz-algemeen/ihz-logistiek/)
of the housing department (IHZ) there is a locked room in which old
hardware can be safely disposed. This is important with data carriers in
order to prevent data leaks. Please do not put old hardware containing
data in a container that can be searched by a passerby.
