---
author: petervc
date: 2012-03-13 23:43:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuwe Matlab licentieserver (oude stopt\!) en Matlab R2012a
---
Er is een nieuwe licentieserver voor het gebruik van
[Matlab](/nl/howto/matlab/). Wie recht heeft op de nieuwe licentiecodes
kan ze krijgen via een mail naar postmaster. **De oude
licentieserver (matlab.science.ru.nl) zal over enige tijd gestopt
worden**, dus om problemen te voorkomen dienen alle
installaties van Matlab aangepast te worden, ook oude versies! De
nieuwste versie van [Matlab](/nl/howto/matlab/), R2012a, is beschikbaar
voor afdelingen die licenties voor het gebruik van Matlab aangeschaft
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op de door C&CZ
beheerde 64-bit Linux machines is R2012a beschikbaar, een oudere versie
(/opt/matlab-R2011b/bin/matlab) is nog tijdelijk te gebruiken. Op de
door C&CZ beheerde Windows-machines zal R2012a ook beschikbaar komen.
