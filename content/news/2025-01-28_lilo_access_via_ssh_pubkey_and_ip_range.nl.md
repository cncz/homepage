---
title: lilo toegang met managed SSH keys en ip reeksen
author: sioo
date: 2025-01-28
tags: [lilo, ssh, medewerkers, studenten]
cover:
  image: img/2025/use_public_key_authentication_with_ssh.png
---
We hebben het authenticatieproces voor [login servers](/nl/howto/hardware-servers/#linux-loginservers) bijgewerkt na de succesvolle test zoals genoemd in [CPK#1383](https://cncz.science.ru.nl/nl/cpk/1383/).

**Vanaf nu wordt je `~/.ssh/authorized_keys` bestand op lilo genegeerd**, in plaats daarvan kun je ons de **publieke** ssh sleutel(s) (de inhoud van het `.pub` bestand) mailen van je ssh-client. Deze wijziging is bedoeld om te voorkomen dat slechte actoren gemakkelijk blijvende toegang kunnen krijgen. Voor het gemak hebben we publieke sleutels uit homedirectories die sinds 9 december j.l. gemaakt of aangepast zijn alvast toegevoegd. Als je wel sleutels gebruikte voor toegang, maar van langer geleden, dan moet je die publieke sleutels alsnog aan ons sturen.

We hebben ook de toegang beperkt tot ip reeksen die we vertrouwen, daarnaast kun je [ons](/nl/howto/contact) vertellen vanaf welke ip-adressen ( [check ip](https://ip.science.ru.nl) ) je met lilo wilt verbinden.

Voor verbindingen uit het beperkte bereik (o.a. Radboud en EduVPN) blijven zowel wachtwoord- als sleutelauthenticatie beschikbare opties.

Een sleutelpaar genereren kan met:

``` shell
ssh-keygen -t ed25519 -C "uw_e-mailadres@ru.nl"
```
