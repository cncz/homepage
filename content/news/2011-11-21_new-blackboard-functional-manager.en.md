---
author: caspar
date: 2011-11-21 17:18:00
tags:
- studenten
- docenten
title: New Blackboard functional manager
---
As of November 15 Fred Melssen started to work for FNWI. Fred’s main
tasks will be: development and management of ICT solutions for
education, and functional manager [Blackboard](/en/howto/blackboard/) for
FNWI. As such Fred is the successor of Pauline Berens and immediate
colleague of Remco Aalbers. Fred was formerly employed at the Nijmegen
School of Management as ICT specialist and programmer for Education, and
functional manager Blackboard, as well as programmer at the UCI. Fred’s
function is jointly managed by the Education Center and C&CZ. His
workplace is at C&CZ.
