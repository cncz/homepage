---
author: mkup
date: 2016-03-23 10:53:00
tags:
- studenten
- medewerkers
- docenten
title: Maintenance network switches April 4th 2016
---
On Monday evening, April 4th, 2016 from 10:30 to 12:00 PM there will be
maintenance on the network switches at FNWI. During this time slot the
wired and wireless network will be unavailable for some time at the
locations Huygens, Linnaeus, FELIX, Nanolab, A1-basement, Logistic
Center, Goudsmit, Experimental Garden, Mercator 1/2/3, UBC and Day-care
Center. In HFML however the network will be available. We advise to
check which systems or set-ups might be affected by this and if possible
shift network activity to a different time slot. You also might think
about systems with alerts via Internet.
