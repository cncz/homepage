---
author: petervc
date: 2014-02-24 13:59:00
tags:
- medewerkers
title: 'SURFnet FileSender: grote bestanden veilig versturen'
---
Sinds 24 februari is de overdracht van grote bestanden via de
experimentele versie van [SURFnet
Filesender](https://filesender.surfnet.nl) ook voor medewerkers met een
`@science.ru.nl` mail-adres mogelijk. Bestanden van maximaal 100GB
kunnen aan maximaal 25 ontvangers gestuurd worden. Ook kan aan anderen
een “guest voucher” gestuurd worden, waarmee zij van deze dienst gebruik
kunnen maken. De bestandsoversdracht vindt versleuteld (via https)
plaats naar een server van SURFnet in Nederland. FileSender is vaak
beter bruikbaar dan alternatieven vanwege de maximale grootte of de
[Informatiebeveiliging](/nl/howto/informatiebeveiliging/). De SURFnet
FileSender dienst is gebaseerd op [het FileSender
project](http://www.filesender.org) waarin SURFnet participeert.
