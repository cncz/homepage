---
author: caspar
date: 2011-08-29 16:58:00
tags:
- medewerkers
- studenten
title: HP accu terugroepactie
---
Eerder dit jaar heeft HP een Notebook PC Battery Pack Replacement
Program aangekondigd betreffende een aantal laptopmodellen met accu’s
die mogelijk oververhit kunnen raken met brand- en verbrandingsrisico
als gevolg. De lijst met laptopmodellen die mogelijk dit probleem hebben
bevat ook een aantal Compaq producten. In de aankondiging van HP staat
dat de eenvoudigste manier om na te gaan of een bepaald model een risico
oplevert is via de [HP Notebook PC Battery Replacement Program
website](http://www.hp.com/support/BatteryReplacement). In de Recall
aankondiging belooft HP om de betreffende accu’s zonder kosten te
vervangen.
