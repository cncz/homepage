---
author: petervc
date: 2015-10-23 13:04:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab mobile for iPhone/iPad and Android
---
Matlab [Mobile](http://nl.mathworks.com/mobile/) is a lightweight Matlab
desktop for iPhone/iPad and Android, that connects to a Matlab session
running on MathWorks Cloud or on your computer. From smartphone or
tablet, typical Matlab tasks like evaluating commands, running scripts,
creating and manipulating plots and figures, and viewing results can be
performed. Custom keyboards on iOS and Android help to easily enter
Matlab syntax. To use it, install the app from the App Store or Play
Store and [create a Mathworks
account](https://nl.mathworks.com/mwaccount/register?uri=%2Fmwaccount%2F)
with an email address ending in: @science.ru.nl, @student.science.ru.nl,
@donders.ru.nl, @pwo.ru.nl, @let.ru.nl, @fm.ru.nl, @ai.ru.nl,
@socsci.ru.nl, @ru.nl, @student.ru.nl or @radboudumc.nl .
