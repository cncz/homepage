---
author: petervc
date: 2014-11-25 14:57:00
tags:
- medewerkers
- docenten
title: Vooraankondiging wijziging Microsoft licenties per 2015
---
Het [ISC](http://www.ru.nl/isc) deelt ons mee dat er per 1 januari 2015
een nieuwe licentieovereenkomst via Surfmarket met Microsoft zal zijn.
Het voornaamste verschil lijkt op dit moment dat de licentie voor
Microsoft Office op Windows PC’s die eigendom van de RU zijn, wordt
opgevolgd door Office 365 volgens Plan E3 (voorheen A3) voor alle
RU-medewerkers. Dit zal hoogstwaarschijnlijk betekenen dat Windows PC’s
en iPads/iPhones die privé-eigendom zijn, onder de licentie zullen
vallen vanaf 1 januari 2015. Nadere informatie volgt in het eerste
kwartaal van 2015.
