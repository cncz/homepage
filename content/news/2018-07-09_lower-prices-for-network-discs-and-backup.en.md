---
author: bbellink
date: 2018-07-09 13:27:00
tags:
- medewerkers
- docenten
- backup
title: Lower prices for network discs and backup
---
After the FNWI data/backup meeting and because of the purchase of a new
storage server, C&CZ determined new prices for [network
discs](/en/howto/diskruimte/) and for [backup](/en/howto/backup/). The new
prices are ca. 2.5 times lower than the old ones, that were set 4 years
ago.
