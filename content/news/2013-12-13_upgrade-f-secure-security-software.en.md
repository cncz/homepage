---
author: petervc
date: 2013-12-13 15:01:00
tags:
- medewerkers
- studenten
title: Upgrade F-Secure security software
---
The most recent version of F-Secure can be bought by RU employees now
for ca. € 5,25 at [Surfspot](http://www.surfspot.nl), search for
“secure”. It is F-Secure Internet Security 2014 for 1 year, for 1 user.
This version also supports Windows 8. F-Secure for Android and Mac are
available with the same license. The support for version 9, that could
be used freely at home, ends on February 6, 2014. The business suite
version that is used on campus, that can be found on the
[Install](/en/howto/install-share/) network share, can’t be used at home,
since it needs to communicate with a policy manager. N.B.: The private
license doesn’t cover use by family members or roommates. See
[Radboudnet
news](http://www.radboudnet.nl/actueel/nieuws/@928616/beveiligingssoftware/)
for more info and updates.
