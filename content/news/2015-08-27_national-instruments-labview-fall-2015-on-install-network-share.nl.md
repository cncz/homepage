---
author: petervc
date: 2015-08-27 18:09:00
tags:
- software
cover:
  image: img/2015/labview.png
title: National Instruments LabVIEW Fall 2015 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, is de zojuist binnengekomen versie “Fall 2015”
op de [Install](/nl/howto/install-share/)-netwerkschijf gezet.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen.
