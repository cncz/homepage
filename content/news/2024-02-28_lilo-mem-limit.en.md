---
title: "Lilo: Per User Memory Limit"
date: 2024-02-28T09:15:00+01:00
author: miek
cover:
        image: img/memory-limit.jpg
---
The [Lilo servers](https://cncz.science.ru.nl/en/howto/hardware-servers/#linux-loginservers) are
Linux servers for general use, with the caveat:

> You can use it just as you like, as long as you do not cause problems for other users.

To help enforce this we are enabling a memory limit of 25% _per user_. This means a single user on
lilo[78] can allocate up to 16 GiB.
