---
author: petervc
date: 2016-12-08 18:03:00
tags:
- studenten
title: 'Study associations: printbudget to Peage?'
---
If student groups that still have C&CZ print budget let C&CZ know before
mid December to which Peage accounts the budget should be transfered,
this can be finished before the end of 2016. After that, the only option
is to transfer the money with a one-time payment order to a bank
account, which is less efficient. All FNWI Konica Minolta MFPs within
the C&CZ-printbudget system have been switched to Peage. The C&CZ
printbudget system is now only used for the HP printer escher (Library
of Science), the posterprinter kamerbreed and the [C&CZ 3D print
service](/en/howto/printers-en-printen/).
