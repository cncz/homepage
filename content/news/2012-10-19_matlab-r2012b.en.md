---
author: petervc
date: 2012-10-19 18:05:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2012b
---
The latest version of [Matlab](/en/howto/matlab/), R2012b, is available
for departments that have bought licenses. The software and license
codes can be acquired through a mail to postmaster for those entitled to
it. The software can also be found on the
[install](/en/tags/software)-disc. The C&CZ-managed 64-bit Linux
machines have this version installed, an older version
(/opt/matlab-R2012a/bin/matlab) is still available temporarily. The
C&CZ-managed Windows machines will receive this version too.
