---
author: petervc
date: 2012-11-19 13:33:00
tags:
- studenten
- medewerkers
- docenten
title: Mail problems after supplying password to phishers
---
Horde webmail again appeared to be misused for sending spam. This could
happen because a naive user gave the Science password to
phishers/spammers. After first stopping horde, early Friday morning we
disabled the account of the naive user and restarted horde. Saturday
morning it appeared that this short spam-outbreak had caused
administrators of hotmail.com to add our mail server to their blacklist.
Therefore we switched the IP-number of this mail server Saturday
morning.
