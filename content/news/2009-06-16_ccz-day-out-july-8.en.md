---
author: petervc
date: 2009-06-16 16:07:00
title: 'C&CZ day out: July 8'
---
The yearly day out of C&CZ is scheduled for July 8. C&CZ can be reached
in case of serious disruptions of service.
