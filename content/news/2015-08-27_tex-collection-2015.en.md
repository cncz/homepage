---
author: petervc
date: 2015-08-27 18:12:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2015
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), June 2015, is available
for MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share and can also be
[borrowed](/en/howto/microsoft-windows/).
