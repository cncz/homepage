---
author: mkup
date: 2019-02-06 08:40:00
tags:
- medewerkers
- docenten
- studenten
title: 'SURFnet network maintenance: Tuesday Feb 12, 05:00-07:00 hours'
---
Tuesday morning February 12, there will be maintenance on the internet
links between RU and SURFnet. Outage of the internet connection is not
likely, however cannot be excluded completely. Please think which
processes depend on connectivity and prepare for this possible outage if
necessary.
