---
author: petervc
date: 2019-10-11 13:36:00
tags:
- studenten
- medewerkers
- docenten
title: Adobe Creative Cloud license prolongation
---
Starting Adobe [Creative Cloud
software](https://www.adobe.com/nl/creativecloud.html) (Acrobat,
Illustrator, Photoshop etc.) gives the error message that the license
will expire November 29, 2019. For the C&CZ managed pc’s this will be
solved before that time. If you manage a Windows pc or Mac, that is
owned by FNWI and has Adobe Creative Cloud software installed, please
contact the [C&CZ helpdesk](/en/howto/contact/) to have the license
prolonged. Checking the expiry date can be done with AdobeExpiryCheck
from the [Install share](/en/howto/install-share/).
