---
author: polman
date: 2018-05-25 14:02:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube- en Exchange-alternatief voor kleine schermen: Rainloop'
---
Voor kleine schermen (smartphones) is de webmail van
[Roundcube](https://roundcube.science.ru.nl) niet erg geschikt. Daarom
is er nu een alternatief: [Rainloop](https://rainloop.science.ru.nl).
Men kan in Rainloop een “threaded view” instellen, wat ook handig is
voor kleine schermen. Het Science adresboek is beschikbaar in Rainloop.
Ook kan Rainloop gebruikt worden voor de Exchange RU-mail. Maar
Roundcube heeft veel andere voordelen t.o.v. Rainloop: het beheren van
het delen van mappen met andere gebruikers, gedeelde labels en het
beheren van Sieve filters en vakantieberichten. Zie voor meer info onze
[mail pagina](/nl/tags/email/).
