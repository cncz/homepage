---
author: caspar
date: 2012-01-03 14:25:00
title: FNWI-onderwijsprijs 2011
---
Op de nieuwjaarsreceptie van de faculteit werd de FNWI-onderwijsprijs
2011 uitgereikt aan C&CZ, voor de “jarenlange uitstekende ondersteuning
van studenten en docenten”, aldus de unanieme jury. De decaan, die de
prijs uitreikte, noemde enkele ontwikkelingen in de ICT-infrastructuur
en de onderwijsondersteunende systemen en de nauwe samenwerking met
docenten en studenten, waardoor producten worden ontwikkeld waar
behoefte aan is. De FNWI-onderwijsprijs wordt sinds 2001 toegekend door
vertegenwoordigers van de [Facultaire Studenten
Raad](http://www.ru.nl/fnwi/fsr/) en van de opleidingscommissies (zowel
studenten als docenten) en staat onder voorzitterschap van de
vice-decaan onderwijs. De prijs bestaat uit een oorkonde en een
kunstwerk van de hand van [Anneke van
Bergen](http://www.annekevanbergen.nl/). C&CZ is bijzonder vereerd met
deze prachtige blijk van waardering.
