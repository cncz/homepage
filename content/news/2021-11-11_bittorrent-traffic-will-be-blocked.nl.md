---
author: sioo
date: 2021-11-11 09:59:00
cover:
  image: img/2021/bittorrent.png
title: BitTorrent verkeer wordt geblokkeerd
---
Omdat [BitTorrent](https://nl.wikipedia.org/wiki/BitTorrent) als een
beveiligingsrisico (malware/reputatieschade) wordt beschouwd en het
vrijwel uitsluitend gebruikt wordt voor illegale downloads, heeft het RU
Security Operations Center (SOC) voorgesteld om dit type netwerkverkeer
te blokkeren binnen de RU. Mocht dit problemen opleveren voor onderzoek
of onderwijs, dan [horen we](/nl/howto/contact) dit graag, dan kunnen we dit melden bij het
SOC en evt. een uitzondering aanvragen.
