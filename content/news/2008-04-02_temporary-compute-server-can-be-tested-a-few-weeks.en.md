---
author: petervc
date: 2008-04-02 18:18:00
title: Temporary compute server can be tested a few weeks
---
A test machine is available to see if 4-core Intel processors can be
used for new machines in the [cn-cluster](/en/howto/hardware-servers/).
The machine will stay for a few weeks. All Science-logins should be able
to login with ssh.

Name: t4150.science.ru.nl Type: Sun Fire X4150 Processors: 2 Intel E5345
CPU’s met ieder 4 cores van 2.33GHz. Memory: 8GB OS: Fedora 7
