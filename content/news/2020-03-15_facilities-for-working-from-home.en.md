---
author: bram
date: 2020-03-15 12:51:00
tags:
- medewerkers
- docenten
title: Facilities for working from home
---
We created [an overview](/en/howto/thuiswerken/) of IT facilities that
can be used to work from home.
