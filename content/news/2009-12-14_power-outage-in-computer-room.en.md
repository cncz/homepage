---
author: caspar
date: 2009-12-14 10:24:00
title: Power outage in computer room
---
On December 13 2009 at 18:50 we suffered a power outage in our computer
room \*behind\* the UPS, that is meant to shield the servers from
problems with the electricity grid. The UVB investigates the cause of
the problem. The outage caused the failure of several file servers, web
servers (including squirrel), one of the LDAP servers, etc. All systems
have been reconnected to a different power supply and at 10:20 all
systems were started. In the next hour all services should become
available again.
