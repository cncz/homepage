---
author: petervc
date: 2022-08-18 11:29:00
tags:
- studenten
- medewerkers
- docenten
title: Shredding/destruction of data carriers / hard drives at C&CZ
cover:
  image: img/2022/shredded-harddrives.jpg
---
The Internal Affairs department manages a contract with an external
specialized company for the secure destruction/shredding of data
carriers. The secure metal container that is used as a
temporary cache, can be found at C&CZ as of the new academic year.
Employees and students of the Faculty of Science can put data carriers
with sensitive data in the container themselves at C&CZ. The C&CZ
helpdesk can help recognizing and removing data carriers from desktops,
laptops etc.
