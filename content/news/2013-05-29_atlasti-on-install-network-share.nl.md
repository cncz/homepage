---
author: petervc
date: 2013-05-29 14:18:00
tags:
- medewerkers
- docenten
- studenten
title: ATLAS.ti op Install-schijf
---
De nieuwste versie van [ATLAS.ti](http://www.atlasti.com), 7, is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. Met ATLAS.ti
kunt u kwalitatieve data (interviews, teksten, grafieken,
geluidsfragmenten) analyseren. De licentievoorwaarden staan gebruik op
RU-computers toe. Licentiecodes zijn bij C&CZ helpdesk of postmaster te
verkrijgen.
