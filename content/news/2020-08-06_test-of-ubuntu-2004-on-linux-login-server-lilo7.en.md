---
author: sioo
date: 2020-08-06 13:41:00
tags:
- medewerkers
- studenten
title: Test of Ubuntu 20.04 on Linux login server lilo7
---
As a test of the new version of Ubuntu, 20.04 LTS, we have installed a
new login server `lilo7.science.ru.nl`. In a few months it will replace
the four year old login server `lilo5`. The name `lilo`, that always
points to the newest/fastest login server, will then be moved from the
more than two year old `lilo6` to the new `lilo7`. The new `lilo7` is a
Dell PowerEdge R7515 with 1 AMD 7502P 2.5GHz 32-core processor and 64GB
RAM. Because we enabled hyper-threading, it is showing 64 processors.
For users who want to check the identity of this new server before
supplying their Science-password to the new server, see [the section
about our loginservers](/en/howto/hardware-servers/). Please [report all
problems](/en/howto/contact/) with this new version of Ubuntu Linux.
