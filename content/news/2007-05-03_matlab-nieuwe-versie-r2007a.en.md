---
author: polman
date: 2007-05-03 15:45:00
title: Matlab nieuwe versie (R2007a)
---
Met ingang van mei 2007 is er een nieuwe versie (7.4, R2007a) van Matlab
beschikbaar op alle door C&CZ beheerde PCs met Linux of Windows-XP en
Suns met Solaris.

De oude versie (R2006b) wordt over enkele weken verwijderd, wanneer
duidelijk is dat de overgang naar R2007a geen grote problemen
veroorzaakt. Om de oude versie tot die tijd nog te kunnen gebruiken moet
het zoekpad uitgebreid worden met /vol/matlab-R2006b/bin (onder Unix).

Voor meer info over deze release, zie
[http://www.mathworks.com/products/new\_products/latest\_features.html?ref=fp2007a
de Mathworks
website](/en/howto/http://www.mathworks.com/products/new-products/latest-features.html?ref=fp2007a-de-mathworks-website/)

Bij vragen/problemen: postmaster\@science.ru.nl, tel 53535/56666,
HG03.055.
