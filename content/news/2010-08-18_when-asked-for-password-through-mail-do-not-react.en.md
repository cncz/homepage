---
author: petervc
date: 2010-08-18 13:31:00
title: 'When asked for password through mail: do not react\!'
---
Every so often our users receive
[phishing](http://nl.wikipedia.org/wiki/Phishing)-mail, in which a
prompt response with username and password is requested. C&CZ likes to
stress \*again\* that it is not very wise to believe something just
because is written in a letter or e-mail and that \*no official
institution\* will ever ask you for your password, just like a bank
never will ask you for your pincode for your bank account. If a naive
user falls for this scheme, his account is normally soon thereafter
misused to send spam, with the effect that our mailservers are included
in blacklists, causing problems for all users.
