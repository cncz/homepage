---
author: petervc
date: 2017-12-21 14:53:00
tags:
- studenten
- medewerkers
- docenten
- dns
title: 'Donatie aan vrije en open source software: ISC, FSF en OpenBSD'
---
C&CZ maakt voor het merendeel van de services gebruik van [vrije
software](https://nl.wikipedia.org/wiki/Vrije_software) en
[opensourcesoftware](https://nl.wikipedia.org/wiki/Opensourcesoftware).
Daarom is al enige tijd geleden het idee ontstaan dat de
C&CZ-medewerkers elk jaar zouden stemmen welke projecten een donatie van
C&CZ zouden krijgen. Dit jaar is de keus gevallen op het [Internet
Systems Consortium](https://www.isc.org/),
[OpenBSD](https://www.openbsd.org/) en net als vorig jaar de [Free
Software Foundation
(FSF)](https://nl.wikipedia.org/wiki/Free_Software_Foundation). Het
Internet Systems Consortium maakt o.a.
[BIND](https://www.isc.org/downloads/bind/) (DNS/nameserver) en [ISC
DHCP](https://www.isc.org/downloads/dhcp/), beide basisdiensten van het
netwerk/Internet in gebruik bij C&CZ. OpenBSD ontwikkelt naast
[OpenBSD](https://www.openbsd.org/) ook o.a.
[OpenSSH](https://www.openssh.com/) en
[LibreSSL](https://www.libressl.org/). De FSF ondersteunt GNU/Linux, dat
op vrijwel alle door C&CZ beheerde servers en vele honderden
werkstations gebruikt wordt, waaronder alle
[PC-cursuszalen](/nl/howto/terminalkamers/).
