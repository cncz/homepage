---
author: visser
date: 2019-05-26 00:10:00
tags:
- studenten
- medewerkers
- docenten
title: VPN met Twee-factor authenticatie voor afgeschermd lab-netwerk
---
De [Science VPN](/nl/howto/vpn/) is uitgebreid met een voorziening,
waarbij gebruikers op individuele basis met twee-factor authenticatie
(certificaat en username/password) toegang kunnen krijgen tot
afgeschermde delen van het netwerk. Dit is ontwikkeld op verzoek van een
afdeling die onderhoudsfirma’s toegang wil geven tot alleen de door hen
te ondersteunen apparatuur. Afdelingen die ook voor dergelijke toegang
belangstelling hebben kunnen [contact opnemen met
C&CZ](/nl/howto/contact/).
