---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: Blackboard nieuwe versie
---
Er is een nieuwe versie van Blackboard in gebruik genomen. Verder is de Blackboard pagina in de C&CZ Wiki vernieuwd en er is een nieuwe facultaire Blackboard-beheerder.
