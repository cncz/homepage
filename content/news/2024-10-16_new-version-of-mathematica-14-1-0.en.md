---
title: New version of Mathematica 14.1.0
author: arnoudt
date: 2024-10-16
tags:
  - medewerkers
  - studenten
  - software
cover:
  image: img/2024/mathematica.png
---

A new version of [Mathematica](/en/howto/mathematica/) (14.1.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`.

When connected wired or wireless on campus or through [VPN](/en/howto/vpn/),
the software for Windows, Linux and macOS can be found on the
[C&CZ Install disc](https://install.science.ru.nl/science/Mathematica/).

During the installation you must specify:

     License server: mathematica.science.ru.nl
     License: L4601-6478

According to the [Mathematica Quick Revision
History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 14.1 introduces the unified Wolfram application and expands Wolfram Language by offering new tools for working with neural nets and LLMs, for finding differences in content, for working with images and videos, and for exploring scientific evaluations through biomolecules, astrophysics and more"
