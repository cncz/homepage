---
title: Science login accounts automatisch dichtgezet op einddatum
author: petervc
date: 2023-09-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/account-disabled.jpg
---
Conform nieuw RU-beleid worden Science login accounts vanaf 1 november automatisch dichtgezet op de einddatum.
Vanaf dat moment is het niet meer mogelijk gebruik te maken van diensten, geleverd door C&CZ, die gebruik maken van de Science login.

* **Eén maand vóór** de einddatum wordt de verantwoordelijke contactpersoon op de hoogte gebracht.
* Als die geen actie onderneemt en de einddatum nog maar **twee weken** vooruit ligt, wordt deeigenaar zelf op de hoogte gesteld. 
* **Eén maand ná** de einddatum wordt het account verwijderd uit door C&CZ beheerde systemen zoals de Science mailservice en home-directory.

We adviseren contactpersonen een pro-actieve werkwijze aan te nemen in het bijhouden van einddata van de accounts, om te voorkomen dat accounts door nalatigheid worden dichtgezet of door gemakzucht te ver mogen doorlopen.
Het onnodig door laten lopen van een account is ook een risico voor de RU.

