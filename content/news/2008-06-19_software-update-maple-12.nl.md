---
author: petervc
date: 2008-06-19 16:24:00
title: 'Software update: Maple 12'
---
Er is een nieuwe versie (12) van [Maple](/nl/howto/maple/) geïnstalleerd
op Linux/Solaris. Begin volgende week zal het ook de [Windows-XP
Beheerde Werkplek PCs](/nl/howto/windows-beheerde-werkplek/) beschikbaar
zijn. Het kan voor Windows geïnstalleerd worden vanaf de
[install](http://www.cncz.science.ru.nl/software/installscience) netwerk
schijf. De CD’s kunnen ook door medewerkers en studenten geleend worden,
ook voor thuisgebruik.
