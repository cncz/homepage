---
author: sioo
date: 2021-04-20 11:10:00
tags:
- medewerkers
- docenten
- studenten
title: Linux login server lilo5 uit
---
Met het aflopen van de ondersteuning van Ubuntu 16.04 LTS per 30 april
2021 wordt het tijd om te stoppen met Ubuntu 16.04 op de Linux login
server lilo5. Lilo5 gaat daarom per 1 mei uit. De default login server
(lilo.science.ru.nl) wijst al naar lilo7 met Ubuntu 20.04 LTS, maar ook
lilo6 met Ubuntu 18.04 LTS zal nog beschikbaar blijven tot de
ondersteuning daarvan afloopt in 2023. De [signatures van de
C&CZ-loginservers](https://wiki.cncz.science.ru.nl/Hardware_servers#Linux_.5Bloginservers.5D.5Blogin_servers.5D)
kunnen gecontroleerd worden. N.B. check ook de [tips voor het aanpassen
van je SSH
instellingen](https://wiki.cncz.science.ru.nl/SSH#.5Ben.5DSSH_tips_and_settings.5B.2Fen.5D.5Bnl.5DSSH_Instellingen_en_tips.5B.2Fnl.5D).
