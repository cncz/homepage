---
author: petervc
date: 2009-06-12 17:55:00
title: 'Software update: Maple 13'
---
Er is een nieuwe versie (13) van [Maple](/nl/howto/maple/) geïnstalleerd
op Linux/Solaris. Begin volgende week zal het ook de [Windows-XP
Beheerde Werkplek PCs](/nl/howto/windows-beheerde-werkplek/) beschikbaar
zijn. Het kan voor Windows geïnstalleerd worden vanaf de
[install](http://www.cncz.science.ru.nl/software/installscience) netwerk
schijf. De DVD kan ook door medewerkers en studenten geleend worden, ook
voor thuisgebruik.
