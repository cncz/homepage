---
title: Nieuwe A0 posterprinter, tweemaal zo snel
author: petervc
date: 2023-11-21
tags:
- medewerkers
- studenten
cover:
  image: img/2023/posterprinter.jpg
---
Omdat de oude posterprinter defect was, is een HP Designjet Z6 44 inch postscript aangeschaft.
De nieuwe printer is tweemaal zo snel, dus onze service zal ook nog sneller zijn.
Zie [de posterprinter pagina](https://cncz.science.ru.nl/nl/howto/print/#posters) voor alle info.
