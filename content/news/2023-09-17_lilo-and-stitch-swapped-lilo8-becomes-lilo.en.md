---
title: Lilo and stitch will be swapped October 2. Lilo8 becomes lilo
author: petervc
date: 2023-09-17 23:03:00
tags:
- medewerkers
- studenten
cover:
  image: img/2023/lilo8.jpg
---
On October 2, the newest login server lilo8, that runs the same version of Ubuntu as the
pcs in the computer labs, Ubuntu 22.04, will become the default lilo.science.ru.nl loginserver.
Lilo7, with Ubuntu 20.04, will then get tha alias 'stitch' for the alternate loginserver.

To prevent warning messages about changed fingerprints, read
[the page about servers](/en/howto/hardware-servers/) and/or  [the SSH info page](/en/howto/ssh/).
