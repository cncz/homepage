---
title: Nieuwe versie van Bookings
author: alexander
date: 2024-06-03
tags:
  - medewerkers
  - software
cover:
  image: img/2024/bookings.jpg
---

Vorige week is er een nieuwe versie van het Boekingssysteem voor wetenschappelijke apparatuur live gegaan.

In samenwerking met [Emendis](https://www.emendis.nl/) heeft de applicatie een volledige make-over gekregen, zowel qua uiterlijk als qua
onderliggende techniek.
Voor de authenticatie maken we vanaf heden gebruik van SurfConext.

Onze dank gaat uit naar alle collega's die hebben geholpen met het testen van de nieuwe applicatie, en in het bijzonder
naar {{< author "fmelssen" >}} voor al zijn inspanningen in de afgelopen jaren.

Voor vragen over de nieuwe applicatie kan men zich wenden tot [C&CZ](/nl/howto/contact/).