---
author: bertw
date: 2012-09-12 14:11:00
tags:
- studenten
- medewerkers
title: Mobiele telefonie in en om het Huygensgebouw
---
Mobiele bedrijfstelefonie via Vodafone Wireless Office (VWO) wordt vanaf
nu als dienst geboden. De indoor dekking van het
[Vodafone](http://www.vodafone.nl) mobiele netwerk in het hele
Huygensgebouw daarvoor goed genoeg. In de planning staat nog de
verbetering van de dekking in de kelders en de gebouwen om het
Huygensgebouw (Logistiek Centrum, NMR, HFML, FEL en Nanolab). De eerder
verwachte aansluiting van KPN en T-Mobile op het hiervoor aangelegde
[Distributed Antenna
System](http://en.wikipedia.org/wiki/Distributed_Antenna_System) is nog
onzeker. Op onze [telefonie-pagina](/nl/tags/telefonie/) is alle
informatie te vinden over VWO (mobiel bellen en gebeld worden met de
RU-interne vijfcijferig nummers, ook buiten de campus). Ook staat daar
meer informatie over IP-telefonie, de toekomst van de vaste telefonie
aan de RU.
