---
title: Matlab R2024a beschikbaar
author: petervc
date: 2024-04-05
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/matlab.png
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2024a, is
beschikbaar. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is deze versie beschikbaar, de vorige versie
(/opt/matlab-R2023b/bin/matlab) zal nog tijdelijk te gebruiken zijn. Op de
door C&CZ beheerde Windows-machines zal Matlab tijdens het semester niet
van versie veranderen om versieafhankelijkheden bij lopende colleges te
voorkomen.
