---
author: polman
date: 2017-08-15 13:24:00
tags:
- medewerkers
- docenten
- studenten
- dns
title: Website certificates more secure with CAA in DNS
---
In all DNS zones C&CZ added [DNS CAA Resource
Records](https://tools.ietf.org/html/rfc6844). As of September 8, 2017,
we have [more control over the SSL certificates for our
domains](https://www.xolphin.com/support/ssl/Terminology/CAA_DNS_Records?cur=EUR).
A hack at a random provider of certificates, like [Diginotar in 2011
(Dutch only)](https://nl.wikipedia.org/wiki/Hack_bij_DigiNotar) can no
longer be used to generate false certificates for our domains, because
in the CAA records we noted that we only use certificates issued
[DigiCert](https://www.digicert.com/).
