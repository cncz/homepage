---
author: petervc
date: 2015-08-06 11:02:00
tags:
- medewerkers
- docenten
title: MS Windows 10 on Install network share
---
The most recent version of [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, is available on
the [Install](/en/howto/install-share/) network share. The license
permits upgrade to this version on university computers. License codes
can be requested from C&CZ helpdesk or postmaster. For privately owned
computers you can order this software relatively cheap at
[Surfspot](http://www.surfspot.nl).
