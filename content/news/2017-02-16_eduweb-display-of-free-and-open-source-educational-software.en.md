---
author: sioo
date: 2017-02-16 15:11:00
tags:
- medewerkers
- studenten
title: 'Eduweb: display of free and open source educational software'
---
Together with [StITPro](http://stitpro.nl), we have developed
[Eduweb](http://eduweb.science.ru.nl), an overview page of Free and Open
Source educational software. Some are in use and/or have been developed
within the Faculty of Science, others at other educational institutions.
