---
author: petervc
date: 2011-12-05 11:10:00
tags:
- studenten
- medewerkers
- docenten
title: Never react to an e-mail that asks for your password\!
---
A few days ago one of our users replied to an e-mail that apparently was
sent by “support” and asked for username and password. This account was
later misused to send hundred thousands spam-mails through our mail
server. This resulted in the blacklisting of our outgoing mail server in
e.g. the blacklists of Windows Live Hotmail. The fastest way to remedy
this is to give our outgoing mail server a different IP-address, which
we did this morning. Lesson: Never reply to an e-mail that asks for your
username and password! It causes a lot of trouble for us and other
users.
