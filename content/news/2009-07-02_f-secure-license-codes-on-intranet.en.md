---
author: petervc
date: 2009-07-02 16:08:00
title: F-Secure license codes on Intranet
---
To make it easier to switch from [McAfee](http://www.mcafee.com) to
[F-Secure](http://www.f-secure.com) the [RU ICT
security](http://www.radboudnet.nl/ict-beveiliging/) website lists all
information on [F-Secure software](http://www.radboudnet.nl/fsecure).
After logging in with U- or S-number, the license codes are immediately
available.
