---
author: wim
date: 2010-08-05 15:42:00
tags:
- studenten
title: Onderwijs PC's bruikbaar voor alle RU-studenten
---
Alle RU-studenten kunnen vanaf nu gebruik maken van de PC’s in de [FNWI
PC-onderwijsruimtes](/nl/howto/terminalkamers/). Hierbij moet moet men
dan kiezen voor het RU domein en inloggen met het RU-account (S-nummer
en RU-wachtwoord).

Pas wanneer de “provisioning” van het RU-account van alle RU-medewerkers
naar het RU Active Directory domein gerealiseerd is, kunnen ook alle
RU-medewerkers op deze manier inloggen.

FNWI-medewerkers en -studenten kunnen uiteraard gebruik blijven maken
van het B-FAC domein met hun Science account. Andere medewerkers of
gasten kunnen via hun afdelingscontactpersoon een tijdelijk Science
account aanvragen.
