---
title: Veel reacties op enquête over dienstverlening C&CZ
author: petervc
date: 2024-01-23
tags:
- medewerkers
- studenten
cover:
  image: img/2023/survey-cncz-nl.png
---
We zijn erg blij dat we 330 volledig ingevulde enquêtes ontvingen.
Veel waren erg positief. Enkele bevatten suggesties voor verbeteringen of
nieuwe diensten. We zullen de resultaten in de komende maanden verder analyseren.
