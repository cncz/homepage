---
author: petervc
date: 2008-04-14 12:43:00
title: Probleem stroomvoorziening 10/11 april
---
Donderdagavond 10 april om 21:30 uur was er een dipje in de
stroomvoorziening van vleugels 1 en 2 van het Huygens gebouw, dat
zekeringen deed springen. Dankzij een UPS in vleugel 1 hebben de router
en netwerkswitches aldaar nog doorgelopen tot ca. 22:30 uur. Het
[UVB](http://www.ru.nl/uvb) heeft om 01:30 uur de stroomvoorziening van
de router en switches in vleugel 1 hersteld. De stroomvoorziening van de
switches in vleugel 2 is pas om 10:00 uur hersteld, zodat de overlast
voor de gebruikers in heel vleugel 2 en de centrale straat aan de
oostzijde tussen vleugels 2 en 4 het grootst is geweest (geen netwerk).
C&CZ zal bij UVB aankaarten dat voor herstel van stroomvoorzieningen in
datakasten hogere prioriteit gewenst is.
