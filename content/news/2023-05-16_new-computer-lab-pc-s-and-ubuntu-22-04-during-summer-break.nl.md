---
title: Nieuwe terminalkamer pc's in zomervakantie
author: petervc
date: 2023-05-16
tags:
- medewerkers
- studenten
cover:
  image: img/2023/Dell-AiO-W10-U22.jpg
---
Tijdens de zomervakantie zullen alle pc's in de [terminalkamers](https://cncz.science.ru.nl/nl/howto/terminalkamers/) vervangen worden, omdat ze tussen 4 en 7 jaar oud zijn.
De nieuwe pc's worden wederom dual-boot, nu met Windows10 22H2 en Ubuntu 22.04.
Docenten die hun cursus-software onder Linux willen testen, kunnen dat nu al doen op de loginserver lilo8, die ook Ubuntu 22.04 heeft.
De nieuwe hardware is [Dell Optiplex 7400 All-In-One](https://www.dell.com/nl-nl/shop/desktops-en-werkstations/optiplex-7400-all-in-one/spd/optiplex-7400-aio/).

Voor medewerkers met een werkplek met een oudere versie van Ubuntu kan dit ook een goede aanleiding zijn om naar Ubuntu 22.04 te upgraden. Neem dan [contact op met C&CZ](/nl/howto/contact) om de upgrade te plannen.

De enige terminalkamer die geen upgrade zal krijgen is TK053 (HG02.053), deze ruimte zal niet langer als terminalkamer gebruikt worden.
