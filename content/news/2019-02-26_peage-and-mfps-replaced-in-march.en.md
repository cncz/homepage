---
author: petervc
date: 2019-02-26 13:01:00
tags:
- medewerkers
- studenten
title: Peage and MFP's replaced in March
---
Konica Minolta won the European tender for multifunctionals (MFPs)
again. All MFPs, associated software and the payment system will be
replaced in March 2019. Every new MFP has a card reader. The campus card
or another chip card, e.g. the OV chip card, can be linked to the RU
account (U-, S- or E-number). Logging in on the MFP can then be done
without a pin code with the linked card. For students or study
associations it becomes possible to use group budgets. Students will use
the [Skuario app](https://www.skuario.com/) for topping up and
transferring budgets. [More info](https://wiki.cncz.science.ru.nl/Peage)
will be published in March.
