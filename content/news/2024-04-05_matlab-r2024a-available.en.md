---
title: Matlab R2024a available
author: petervc
date: 2024-04-05
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/matlab.png
---
The latest version of [Matlab](/en/howto/matlab/), R2024a, is available.
The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-share. All
C&CZ-managed Linux machines have this version installed, the older
version (/opt/matlab-R2023b/bin/matlab) will be available temporarily.
The C&CZ-managed Windows machines will not receive a new version during
the semester to prevent problems with version dependencies in current
lectures.
