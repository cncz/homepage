---
author: petervc
date: 2013-11-26 14:57:00
tags:
- studenten
- medewerkers
- docenten
title: End of support for X-win32, switch to Xming
---
C&CZ will discontinue the support of the commercial X server software
for Windows X-win32 at the end of January, 2014. We
advise users to switch to the freeware Xming
software.
