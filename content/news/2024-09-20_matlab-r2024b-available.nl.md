---
title: Matlab R2024b beschikbaar
author: arnoudt
date: 2024-09-20
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/matlab.png
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2024b, is
beschikbaar. De software en de licentiecodes zijn
[direct beschikbaar voor Science gebruikers](https://cncz.pages.science.ru.nl/licenses/Matlab/)
en voor anderen via een [mail naar de helpdesk](https://cncz.science.ru.nl/nl/howto/contact/)
te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is deze versie beschikbaar, de vorige versie
(/opt/matlab-R2024a/bin/matlab) zal nog tijdelijk te gebruiken zijn. Op de
door C&CZ beheerde Windows-machines zal Matlab tijdens het semester niet
van versie veranderen om versieafhankelijkheden bij lopende colleges te
voorkomen.
