---
author: petervc
date: 2011-09-16 13:11:00
tags:
- studenten
- medewerkers
- docenten
title: Kun.nl mail-addresses removed
---
All users with a `@*.kun.nl` email address, which almost always is used
only to forward the mail to a `@*.ru.nl` address, received almost two
months ago an email that these addresses would be removed today. Today
all `@*.kun.nl` have been removed, with a temporary exception for
departments that have not been able to get externally registered
addresses changed. This removal of `@*.kun.nl` email addresses has been
coordinated between the administrators of email servers at Radboud
University.
