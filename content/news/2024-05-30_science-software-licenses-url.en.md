---
title: Science software licenses URL
author: petervc
date: 2024-05-30
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/license-agreement-vectorportal.jpg
---
All license info, including the license codes, are now readily available for all
Science users at [the Science license overview page](https://cncz.pages.science.ru.nl/licenses/)
for the Science software managed by C&CZ.
