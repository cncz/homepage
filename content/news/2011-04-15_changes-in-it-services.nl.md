---
author: caspar
date: 2011-04-15 18:07:00
tags:
- studenten
- medewerkers
- docenten
title: Ontwikkelingen ICT dienstverlening
---
Op 1 september 2009 is [GDI](http://www.ru.nl/gdi) (Gebruikersdienst
ICT) gevormd uit de voormalige Computer Ondersteuningsgroepen (COG’s)
van de verschillende faculteiten en clusters van onze universiteit.
Vanwege de bijzondere rol van ICT voor FNWI is C&CZ als facultaire
ICT-dienst blijven bestaan.

Op grond van een besluit van het College van Bestuur worden RU-breed
alle standaard Windows werkplekken en studenten-PC’s ondergebracht bij
GDI. De overige ICT diensten bij FNWI blijven in beheer bij C&CZ. GDI
bereidt momenteel de overname van de Windows werkplekken van de
stafafdelingen alsmede alle onderwijs-PC’s voor. De gebruikers zullen
ruim van tevoren worden geinformeerd over de wijzigingen die dit met
zich meebrengt. **Tot de overname, die gefaseerd zal plaatsvinden,
blijft C&CZ verantwoordelijk voor alle ICT dienstverlening t.b.v. FNWI
en blijven wij [bereikbaar via de bekende kanalen](/nl/howto/contact/).**
