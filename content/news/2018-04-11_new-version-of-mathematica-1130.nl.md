---
author: petervc
date: 2018-04-11 16:18:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van Mathematica (11.3.0)
---
Er is een nieuwe versie (11.3.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden. De installatiebestanden voor Windows en Linux zijn op de
[Install](/nl/howto/install-share/)-schijf te vinden, ook voor oudere
versies. Afdelingen die meebetalen aan de licentie kunnen bij C&CZ de
licentie- en installatie-informatie krijgen.
