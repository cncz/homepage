---
author: petervc
date: 2012-02-28 14:29:00
tags:
- studenten
title: Reset button for Info-PCs
---
Users were unable to reset or switch on the Info-PCs in the central
street in front of the [Library of
Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/), because
they are hidden in a cupboard That has been solved now by attaching the
reset buttons to the sides of the cupboard.
