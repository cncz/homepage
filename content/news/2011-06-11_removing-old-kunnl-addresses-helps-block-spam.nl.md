---
author: petervc
date: 2011-06-11 19:41:00
tags:
- studenten
- medewerkers
title: Oud kun.nl adres laten verwijderen scheelt spam\!
---
De laatste dagen blijkt er bij sommige gebruikers meer spam binnen te
komen dan normaal. Vaak is die spam gestuurd naar een oud kun.nl
mail-adres. Op de [Doe Het Zelf website](http://dhz.science.ru.nl) kunt
u zien welke oude adressen u nog heeft. Als u een mailtje naar
postmaster\@science.ru.nl stuurt met welke adressen u verwijderd wil
hebben, kan dat snel veel spam schelen. Er wordt een plan gemaakt om
alle kun.nl adressen op te ruimen, maar misschien wilt u daar niet op
wachten.
