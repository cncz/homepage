---
author: sioo
date: 2021-06-18 11:18:00
tags:
- medewerkers
- studenten
title: Expansion of Science datacenter to Forum building
---
Because FNWI research groups are increasingly purchasing powerful
computers/computing clusters with many CPUs and GPUs, C&CZ has expanded
the data center to the existing server room of Radboud Services, in the
Forum on the other side of Heijendaalseweg. As a result, there will be
sufficient physical space, power and cooling for the servers purchased
by departments in the coming years.
