---
author: petervc
date: 2017-09-22 17:34:00
tags:
- studenten
- medewerkers
- docenten
title: Oude serienummers Adobe Creative Cloud stoppen met werken
---
Via ISC licentiebeheer kregen we de mededeling van SURFmarket dat de
oude serienummers van de Adobe Creative Cloud software per half november
verlopen. Alle eerder met de oude serienummers geïnstalleerde software
zal niet meer werken. De oude serienummers horen bij het contract dat 31
mei 2017 afliep, de nieuwe horen bij het contract 2017-2020.
