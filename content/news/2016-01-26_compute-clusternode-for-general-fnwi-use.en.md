---
author: petervc
date: 2016-01-26 16:29:00
tags:
- medewerkers
title: Compute clusternode for general FNWI use
---
In order to facilitate FNWI departments that don’t have their own
clusternodes, but that incidentally have computing needs, C&CZ has
purchased a [cn-clusternode](/en/howto/hardware-servers/). The original
reason for C&CZ to buy this computer server actually was to help
departments phase out old clusternodes that are much less energy
efficient (performance per Watt). The C&CZ clusternode is a Dell
PowerEdge R730 with 2 processors (10-core Xeon E5-2660V3 at 2.6 GHz) and
256 GB memory.
