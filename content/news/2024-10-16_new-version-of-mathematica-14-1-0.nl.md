---
title: Nieuwe versie van Mathematica 14.1.0
author: arnoudt
date: 2024-10-16
tags:
  - medewerkers
  - studenten
  - software
cover:
  image: img/2024/mathematica.png
---

Er is een nieuwe versie (14.1.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden.

Bij een bedrade of draadloze verbinding met het
campusnetwerk of via [VPN](/nl/howto/vpn/) zijn de
installatiebestanden voor Windows, Linux en macOS op de [C&CZ
Install](https://install.science.ru.nl/science/Mathematica/)-schijf te
vinden.

Bij de installatie moet je aangeven:

    License server: mathematica.science.ru.nl
    License: L4601-6478

Volgens de [Mathematica Quick Revision History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 14.1 introduces the unified Wolfram application and expands Wolfram Language by offering new tools for working with neural nets and LLMs, for finding differences in content, for working with images and videos, and for exploring scientific evaluations through biomolecules, astrophysics and more"
