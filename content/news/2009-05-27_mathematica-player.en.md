---
author: polman
date: 2009-05-27 12:07:00
title: Mathematica Player
---
Mathematica Player is installed in all terminal rooms, this enables you
to play Mathematica notebooks. See the
[Wolfram](http://www.wolfram.com/products/player/) website for more
information.
