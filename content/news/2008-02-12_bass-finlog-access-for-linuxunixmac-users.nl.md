---
author: remcoa
date: 2008-02-12 16:15:00
title: BASS-Finlog toegang voor Linux/Unix/Mac gebruikers
---
Het is nu mogelijk om BASS-Finlog te bereiken voor Linux/Unix/Mac
gebruikers, zie [BASS](/nl/howto/bass/).
