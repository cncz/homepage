---
author: polman
date: 2010-09-23 18:23:00
tags:
- studenten
title: 'Eerstejaars: Science-mail doorgestuurd naar @student.ru.nl'
---
De eerste paar weken van het nieuwe collegejaar werd mail naar nieuwe
studenten @student.science.ru.nl, doorgestuurd naar hun
[Studielink](http://www.studielink.nl) adres, omdat er nog geen
[@student.ru.nl](http://share.ru.nl) adres beschikbaar was. Vandaag is
dit doorsturen gewijzigd naar het @student.ru.nl adres, omdat Biologie
anders geen mail kon sturen naar alle nieuwe studenten. Als een nieuwe
student het doorstuur-adres wil wijzigen, dan kan dat op de
[Doe-Het-Zelf website](http://dhz.science.ru.nl).
