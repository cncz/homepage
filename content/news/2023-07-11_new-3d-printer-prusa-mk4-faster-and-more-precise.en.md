---
title: New 3D printer Prusa MK4 - faster and more precise
author: petervc
date: 2023-07-11
tags:
- medewerkers
- studenten
cover:
  image: img/2023/MK4.jpg
---
The [3D printservice](https://cncz.science.ru.nl/en/howto/print/#3d-printen) has been upgraded with the addition of a [Prusa MK4](https://www.prusa3d.com/product/original-prusa-mk4-2/).
