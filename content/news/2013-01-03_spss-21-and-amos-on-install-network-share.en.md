---
author: petervc
date: 2013-01-03 14:21:00
tags:
- studenten
- medewerkers
title: SPSS 21 and AMOS on Install network share
---
The most recent version of [IBM
SPSS](http://www-01.ibm.com/software/analytics/spss/products/statistics/),
21, is available for MS-Windows, Mac OS X and Linux. It can be found on
the [Install](/en/howto/install-share/) network share. SURFdiensten noted
that the license now incorporates e.g. Amos, Modeller en Bootstrapping.
The license permits use on university computers and also home use by RU
employees and students. License codes can be requested from C&CZ
helpdesk or postmaster. One can also order the DVD’s on
[Surfspot](http://www.surfspot.nl).
