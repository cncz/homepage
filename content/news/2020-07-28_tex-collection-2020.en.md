---
author: petervc
date: 2020-07-28 18:03:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2020
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2020, is available for
MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share.
