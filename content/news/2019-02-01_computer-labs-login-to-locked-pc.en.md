---
author: wim
date: 2019-02-01 12:28:00
tags:
- medewerkers
- studenten
title: 'Computer labs: login to locked pc'
---
From now on, it is possible to login to a PC in [the computer labs, the
library and study landscape](/en/howto/terminalkamers/) when another user
has locked the PC. The hope is that this contributes somewhat to
reducing the shortage of workplaces. When these PCs were running
Windows7, after 15 minutes of inactivity the logged-in user could be
logged out without restarting the PC, but this software no longer works
with Windows10. To login as another user, click on “Switch User” in the
the bottom left corner of the lock screen.
