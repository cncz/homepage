---
author: petervc
date: 2015-12-15 17:03:00
tags:
- medewerkers
- docenten
- studenten
title: Dell/Intel HPC code optimization event Amsterdam
---
January 14, 2016, Dell is organizing a high performance (HPC) community
event in Amsterdam. Martin Hilgeman (Dell) will talk about optimization
of software, Laurent Duhem (Intel) about developer tools. Interested
employees and students may register for the event at [the event’s
website](https://www.eventbrite.nl/e/registratie-the-hpc-community-event-19421646668).
Topics as parallellization, GPU’s and co-processors will be discussed.
