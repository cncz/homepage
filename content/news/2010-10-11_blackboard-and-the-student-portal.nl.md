---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: Blackboard en de Studentenportal
---
De
[Studenten-portal](/nl/howto/blackboard#.5bblackboard-en-de-nieuwe-studentenportal.5d.5bblackboard-and-the-new-student-portal.5d/)
wordt de centrale dagelijkse ingang tot de RU web services voor
studenten. Ook de meest recente Blackboard announcements staan op de
portal.
