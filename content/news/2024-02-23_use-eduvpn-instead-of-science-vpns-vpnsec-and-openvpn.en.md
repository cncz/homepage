---
title: Use eduVPN instead of Science VPNs VPNsec/OpenVPN
author: petervc
date: 2024-02-23
tags:
- medewerkers
- studenten
cover:
  image: img/2024/eduVPN.jpg
---
For RU students and employees, the [SURF service eduVPN](https://www.surf.nl/en/services/eduvpn) has many advantages over
the older Science VPNs: VPNsec and OpenVPN. It's very easy to
[install and use eduVPN on Windows/macOS/Android/iOS/Linux](https://www.ru.nl/en/staff/services/services-and-facilities/ict/working-off-campus/why-should-you-use-eduvpn/using-eduvpn)
and you have access to all Science services that are also available through the Science VPNsec/OpenVPN.
We list it on [our VPN page](/en/howto/vpn/) as the primary VPN to use.

We advise users of the Science VPNs to [move over to eduVPN](https://www.ru.nl/en/staff/services/services-and-facilities/ict/working-off-campus/why-should-you-use-eduvpn/using-eduvpn).
