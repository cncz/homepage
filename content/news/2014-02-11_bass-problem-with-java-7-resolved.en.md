---
author: petervc
date: 2014-02-11 17:40:00
tags:
- medewerkers
title: BASS problem with Java 7 resolved
---
The [ISC](http://www.ru.nl/isc) let us know that after the installation
of patches installed in [BASS](/en/howto/bass/) during the weekend of
December 1, the possibility to use a recent version of Java (version 7)
had erroneously disappeared. As of February 11, 2014 this issue has been
resolved. The function in BASS to work with all versions of Java SE
(JRE) on the client (PC) has been reactivated. Furthermore all JAR-files
in BASS have received a security certificate (they have been signed),
with which it possible for BASS to work with the latest Java SE versions
(Java SE 7.51 en hoger). BASS users who work with Forms, will see pop-up
windows asking “Do you want to run this application?”. Of this a
[description has been made on the RU
Intranet](http://www.radboudnet.nl/publish/pages/685146/meldingen_java_nieuwe_versie_4.pdf).
