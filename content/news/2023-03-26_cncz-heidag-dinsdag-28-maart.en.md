---
title: 'C&CZ awayday Tuesday March 28'
author: petervc
date: 2023-03-26
tags:
- medewerkers
- studenten
cover:
  image: img/2023/heidag.jpg
---
An awayday for the C&CZ department is scheduled for Tuesday March 28.
C&CZ can be reached in case of serious disruptions of services.
