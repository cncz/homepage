---
author: petervc
date: 2017-08-08 14:25:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: dinsdag 29 augustus'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor dinsdag 29 augustus.
Voor bereikbaarheid in geval van ernstige storingen wordt gezorgd.
