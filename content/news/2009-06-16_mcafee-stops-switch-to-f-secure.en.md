---
author: wim
date: 2009-06-16 15:44:00
title: McAfee stops, switch to F-Secure
---
The end date of the [earlier announced](/en/news/) license for
[McAfee](http://www.mcafee.com) security software, September 1, 2009,
comes nearer! On the [Windows Managed pc’s](/en/howto/windows-beheerde-werkplek/)
McAfee is step by step being replaced by
[F-Secure](http://www.f-secure.com), starting with [PC-rooms and study
landscape](/en/howto/terminalkamers/). Employees and students can also
use The F-Secure Client Security software at home. It can be found on
the [install](http://www.cncz.science.ru.nl/software/installscience)
network disk. License codes can be requested from C&CZ. Adequate
security is more than just a virus scanner, see e.g. the [RU
ICT-security website (in Dutch)](http://www.ru.nl/ict-beveiliging/) on
securing and cleaning a pc.
