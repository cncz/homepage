---
author: bram
date: 2014-09-19 12:39:00
tags:
- medewerkers
- studenten
title: Disk and mail quota enlarged to 2 GB
---
The [quota](/en/howto/quota-bekijken/) for the Science [home directory /
U:-disk](/en/howto/diskruimte/) has been enlarged to 2 GB. Also the
Science mail quota have been enlarged to 2 GB for users with less quota.
Please mail postmaster if you need more quota. This [disk
space](/en/howto/diskruimte/) on [C&CZ
servers](/en/howto/hardware-servers/) is being [backed
up](/en/howto/backup/).
