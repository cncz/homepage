---
author: john
date: 2017-01-09 11:15:00
tags:
- medewerkers
- docenten
- studenten
title: Computer lab HG00.137 renewed
---
During the Christmas break all PCs in the [computer
lab](/en/howto/terminalkamers/) TK137 (HG00.137) have been replaced by
new ones, of the same type as with [the previous
replacement](/en/news/). These are [Dell OptiPlex 7440
AIO](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd) with
24-inch Full-HD screen, a Core i5 6500 (3.2GHz,QC) processor, 1x8GB
memory, 512GB SSD hard disc and relatively quiet [Cherry Stream 3.0
keyboards](https://www.techpowerup.com/216759/cherry-announces-the-stream-3-0-keyboard).
For possible use with digital exams we have chosen the [Dell
Articulating
Stand](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd).
