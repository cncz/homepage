---
author: petervc
date: 2013-05-31 16:21:00
title: BASS te gebruiken met Java 7
---
Java 7 kan per 1 juni gebruikt worden om [BASS](/nl/howto/bass/) (Oracle
Forms) te benaderen, omdat dan de BASS serversoftware een upgrade gehad
heeft. Clients kunnen daarna ook de reguliere Java updates installeren,
waardoor ze beter beveiligd zijn.
