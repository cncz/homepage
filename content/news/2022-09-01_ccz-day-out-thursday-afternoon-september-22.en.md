---
author: petervc
date: 2022-09-01 13:30:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Thursday afternoon September 22'
---
The C&CZ yearly day out is scheduled for Thursday afternoon September
20. C&CZ can be reached in case of serious disruptions of services.
