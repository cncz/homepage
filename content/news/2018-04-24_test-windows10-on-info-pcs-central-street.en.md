---
author: wim
date: 2018-04-24 16:04:00
tags:
- medewerkers
- studenten
title: Test Windows10 on info pcs central street
---
During the summer break all [pc’s in the computer
labs](/en/howto/terminalkamers/) will get an upgrade from Windows7 to
Windows10. This can now be tested on the 4 info column pcs in the
central street near the Library of Science (HG00.011). All [remarks and
questions](/en/howto/contact/) are welcome. N.B.1: the Windows7 roaming
profile has not been transfered, every user gets a clean profile. N.B.2:
because the info column pcs are used less and less, they will be removed
during the summer break.
