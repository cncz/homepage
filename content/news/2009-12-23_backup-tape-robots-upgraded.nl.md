---
author: caspar
date: 2009-12-23 16:48:00
title: Backup tape robots ge-upgrade
---
We hebben onze twee 5 jaar oude LTO2 (200 Gb/tape ongecomprimeerd)
8-Tape Library units naar twee LTO4 (800 Gb/tape ongecomprimeerd)
24-Tape Library units. We hebben ze *R2D2* en *C3PO* genoemd. Deze
upgrade verhoogt de backup-capaciteit dus met een factor 12. Dit was
nodig omdat er steeds meer data [gebackupped](/nl/howto/backup/) moet
worden. Bovendien zijn de nieuwe drives sneller waardoor backups en
restores sneller worden en de operationele tijd die hiermee gemoeid is
afneemt.

Binnenkort zullen we een nieuwe bestemming voor onze oude, maar nog
steeds goed functionerende LTO2 units zoeken. Neem voor meer informatie
hierover contact op met postmaster at science.ru.nl.
