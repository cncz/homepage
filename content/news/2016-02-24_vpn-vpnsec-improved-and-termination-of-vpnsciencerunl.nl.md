---
author: wim
date: 2016-02-24 14:35:00
tags:
- medewerkers
- studenten
title: 'VPN: vpnsec verbeterd en einde oude vpn.science.ru.nl'
---
De configuratie en [handleiding](/nl/howto/vpn/) van de nieuwe VPN (de op
IPsec gebaseerde vpnsec.science.ru.nl) is verbeterd, i.h.b. voor OS X
apparaten. De
[Vpnsec-macosx.mobileconfig](/nl/howto/images/6/61/vpnsec-macosx.mobileconfig/)
is nu getekend met een [verifieerbaar juist
certificaat](https://www.digicert.com/intranet-server-security.htm).
Hierdoor werkt de nieuwe VPN nu met alle geteste apparatuur. De
bedoeling is dat alle gebruikers voor 1 mei 2016 overstappen naar de
nieuwe VPN. De oude VPN, op basis van
[PPTP](https://nl.wikipedia.org/wiki/Point-to-Point_Tunneling_Protocol),
kan dan uitgezet worden.
