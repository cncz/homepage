---
author: petervc
date: 2009-09-25 16:31:00
title: PC-zalen vernieuwd en uitgebreid
---
De meer dan vijf jaar oude pc’s in [PC-zalen](/nl/howto/terminalkamers/)
HG00.029 en HG02.702 zijn vervangen door nieuwe. Hierbij is
tegelijkertijd uitbreiding met in totaal 27 pc’s gerealiseerd vanwege de
gestegen studentenaantallen. Beide zalen zijn dual-boot: iedere pc is
zowel bruikbaar met Microsoft Windows als met Fedora Linux. Door te
wachten op de lagere prijzen van de
[huisleverancier](/nl/howto/huisleverancier-pcs/), na de nieuwe Europese
aanbesteding van werkplek-pc’s, kon een besparing van duizenden euro’s
gerealiseerd worden. De overige kleine honderd pc’s die in 2004, bij de
oplevering van de eerste fase van het Huygensgebouw, aangeschaft zijn
voor de pc-zalen, zullen volgens plan eind 2009 vervangen worden.
