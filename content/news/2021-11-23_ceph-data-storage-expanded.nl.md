---
author: wim
date: 2021-11-23 14:06:00
tags:
- storage
cover:
  image: img/2021/ceph.png
title: Ceph data-opslag uitgebreid
---
Sinds eind 2019 heeft C&CZ
[Ceph](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=nl#Ceph_Storage)
als keus voor de opslag van data, naast de traditionele RAID-opslag op
individuele fileservers. Omdat er steeds meer gebruik gemaakt wordt van
Ceph, is de Ceph-opslag uitgebreid van bruto 1.8 PiB naar 2.8 PiB.
Hierbij zijn ook servers geplaatst in een centraal RU-datacentrum,
waardoor de Ceph-opslag nu verdeeld is over drie datacentra. Wie wil
proberen of Ceph voordelen heeft, kan [contact opnemen met
C&CZ](/nl/howto/contact/).
