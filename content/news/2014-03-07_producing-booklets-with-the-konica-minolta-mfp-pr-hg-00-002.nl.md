---
author: wim
date: 2014-03-07 12:12:00
tags:
- medewerkers
- studenten
title: Boekjes maken met de Konica Minolta MFP pr-hg-00-002
---
In de [Konica Minolta MFP](/nl/howto/copiers/) pr-hg-00-002 zijn een
gaatjesmaker en een rug-nieter die kan vouwen geplaatst. Hiermee wordt
het mogelijk om complete boekjes te produceren. Er kunnen 2 of 4 gaatjes
worden geponst. Maximaal 20 vel kan in het midden (op de rug) geniet
worden en gevouwen, waardoor van A3-vellen een A4-boekje gemaakt kan
worden, of van A4-vellen een A5-boekje.
