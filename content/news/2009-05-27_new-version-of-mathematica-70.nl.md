---
author: polman
date: 2009-05-27 16:54:00
title: Nieuwe versie van Mathematica (7.0)
---
Er is een nieuwe versie (7.0) van [Mathematica](/nl/howto/mathematica/)
geïnstalleerd. De CD’s zijn te leen voor afdelingen die meebetalen aan
de licentie.
