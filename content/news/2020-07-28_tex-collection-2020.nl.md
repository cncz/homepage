---
author: petervc
date: 2020-07-28 18:03:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2020
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2020, voor MS-Windows,
Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf.
