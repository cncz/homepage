---
title: Maple license continued for 2 concurrent users
author: petervc
date: 2023-12-01
tags:
- medewerkers
- studenten
cover:
  image: img/2023/maple.png
---
The license of [Maple](/en/howto/maple/) has been continued in 2024 as a 2-user license.
All FNWI employees and students are allowed to use Maple. If you want to use Maple,
please contact [C&CZ](/en/howto/contact/).
