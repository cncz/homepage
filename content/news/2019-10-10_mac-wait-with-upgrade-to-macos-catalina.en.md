---
author: petervc
date: 2019-10-10 14:45:00
tags:
- studenten
- medewerkers
- docenten
- macos
title: 'Mac: wait with upgrade to macOS Catalina'
---
The brand new macOS Catalina is now available. It is \*\*not\*\* a good
idea to upgrade with the first group of users, this will give problems.
Up till now, we received reports about Adobe software not working,
F-Secure Client Security not working, MestreNova not working, EndNote
needing an upgrade to 9.3.1 or higher, Science VPNsec only working after
reinstalling VPN. For more information what could go wrong, see [Hold
Off
Catalina](https://www.zdnet.com/article/macos-catalina-hold-off-for-a-couple-of-weeks/)
and [Wait on Catalina](https://cdm.link/2019/10/wait-on-catalina/). If
the upgrade has already been done, the problems can be solved by
installing an older version of macOS and restoring files with Time
Machine.
