---
author: mkup
date: 2015-03-17 09:56:00
tags:
- medewerkers
- studenten
title: Vernieuwing WiFi netwerk Huygens en Mercator 1
---
In de periode van 23 maart tot 20 april 2015 wordt het WiFi netwerk
(eduroam en ru-guest) vernieuwd in het gehele Huygensgebouw, het terras
en de grasvelden rondom Huygens en in Mercator 1, verdiepingen 0 t/m 4.
Alle bestaande accesspoints worden vervangen door een nieuw type en het
aantal wordt flink uitgebreid om zodoende de bandbreedte, dekking en
capaciteit te vergroten. De werkzaamheden zullen worden uitgevoerd door
JS Network Solutions, in opdracht van het ISC, onder regie van het UVB.
De monteurs van JS zullen voornamelijk in de gangzones werken, maar er
zal ook toegang tot sommige werkkamers en/of labs nodig zijn, uiteraard
altijd na overleg met de betreffende bewoners, secretariaten of
lab-beheerders. Men start in Huygens vleugel 1 en werkt richting vleugel
8. Als laatste is Mercator 1 aan de beurt. De vernieuwing van het WiFi
netwerk in de overige FNWI-gebouwen zal naar verwachting in 2016
plaatsvinden. Voor nadere informatie kunt u contact opnemen met Marcel
Kuppens.
