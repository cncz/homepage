---
author: bram
date: 2018-06-26 14:14:00
tags:
- studenten
- medewerkers
- docenten
title: Digital testing/assessment with cloud application streaming
---
Since February 2015, [digital
tests/assessments](/en/howto/digitaal-toetsen/) can be held within the
Faculty of Science. Last week, more than 300 students Computing Science
were successfully graded in a large scale digital test/assessment with
use of the [Netbeans](https://netbeans.org/) integrated development
environment. This IDE was streamed as a Windows application from cloud
supplier [Amazon](https://aws.amazon.com). The application ran in a tab
of the RU Kiosk App and was delivered through [Amazon Appstream
2.0](https://aws.amazon.com/appstream2/) on Chromebooks. The Chromebooks
were purchased last year by the university to make large scale digital
testing/assessment possible.
