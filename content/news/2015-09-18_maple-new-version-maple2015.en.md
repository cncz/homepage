---
author: polman
date: 2015-09-18 10:41:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2015'
---
The latest version of [Maple](/en/howto/maple/), Maple2015, is available
as of today on all C&CZ managed Linux PC’s and will be available shortly
on all Windows PC’s. The license of Maple is a 5-user license. Of these,
4 are financed by the Faculty of Science departments [EHEF and
THEF](http://www.ru.nl/highenergyphysics/). Because 1 license is paid by
[C&CZ](/), all FNWI employees and students are allowed to use Maple once
in a while. If you want to use Maple more often, please contact
[C&CZ](/en/howto/contact/). The software can be found on the
[Install](/en/howto/install-share/) network share. License codes can be
obtained from C&CZ helpdesk or postmaster.
