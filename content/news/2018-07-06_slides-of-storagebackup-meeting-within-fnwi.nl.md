---
author: bbellink
date: 2018-07-06 15:20:00
tags:
- medewerkers
- docenten
title: Slides van data/backup bijeenkomst in FNWI
---
Op 28 juni heeft C&CZ een bijeenkomst gehouden over o.a. huidige status
en toekomst van storage en backup binnen FNWI, o.a. omdat C&CZ binnen
afzienbare tijd het datacenter netwerk en de storage- en
backup-oplossingen moet vernieuwen. Hier zijn o.a. de facultaire data
stewards voor uitgenodigd. De [slides van de presentatie (English
only)](/nl/howto/dataslides//) zijn beschikbaar.
