---
author: remcoa
date: 2015-09-29 14:45:00
tags:
- medewerkers
- studenten
title: 'Online surveys: New version of LimeSurvey, login with Science account'
---
A new version of [LimeSurvey](/en/howto/limesurvey/) has been installed,
along with some new and modern templates. It is no longer necessary to
request a login, any Science account can log in now. More information
about [LimeSurvey](/en/howto/limesurvey/)…
