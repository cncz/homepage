---
title: "Lilo: Per Gebruiker Geheugen Limiet"
date: 2024-02-28T09:15:00+01:00
author: miek
cover:
        image: img/memory-limit.jpg
---

De [Lilo servers](https://cncz.science.ru.nl/en/howto/hardware-servers/#linux-loginservers) zijn
voor algemeen gebruik, met als voorbehoud:

> Iedereen mag ze gebruiken zoals men wil, zolang het geen overlast voor anderen oplevert.

Om dit wat meer af te dwingen, gaan we een geheugenlimiet van 25% _per gebruiker_ introduceren. Dit
houdt in dat op lilo[78] je tot 16 GiB mag alloceren.
