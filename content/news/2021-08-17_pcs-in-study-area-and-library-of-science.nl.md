---
author: petervc
date: 2021-08-17 16:47:00
title: PC's in Studielandschap en bibliotheek
---
Sinds de zomer van 2021 worden de computers in het studielandschap
(HG00.201) en de bibliotheek (HG00.011) bij FNWI niet meer beheerd door
C&CZ, maar door het [ILS](/nl/howto/contact/#ils-helpdesk).
Dat maakt alle Windows-computers in alle RU-studielandschappen en
bibliotheeklocaties uniform. Informatie over o.a. het reserveren van een werkplek en openingstijden
vind je op de website van de [Library of Science](https://www.ru.nl/ubn/bibliotheek/locaties/library-of-science/).
Naast de door het ISC beheerde Windows computers, zijn er nog acht door
C&CZ beheerde Linux computers aanwezig, vooraan in het
studielandschap (HG00.201)
