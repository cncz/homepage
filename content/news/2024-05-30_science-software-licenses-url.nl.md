---
title: Science software licenties URL
author: petervc
date: 2024-05-30
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/license-agreement-vectorportal.jpg
---
Alle informatie over licenties, waaronder de licentiecodes, is nu direct te zien voor alle
Science gebruikers op [de Science licentie-overzicht pagina](https://cncz.pages.science.ru.nl/licenses/)
voor de Science software waarvan de licenties door C&CZ beheerd worden.
