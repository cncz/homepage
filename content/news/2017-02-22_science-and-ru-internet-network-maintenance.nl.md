---
author: mkup
date: 2017-02-22 14:56:00
tags:
- medewerkers
- studenten
title: Onderhoud netwerk FNWI en RU-Internet
---
Op maandagavond 6 maart zal onderhoud plaatsvinden aan diverse
netwerkonderdelen van de RU. Tussen ca. 22:00-22:30 uur zal bij FNWI
geen netwerkverkeer mogelijk zijn op het vaste en draadloze netwerk.
Vanaf 23:00 tot 7 maart 02:00 uur zal gedurende enige tijd geen verkeer
van/naar internet mogelijk zijn voor de hele RU. Denk s.v.p. na of deze
verstoring voor u gevolgen heeft en bereid u indien nodig voor.
