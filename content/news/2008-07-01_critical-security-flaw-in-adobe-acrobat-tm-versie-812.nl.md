---
author: petervc
date: 2008-07-01 14:09:00
title: Kritiek lek in Adobe Acrobat t/m versie 8.1.2
---
In Adobe Acrobat is een [kritiek
beveiligingslek](http://www.adobe.com/support/security/bulletins/apsb08-15.html)
ontdekt. Alle versie 8 gebruikers wordt aangeraden om naar versie “8.1.2
Security Update 1” te gaan. Omdat de site-license voor Adobe software
onlangs eindelijk rond kwam, kan men deze update waarschijnlijk het
beste combineren met de upgrade naar de Professional versie.
