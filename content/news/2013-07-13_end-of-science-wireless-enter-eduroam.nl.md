---
author: mkup
date: 2013-07-13 12:10:00
title: Science wireless stopt, Eduroam komt
---
Binnenkort starten de voorbereidingen voor de komst van het Eduroam
draadloze netwerk in de FNWI-gebouwen en de vergroting van de capaciteit
van het draadloze netwerk. Bij deze actie zal het Science draadloze
netwerk definitief verdwijnen, zoals [eerder
aangekondigd](/nl/news/).

De werkzaamheden beginnen naar verwachting al op 5 augustus a.s.
Gedurende een periode van enkele weken zal het Science netwerk
geleidelijk verdwijnen terwijl Eduroam verschijnt. I.v.m. deze
overgangsperiode adviseren wij om voor die tijd alle draadloze
apparatuur [over te zetten op
ru-wlan](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/wireless/wireless-ru-1/).
