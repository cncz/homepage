---
author: mkup
date: 2017-10-10 11:30:00
tags:
- studenten
- medewerkers
- docenten
title: Network maintenance October 16
---
Monday morning October 16 at 05:00 am three network switches will be
upgraded with new firmware. Connected devices (including ip-phones and
wireless access points) at three locations will lose their network
connectivity for about 20 minutes:

`- Mercator 1 floor 1 (ICIS, all devices)`
`- Huygens wing 4 (all devices, including reading rooms Library of Science)`
`- Huygens wing 5 (approx 50% of all devices)`
