---
title: Migratie externe GitLab-gebruikers naar Science-logins met alleen toegang tot GitLab
author: bram
date: 2023-03-10
cover:
  image: img/2023/gitlab-logo.svg
---
Met ingang van maart 2023 zullen externe GitLab-gebruikers van
[gitlab.science.ru.nl](https://gitlab.science.ru.nl) gemigreerd worden
naar Science-logins die alleen toegang tot GitLab hebben.

Tot nu toe werden externe GitLab-logins aangemaakt in GitLab zelf, los van
de Science-loginadministratie. Door de externe GitLab-logins te migreren
naar Science-logins met alleen toegang tot GitLab verbetert het beheer van
deze logins. De aanvrager (verantwoordelijk voor de Science-login) kan de logins beheren in
[DHZ](https://dhz.science.ru.nl).

Voor externe GitLab-logins die na 1 april 2023 nog
nodig zijn kunnen Science-logins aangevraagd worden
met enkel toegang tot GitLab, zie hiervoor [Science login
aanvragen](/nl/howto/login/#science-login-aanvragen). Geef bij de aanvraag
door om welk bestaand GitLab-account het gaat, dan kan dat account aan
de nieuwe Science-login worden gekoppeld.

Zodra de nieuwe Science login is aangemaakt zal eerst in [DHZ](https://dhz.science.ru.nl) een wachtwoord moeten worden ingesteld. Pas daarna is het mogelijk om in te loggen op gitlab.science.ru.nl.
Compleet nieuwe Science logins voor GitLab zijn pas na de eerste keer in GitLab inloggen zichtbaar voor anderen. Bijvoorbeeld als je deze personen aan een project wilt toevoegen.