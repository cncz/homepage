---
author: petervc
date: 2016-06-22 14:48:00
tags:
- medewerkers
- docenten
- studenten
title: "Printers op begane grond gaan over naar Péage"
---
Na de [test van Péage op twee MFP’s](/nl/news/) zullen we naar
verwachting begin augustus alle KM’s op de begane grond en de
insteekverdieping in het Huygensgebouw overzetten van het
C&CZ-printsysteem naar [het RU-brede Péage](http://www.ru.nl/peage). De
andere KM’s volgen kort daarna. C&CZ zal FNWI-specifieke informatie
bijhouden op de [C&CZ Péage pagina](/nl/howto/peage/).
