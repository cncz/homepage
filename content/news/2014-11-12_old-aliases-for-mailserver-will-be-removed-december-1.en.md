---
author: petervc
date: 2014-11-12 11:03:00
tags:
- studenten
- medewerkers
- docenten
title: Old aliases for mailserver will be removed December 1
---
C&CZ will remove the aliases for imap, imap-srv, imap-server, pop,
pop-srv, pop-server, smtp, smtp-srv and smtp-server for domains cs ,
math, nmr, sci and theochem as of December 1. The [automatic
configuration of mailclients](/en/howto/categorie%3aemail/) only uses
post.science.ru.nl and smtp.science.ru.nl , as one should do when
[manually configuring a mailclient](/en/howto/categorie%3aemail/). Since
the certificate of the mailserver is not valid for these old aliases, it
raised security questions.
