---
author: petervc
date: 2013-05-26 12:13:00
tags:
- studenten
title: Stem-PC's voor studentenverkiezingen
---
Ook dit jaar heeft C&CZ voor de
[studentenverkiezingen](http://www.ru.nl/verkiezingen/) voor de
[USR](http://www.ru.nl/usr/), [FSR](http://www.ru.nl/fnwi/fsr/) en
[OLC](http://www.ru.nl/fnwi/fsr/medezeggenschap/olc/) enkele stem-pc’s
in het Huygensgebouw verzorgd. Deze pc’s zijn Ubuntu 12.04 Linux pc’s,
waarop de browser [Opera](http://www.opera.com/nl/O) draait in
[kiosk-mode](http://choice.opera.com/support/mastering/kiosk/), zodat de
pc alleen te gebruiken is om te stemmen op de website van de
verkiezingen, zonder dat men in hoeft te loggen
