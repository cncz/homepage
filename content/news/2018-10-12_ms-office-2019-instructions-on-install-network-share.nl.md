---
author: petervc
date: 2018-10-12 15:21:00
tags:
- medewerkers
- docenten
title: MS Office 2019 instructies op Install-schijf
---
De instructies om de nieuwste versie van [Microsoft
Office](http://office.microsoft.com), 2019, te installerem, zijn op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. Installatie
gaat op een andere manier dan voorheen, met de [Office Deployment
Tool](https://www.microsoft.com/en-us/download/details.aspx?id=49117).
De MAK-licentiecode is bekend bij de C&CZ helpdesk. Zij kunnen het
scherm van een PC overmemen en de code invoeren. Dat doen zij alleen als
de PC eigendom van FNWI is. Voor eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden.
