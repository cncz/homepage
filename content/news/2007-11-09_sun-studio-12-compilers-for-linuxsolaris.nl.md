---
author: petervc
date: 2007-11-09 17:57:00
title: Sun Studio 12 compilers voor Linux/Solaris
---
De nieuwste versie (12) van de [Sun
Studio](http://developers.sun.com/sunstudio/) compilers en tools (C,
C++, Fortran) is geïnstalleerd. Ze zijn te vinden in

/opt/sun/sunstudio12/ (Linux) of /vol/sunstudio12/SUNWspro
(Solaris).
