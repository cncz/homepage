---
author: caspar
date: 2013-10-30 18:24:00
tags:
- medewerkers
- docenten
title: Informatiebeveiliging en cloud-diensten
---
Bij het gebruik van cloud-diensten zoals Dropbox, Google+, Gmail,
iCloud, enz. is het van groot belang om de juiste afweging te maken of
de dienst veilig, conform de wet en/of conform het geldende [RU
informatiebeveiligingsbeleid](/nl/howto/informatiebeveiliging/) kan
worden ingezet. Met name de opslag van persoonsgegevens is aan strikte
regelgeving gebonden. Bij twijfel kan C&CZ hierover proberen te
adviseren.
