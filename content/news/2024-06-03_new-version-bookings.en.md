---
title: New version of Bookings
author: alexander
date: 2024-06-03
tags:
- medewerkers
- software
cover:
  image: img/2024/bookings.jpg
---

Last week, a new version of the Bookings Platform for Scientific Devices has been released.

Together with [Emendis](https://www.emendis.nl/), we have given the application a total make-over: not only the look and feel have changed, but also the underlying technique has been revised. 
From now on, we will use SurfConext for authentication.

We wish to thank all colleagues who helped us to test the new application, and in particular, we thank {{< author "fmelssen" >}} for all his efforts in the last years.

For questions concerning the new application, please contact  [C&CZ](/en/howto/contact/).
