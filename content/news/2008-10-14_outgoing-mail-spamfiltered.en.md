---
author: josal
date: 2008-10-14 17:40:00
title: Outgoing mail spamfiltered
---

As of today mail which is recognized by the Science
spamfilter as being spam is _not sent to external
mailservers anymore_, but saved to a central quarantine folder. This
change in the behaviour of the [Science
mailservers](/en/howto/hardware-servers/) was necessary because those
servers otherwise ended up in a blacklist. The [forwarding of incoming
mail](/en/tags/email) thus gives a _filtered mail feed_ from now on.
