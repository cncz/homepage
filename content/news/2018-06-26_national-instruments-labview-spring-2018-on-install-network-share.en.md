---
author: polman
date: 2018-06-26 14:16:00
tags:
- software
cover:
  image: img/2018/labview.png
title: National Instruments LabVIEW Spring 2018 on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, the newly arrived
“Spring 2018” version has been copied to the
[Install](/en/howto/install-share/) network share. Install media can also
be borrowed. License codes can be obtained from C&CZ helpdesk or
postmaster.
