---
title: New version of Mathematica (13.3.0)
author: petervc
date: 2023-06-30
tags:
- medewerkers
- studenten
cover:
  image: img/2023/mathematica-13.png
---
A new version of [Mathematica](/en/howto/mathematica/) (13.3.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`.

When connected wired or wireless on campus or through [VPN](/en/howto/vpn/),
the software for Windows, Linux and macOS can be found on the
[C&CZ Install disc](https://install.science.ru.nl/science/Mathematica/).

During the installation you must specify:

     License server: mathematica.science.ru.nl
     License: L4601-6478

According to the [Mathematica Quick Revision
History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 13.3 introduces new functions that utilize large language models, as well as expands functionality for machine learning, mathematical computations, foreign function interface and more."

