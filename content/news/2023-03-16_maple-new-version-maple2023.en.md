---
title: 'Maple new version Maple2023'
author: petervc
date: 2023-03-16
tags:
- medewerkers
- studenten
cover:
  image: img/2023/maple.png
---
The latest version of [Maple](/en/howto/maple/), Maple2023, for
Windows/macOS/Linux can be found on the
[Install](/en/howto/install-share/) network share and has been installed
on C&CZ managed Linux computers. License codes can be requested from
C&CZ helpdesk or postmaster.
