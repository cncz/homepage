---
author: pberens
date: 2011-01-26 16:25:00
title: Informatie over storingen en onderhoud
---
Actuele storingen vindt u voortaan op de pagina
[Storingen](/nl/tags/storingen). Snel geïnformeerd worden? Gebruik de
[CPK mailinglist](http://mailman.science.ru.nl/mailman/listinfo/CPK) of
de [RSS feed](/nl/howto/storingen/). Het archief bevat [Recente Storingen](/nl/cpk/).
