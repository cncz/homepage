---
author: petervc
date: 2020-03-26 14:36:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2020'
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2020, is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf en is op door
C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen voor de deelnemende afdelingen.
