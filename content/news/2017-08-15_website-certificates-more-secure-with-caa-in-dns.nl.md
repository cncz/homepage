---
author: polman
date: 2017-08-15 13:24:00
tags:
- medewerkers
- docenten
- studenten
- dns
title: Certificaten van websites beter beveiligd door CAA in DNS
---
C&CZ heeft in alle DNS-zones [DNS CAA Resource
Records](https://tools.ietf.org/html/rfc6844) ingevoerd. Daardoor hebben
we per 8 september 2017 [meer controle over
SSL-certificaten](https://www.sslcertificaten.nl/support/Terminologie/CAA_DNS_Records)
voor onze domeinen. Een hack bij een willekeurige uitgever van
certificaten, [zoals die in 2011 bij
Diginotar](https://nl.wikipedia.org/wiki/Hack_bij_DigiNotar), kan dan
niet gebruikt worden om valse certificaten voor onze domeinen te maken,
omdat we in het CAA-record aangegeven hebben dat we alleen certificaten
uitgegeven door [DigiCert](https://www.digicert.com/) gebruiken.
