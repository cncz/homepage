---
title: Hardware reparatie
author: petervc
date: 2024-10-03
tags:
- medewerkers
- studenten
cover:
  image: img/2024/repair.jpg
---
Onlangs stond in het [Radboud nieuws](https://www.ru.nl/studenten/nieuws/vanaf-1-oktober-gratis-kleine-reparaties-aan-je-laptop-bij-de-rupair-shop)
dat studenten kleine reparaties aan hun laptop in de
[RUpair-shop](https://www.ru.nl/studenten/services/campusfaciliteiten-gebouwen/ict/hardware/rupair-shop-gratis-kleine-reparaties-van-je-laptop)
kunnen laten uitvoeren, waarbij geen arbeidskosten gerekend worden.

Voor studenten en medewerkers van FNWI levert de [C&CZ
helpdesk](https://cncz.science.ru.nl/nl/howto/contact/) een
vergelijkbare service. Vooral [Stefan
Reijgwart](https://www.radboudnet.nl/personen/reijgwart-s)
en [Dominic van den Broek](https://www.radboudnet.nl/personen/broek-d-r-van-den) zijn specialisten
in het repareren van o.a. laptops.
