---
author: fmelssen
date: 2017-02-22 00:13:00
tags:
- medewerkers
- studenten
title: Nieuw telefoonnummer 25225 (BLACK) voor beide Blackboard ondersteuners
---
Getriggerd door de komst van de nieuwe OWC-medewerker Maarten de Meijer,
die samen met Fred Melssen de ondersteuning voor Blackboard levert, is
besloten een nieuw telefoonnummer (+31 24 36) 25225 te gebruiken, dat
door beiden beantwoord kan worden. Het interne deel van het nummer is
eenvoudig te onthouden, omdat het [gelezen kan
worden](https://nl.wikipedia.org/wiki/Telefoonnummer#Gebruik_van_letters)
als BLACK.
