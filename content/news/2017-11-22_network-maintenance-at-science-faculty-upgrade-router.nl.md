---
author: mkup
date: 2017-11-22 10:03:00
tags:
- medewerkers
- docenten
title: 'Netwerkonderhoud bij FNWI: upgrade router'
---
Op maandagavond 27 november zal onderhoud plaatsvinden aan diverse
netwerkonderdelen van de RU. Tussen 23:30-02:00 uur zal de
distributierouter van Heyendaalnet-Oost wordt voorzien van nieuwe
firmware. Het vaste en draadloze netwerk zullen tussen genoemde
tijdstippen gedurende ca 30 minuten niet beschikbaar zijn. Ook de Mitel
telefoons zullen niet werken, de groene noodtelefoons blijven echter wel
functioneren. Denk s.v.p. na of deze verstoring voor u gevolgen heeft en
bereid u indien nodig voor.
