---
author: petervc
date: 2019-03-26 16:53:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2019'
---
The latest version of [Maple](/en/howto/maple/), Maple2019, can be found
on the [Install](/en/howto/install-share/) network share and has been
installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster by departments that take part
in the license.
