---
author: petervc
date: 2017-11-03 11:37:00
tags:
- studenten
- medewerkers
title: Euroglot 8.3C op Install-schijf
---
De nieuwste versie (8.3C) van de [Euroglot](http://www.euroglot.nl)
vertaalsoftware is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden.
Licentiecodes die geldig zijn tot 1 januari 2019 zijn via [de
helpdesk](/nl/howto/contact/) te verkrijgen. Thuisgebruik is volgens deze
licentie niet toegestaan. Voor thuisgebruik kan relatief goedkoop een
licentie gekocht worden bij [Surfspot](http://www.surfspot.nl).
