---
author: wim
date: 2020-09-24 09:00:00
tags:
- medewerkers
- docenten
- studenten
title: Microsoft Windows 10 upgrade
---
Microsoft Windows 10 upgrade available for C&CZ managed PCs in the
Software Center.

We urge everyone to upgrade managed PCs during the next few weeks. End
of October this upgrade will be made mandatory, due to the lifecycle
policy for [Windows 10
products](https://docs.microsoft.com/en-us/lifecycle/products/windows-10-enterprise-and-education).

For C&CZ managed PCs please [contact the C&CZ
helpdesk](/en/howto/contact/) for assistance in case of problems. For
other pcs, they can also offer assistance.
