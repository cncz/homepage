---
title: "Donation to free and open source software: Project Jupyter and Mozilla"
author: petervc
date: 2022-10-21
tags:
- medewerkers
- studenten
cover:
  image: img/2022/donation-to-free-and-open-source-software-project-jupyter-and-mozilla.png
---
For the majority of the services C&CZ uses [free
software](https://en.wikipedia.org/wiki/Free_software) and [open source
software](https://en.wikipedia.org/wiki/Open-source_software).
Therefore, some time ago C&CZ decided that each year the employees
vote which projects receive a donation from C&CZ. This
year [Project Jupyter](https://jupyter.org/about) was chosen as new recipient.
Project Jupyter is a non-profit, open-source project, born out of the IPython Project in 2014 as it
evolved to support interactive data science and scientific computing across all programming
languages. C&CZ makes use of it in the [JupyterHub](https://cncz.science.ru.nl/nl/howto/jupyterhub/) for FNWI courses.
As a second recipient [Mozilla Foundation](https://foundation.mozilla.org/) was
chosen, just like in 2021. Many people will know [the Mozilla
Firefox](https://en.wikipedia.org/wiki/Firefox) webbrowser and the
mailclient [Mozilla
Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird) and
maybe use it hours a day. But [the Mozilla Foundation does a lot
more](https://foundation.mozilla.org/en/what-we-do/) to try to ensure
the internet remains a public resource that is open and accessible to us
all.
