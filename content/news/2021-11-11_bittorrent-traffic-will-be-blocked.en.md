---
author: sioo
date: 2021-11-11 09:59:00
title: BitTorrent traffic will be blocked
cover:
  image: img/2021/bittorrent.png
---
Because [BitTorrent](https://en.wikipedia.org/wiki/BitTorrent) is
considered to be a security risk (malware/reputation damage) and the
traffic primarily consists of illegal downloads, the RU Security
Operations Center (SOC) proposed to block this type of traffic from the
RU network. Should this cause problems for research or education, we
would like to [hear from you](/en/howto/contact), so we can notify the SOC and perhaps
request an exception.
