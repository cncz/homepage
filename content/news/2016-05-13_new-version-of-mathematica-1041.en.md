---
author: petervc
date: 2016-05-13 13:54:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (10.4.1)
---
A new version of [Mathematica](/en/howto/mathematica/) (10.4.1) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows, Linux
and Apple Mac can be found on the
[Install](/en/howto/install-share/)-disk, also for older versions of
Mathematica. Departments that take part in the license can request
installation and license info from C&CZ.
