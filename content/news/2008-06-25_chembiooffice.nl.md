---
author: keesk
date: 2008-06-25 15:24:00
title: ChemBioOffice
---
Floris van Delft (afdeling Organische Chemie) heeft een beschrijving
gemaakt van de functionaliteit van het pakket
[ChemBioOffice](/nl/howto/chembiooffice/). Het pakket kan door iedereen
met een science-mailadres (studenten en staf) gratis gedownload en
geinstalleerd worden. Floris (F.vanDelft\@science.ru.nl) is bereid om
vragen over gebruik van het pakket te beantwoorden.
