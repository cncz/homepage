---
author: caspar
date: 2013-10-30 18:24:00
tags:
- medewerkers
- docenten
title: Information security and cloud services
---
If the use of cloud services such as Dropbox, Google+, Gmail, iCloud,
etc. is considered, it is vitally important to assess if the service can
be used safely, in adherence with legislation, and with the [RU
information security policy](/en/howto/informatiebeveiliging/).
Especially the storage of personal data (persoonsgegevens) is subject to
strict rules and regulations. When in doubt, C&CZ can try to give advice
on these issues.
