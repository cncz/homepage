---
author: petervc
date: 2008-04-03 19:00:00
title: Surfnet/Internetverbinding onderbroken maandag 7 april 18:00 uur-22:00 uur
---
Het [UCI](http://www.ru.nl/uci) zal a.s. maandagavond nogmaals proberen
de verbinding tussen de RU en SURFnet te upgraden van 1 Gb/s naar 10
Gb/s. Hierdoor zal tijdens deze werkzaamheden de verbinding tussen de RU
en SURFnet onderbroken zijn. Hierna zal C&CZ een upgrade van de FNWI
router naar 10 Gb/s plannen.
