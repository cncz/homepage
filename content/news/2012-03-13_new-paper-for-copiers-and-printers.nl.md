---
author: petervc
date: 2012-03-13 15:57:00
tags:
- studenten
- medewerkers
title: Nieuw papier voor printers en kopieerapparaten
---
Het nieuwe Oce BlacklabelZero papier dat uit de [Europese aanbesteding
van kopieerpapier](http://www.ru.nl/inkoop/nieuws/nieuws_0/europese-1/)
gekomen is, wordt sinds kort ook in de [C&CZ
printers](/nl/howto/printers-en-printen/), waaronder de [Ricoh
multifunctionals](/nl/howto/ricoh/) gebruikt. Het nieuwe papier is
witter, milieuvriendelijker en goedkoper. Het minder witte, Biotop
papier is nog leverbaar zolang er nog RU-briefpapier van dezelfde kleur
is.
