---
author: petervc
date: 2013-01-07 13:34:00
tags:
- studenten
- medewerkers
title: 'Euroglot: new license on Install network share'
---
The most recent license for [Euroglot](http://www.euroglot.nl), for 2013
, is available on the [Install](/en/howto/install-share/) network share.
