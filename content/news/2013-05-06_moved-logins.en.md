---
author: bram
date: 2013-05-06 17:35:00
tags:
- medewerkers
- docenten
title: Moved logins
---
A number of Science-logins has been migrated to more modern file
servers. You might be affected when connecting to your [home
directory](/en/howto/diskruimte#home-directories/) from a computer that’s
not managed by C&CZ, for example when using VPN at home. Please consult
[the Do-It-Yourself website](https://diy.science.ru.nl) for the current
path of your home directory.
