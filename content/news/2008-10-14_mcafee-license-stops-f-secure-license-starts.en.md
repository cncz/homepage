---
author: petervc
date: 2008-10-14 18:10:00
title: McAfee license stops, F-Secure license starts
---
The [RU ICT Overleg (RIO)](http://www.ru.nl/@750491/ru_ict-overleg/)
decided that the RU transitions from [McAfee](http://www.mcafee.com) to
[F-Secure](http://www.f-secure.com) for PC security before September 1,
2009. The F-Secure Client Security software has been placed on the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk. License codes can be requested from C&CZ.
