---
author: petervc
date: 2019-10-10 14:45:00
tags:
- studenten
- medewerkers
- docenten
- macos
title: 'Mac: wacht met upgrade naar macOS Catalina'
---
Sinds enkele dagen is macOS Catalina beschikbaar. Het is \*\*geen\*\*
goed idee om als een van de eersten te upgraden, dat zal problemen
opleveren. Tot nu toe is aan ons gemeld: Adobe software werkt niet,
F-Secure Client Security werkt niet, MestreNova werkt niet, EndNote
heeft een upgrade nodig naar 9.3.1 of hoger, Science VPNsec werkt pas na
herinstallatie VPN. Voor meer informatie over wat mis kan gaan, zie
[Hold Off
Catalina](https://www.zdnet.com/article/macos-catalina-hold-off-for-a-couple-of-weeks/)
en [Wait on Catalina](https://cdm.link/2019/10/wait-on-catalina/). Als
de upgrade al gedaan is, kunnen de problemen opgelost worden door een
oude versie macOS te installeren en de bestanden terug te zetten met
Time Machine.
