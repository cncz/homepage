---
author: polman
date: 2016-11-22 15:31:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2016 and TexWorks
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2016, is available for
MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share. For Windows PC’s this
has been installed on the S:-drive in the directory texlive/2016. The
graphical front-end [TeXworks](https://www.tug.org/texworks/) can be
found in texlive/2016/bin/win32/texworks.exe.
