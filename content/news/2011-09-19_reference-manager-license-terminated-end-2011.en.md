---
author: petervc
date: 2011-09-19 17:41:00
tags:
- medewerkers
title: Reference Manager license terminated end 2011
---
Many years the [UMCN](http://www.umcn.nl) has had a license for the use
of [Reference Manager](http://www.refman.com/) within RU and UMCN. Some
employees within the Faculty of Science used this, instead of the RU
standard [EndNote](http://www.endnote.com/). At the end of 2011, the
UMCN terminates the license for Reference Manager, it has switched to
EndNote. More information about the switch to EndNote can be found on
the [University Library
website](http://www.ru.nl/ubn/bibliotheeklocaties/medische/endnote/).
