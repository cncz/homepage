---
author: bram
date: 2011-10-07 16:35:00
tags:
- medewerkers
title: BASS en enkelvoudige logon
---
Om veiligheidsredenen was het in het verleden niet toegestaan om
rechtstreeks in te loggen in de concernapplicatie BASS, ook niet vanaf
het RU interne netwerk; een tussenstap via de Port Forwarder was nodig
hetgeen betekende dat er twee keer moest worden ingelogd. Met de
invoering van de centrale RU firewall is de tussenstap niet meer vereist
(maar nog wel mogelijk) voor PC’s die netwerk-technisch ‘achter’ de
firewall staan. Een belangrijk deel van de PC’s binnen FNWI staat
inmiddels achter de firewall. Op de [BASS](/nl/howto/bass/) pagina onder
*Systeemconfiguratie* is te zien wat de status is van uw PC en hoe de
configuratie moet worden aangepast om naar enkelvoudige logon over te
stappen.
