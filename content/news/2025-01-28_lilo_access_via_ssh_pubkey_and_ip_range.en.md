---
title: lilo access with managed SSH keys and ip range
author: sioo
date: 2025-01-28
tags: [lilo, ssh, medewerkers, studenten]
cover:
  image: img/2025/use_public_key_authentication_with_ssh.png
---

We have updated the authentication process for [login servers](/en/howto/hardware-servers/#linux-login-servers) after a succesful test we announced in [CPK#1383](https://cncz.science.ru.nl/en/cpk/1383/).

**As of now, your `~/.ssh/authorized_keys` file on lilo is ignored!**, instead you can send us your public ssh key (contents of the `.pub` file) to keep for your account. This change is designed to prevent bad actors from easily gaining persistent access. For convenience we have added the keys we have found in homedirectories that were added or modified after we restricted access to lilo, if your key is from before December 9th, you will need to send the public key to us.

We have also restricted access to the lilo's to a limited number of ip ranges. With the new authentication we feel confident to open the access to more ip addresses and ip ranges where you [tell us](/en/howto/contact) you are coming from (you can check this using [ip check](https://ip.science.ru.nl).

Without an ssh-public key, you can still use your plain login from within the Radboud network and EduVPN.

Generating a key pair can be done with:

``` shell
ssh-keygen -t ed25519 -C "your_email@ru.nl"
```

