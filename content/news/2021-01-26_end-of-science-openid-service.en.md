---
author: remcoa
date: 2021-01-26 17:39:00
tags:
- medewerkers
- studenten
title: End of Science OpenID service
---
The Science [OpenID-service](https://openid.science.ru.nl/), that was
introduced in 2007 as authentication method for the wikis managed by
C&CZ, will be terminated March 1, 2021. All wikis will be switched over
to a more modern authentication method via
[SAML](https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language).
