---
author: stefan
date: 2016-06-21 15:40:00
tags:
- medewerkers
- docenten
title: HP laptop accu terugroepactie
---
HP is een terugroepactie begonnen voor brandgevaarlijke laptopaccu’s die
tussen maart 2013 en augustus 2015 zijn verkocht. Mocht u in die tijd
een HP laptop of accu gekocht hebben, dan is het verstandig te
[controleren of de accu vervangen moet
worden](https://h30686.www3.hp.com/).
