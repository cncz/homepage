---
author: petervc
date: 2008-04-23 18:16:00
title: Network upgrade successful
---
Sunday morning April 20 between 09:30 and 11:30 hours the router/switch
that couples the networks on the Toernooiveld with each other and with
the rest of the RU campus network (and by that with Surfnet/Internet),
was replaced by a new, more powerful machine. The work was planned by
C&CZ en [UCI](http://www.ru.nl/uci) to cause minimal trouble for
employees and students.

The next few weeks other changes to the network will be made, but the
trouble this will cause will be much less.

Some aspects of this upgrade: 1 router/switch that is spread over 2
locations, with 10Gb uplinks to the UCI. The router/switch can provide
better protection since it has more room for “access control lists”and
by using “reverse path forwarding”. By using FlexLinks desktop switches
can easily be connected redundantly. Cascades of desktop switches can be
split, since more interfaces are available. Also antique connections to
remote locations can be upgraded.

About the new network (NiNe) the UCI reported in the [Enter
magazine](http://www.ru.nl/aspx/download.aspx?File=/contents/pages/12801/enter76_okt2007.pdf)
