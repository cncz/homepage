---
author: petervc
date: 2017-02-21 17:11:00
tags:
  - medewerkers
  - studenten
title: "Science mail: HTML in ZIP-file bijlagen geblokkeerd tegen ransomware"
---

Vanwege twee gevallen van [ransomware](https://www.politie.nl/themas/ransomware.html) waarbij bestanden versleuteld worden en deze alleen tegen betaling ontsleuteld
kunnen worden, hebben we besloten om in het Science mail virusfilter ZIP-bijlagen met HTML-inhoud te blokkeren. In zo’n geval vindt men de mail zonder bijlage in de Virus
submap van de Inbox. Overigens hebben we in beide gevallen de originele bestanden terug kunnen zetten van [backup](/nl/howto/backup/).
