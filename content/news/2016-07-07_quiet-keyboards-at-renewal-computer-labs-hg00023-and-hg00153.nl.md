---
author: john
date: 2016-07-07 13:36:00
tags:
- medewerkers
- docenten
- studenten
title: Stillere toetsenborden bij vernieuwing terminalkamers HG00.023 en HG00.153
---
In de zomervakantie worden alle pc’s uit de
[Terminalkamers](/nl/howto/terminalkamers/) TK023 (HG00.023) en TK153
(HG00.153) vervangen door PC’s van het type [Dell OptiPlex 7440
AIO](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd) met
24-inch Full-HD scherm, een Core i5 6500 (3.2GHz,QC) processor, 1x8GB
geheugen en 512GB SSD harde schijf. Om geluidsoverlast, speciaal bij bv.
tentamens te verminderen is gekozen voor de vrij stille [Cherry Stream
3.0
toetsenborden](https://www.techpowerup.com/216759/cherry-announces-the-stream-3-0-keyboard).
Ook voor andere plaatsen waar veel toetsenborden in dezelfde ruimte
gebruikt worden, zal dit ook een goede keus zijn.
