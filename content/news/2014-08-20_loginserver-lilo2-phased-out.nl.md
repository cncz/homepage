---
author: visser
date: 2014-08-20 15:38:00
tags:
- medewerkers
- studenten
title: Loginserver lilo2 uitgefaseerd
---
De vijf jaar oude Linux loginserver lilo2 (Ubuntu 10.04) zal op korte
termijn uitgezet worden. Alle gebruikers worden verzocht om over te
stappen naar lilo3 (Ubuntu 12.04) of de onlangs aangekondigde lilo4
(Ubuntu 14.04). De namen `lilo` en `stitch` wijzen altijd naar de twee
door C&CZ aangeraden [Linux
loginservers](/nl/howto/hardware-servers#login-servers/).
