---
author: petervc
date: 2017-09-22 15:58:00
tags:
- medewerkers
- docenten
title: 'MS Windows 10: newer versions on Install network share'
---
Newer versions of [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, are available on
the [Install](/en/howto/install-share/) network share. The license
permits upgrade to these version on university computers. License codes
can be requested from C&CZ helpdesk or postmaster. For privately owned
computers you can order this software relatively cheap at
[Surfspot](http://www.surfspot.nl). The added versions are: 2016 LTSB,
1607 with Multilanguage Pack, 1703 and Features on Demand.
