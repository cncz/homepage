---
author: wim
date: 2017-11-23 10:42:00
tags:
- medewerkers
- studenten
title: Linux printing to Peage now simple
---
Initially as a test, there is now a much simpler alternative to the [SMB
printing from Linux](/en/howto/peage/). A printer “Peage” now is
available on the Linux workstations and servers managed by C&CZ. The
C&CZ print server translates the Science username to the U- or S-number
and sends the print job onwards to a RU payprint server. Command line
printing with the command `lpr` is also possible
(`lpoptions -pPeage -o SelectColor=Color` to make color prints by
default).
