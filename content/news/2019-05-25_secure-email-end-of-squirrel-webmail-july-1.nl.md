---
author: visser
date: 2019-05-25 23:59:00
tags:
- studenten
- medewerkers
- docenten
title: 'Veilige Email: Squirrel webmail stopt 1 juli'
---
Omdat [Squirrel webmail](https://squirrel.science.ru.nl) het gewenste
beveiligingsniveau dat we met
[STARTTLS](https://nl.wikipedia.org/wiki/STARTTLS) willen bereiken niet
aan kan, wordt Squirrel per 1 juli uitgefaseerd. Gebruikers wordt
geadviseerd om over te stappen op een van de modernere [Science
webmailsystemen](https://wiki.cncz.science.ru.nl/Categorie:Email).
