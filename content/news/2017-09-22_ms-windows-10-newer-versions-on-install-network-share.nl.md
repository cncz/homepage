---
author: petervc
date: 2017-09-22 15:58:00
tags:
- medewerkers
- docenten
title: 'MS Windows 10: nieuwere versies op Install-schijf'
---
Nieuwere versies van [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, zijn op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan upgrade naar deze versies op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden. De toegevoegde
versies zijn: 2016 LTSB, 1607 met Multilanguage Pack, 1703 en Features
on Demand.
