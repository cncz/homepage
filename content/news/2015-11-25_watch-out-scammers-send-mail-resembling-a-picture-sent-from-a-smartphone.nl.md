---
author: visser
date: 2015-11-25 00:00:00
tags:
- medewerkers
- docenten
- studenten
title: Let op\! Oplichters versturen e-mail alsof het een foto vanaf een smartphone
  is\!
---
Begin deze week ontvingen veel medewerkers een mail met als onderwerp
“img”, als volledige inhoud “Sent from my Lenovo” en als [bijlage
malware met een naam als “IMG\_0112201135\_2015
JPEG.cab”](http://www.myce.com/news/malware-spreads-by-cab-e-mail-attachments-to-evade-ziprar-filters-74635/).
Een “.cab”-bestand is een gecomprimeerd archief, vergelijkbaar met een
zip-bestand. Omdat men vaak op Windows PC’s de extensie van bestanden
niet ziet, lijkt dit dan een plaatje (JPEG), terwijl het een
“….JPEG.exe” is. Dubbelklikken op het namaakplaatje infecteert dan de
computer door het uitvoeren van de “.exe”. Daarom heeft C&CZ in het
[MIMEDefang filter](http://www.mimedefang.org/) op de [Science
mailserver](/nl/tags/email) ingesteld dat “.cab”-bijlagen verwijderd
worden uit mails, zoals al veel langer gebeurt met andere gevaarlijke
extensies als “.exe”. Bij een gebruiker die in deze malware trapte,
werden allerlei bestanden versleuteld en kreeg de gebruiker een melding
over het betalen van losgeld om de bestanden te ontsleutelen. Dit kon
hersteld worden door de PC te herinstalleren en een
[backup](/nl/howto/backup/) terug te zetten van voor de besmetting. Pas
vandaag wordt deze malware ook door F-Secure herkend. Tip: stel in
Windows in [bestandsextensies niet te
verbergen](http://windows.microsoft.com/nl-nl/windows/show-hide-file-name-extensions#show-hide-file-name-extensions=windows-7).

16:22
