---
title: Network maintenance Huygens, NMR, Nanolab, HFML-FELIX January-April 2023
author: petervc
date: 2023-01-24
tags:
- medewerkers
- studenten
cover:
  image: img/2023/network-maintenance-huygens-nmr-nanolab-hfml-felix-january-april-2023.png
---
RU ILS Connectivity is replacing network access switches in FNWI. This
causes short disturbances of network connectivity for wired and
wireless systems on several evenings between 19:00 and 23:00 hours
for affected wings/locations, i.e. connected to a specific SER
(Satellite Equipment Room).  For wired network outlets, one can
check whether they will be affected by looking at the outlet number
(starting with the SER number for affected systems) or visiting [FNWI
ethergids](https://cncz.science.ru.nl/en/howto/netwerk-ethergids/).

Global schedule:
- Monday Jan 30: SER 105 (in and around wing 1 Huygens, all floors).
- Wednesday Feb 1: SER 106 (in and around wing 2 Huygens, all floors).
- Mid-February: SER 111.
- Late February: brief interruption in the evening for networks in Huygens, NMR, Nanolab, HFML-FELIX.
- March: SER 109, 73, 72, 116, 101 and 135.
- Early April: SER 107, 110 and 113.
