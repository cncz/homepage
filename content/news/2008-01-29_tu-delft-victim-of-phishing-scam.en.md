---
author: petervc
date: 2008-01-29 12:24:00
title: TU Delft victim of phishing scam
---
The [CERT-RU](http://www.ru.nl/cert), that coordinates security
incidents within the RU, reported that employees and students of the TU
Delft had received emails, that at first sight looked as if they were
sent by TU Delft, in which was asked for a reply with loginname and
password. The reply of a naive user went to a “@hotmail.co.uk”
mail-address. C&CZ likes to stress that \*no official institution\* will
ever ask you for your password or pincode for your bank account. For the
original message of the CERT of TU Delft, see

[TU Delft Security
Alert](http://www.tudelft.nl/live/pagina.jsp?id=17d7e6d4-a6ae-475a-8a63-70d0ab4629a1&lang=nl).
