---
author: bertw
date: 2017-10-23 11:32:00
tags:
- medewerkers
- studenten
title: Groene telefoontoestellen kwartier buiten gebruik op 22 november
---
De groene noodtelefoontoestellen zullen op woensdag 22 november tussen
11.30 uur tot 12.00 uur een kwartier buiten bedrijf zijn. In die periode
worden [T38 (Fax over IP)](https://en.wikipedia.org/wiki/T.38) licenties
geïnstalleren op de calamiteitenservers. Alle andere toestellen hebben
geen last van dit onderhoud en blijven dus gewoon in bedrijf.
