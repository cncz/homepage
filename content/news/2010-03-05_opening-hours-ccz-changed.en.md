---
author: petervc
date: 2010-03-05 17:50:00
title: Opening hours C&CZ changed
---
C&CZ can’t maintain the [opening hours](/en/howto/helpdesk/) that were
extended in 2008, our new opening hours are from 08:30 to after 18:00.
