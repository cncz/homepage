---
author: bbellink
date: 2022-02-23 16:49:00
tags:
- medewerkers
- studenten
title: Survey computer labs and BYOD
---
FNWI asks lecturers and students to fill in a [survey about the computer
labs](https://u1.survey.science.ru.nl/index.php/735564?lang=en). The
reason is that the Faculty of Science plans to remodel its teaching
rooms in a multifunctional way, with computer labs that use [‘Bring Your
Own Device’
(BYOD)](https://en.wikipedia.org/wiki/Bring_your_own_device). This means
that the existing computer labs (terminal rooms, TK’s) in the Huygens
building will be phased out in the coming years. In order to plan the
further implementation, we would like to [request your input on the
current use of the computer labs by means of a
survey](https://u1.survey.science.ru.nl/index.php/735564?lang=en) in
order to determine the pace and which alternatives may be needed to
replace the various current uses of the current computer labs. The
project group will advise the Faculty Board based on this survey and the
data from the actual use of the rooms and equipment (laptops and PC’s).
