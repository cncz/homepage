---
author: sioo
date: 2021-10-01 15:40:00
tags:
- medewerkers
- docenten
- studenten
title: BigBlueButton videoconferencing updated
---
The experimental BigBlueButton videoconferencing website has been moved
and updated. The new URL is <https://bigbluebutton.science.ru.nl/> , you
can create your own login, or use the one you had on
<https://bigbb.science.ru.nl>, which is being redirected to the new
site. The status remains “experimental”; without warranty.
