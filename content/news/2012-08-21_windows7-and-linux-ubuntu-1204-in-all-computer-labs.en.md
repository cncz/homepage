---
author: petervc
date: 2012-08-21 18:15:00
tags:
- studenten
- medewerkers
- docenten
title: Windows7 and Linux Ubuntu 12.04 in all computer labs
---
During the summer break, C&CZ has reinstalled all [pc’s in computer
labs](/en/howto/terminalkamers/) (including college and colloquium
rooms), with new versions of Windows (7) and Ubuntu Linux (12.04).
Existing software (for Windows on the S: and T: discs) is available, but
has not always been tested. For Windows7, there is a [list of installed
software](/en/howto/windows-beheerde-werkplek/).

Please report all problems a.s.a.p. to C&CZ, e.g. by mailing to
postmaster\@science.ru.nl, to give us time to solve problems.
