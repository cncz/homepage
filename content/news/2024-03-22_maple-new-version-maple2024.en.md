---
title: Maple new version Maple2024
author: petervc
date: 2024-03-22
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/maple.png
---
The latest version of [Maple](/en/howto/maple/), Maple2024, for
Windows/macOS/Linux can be found on the
[Install](/en/howto/install-share/) network share and has been installed
on C&CZ managed Linux computers. License codes can be requested from
C&CZ helpdesk or postmaster.

