---
author: petervc
date: 2017-08-08 14:25:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Tuesday August 29'
---
The C&CZ yearly day out is scheduled for Tuesday August 29. C&CZ can be
reached in case of serious disruptions of services.
