---
author: petervc
date: 2013-02-14 10:52:00
tags:
- studenten
- medewerkers
- docenten
title: 'Phishing mails: "Belangrijke boodschap met betrekking tot uw Radboud Universiteit
  Webmail" en "Fwd: Your e-mail will expire soon"'
---
Diverse medewerkers van FNWI hebben de afgelopen dagen een phishing mail
ontvangen met als onderwerp “Belangrijke boodschap met betrekking tot uw
Radboud Universiteit Webmail” en als inhoud “Nieuw Belangrijk Veiligheid
Bericht Alert! Inloggen om het probleem op te lossen.” Anderen kregen
phishing mails met als onderwerp “Fwd: Your e-mail will expire soon”. De
link die daarbij gegeven wordt, wijst naar een kopie van onze Horde
webmail. Grote verschillen zijn:

-   de URL is niet binnen science.ru.nl
-   het is geen veilige https verbinding, het slotje ontbreekt
-   de ingevoerde loginnaam en wachtwoord komen niet uit bij C&CZ
    servers, maar bij Internet-criminelen. Die hebben dan toegang tot
    alle bestanden en mail van dat Science-account. Meestal zal men met
    de logingegevens proberen om spam te versturen via onze
    mailservers.Dit valt op; C&CZ zal in zo’n geval de gecompromitteerde
    login z.s.m. uitzetten, om verder misbruik te voorkomen en te
    voorkomen dat onze mailservers op zwarte lijsten komen.

Het is niet de eerste keer dat we een phishing aanval zien die specifiek
op FNWI-gebruikers gericht is, het zal ook niet de laatste keer zijn.
Blijf a.u.b. alert! Voor wie meer wil lezen over phishing is [deze
maandelijkse
beveiligingsnieuwsbrief](http://www.securingthehuman.org/newsletters/ouch/issues/OUCH-201302_nl.pdf)
vast erg interessant.
