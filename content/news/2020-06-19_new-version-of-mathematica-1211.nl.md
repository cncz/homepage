---
author: petervc
date: 2020-06-19 18:04:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van Mathematica (12.1.1)
---
Er is een nieuwe versie (12.1.1) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden. De installatiebestanden voor Windows, Linux en Mac zijn op de
[Install](/nl/howto/install-share/)-schijf te vinden, ook voor oudere
versies. Afdelingen die meebetalen aan de licentie kunnen bij C&CZ de
licentie- en installatie-informatie krijgen.
