---
author: visser
date: 2017-07-18 15:41:00
tags:
- medewerkers
title: 'Sustainability: automatically switching off computers'
---
If a department wants to switch off the C&CZ managed computers that are
not currently in use after working hours and/or lunch break, then C&CZ
can automate that. Please [contact Postmaster](/en/howto/contact/). Since
years already, the pc’s in the [computer labs, study landscape and
Library of Science](/en/howto/terminalkamers/) are being switched off
after working hours and a few times per day when not in use. This
mechanism is also used to automatically install the Windows and Ubuntu
Linux updates during the night.
