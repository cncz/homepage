---
author: wim
date: 2015-03-24 15:14:00
tags:
- medewerkers
- studenten
title: Renewal Computer lab HG03.761
---
All PCs in [computer lab](/en/howto/terminalkamers/) HG03.761 have been
replaced. There are 18 student PCs available, type [HP EliteOne 800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, i5-4570S Core, 2.9GHz, 8GB, 23 inch widescreen LCD monitor,
sound), dual boot with Windows and Linux (Ubuntu). This computer lab
should be available as of 25-03-2015.
