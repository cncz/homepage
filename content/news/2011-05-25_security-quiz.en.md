---
author: petervc
date: 2011-05-25 18:07:00
tags:
- studenten
- medewerkers
title: Security Quiz
---
If you want to know how smart you are w.r.t. IT-security, the [Security
Quiz](http://securityquiz.wlu.ca/) of Canadian Wilfrid Laurier
University can help. Can you get a 100% score there? A [quiz with direct
feedback](http://security.gmu.edu/quiz/question1.html) can be found at
George Mason University, USA.
