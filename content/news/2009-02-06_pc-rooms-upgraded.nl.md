---
author: theon
date: 2009-02-06 11:06:00
title: PC-zalen opgewaardeerd
---
In de afgelopen weken hebben vrijwel alle oudere
[PC-zalen](/nl/howto/terminalkamers/) een upgrade gehad. Die PCs hebben
nu minimaal 1 GB geheugen en een [verbeterde MS Windows (WDS)
installatie](/nl/howto/windows-beheerde-werkplek/). Uitzonderingen zijn
de hiervoor te oude PCs in TK702 (TK-GIS, HG02.702) en de
zelfstudiekamertjes (HG00.2xx).
