---
author: bram
date: 2018-04-23 13:34:00
tags:
- medewerkers
- studenten
title: 'EveryOnePrint: Peage printing through app, web upload or mail'
---
Recently the ISC made
[EveryOnePrint](http://www.ru.nl/fb/english/print/printing-from-smartphone-tablet-etc-everyoneprint/)
available. With this, you can print from smartphone, tablet and every
Internet-device to [Peage](/en/howto/peage/), through web upload, app or
mail. Unluckily enough, the mail option was chosen to work only with
@ru.nl mail addresses, not with @science.ru.nl or other addresses.
