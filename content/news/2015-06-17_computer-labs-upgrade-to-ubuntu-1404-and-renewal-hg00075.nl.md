---
author: wim
date: 2015-06-17 09:01:00
tags:
- medewerkers
- docenten
- studenten
title: 'Terminalkamers: upgrade naar Ubuntu 14.04 en vernieuwing HG00.075'
---
In de zomervakantie worden alle pc’s uit de
[Terminalkamers](/nl/howto/terminalkamers/) met Ubuntu 14.04 LTS
ge(her)installeerd. Cursus-software kan al lang getest worden op de
Ubuntu 14.04 [loginserver](/nl/howto/hardware-servers/) lilo4. Alle PC’s
in TK075 (HG00.075) worden in die periode vervangen door PC’s van het
type [HP EliteOne 800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, Core i5-4570S, 2.9GHz, 8GB, 23-inch widescreen LCD monitor,
sound), dual boot met Windows and Linux (Ubuntu).
