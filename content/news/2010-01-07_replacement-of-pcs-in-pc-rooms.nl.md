---
author: theon
date: 2010-01-07 16:07:00
title: Vervanging van pc's in pc-zalen
---
In de derde week van januari zullen 122 pc’s in
[pc-zalen](/nl/howto/terminalkamers/) TK149/TK153, biologie practicum
TK329 en het studielandschap worden vervangen door nieuwe. De overlast
voor studenten zal minimaal zijn in deze week. Afdelingen die interesse
hebben om een aantal van deze meer dan 5 jaar oude pc’s over te nemen,
kunnen contact opnemen met C&CZ (Theo Neuij, 56666).
