---
author: petervc
date: 2008-06-16 16:59:00
title: 'Matlab: TIMEOUTALL ingevoerd, installatieprocedure gewijzigd'
---
Op voorstel van grootgebruiker [FC Donders](http://www.ru.nl/fcdonders)
is op de door C&CZ beheerde [Matlab](/nl/howto/matlab/) licentie server
de optie ‘TIMEOUTALL 3600’ aangezet. Dat heeft het voordeel dat een
inactieve gebruiker een licentie van Matlab of een toolbox hooguit 1 uur
vasthoudt, maar zou voor de vele tientallen gelijktijdige Matlab
gebruikers binnen Radboud Universiteit en Ziekenhuis ook enige overlast
kunnen geven, omdat na 1 uur inactiviteit de licentie vrijgegeven wordt.
In principe kan het dus voorkomen dat een lange tijd lopende Matlab job
stopt omdat er op dat moment even geen licentie beschikbaar is.

Met Matlab versie R2008a is ook de [Matlab
installatieprocedure](/nl/howto/matlab/) gewijzigd.
