---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: 'Onderwijs in beeld: tips voor docenten'
---
-   Als uw college opgenomen wordt denk er dan a.u.b. aan dat u vragen
    van studenten herhaalt, de microfoon staat op u afgesteld.
-   Uw Powerpoint-presentatie wordt gescand met OCR software, zodat
    studenten op tekst kunnen zoeken in het Flash-filmpje. OCR werkt het
    best als een sheet met tekst begint, niet als het met een plaatje
    begint.
