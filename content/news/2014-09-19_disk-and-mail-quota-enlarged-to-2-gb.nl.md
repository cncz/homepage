---
author: bram
date: 2014-09-19 12:39:00
tags:
- medewerkers
- studenten
title: Disk- en mailquota verhoogd tot 2 GB
---
De [quota](/nl/howto/quota-bekijken/) voor de Science [home directory /
U:-schijf](/nl/howto/diskruimte/) zijn verhoogd tot 2 GB voor alle
gebruikers die een lager quotum hadden. Ook de Science mailquota zijn
tot 2 GB opgehoogd voor iedereen met een lager quotum. Wie meer quota
nodig heeft, kan dat altijd aanvragen via een mail naar postmaster. Deze
[schijfruimte](/nl/howto/diskruimte/) op de
[C&CZ-servers](/nl/howto/hardware-servers/) wordt
[gebackupt](/nl/howto/backup/).
