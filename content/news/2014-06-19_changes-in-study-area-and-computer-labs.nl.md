---
author: john
date: 2014-06-19 13:59:00
tags:
- medewerkers
- studenten
title: Aanpassing studielandschap en terminalkamers
---
In de zomervakantie zullen na een verbouwing 28 nieuwe All-in-one (AIO)
PC’s bijgeplaatst worden in het
[studielandschap](/nl/howto/terminalkamers/). Terminalkamer TK702,
(HG02.702) wordt in deze periode opgeheven.
