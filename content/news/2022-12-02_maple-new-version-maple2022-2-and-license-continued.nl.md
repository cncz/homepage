---
title: 'Maple nieuwe versie: Maple2022.2 en licentie verlengd'
author: petervc
date: 2022-12-02
tags:
- medewerkers
- studenten
cover:
  image: img/2022/maple.png
---
De door C&CZ beheerde “vier gelijktijdige gebruikers”-licentie van Maple
is verlengd tot 31 december 2023. De nieuwste versie van
[Maple](/nl/howto/maple/), Maple2022.2, is voor Windows/macOS/Linux te
vinden op de [Install](/nl/howto/install-share/)-netwerkschijf en is op
door C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij
C&CZ helpdesk of postmaster te verkrijgen.
