---
author: mkup
date: 2013-09-03 14:31:00
tags:
- studenten
- medewerkers
- docenten
title: Eduroam draadloos overal, Science draadloos gestopt
---
Overal in de FNWI-gebouwen is nu het [draadloze netwerk
Eduroam](/nl/howto/netwerk-draadloos/) beschikbaar. Zie [de
website](http://www.ru.nl/draadloos) voor alle informatie. Hiermee is
het Science draadloze netwerk verdwenen. Over enige tijd zal ook ru-wlan
verdwijnen, dus schakel svp zo snel mogelijk over op Eduroam.
