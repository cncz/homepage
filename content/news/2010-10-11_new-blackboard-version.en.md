---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: New Blackboard version
---
There is a new version of Blackboard. Furthermore the Blackboard page in the C&CZ wiki was renewed and there is a new Blackboard maintainer for the Faculty.
