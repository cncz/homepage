---
author: petervc
date: 2009-02-06 13:15:00
title: Software licenties voor medewerkers en studenten van de RU
---
Omdat er veel softwarelicenties zijn, waarbij de voorwaarden vaak
verschillen voor gebruik door medewerkers, studenten, op de campus en
thuis, heeft het [UCI](http://www.ru.nl/uci) een [nieuwsartikel over het
bestellen van
software](http://www.ru.nl/uci/actueel/nieuws/software_bestellen/)
gemaakt. Veel informatie over (licentie)software is ook te vinden op de
[C&CZ softwarepagina](/nl/tags/software), bv over [software voor
computers met Microsoft Windows](/nl/howto/microsoft-windows/).
