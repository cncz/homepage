---
author: petervc
date: 2018-12-18 15:20:00
tags:
- medewerkers
- studenten
title: Intel compilers nieuwe versie
---
C&CZ heeft samen met TCM en Theoretische Chemie twee licenties voor
gelijktijdig gebruik van de nieuwste versie van de [Intel Parallel
Studio XE voor
Linux](https://software.intel.com/en-us/parallel-studio-xe) aangeschaft.
Deze is geïnstalleerd in `/vol/opt/intelcompilers` en beschikbaar op
o.a. [clusternodes](/nl/howto/hardware-servers/) en
[loginservers](/nl/howto/hardware-servers/). Ook de oude (2014) versie is
daar te vinden. Om de omgevingsvariabelen goed te zetten, moeten
BASH-gebruikers vooraf uitvoeren:

source /vol/opt/intelcompilers/intel-2019/composerxe/bin/compilervars.sh
intel64

Daarna levert `icc -V` het versienummer. Voor meer info zie [de pagina
over de
Intel-compilers](https://wiki.cncz.science.ru.nl/index.php?title=Intel_compilers&setlang=nl).
