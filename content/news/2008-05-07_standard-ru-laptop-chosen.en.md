---
author: polman
date: 2008-05-07 17:24:00
title: Standard RU-laptop chosen
---
Recently the RU hardware specification team has added to the standard PC
from the “core hardware” from the [default
supplier](/en/howto/huisleverancier-pcs/) also a standard RU-laptop: the
HP6710b. There are 2 versions, the more expensive one has at least a
faster processor, a bigger hard disk a screen with higher resolution and
standard more memory. C&CZ currently works on making it possible to use
these laptops as “Windows managed laptop”, comparable to the
[[Windows\_beheerde\_werkplek\|Windows managed PC].
