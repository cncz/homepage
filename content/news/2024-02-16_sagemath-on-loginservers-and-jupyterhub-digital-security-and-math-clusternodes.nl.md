---
title: SageMath op loginservers en JupyterHub, Digital Security and Math clusternodes
author: petervc
date: 2024-02-16
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/sagemath.jpg
---
Het open-source wiskundesoftwaresysteem [SageMath](https://www.sagemath.org/)
versie 9.5 is geïnstalleerd op de Science [loginservers](https://cncz.science.ru.nl/nl/howto/hardware-servers/#linux-login-servers) en de
[clusternodes](https://cncz.science.ru.nl/nl/howto/hardware-servers/#compute-serversclusters) van [JupyterHub](https://cncz.science.ru.nl/nl/howto/jupyterhub/), Digital Security en Wiskunde.

De missie van SageMath is het creëren van een levensvatbaar gratis open source alternatief voor Magma, Maple, Mathematica en Matlab.

SageMath bouwt voort op veel bestaande open-sourcepakketten: NumPy, SciPy,
matplotlib, Sympy, Maxima, GAP, FLINT, R en nog veel meer. Gebruik hun
gecombineerde kracht via een gemeenschappelijke, op Python gebaseerde
taal of rechtstreeks via interfaces of wrappers.
