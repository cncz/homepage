---
author: stefan
date: 2014-05-16 12:15:00
tags:
- medewerkers
- studenten
title: Posters on canvas (for hand luggage)
---
It is now possible to print on canvas on the poster printer
“[Kamerbreed](/en/howto/kamerbreed/)”. Canvas prints can be folded to fit
in the hand luggage. This can be an advantage over a poster print on
photographic paper, to be carried in a large tube. The price for
printing on canvas is 36 € per A0.
