---
title: Korte enquête over de dienstverlening van C&CZ
author: petervc
date: 2023-12-13
draft: false
tags:
- medewerkers
- studenten
cover:
   image: img/2023/survey-cncz-nl.png
---
Graag willen we je aandacht vragen voor [een korte enquête met betrekking tot de dienstverlening van C&CZ](https://u1.survey.science.ru.nl/index.php/515387?lang=nl).

We zouden het zeer waarderen als je een paar minuten de tijd neemt om ons te voorzien van feedback. 
Op basis van de resultaten gaan we bepalen hoe we onze diensten voor de faculteit verder kunnen verbeteren de komende jaren.

Alvast hartelijk dank voor je reactie!

{{< notice note >}}
De enquete is inmiddels gesloten. Heel erg bedankt voor de waardevolle feedback!
{{< /notice >}}
