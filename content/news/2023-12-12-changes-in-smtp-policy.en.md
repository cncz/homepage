---
title: Policy changes for outgoing email (SMTP)
author: petervc
date: 2023-12-12
tags:
- medewerkers
- studenten
cover:
  image: img/2023/authenticated-smtp.png
---
To counteract email spoofing, the following changes are forthcoming, affecting the way email can be sent.

## Science SMTP authentication requirement from specific networks
Currently, it is possible to send emails via SMTP _without_ authentication when connected to the campus network. For user devices, it has always been recommended to use [authenticated SMTP](../../howto/email/#outgoing-mail). Starting from January 8, 2024, C&CZ will no longer allow unauthenticated SMTP from user networks. The first networks for which this policy will be enforced will be [Eduroam](../../howto/netwerk-draadloos/) and [VPN](../..//howto/vpn/).

## Sending Emails via Science SMTP no longer Allowed for `@ru.nl` Sender Addresses
From January 8, 2024, C&CZ will contact employees using `smtp.science.ru.nl` and sending emails with a sender address ending in `@ru.nl`. They will be requested to modify their SMTP settings, either use their Science mail address or use [the central RU mail service](https://www.ru.nl/en/services/campus-facilities/work-and-study-support-services/ict/email-and-calendar). The intention is to block @ru.nl sender addresses on the Science SMTP service from February 1, 2024.
