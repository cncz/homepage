---
author: bram
date: 2020-03-15 12:51:00
tags:
- medewerkers
- docenten
title: Faciliteiten voor thuiswerken
---
Op de [thuiswerkpagina](/nl/howto/thuiswerken/) hebben we een opsomming
gemaakt van ICT-faciliteiten die gebruikt kunnen worden bij het werken
vanuit huis.
