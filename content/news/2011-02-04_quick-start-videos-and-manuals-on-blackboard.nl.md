---
author: pberens
date: 2011-02-04 13:44:00
tags:
- docenten
title: Snel op weg met Blackboard video's en handleidingen
---
In Blackboard,
voor elk wat wils: beginners en gevorderden. Manfred te Grotenhuis,
docent bij FSW, runt een Blackboard organization speciaal over onderwerp
Digitaal Toetsen met Blackboard 9.1. Tot uiterlijk 18 februari 2011
kunnen docenten zich aanmelden bij bb-nwi@science.ru.nl o.v.v. hun
u-nummer om lid te worden van deze organization. Echt een aanrader!
