---
author: petervc
date: 2011-12-16 17:26:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2011
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2011, geschikt voor
MS-Windows, Mac OS X en Linux/Unix, is beschikbaar. De DVD is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf, maar is ook [te
leen](/nl/howto/microsoft-windows/).
