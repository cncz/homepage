---
author: polman
date: 2014-11-13 12:03:00
tags:
- studenten
- medewerkers
- docenten
title: Uitfaseren SHA-1 certificaten
---
Alle [SHA-1](http://nl.wikipedia.org/wiki/SHA-familie) certificaten van
door C&CZ beheerde servers worden op korte termijn [vervangen door
nieuwe SHA-2
certificaten](https://www.digicert.com/transitioning-to-sha-2.htm). Dat
is nodig omdat SHA-1 niet meer als [landurig
veilig](https://www.schneier.com/blog/archives/2012/10/when_will_we_se.html)
beschouwd wordt. De [Chrome browser zal al in November 2014 een
waarschuwing
geven](http://googleonlinesecurity.blogspot.nl/2014/09/gradually-sunsetting-sha-1.html)
bij een website met een SHA-1 certificaat, andere browsers [volgen
later](https://www.digicert.com/sha-2-ssl-certificates.htm).
