---
author: mkup
date: 2013-11-26 09:02:00
tags:
- studenten
- medewerkers
- docenten
title: Open inprikpunten met authenticatie in Library of Science en op insteekverdieping
---
Sinds enige tijd zijn in de leeszaal van de bibliotheek en op de
insteekverdieping boven het restaurant respectievelijk 32 en 64
geauthenticeerde netwerk-inprikpunten beschikbaar met een snelheid van
100 Mb/s. Sluit een computer aan met een (eigen) UTP-kabel en start een
browser. Dan komt men terecht op een website waar men zich kan
registreren met het personeelsnummer (`U123456`) of studentnummer
(`s123456`) of `scienceloginnaam@science.ru.nl`. Daarna heeft men
toegang tot Internet. De bedoeling is om in de toekomst alle openbaar
toegankelijke actieve inprikpunten van authenticatie te voorzien. De
techniek hierachter is Qmanage van
[Quarantainenet](http://www.quarantainenet.com/?language=nl;page=main-home).
