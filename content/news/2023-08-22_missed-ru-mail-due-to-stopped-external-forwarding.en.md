---
title: Missed RU mail due to stopped external forwarding
author: petervc
date: 2023-08-22
tags:
- medewerkers
- studenten
cover:
  image: img/2023/missed-mail.jpg
---
RU mail management let us know that yesterday the forwarding to external (non-RU) mail addresses has been stopped as announced earlier. Unfortunately, mail for several dozens of Science users was/is not forwarded to the Science mailservers. These mails can still be found in MS365 (RU mail), either in the Inbox or in the Deleted Items. RU mail management promised that the forwarding will be corrected tomorrow.

Automatic forwarding from Science mail by employees and students to
external addresses has been stopped on August 22.

Also see [cpk/1341](/en/cpk/1341).
