---
author: petervc
date: 2012-09-25 16:16:00
tags:
- studenten
- medewerkers
- docenten
title: End of life of transparency printer nicolas
---
Year after year the inkjet transparency
[printer](/en/howto/printers-en-printen/) nicolas was used to produce
good-looking color transparencies. Because the printer shows a hardware
problem and is used only very seldom, we decided to remove the printer.
If one still needs to print transparancies, one can use \*laserprinter
compatible\* transparancies in the
[laserprinters](/en/howto/printers-en-printen/).
