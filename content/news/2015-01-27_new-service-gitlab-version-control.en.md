---
author: bram
date: 2015-01-27 16:51:00
tags:
- studenten
- medewerkers
title: 'New service: GitLab version control'
---
[GitLab](/en/howto/gitlab/) is available as a successor to our
[Subversion service](/en/howto/subversion/).
[GitLab](https://about.gitlab.com/features/) is an open source code
collaboration platform, git repository manager, issue tracker and code
reviewer. Students and employees of the faculty can register on
<https://gitlab.science.ru.nl> with their Science login. It is possible
to transfer projects from svn to GitLab.
