---
author: petervc
date: 2013-07-02 12:05:00
tags:
- studenten
- medewerkers
- docenten
title: Mail problems after supplying password to phishers
---
Horde webmail again appeared to be misused for sending spam. This could
happen because a naive user gave the Science password to
phishers/spammers. After first stopping horde, this night we disabled
the account of the naive user and restarted horde. This morning it
appeared that this short spam-outbreak had caused administrators of
hotmail.com to add our mail server to their blacklist. Therefore we
switched the IP-number of this mail server and asked Microsoft to remove
the old IP address from their blacklist.
