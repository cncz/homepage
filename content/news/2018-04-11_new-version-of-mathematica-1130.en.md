---
author: petervc
date: 2018-04-11 16:18:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (11.3.0)
---
A new version of [Mathematica](/en/howto/mathematica/) (11.3.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows and
Linux can be found on the [Install](/en/howto/install-share/)-disk, also
for older versions of Mathematica. Departments that take part in the
license can request installation and license info from C&CZ.
