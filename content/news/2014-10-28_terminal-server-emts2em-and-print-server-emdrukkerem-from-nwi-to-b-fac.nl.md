---
author: wim
date: 2014-10-28 14:57:00
tags:
- medewerkers
- docenten
title: Terminalserver <em>ts2</em> en printserver <em>drukker</em> van NWI naar B-FAC
---
In het kader van het opheffen van het
*NWI.RU.NL* Active Directory domain zullen de
terminalserver *ts2* en de printerserver voor
afdelingsprinters *drukker* op 16 november
verhuizen van *NWI.RU.NL* naar
*B-FAC.RU.NL*. Deze aanpassing heeft
consequenties voor het inloggen op de terminal server
*ts2* en het aankoppelen van printers van de
printserver *drukker*.

Indien men nu op de *ts2* inlogt met een
*NWI\\loginnaam*, dan zal men dit moeten
veranderen in
***B-FAC**\\loginnaam*. In
het geval van de printserver *drukker*, als het
aankoppelen van een printer via
*\\\\drukker.nwi.ru.nl\\printernaam* gebeurt,
moet dit aangepast worden tot
*\\\\drukker.**b-fac**.ru.nl\\printernaam*.
Dit laatste kan het eenvoudigste gebeuren door de printer te verwijderen
en opnieuw aan te koppelen.
