---
author: petervc
date: 2017-02-21 17:11:00
tags:
  - medewerkers
  - studenten
title: "Science mail: HTML in ZIP-file attachments blocked to fight ransomware"
---

Because of two cases of [ransomware](https://en.wikipedia.org/wiki/Ransomware) where files are encrypted and can only be decrypted after paying a ransom we decided to block ZIP-attachments containing HTML files in the Science mail virus filter. In that case one finds the mail without attachment in the Virus submap of the Inbox. In both cases of ransomware, we were able to restore the original files from [backup](/en/howto/backup/).
