---
title: New version of Mathematica (13.2.0)
author: petervc
date: 2022-12-23
tags:
- medewerkers
- studenten
cover:
  image: img/2022/mathematica-13.png
---
A new version of [Mathematica](/en/howto/mathematica/) (13.2.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows, Linux
and macOS can be found on the [Install](/en/howto/install-share/)-disk,
also for older versions of Mathematica. Installation and license info
can be [requested from C&CZ](/en/howto/contact/).

According to the [Mathematica Quick Revision History](https://www.wolfram.com/mathematica/quick-revision-history.html): "Version 13.2 introduces
new functionality in astronomy and compilation, as well as
substantially enhancing functions for machine learning, trees,
mathematical computations, video and more. This release also includes
over a thousand bug fixes, documentation enhancements and significant
performance improvements."
