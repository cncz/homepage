---
author: bbellink
date: 2018-07-06 15:20:00
tags:
- medewerkers
- docenten
title: Slides of storage/backup meeting within FNWI
---
June 28, C&CZ held a meeting on a.o. current status and future of
storage and backup within FNWI, a.o. because C&CZ will have to renew the
datacenter network and the storage and backup solutions. For this
meeting, the faculty data stewards and others were invited. The [slides
of the presentation (English only)](/en/howto/dataslides//) are
available.
