---
author: mkup
date: 2009-07-20 12:06:00
title: Redundante netwerkverbindingen
---
Sinds vandaag hebben alle netwerkswitches in Huygensgebouw en overige
gebouwen op het Toernooiveld een tweede, redundante glasvezelverbinding
naar de distributierouter van FNWI. Deze router bestaat uit twee
gekoppelde, zelfstandig draaiende switches op verschillende locaties.
Resultaat is dat wanneer een glasvezelverbinding onderbroken raakt of
één van de routerswitches uitvalt, dit een nauwelijks merkbaar effect
heeft op bestaande netwerkverbindingen. Een sterk verbeterde
beschikbaarheid van de netwerkvoorzieningen dus.
