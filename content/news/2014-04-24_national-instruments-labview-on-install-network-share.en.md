---
author: petervc
date: 2014-04-24 15:58:00
tags:
- software
cover:
  image: img/2014/labview.png
title: National Instruments LabVIEW on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, all DVDs of the “Fall
2013” version have been copied to the [Install](/en/howto/install-share/)
network share. License codes can be obtained from C&CZ helpdesk or
postmaster. When the 2014 version arrives, it will also be copied to the
Install share.
