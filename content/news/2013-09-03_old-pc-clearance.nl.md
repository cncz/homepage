---
author: stefan
date: 2013-09-03 17:40:00
tags:
- studenten
title: Opruiming oude PC's
---
Update 6 september: *Er zijn al meer aanmeldingen dan beschikbare PC’s.*
Volgend jaar zal er waarschijnlijk weer zo’n actie komen.

Ook dit jaar ruimt C&CZ weer oude PC’s uit de
[terminalkamers](/nl/howto/terminalkamers/) op. Het zijn Dell Optiplex
USFF 760 “rugzakjes” inclusief monitor, toetsenbord en muis. De harde
schijf is leeggemaakt, de PC’s worden zonder bedrijfssysteem of enige
vorm van garantie, hulp of uitleg weggegeven. De PC’s worden ‘alleen aan
FNWI-studenten’ geschonken, maximaal 1 PC per persoon. Aanmelding alleen
onder vermelding van je Science mail-adres, dat dus eindigt op
`@student.science.ru.nl`, door een mail te sturen naar
`helpdesk@science.ru.nl`, de [C&CZ helpdesk](/nl/howto/helpdesk/).
Aanmelding sluit op 1 oktober 2013 of wanneer de voorraad op is. Men
krijgt een mail met een datum/tijd om de PC op te halen.
