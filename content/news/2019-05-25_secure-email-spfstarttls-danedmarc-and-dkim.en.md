---
author: visser
date: 2019-05-25 23:46:00
tags:
- studenten
- medewerkers
- docenten
title: 'Secure Email: SPF/STARTTLS, DANE/DMARC and DKIM'
---
Recently, C&CZ worked on DNS based techniques that can be used to fight
spam, phishing and eavesdropping of mail. Of the list of mandatory open
standards of the
[<https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68>
Pas-Toe-of-Leg-Uit] list w.r.t. email, C&CZ implemented
[SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework) a while
ago. Because strict implementation
[<https://en.wikipedia.org/wiki/Sender_Policy_Framework#FAIL_and_forwarding>
breaks simple/automatic forwarding], SPF has not been implemented in a
strict manner. Also
[STARTTLS](https://en.wikipedia.org/wiki/Opportunistic_TLS) for mail
encryption has been implemented a while ago. Recently, we introduced
[<https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities>
DANE] and [DMARC](https://en.wikipedia.org/wiki/DMARC), which makes it
possible to start with
[DKIM](https://nl.wikipedia.org/wiki/DomainKeys_Identified_Mail).
