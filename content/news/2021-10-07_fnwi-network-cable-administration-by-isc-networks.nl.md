---
author: bbellink
date: 2021-10-07 18:15:00
tags:
- studenten
- medewerkers
- docenten
title: Beheer netwerkbekabeling in FNWI-gebouwen door ISC netwerken
---
Per 11 oktober 2021 draagt C&CZ het beheer van de vaste
data-aansluitpunten (network outlets, switchpoorten) in de FNWI-gebouwen
(Huygens, Mercator, HFML/FELIX, Goudsmit, Proeftuin) over aan [ILS netwerken](/nl/howto/contact/#ils-helpdesk).
Dit betreft zowel het activeren/deactiveren van een outlet (patching) als het aanpassen van
instellingen of het oplossen van storingen. Het verzorgen van de
IP-adressen (DNS/DHCP) en de afscherming/beveiliging d.m.v. Access
Control Lists blijven wel bij [C&CZ](/nl/howto/contact/). Alle verzoeken
en storingen met betrekking tot data-aansluitpunten kunnen dus per 11
oktober gericht worden aan de [helpdesk van het ILS](/nl/howto/contact/#ils-helpdesk).
