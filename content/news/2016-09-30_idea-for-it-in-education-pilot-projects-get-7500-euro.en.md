---
author: fmelssen
date: 2016-09-30 11:50:00
tags:
- medewerkers
- docenten
- studenten
title: Idea for IT in education? Pilot projects get 7500 euro\!
---
The new round of pilot projects IT in education has started. Closing
date for applications is November 4, 2016. A teacher, student or member
of support staff with an innovative idea can apply for a contribution of
up to € 7500. More information about the conditions and the application
form can be found at the [IT in education (Dutch
only)](http://www.radboudnet.nl/ictinhetonderwijs/) website on
Radboudnet.
