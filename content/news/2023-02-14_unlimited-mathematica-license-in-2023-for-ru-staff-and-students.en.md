---
title: Unlimited Mathematica license in 2023 for RU staff and students
author: petervc
date: 2023-02-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/mathematica-13.png
---
The license for [Mathematica](/en/howto/mathematica/) has been expanded
temporarily to an unlimited campus license for the whole of 2023: all
employees and students may use Mathematica unlimitedly in 2023 on the
RU network. For home use, one must therefore use a [VPN](/en/howto/vpn/).

The software can be found on the [C&CZ Install disc](https://install.science.ru.nl/science/Mathematica/). During the installation you must specify:

     License server: mathematica.science.ru.nl
     License: L4601-6478

The introductory course ["Mathematica Basic Principles: An Introduction"](https://can.nl/events/mathematica-basic-principles/)
can be taken for free at CANdiensten in Amsterdam.

For questions or problems, mail [mathematica@science.ru.nl](mailto:mathematica@science.ru.nl).
