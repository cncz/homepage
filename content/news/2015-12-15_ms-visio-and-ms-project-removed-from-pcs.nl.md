---
author: wim
date: 2015-12-15 11:12:00
tags:
- medewerkers
- docenten
- studenten
title: MS Visio en MS Project verwijderd van PC's
---
Omdat de ‘grace period’ voor het gebruik van MS
[Visio](https://products.office.com/nl-nl/visio/flowchart-software) en
MS
[Project](https://products.office.com/en-us/project/project-and-portfolio-management-software)
per 1 januari 2016 stopt, wordt deze software van alle door C&CZ
[Beheerde Werkplekken](/nl/howto/windows-beheerde-werkplek/) verwijderd.
Medewerkers die Project en/of Visio willen (blijven) gebruiken, kunnen
zich voor een per installatie betaalde licentie [melden bij
C&CZ](/nl/howto/contact/).
