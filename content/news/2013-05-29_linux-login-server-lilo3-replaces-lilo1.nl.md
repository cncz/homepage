---
author: petervc
date: 2013-05-29 14:32:00
title: Linux loginserver lilo3 vervangt lilo1
---
Er is een nieuwe Ubuntu 12.04 Linux loginserver beschikbaar, onder de
naam `lilo3.science.ru.nl`. Daarom zal binnenkort de zes jaar oude
loginserver `lilo1` uitgefaseerd worden. De naam `lilo`, die altijd naar
de nieuwste/snelste loginserver wijst, zal ook omgezet wprden van de
vier jaar oude `lilo2` naar de nieuwe `lilo3`. De nieuwe `lilo3` is een
Dell PowerEdge R410 met 2 Xeon L5640 processoren en 32 GB geheugen. Het
zal noodzakelijk zijn de oude lilo(.science.ru.nl) entry uit de file
\~/.ssh/known\_hosts te verwijderen. Voor wie de vingerafdruk van de
publieke RSA-sleutel wil controleren voor het Science wachtwoord aan de
nieuwe server te geven:
`3e:b9:89:4d:53:9b:8f:4a:97:b2:a1:c3:0d:74:34:2c`.
