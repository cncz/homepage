---
author: petervc
date: 2008-06-16 18:06:00
title: 'Software update: TeXlive 2007'
---
Today TeXlive 2007 replaced TeXlive 2005 as the standard version of
(La)TeX on Windows and Unix computers managed by C&CZ. If no problems
are reported, the old version will be removed in a few months.
