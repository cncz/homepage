---
author: wim
date: 2010-04-23 15:25:00
title: Nieuwe Windows Terminalserver voor BASS-Finlog en Tracelab
---
Omdat de huidige Windows terminalserver
**ts1.nwi.ru.nl** hardware problemen vertoont, is
besloten om de machine te vervangen. De nieuwe machine is een virtuele
Windows 2008 terminal server. Dit betekent onder andere dat het
gebruikersinterface veranderd is, het lijkt meer op Windows Vista of
Windows 7. Een connectie maken naar deze machine gaat op dezelfde manier
als voorheen, wijzig enkel de naam
**ts1.nwi.ru.nl** in
**ts2.nwi.ru.nl**. Deze machine is primair bedoeld
voor het gebruik van [BASS-Finlog](/nl/howto/bass/) en
[Tracelab](/nl/howto/tracelab/).
