---
author: petervc
date: 2008-03-01 00:46:00
title: Printbudgetten te bekijken/beheren via DHZ
---
Het is nu mogelijk om via de [Doe-Het-Zelf](https://dhz.science.ru.nl)
zelfservice website voor Science logins ook printbudgetten te bekijken
en te beheren. Zie voor meer info de [pagina over DHZ](/nl/howto/dhz/).
