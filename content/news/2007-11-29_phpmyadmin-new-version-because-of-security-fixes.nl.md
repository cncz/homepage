---
author: polman
date: 2007-11-29 10:28:00
title: PhpMyadmin, nieuwe versie i.v.m. veiligheidslekken
---
[PhpMyadmin](http://www.phpmyadmin.net) is een webapplicatie om mysql
databases te beheren, alle mysql databases beheert door C&CZ kun je via
<http://phpmyadmin.science.ru.nl> bewerken. De nieuwe versie (11.2.2)
repareert een veiligheidslek, zie
[1](http://www.phpmyadmin.net/home_page/security.php?issue=PMASA-2007-8).
