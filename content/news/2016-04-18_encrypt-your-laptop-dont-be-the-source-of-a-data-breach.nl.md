---
author: petervc
date: 2016-04-18 12:56:00
tags:
- studenten
- medewerkers
- docenten
title: 'Versleutel je laptop: word zelf geen bron van een datalek'
---
Verlies en diefstal van een laptop komen geregeld voor. Als er
persoonsgegevens op een verloren laptop staan, dan is er sprake van een
datalek, dat verplicht gemeld moet worden en waarop mogelijk een boete
voor de RU volgt. Om lekken en boetes te voorkomen, raden we met klem
aan de harde schijf van je laptop te
[versleutelen](http://www.ru.nl/privacy/data/laptop-versleutelen/). Dat
kan eenvoudig, voor Windows met
[BitLocker](https://nl.wikipedia.org/wiki/BitLocker) of
[VeraCrypt](https://veracrypt.codeplex.com/), voor Mac met
[FileVault](https://support.apple.com/nl-nl/HT204837) en voor Linux met
[VeraCrypt](https://veracrypt.codeplex.com/). Indien nodig kan de [C&CZ
helpdesk](/nl/howto/contact/) adviseren.
