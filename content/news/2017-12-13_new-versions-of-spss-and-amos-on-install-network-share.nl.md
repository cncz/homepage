---
author: petervc
date: 2017-12-13 11:19:00
tags:
- studenten
- medewerkers
title: Nieuwe versies SPSS en Amos op Install-schijf
---
Nieuwe versies van [IBM SPSS (23 t/m 25) en Amos (23 en
24)](http://www-01.ibm.com/software/analytics/spss/products/statistics/),
zijn op de [Install](/nl/howto/install-share/)-netwerkschijf te vinden.
SPSS voor zowel Windows als Mac OS X, Amos alleen voor MS-Windows. De
licentievoorwaarden staan gebruik op RU-computers toe en ook
thuisgebruik door RU-medewerkers en RU-studenten. Licentiecodes zijn bij
C&CZ helpdesk of postmaster te verkrijgen. Eigen DVD’s kunnen ook op
[Surfspot](http://www.surfspot.nl) besteld worden.
