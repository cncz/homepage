---
author: petervc
date: 2010-09-22 15:44:00
title: New head of department
---
November 1, 2010, {{< author "petervc" >}} will go back to an
administrator/developer function, the new head of C&CZ will be
{{< author "caspar" >}}.
