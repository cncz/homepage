---
author: petervc
date: 2013-11-26 14:57:00
tags:
- studenten
- medewerkers
- docenten
title: Onderhoud X-win32 stopt, stap over op Xming
---
C&CZ stopt eind januari 2014 het onderhoud van de commerciële X server
software voor Windows X-win32. Wij adviseren
gebruikers over te stappen op de freeware Xming
software.
