---
author: petervc
date: 2014-08-20 14:41:00
tags:
- studenten
title: Office 365 ProPlus vrijwel gratis voor studenten
---
Het [ISC](http://www.ru.nl/ictservicecentrum/) heeft
[meegedeeld](http://www.ru.nl/ictservicecentrum/actueel/nieuws/berichten/studenten-office-365/)
dat [Microsoft Office 365
ProPlus](http://office.microsoft.com/nl-nl/business/office-365-proplus-office-online-FX103213513.aspx)
per direct vrijwel gratis is voor studenten van de RU. De enige kosten
zijn € 2,99 per jaar administratiekosten van
[SURFspot.nl](http://www.surfspot.nl). Zie voor alle info [de
SURFspot.nl Office365 pagina](https://www.surfspot.nl/officestudent).
