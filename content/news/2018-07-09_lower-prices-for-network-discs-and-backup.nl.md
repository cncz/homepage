---
author: bbellink
date: 2018-07-09 13:27:00
tags:
- medewerkers
- docenten
- backup
title: Lagere prijzen voor netwerkschijven en backup
---
Naar aanleiding van de FNWI-data/backup bijeenkomst en de aanschaf van
een nieuwe storageserver heeft C&CZ de prijzen van
[netwerkschijven](/nl/howto/diskruimte/) en van
[backup](/nl/howto/backup/) opnieuw vastgesteld. De nieuwe prijzen zijn
ca. 2.5 maal lager geworden t.o.v. de oude, die 4 jaar geleden
vastgesteld werden.
