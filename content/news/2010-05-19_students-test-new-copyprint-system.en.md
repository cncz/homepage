---
author: petervc
date: 2010-05-19 17:22:00
title: Students test new copy/print system
---
The test by students mentioned below has been ended, the [Peage
project](http://www.ru.nl/uci/peage/peage/) continues. For the Peage
project, that aims to deliver a new print/copy-system for Radboud
University, C&CZ was looking for 25 students. These students received €
50,- print/copy budget in the new system. The requirements to
participate were:

-   student of the Science Faculty
-   having an OV-chipcard
-   present at the instructional meeting, Thursday May 27, 12:30-13:30
    hours in CC3
-   willing to print/copy during the test period (until July 2, 2010) on
    black/white printers/copiers in the Faculty of Arts, Medical
    Faculty, Nijmegen School of Management and the Science Faculty
-   willing to share experiences in the evaluation

The first 25 reacting by mail to Peter van Campen have been selected,
closing date was May 25.
