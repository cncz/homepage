---
title: Nieuwe versie van Mathematica (13.3.0)
author: petervc
date: 2023-06-30
tags:
- medewerkers
- studenten
cover:
  image: img/2023/mathematica-13.png
---
Er is een nieuwe versie (13.3.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden.

Bij een bedrade of draadloze verbinding met het
campusnetwerk of via [VPN](/nl/howto/vpn/) zijn de
installatiebestanden voor Windows, Linux en macOS zijn op de [C&CZ
Install](https://install.science.ru.nl/science/Mathematica/)-schijf te
vinden. 

Bij de installatie moet je aangeven:

    License server: mathematica.science.ru.nl
    License: L4601-6478


Volgens de [Mathematica Quick Revision History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 13.3 introduces new functions that utilize large language models, as well as expands functionality for machine learning, mathematical computations, foreign function interface and more."
