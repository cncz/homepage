---
title: Maple nieuwe versie Maple2024
author: petervc
date: 2024-03-22
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/maple.png
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2024, is voor
Windows/macOS/Linux te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf en is op door C&CZ
beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen.

