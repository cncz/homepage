---
author: wim
date: 2010-03-23 10:35:00
title: Nieuwe pc's in pc-zalen dual-boot Windows/Linux
---
Alle nieuwe pc’s in de [pc-zalen/terminalkamers en
studielandschap](/nl/howto/terminalkamers/) zijn als dual-boot pc
ingericht: een individuele student of medewerker kan kiezen voor het
starten van Windows-XP of van Linux Fedora 11 Linux. De zalen kunnen nu
dus ook voor cursussen gebruikt worden waar Linux nodig is. Alleen de
oudere pc’s in TK075 (HG00.075) zijn nog single-boot Windows-XP. In de
loop van 2010 zal de migratie naar Windows 7 en Ubuntu 10.04 Linux gaan
beginnen.
