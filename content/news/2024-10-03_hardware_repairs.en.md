---
title: Hardware repairs
author: petervc
date: 2024-10-03
tags:
- medewerkers
- studenten
cover:
  image: img/2024/repair.jpg
---
Recently, the [Radboud
news](https://www.ru.nl/en/students/news/rupair-shop-kicks-off-1-october-free-small-repairs-of-your-laptop)
reported that students can have small repairs to their laptops done in the
[RUpair-shop](https://www.ru.nl/en/students/services/campus-facilities-buildings/ict/hardware/rupair-shop-free-small-repairs-of-your-laptop)
without any labor time being charged.

For students and employees of FNWI, the [C&CZ
helpdesk](https://cncz.science.ru.nl/en/howto/contact/) provides a
similar service. [Stefan
Reijgwart](https://www.radboudnet.nl/personen/reijgwart-s) and [Dominic van den Broek](https://www.radboudnet.nl/personen/broek-d-r-van-den) in particular are specialists
in repairing laptops, among other things.
