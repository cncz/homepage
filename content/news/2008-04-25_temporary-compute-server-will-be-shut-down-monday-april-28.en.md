---
author: petervc
date: 2008-04-25 12:23:00
title: Temporary compute server will be shut down Monday April 28
---
The “t4150” test-machine with 4-core Intel processors (Sun Fire X4150,
E5345 CPU’s) will be shut down on Monday April 28. We’ll conduct a
hardware RAID-test and re-installation of the OS. After that, the
machine will be shipped back to Sun.
