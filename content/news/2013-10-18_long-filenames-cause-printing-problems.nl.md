---
author: petervc
date: 2013-10-18 15:43:00
tags:
- medewerkers
- docenten
- studenten
title: Lange bestandsnamen geven problemen bij printen
---
Het komt voor dat een printjob niet uit een printer komt als de
bestandsnaam langer is dan ca. 110 tekens. De oorzaak is gelegen in een
van onze printservers. Er is hierover een waarschuwing op de
[printerpagina](/nl/howto/printers-en-printen/) vermeld. Wij werken aan
een oplossing.
