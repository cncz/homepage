---
author: polman
date: 2009-02-20 16:54:00
title: New version squirrel (webmail)
---
[squirrel.science.ru.nl](http://squirrel.science.ru.nl/) has been
replaced by the latest version and has been moved to a new web server.
