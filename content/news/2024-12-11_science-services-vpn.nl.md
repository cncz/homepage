---
title: Science diensten achter VPN
author: miekg
date: 2024-12-11
tags:
  - medewerkers
  - studenten
cover:
  image: img/2024/.png
---

Na een recente phishing aanval op science accounts en het misbruik van RU mail servers daarna om
spam en phishing mail te versturen namens valide science accounts, zijn we gedwongen om een aantal
defensieve maatregelen te namen. Dit is ook die zien in deze lijst van CPKs:
[cpk/1381](/nl/cpk/1383/), [cpk/1382](/nl/cpk/1382/) en [cpk/1381](/nl/cpk/1381/).

Om verder misbruik (en hinder) van gecompromitteerde accounts te voorkomen, hebben we de toegang tot
_sommige_ science diensten **tijdelijk** dicht(er) moeten zitten. Deze diensten zijn:

- Alles gerelateerd aan e-mail: Roundcube, authenticated SMTP, IMAP en POP3.
- SSH toegang tot de LILOs (maar zie [cpk/1383](/nl/cpk/1383/) voor mogelijke oplossing).
- [DHZ](http://dhz.science.ru.nl) en [de nieuwe vervanger](/nl/news/2024-12-09-new-dhz/).

De e-mail diensten worden nu _per_ science account _open_ gezet. Een set van ongeveer 1000 accounts
heeft nu weer normaal toegang. Om toegang te krijgen neem dan [contact met ons op](/nl/howto/contact/).

Om de andere diensten te blijven gebruiken moet je het campus netwerk gebruiken, of via [EduVPN](/nl/howto/vpn/#eduvpn) inloggen.

Begin 2025 komen we met een nieuwe update over hoe wij denken dat we de toegang tot onze diensten
veiliger kunnen maken.
