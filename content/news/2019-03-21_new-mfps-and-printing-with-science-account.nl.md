---
author: wim
date: 2019-03-21 13:02:00
tags:
- medewerkers
- docenten
- studenten
title: Nieuwe MFP's en printen met Science account
---
De [nieuwe Konica Minolta
MFP’s](https://www.ru.nl/facilitairbedrijf/printen/printen-campus/#ha8c20a0c-cdc3-41cf-8748-038a27bb6098)
die de RU-brede FollowMe printqueue aanbieden, kunnen ook [gebruikt
worden met het Science account](https://wiki.cncz.science.ru.nl/Peage).
Dat werkt alleen als bij het Science-account het U- of S-nummer bekend
is bij C&CZ, wat gecontroleerd kan worden op de [Doe-Het-Zelf
website](https://dhz.science.ru.nl). Op door C&CZ beheerde Windows PC’s
kan men dan printen naar de queue “FollowMeScience”, die het
Science-account vertaalt naar het U/S-nummer en doorstuurt naar de RU
“FollowMe” queue. Bij C&CZ-beheerde Linux machines heet deze queue
“FollowMe”. De zes jaar oude MFP’s worden/zijn op de hele RU 1-op-1
vervangen door nieuwe, inclusief opties zoals de bookletmaker bij
HG00.002.
