---
title: "CephFS uitgezet"
date: 2024-04-15T10:05:14+02:00
author: miek
tags:
- medewerkers
- studenten
- ceph
cover:
  image: img/2024/ceph-retired.jpg
---

Na de [problemen](https://cncz.science.ru.nl/nl/cpk/1359/) met CephFS op ons Ceph cluster is in overleg
de gebruikers besloten om te stoppen met deze service.

Alle data is inmiddels uit CephFS gekopieerd.

De CephFS service is gestopt. Het Ceph cluster wordt nog wel gebruikt om backups op te slaan via de S3 API.
