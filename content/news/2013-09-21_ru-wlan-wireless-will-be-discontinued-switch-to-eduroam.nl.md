---
author: mkup
date: 2013-09-21 16:01:00
tags:
- studenten
- medewerkers
- docenten
title: Ru-wlan draadloos stopt, stap over op Eduroam
---
Overal in de RU-gebouwen is nu het [draadloze netwerk
Eduroam](/nl/howto/netwerk-draadloos/) beschikbaar. Daarom zal het
draadloos netwerk ru-wlan per 1 november 2013 verdwijnen. Schakel dus
s.v.p. zo snel mogelijk over op Eduroam. Men kan inloggen met *U-* of
*S-nummer*\@ru.nl en het RU-wachtwoord of met
*science-username*\@science.ru.nl en het Science-wachtwoord. Let op:
Laptops met Windows8 verbinden soms niet automatisch met Eduroam. Kies
in dat geval voor “Microsoft: Beveiligde EAP (PEAP)” bij “Selecteer een
methode voor netwerkauthenticatie” tijdens de configuratie.
[Configuratiehandleidingen](http://www.ru.nl/draadloos/) voor het
draadloos netwerk voor allerlei apparaten en besturingssystemen zijn
beschikbaar op de website van het [ICT
Servicecentrum](http://www.ru.nl/isc).
