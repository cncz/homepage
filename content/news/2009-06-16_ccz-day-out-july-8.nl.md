---
author: petervc
date: 2009-06-16 16:07:00
title: 'C&CZ dagje uit: 8 juli'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor 8 juli. Voor
bereikbaarheid in geval van ernstige storingen wordt gezorgd.
