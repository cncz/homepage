---
author: petervc
date: 2016-08-19 18:17:00
tags:
- studenten
- medewerkers
title: Tijdelijk gratis beveiligingssoftware voor studenten en medewerkers
---
Tussen 15 augustus en 15 september kunnen RU-studenten en -medewerkers
gratis de beveiligingssoftware F-Secure SAFE voor 1 apparaat voor 1 jaar
bestellen. Normaal kost dat € 2 per jaar. De software is beschikbaar
voor Windows, Mac, Android, iOS en Windows Phone. Zie voor alle info de
[Surfspot
website](https://www.surfspot.nl/f-secure-safe-1-apparaat-radboud-universiteit-nijmegen-en-fontys.html).
