---
author: visser
date: 2019-05-25 23:22:00
tags:
- studenten
- medewerkers
- docenten
- dns
title: Veilige DNS (DNSSEC) ingevoerd
---
Als vereiste voor het RU-project “Invoering Veilige Email-standaarden”,
waarin de
[Pas-Toe-of-Leg-Uit](https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68)
lijst van verplichte open standaarden afgewerkt wordt, is
[DNSSEC](https://nl.wikipedia.org/wiki/DNSSEC) ingevoerd voor de
maildomeinen die C&CZ beheert onder ru.nl (science.ru.nl, cs.ru.nl,
astro.ru.nl, math.ru.nl, …). Voor meer info over DNSSEC zie bv. de
[Engelstalige Wikipedia over
DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions).
