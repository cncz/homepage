---
author: wim
date: 2009-12-23 15:13:00
title: 'F-Secure: new version, less system impact'
---
Version 9 of the [F-Secure](http://www.f-secure.com) security software
is [available on Radboudnet](http://www.radboudnet.nl/fsecure). The new
version has [less system
impact](http://www.f-secure.com/en_EMEA/about-us/pressroom/news/2009/fs_news_20091130_01_eng.html).
The RU-license encompasses home use by employees and students. In
January 2010, C&CZ will start deploying this version on the
[C&CZ-managed MS-Windows pc’s](/en/howto/windows-beheerde-werkplek/).
