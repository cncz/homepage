---
title: New Intel compilers (oneAPI Base and HPC Toolkits)
author: petervc
date: 2023-10-27
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/intel-one-api.jpg
---
The newest versions of the Intel compilers, available under the name [Intel oneAPI](https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html),
do no longer require a license, they are freely available.
The 2023.2 version has been installed on C&CZ managed Ubuntu systems, clusternodes with Ubuntu 22.04 and loginservers. When these compilers are available, the module command

  module avail

will list among others “compiler/latest”. Then e.g. the Intel C (icc) and Fortran (ifort) compilers can be used after typing

  module add compiler/latest
