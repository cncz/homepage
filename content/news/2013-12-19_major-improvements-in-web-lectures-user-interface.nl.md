---
author: bram
date: 2013-12-19 14:10:00
tags:
- docenten
- studenten
title: Grote verbeteringen gebruikersinterface Onderwijs in Beeld
---
Het gebruikersinterface van Onderwijs in Beeld heeft een metamorfose
ondergaan. In Blackboard zie je per cursusjaar meteen of er video’s
aanwezig zijn. In de afspeellijst kun je per video, voor zover aanwezig,
de volgende knoppen vinden:

-   externe speler\*
-   slides als pdf document downloaden\*
-   permanente link\*\*
-   video downloaden

\* alleen voor nieuwe opnames van colleges met slides \*\* alleen voor
nieuwe opnames
