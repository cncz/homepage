---
author: wim
date: 2012-02-28 15:53:00
tags:
- docenten
title: 'Docenten: test uw cursus-software in de zomer\!'
---
C&CZ is van plan om in de zomer alle pc’s in de
[onderwijsruimtes](/nl/howto/terminalkamers/) te voorzien van Windows 7
en Ubuntu 12.04, beide 64-bit. Omdat het niet gegarandeerd is dat alle
software gewoon werkt met deze versies van de bedrijfssystemen, wordt
aangeraden om tijd vrij te houden voor het testen van uw
onderwijsprogrammatuur. Hiervoor zullen tijdig bij C&CZ testsystemen
beschikbaar zijn.
