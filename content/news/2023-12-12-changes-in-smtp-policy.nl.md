---
title: Veranderingen in beleid voor uitgaande mail (SMTP)
author: petervc
date: 2023-12-12
tags:
- medewerkers
- studenten
cover:
  image: img/2023/authenticated-smtp.png
---
Om e-mailspoofing tegen te gaan, zijn de volgende wijzigingen op komst. Deze wijzigingen hebben invloed op de manier hoe mail verstuurd kan worden.

## Science smtp vanaf bepaalde netwerken alleen nog maar geauthenticeerd
Wanneer men verbonden is met het campusnetwerk is het op dit moment mogelijk om mail te versturen met SMTP
_zonder_ authenticatie. Voor gebruikersapparaten werd altijd aangeraden om [geauthenticeerde SMTP](../../howto/email/#uitgaande-mail) te gebruiken.
Vanaf 8 januari 2024 staat C&CZ niet-geauthenticeerde SMTP van gebruikersnetwerken niet meer toe. De eerste netwerken die we zullen blokkeren zijn
[Eduroam](../../howto/netwerk-draadloos/) en [VPN](../..//howto/vpn/) .

## Mail versturen via Science SMTP niet meer voor `@ru.nl` afzenderadressen
Vanaf 8 januari 2024 neemt C&CZ contact op met medewerkers die gebruik maken van 
`smtp.science.ru.nl` en daarbij mail versturen met een afzenderadres eindigend op `@ru.nl`.
Verzocht wordt om hun SMTP-instellingen te wijzigen, met de keus om ofwel het Science mailadres als afzender te gebruiken of
gebruik te maken van [de centrale RU mailvoorziening](https://www.ru.nl/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/e-mail-en-agenda).
Het is de bedoeling om per 1 februari 2024 @ru.nl afzenderadressen te blokkeren op de Science SMTP service.
