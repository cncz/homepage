---
title: SageMath on loginservers and JupyterHub, Digital Security and Math clusternodes
author: petervc
date: 2024-02-16
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2024/sagemath.jpg
---
The free open-source mathematics software system [SageMath](https://www.sagemath.org/) version 9.5 has been installed on Science [loginservers](https://cncz.science.ru.nl/en/howto/hardware-servers/#linux-login-servers) and [JupyterHub](https://cncz.science.ru.nl/en/howto/jupyterhub/), Digital Security and Math [clusternodes](https://cncz.science.ru.nl/en/howto/hardware-servers/#compute-serversclusters).

The mission of SageMath is to create viable free open source alternative to Magma, Maple, Mathematica and Matlab.

SageMath builds on top of many existing open-source packages: NumPy,
SciPy, matplotlib, Sympy, Maxima, GAP, FLINT, R and many more. Access
their combined power through a common, Python-based language or directly
via interfaces or wrappers.
