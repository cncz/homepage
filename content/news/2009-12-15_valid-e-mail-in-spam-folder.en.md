---
author: petervc
date: 2009-12-15 18:08:00
title: Valid e-mail in Spam folder
---

On 7 december, during the regular update of the spamfilter, a new filter
set turned out to tag too many valid messages as spam. Therefore some
valid mail ended up in people’s Spam folders, so-called false positives.
Please search your Spam folder (a subfolder of Inbox) for false
positives. It is advisable to do so once a week even under normal
circumstances if you do not want to miss any mail, but for December 7
and 8 there is a direct reason to do so.

We apologise for any inconvenience this may have caused.
