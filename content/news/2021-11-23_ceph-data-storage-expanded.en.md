---
author: wim
date: 2021-11-23 14:06:00
tags:
- storage
cover:
  image: img/2021/ceph.png
title: Ceph data storage expanded
---
Since the end of 2019, C&CZ offers
[Ceph](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=en#Ceph_Storage)
as a choice for data storage, next to the traditional RAID storage on
individual fileservers. Because demand for Ceph storage is rising, the
Ceph storage has been upgraded from 1.8 PiB to 2.8 PiB gross. With this
upgrade, storage servers have been placed in a central RU datacenter, so
the Ceph storage is now distributed over three datacenters. If you want
to try whether Ceph suits you, please [contact C&CZ](/en/howto/contact/).
