---
author: polman
date: 2008-05-07 17:24:00
title: Standaard RU-laptop uitgezocht
---
Sinds kort is er door het RU-assortimentsteam naast de al veel langer
bestaande standaard-PC’s uit het kernassortiment van de
[huisleverancier](/nl/howto/huisleverancier-pcs/) ook een standaard
RU-laptop uitgezocht: de HP6710b. Er zijn twee varianten, de duurdere
heeft o.a. een snellere processor, een grotere harde schijf, een scherm
met hogere resolutie en standaard meer geheugen. C&CZ is nu bezig met
het mogelijk te maken om deze laptops in te zetten als “Windows beheerde
laptop”, vergelijkbaar met de [[Windows\_beheerde\_werkplek\|Windows
beheerde werkplek].
