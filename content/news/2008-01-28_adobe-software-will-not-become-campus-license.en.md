---
author: petervc
date: 2008-01-28 14:21:00
title: Adobe software will not become campus license\!
---
Today [SURFdiensten](http://www.surfdiensten.nl) reported that not
enough universities were willing to participate in the proposed
multi-year license agreement for [Adobe](http://www.adobe.com) products.
Therefore if one wants to buy Adobe products in the near future, one has
to resort to individual licences.
