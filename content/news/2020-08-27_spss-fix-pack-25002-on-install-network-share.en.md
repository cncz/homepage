---
author: petervc
date: 2020-08-27 15:04:00
tags:
- medewerkers
- docenten
title: SPSS Fix Pack 25.0.0.2 on Install network share
---
A [Fix Pack for SPSS
25](https://www.ibm.com/support/pages/spss-statistics-250-fix-pack-2),
is available on the [Install](/en/howto/install-share/) network share.
