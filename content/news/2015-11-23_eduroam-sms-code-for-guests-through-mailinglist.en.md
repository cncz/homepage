---
author: bram
date: 2015-11-23 14:31:00
tags:
- medewerkers
title: Eduroam SMS code for guests through mailinglist
---
Displaying the daily changing [eduroam Visitor
Access](https://eva.eduroam.nl/inloggen) (eVA) SMS code on the Huygens
information screens will be stopped at the request of SURFnet. With this
code guests could connect to the guest part of the wireless network
eduroam without help from others. C&CZ has developed an alternative: the
code can be mailed daily to e.g. secretaries of departments of the
Faculty of Science. This can be requested by mail to
[postmaster](/en/howto/contact/). Employees who want to give wifi access
to (groups of) guests, may do so by contacting the [C&CZ
helpdesk](/en/howto/werkplekondersteuning/), tel. 20000. As of December
1, the SMS codes will also be available through the [Library of
Science](http://www.ru.nl/library/library/library-locations/library-science/).
