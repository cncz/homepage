---
author: bram
date: 2018-06-26 14:14:00
tags:
- studenten
- medewerkers
- docenten
title: Digitaal tentamen m.b.v. cloud application streaming
---
Sinds februari 2015 wordt er binnen FNWI ook [digitaal
getoetst](/nl/howto/digitaal-toetsen/). Vorige week is succesvol een
grootschalige toets afgenomen bij meer dan 300 studenten Informatica met
gebruik van de [Netbeans](https://netbeans.org/) geïntegreerde
programmeeromgeving. Deze IDE werd als een Windows-applicatie gestreamd
vanaf cloudleverancier [Amazon](https://aws.amazon.com). De applicatie
liep in een tab-blad van de RU Kiosk App en werd geleverd via [Amazon
Appstream 2.0](https://aws.amazon.com/appstream2/) op Chromebooks. De
Chromebooks zijn vorig jaar door de RU aangeschaft om grootschalig
digitaal toetsen mogelijk te maken.
