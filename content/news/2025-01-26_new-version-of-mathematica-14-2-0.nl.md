---
title: Nieuwe versie van Mathematica 14.2.0
author: arnoudt
date: 2025-01-27
tags:
  - medewerkers
  - studenten
  - software
cover:
  image: img/2024/mathematica.png
---

Er is een nieuwe versie (14.2.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden.

Bij een bedrade of draadloze verbinding met het
campusnetwerk of via [VPN](/nl/howto/vpn/) zijn de
installatiebestanden voor Windows, Linux en macOS op de [C&CZ
Install](https://install.science.ru.nl/science/Mathematica/)-schijf te
vinden.

Bij de installatie moet je aangeven:

    License server: mathematica.science.ru.nl
    License: L4601-6478

Volgens de [Mathematica Quick Revision History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 14.2 introduces a powerful tool in Tabular, which provides a very streamlined and efficient way to handle tables of data laid out in rows and columns, with hundreds of other functions enhanced to make use of its special features. Additional new functions and improvements have been added to enhance neural nets and LLMs, work with game theory, improve GPU computation, and expand images and videos."
