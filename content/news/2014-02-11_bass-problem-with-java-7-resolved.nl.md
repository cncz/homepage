---
author: petervc
date: 2014-02-11 17:40:00
tags:
- medewerkers
title: BASS probleem met Java 7 opgelost
---
Het [ISC](http://www.ru.nl/isc) heeft ons meegedeeld dat bij de
invoering van technische patches voor [BASS](/nl/howto/bass/) in het
weekend van 1 december, de mogelijkheid om te werken met een recente
Java versie (versie 7) abusievelijk was weggevallen. Per 11 februari
2014 is dit opgelost. De functie in BASS om te werken met alle Java SE
versies (JRE) op de client (PC) is weer geactiveerd. Tevens hebben alle
JAR-bestanden in BASS een security certificaat (ze zijn “gesigned”),
waarmee BASS ook voldoet aan de eisen om met de laatste Java SE versies
te kunnen werken (Java SE 7.51 en hoger). BASS-gebruikers die met Forms
werken zullen pop-up schermen krijgen waarin gevraagd wordt “Do you want
to run this application?”. Hiervan is een [beschrijving gemaakt op het
RU-Intranet](http://www.radboudnet.nl/publish/pages/685146/meldingen_java_nieuwe_versie_4.pdf).
