---
author: wim
date: 2021-03-23 10:22:00
tags:
- medewerkers
- docenten
title: Windows 7 computers disabled in B-FAC domain
---
Because of security issues the last remaining Windows 7 machines wil be
disabled, effective 24-03-2021, as member of the Active Directory Domain
B-FAC. Please upgrade these computers to a more up-to-date OS. See also
previous announcements on [Windows
10](https://wiki.cncz.science.ru.nl/Nieuws#.5BMicrosoft_Windows_10_upgrade.5D.5BMicrosoft_Windows_10_upgrade.5D)
and [the end of Windows
7](https://wiki.cncz.science.ru.nl/Nieuws_archief#.5BWindows_7_stopt_januari_2020:_Upgrade_nu.21.5D.5BWindows_7_ends_January_2020:_Upgrade_now.21.5D).
