---
title: Maple licentie voortgezet voor 2 gelijktijdige gebruikers
author: petervc
date: 2023-12-01
tags:
- medewerkers
- studenten
cover:
  image: img/2023/maple.png
---
De licentie van [Maple](/nl/howto/maple/) is voortgezet als een
2-gebruikers licentie in 2024.
Elke FNWI-medewerker en -student mag Maple gebruiken.
Belangstellenden die Maple willen gebruiken, kunnen
zich melden bij [C&CZ](/nl/howto/contact/).
