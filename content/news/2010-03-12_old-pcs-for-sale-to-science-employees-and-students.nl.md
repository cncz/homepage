---
author: theon
date: 2010-03-12 19:15:00
title: Oude pc's te koop voor FNWI-medewerkers en -studenten
---
In januari zijn alle meer dan 5 jaar oude pc’s in de
[pc-zalen/terminalkamers en studielandschap](/nl/howto/terminalkamers/)
vervangen door nieuwe. FNWI-afdelingen hebben al de mogelijkheid gehad
om hun belangstelling voor deze oude pc’s door te geven. Er zijn nog
enkele tientallen pc’s beschikbaar, die met niet veel meer dan “garantie
tot om de hoek” aangeboden worden aan individuele FNWI-medewerkers en
-studenten voor 100 euro per stuk. Wie interesse heeft, kan contact
opnemen met C&CZ ([Helpdesk](mailto:Helpdesk@science.ru.nl), Theo
Neuij/Mathieu Bouwens, 20000). Specificaties: Dell Optiplex SX280;
Pentium 4 dual core processor 2.8 GHz, 1 GB werkgeheugen, 80 GB harde
schijf, inclusief (aangekoppeld) 17" TFT-scherm, toetsenbord en muis.
