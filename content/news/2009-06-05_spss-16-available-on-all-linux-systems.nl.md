---
author: polman
date: 2009-06-05 16:08:00
title: SPSS 16 beschikbaar op alle linux systemen
---
Het statistische software pakket [SPSS](http://www.spss.com/nl/) is nu
ook op alle linux systemen beschikbaar.
