---
title: NI LabVIEW licentie voortgezet en nieuwe versie
author: petervc
date: 2024-03-19
tags:
- medewerkers
- studenten
- software
cover:
  image: img/2023/labview.png
---
De licentie voor [LabVIEW](/nl/howto/labview/) van [NI.com](https://ni.com) is verlengd tot 16 april 2025.
Dit is de licentie voor versie 2023Q3 en hoger, die een abonnementsmodel hanteren.

De nieuwe 2024Q1 software is te vinden op de [C&CZ Install schijf](https://cncz.science.ru.nl/nl/howto/install-share/). De licentieserver is:

         labview.science.ru.nl:28000

Om een licentie te kunnen uitchecken, moet C&CZ namen van pc's en/of gebruikers
invoeren in de licentieserver.
Voor pc's buiten science.ru.nl moet dit DNS-achtervoegsel worden toegevoegd
aan de IPv4 DNS-configuratie: science.ru.nl

C&CZ beheert een FlexNet-licentieserver voor LabVIEW 2022Q3, met een
beperkte aantal licenties en een deadline in april voor verlenging van de licentie.
Eerdere versies van LabVIEW werken standalone en onbeperkt in tijd.

