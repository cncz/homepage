---
author: polman
date: 2009-08-26 11:38:00
title: Nieuwe versie van Mediawiki
---
`Er is een nieuwe versie van`[`Mediawiki`](/nl/tags/wiki)`(1.15.1), de software waar alle wikis van de Faculteit op draaien,`

geïnstalleeerd. Tevens zijn de
[extensies](/nl/howto/geïnstalleerde-mediawiki-extensies/) bijgewerkt
naar nieuwste versies.
