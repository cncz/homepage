---
author: petervc
date: 2018-12-07 11:29:00
tags:
- medewerkers
- docenten
title: New prices MS Visio and Project
---
As of [January,
2016](https://wiki.cncz.science.ru.nl/index.php?title=Nieuws_archief&setlang=en#.5BMS_Visio_en_MS_Project_verwijderd_van_PC.27s.5D.5BMS_Visio_and_MS_Project_removed_from_pcs.5D),
using Microsoft Visio Pro and Microsoft Project has to be paid per pc
per year. Most other Microsoft products for which RU has licenses, can
be used freely on RU pcs. As of January 2019, prices for Viso and
Project have changed considerably, see below. Please [contact
C&CZ](/en/howto/contact/) if you use or want to start using Visio or
Project.

  ---------------------------------------------------------------------------------
  *Product*                                                       *2018*   *2019*
  --------------------------------------------------------------- -------- --------
  Microsoft Visio Pro (Employees, group license)                            

  Microsoft Project Professional (with 1 Project Server CAL)      € 3,97   € 34,08
  (Employees, group license)            

  Microsoft Project Server, Server licentie                       € 396,98 € 0,00
  (Group license)                                    
  ---------------------------------------------------------------------------------
