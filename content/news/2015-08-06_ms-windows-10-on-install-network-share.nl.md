---
author: petervc
date: 2015-08-06 11:02:00
tags:
- medewerkers
- docenten
title: MS Windows 10 op Install-schijf
---
De nieuwste versie van [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan upgrade naar deze versie op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden.
