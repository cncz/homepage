---
author: mkup
date: 2013-05-06 11:28:00
tags:
- studenten
- docenten
- medewerkers
title: Draadloos netwerk rondom Huygensgebouw
---
Al enige tijd kan men op het terras aan de achterzijde van het
Huygensgebouw verbinding maken met de draadloze netwerken eduroam,
ru-wlan en ru-guest (Wireless\@RU). Vanaf heden zijn deze netwerken ook
beschikbaar op de grasvelden rond Huygens, op het plein bij de
hoofdingang en op het plein naast/voor het Linnaeusgebouw. Nadere info
over het gebruik en installatiehandleidingen van Wireless\@RU zijn te
vinden op <http://www.ru.nl/gdi/voorzieningen/netwerken/wireless>.
