---
author: wim
date: 2018-02-28 18:55:00
tags:
- medewerkers
- docenten
title: Download your "Onderwijs in Beeld" recordings of lectures
---
As of August 1, 2018, the service “Onderwijs in Beeld” will be
terminated, the videos will no longer be accessible.

This predecessor of the [RU weblectures](http://www.ru.nl/weblectures/)
hosted video recordings of lectures of the Faculty of Science, recorded
during the academic years 2007-2015. These videos could be accessed via
[BlackBoard](https://blackboard.ru.nl/).

If you want to download your old recordings, you can do that based on
the course code via [the Podcast
service](https://podcast.science.ru.nl/search/).
