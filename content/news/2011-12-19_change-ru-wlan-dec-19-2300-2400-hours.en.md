---
author: mkup
date: 2011-12-19 16:31:00
tags:
- studenten
- medewerkers
- docenten
title: Change ru-wlan, Dec 19 23:00-24:00 hours
---
The [UCI](http://www.uci.ru.nl) let us know that the [ru-wlan wireless
network](http://www.ru.nl/uci/diensten_voor/medewerkers/wireless_ru/)
will have a service window on december 19 between 23:00 and 24:00 hours.
In this service window the number of IP addresses is enlarged from 2000
to 4000 and the authentication gets quickened and for iPhones improved.
After this service window it is no longer necessary to use profiles for
iPads and iPhones. During this service window use of ru-wlan will not be
possible.
