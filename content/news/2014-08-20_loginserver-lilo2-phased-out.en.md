---
author: visser
date: 2014-08-20 15:38:00
tags:
- medewerkers
- studenten
title: Loginserver lilo2 phased out
---
The five year old Linux loginserver lilo2 (Ubuntu 10.04) will be phased
out shortly. All users are requested to switch to lilo3 (Ubuntu 12.04)
or the recently announced lilo4 (Ubuntu 14.04). The names `lilo` and
`stitch` always point to the two [Linux
loginservers](/en/howto/hardware-servers#login-servers/) recommended by
C&CZ. .
