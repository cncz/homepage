---
title: Gemiste RU-mail vanwege stoppen van doorsturen naar extern
author: petervc
date: 2023-08-22
tags:
- medewerkers
- studenten
cover:
  image: img/2023/missed-mail.jpg
---
RU-mailbeheer liet weten dat gisteren het doorsturen naar externe (niet-RU) mailadressen is stopgezet zoals eerder aangekondigd. Helaas werd/wordt de mail van enkele tientallen Science-gebruikers niet doorgestuurd naar de Science mailservers. Deze mails zijn nog steeds te vinden in MS365 (RU-mail), hetzij in de Inbox of in de Verwijderde Items. RU-mailbeheer heeft toegezegd dat het doorsturen morgen gecorrigeerd zal worden.

Automatisch doorsturen van Science mail door medewerkers en studenten naar
externe adressen is op 22 augustus stopgezet.

Zie ook [cpk/1341](/nl/cpk/1341).
