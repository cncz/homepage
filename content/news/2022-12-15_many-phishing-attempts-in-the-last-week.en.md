---
title: Many phishing attempts in the last week
author: bram
date: 2022-12-15
tags: []
cover:
  image: img/2022/many-phishing-attempts-in-the-last-week.png
---
Last week we have seen a strong increase in [phishing](/en/howto/know-about-phishing/) attempts targeting the Science domain. It is notable that these attempts use well-constructed Science environments such as Roundcube that are difficult to distinguish from the real environment. The phishing email often contains something about salary or a payment, which means that it is clicked a lot. C&CZ is doing everything it can to limit the nuisance as much as possible by blocking these phishing emails as much as possible.

We would like to ask you to be extra alert to e-mails and especially not to click on URL's asking you to enter your credentials for a salary payment, another payment, an account reset, etc. These are e-mails that are generally never sent by our departments. In the unlikely event that you have logged in somewhere that you later have doubts about, please report this as soon as possible to [C&CZ](/en/howto/contact).
