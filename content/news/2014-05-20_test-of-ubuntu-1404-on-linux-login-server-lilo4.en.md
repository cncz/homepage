---
author: visser
date: 2014-05-20 17:02:00
tags:
- medewerkers
- studenten
title: Test of Ubuntu 14.04 on Linux login server lilo4
---
As a test of the new version of Ubuntu, 14.04 LTS, we have installed a
new login server `lilo4.science.ru.nl`. In a few months it will replace
the five year old login server `lilo2`. The name `lilo`, that always
points to the newest/fastest login server, will then be moved from the
two year old `lilo3` to the new `lilo4`. The new `lilo4` is a Dell
PowerEdge R420 with 2 Xeon E5 2430L v2 processors with 6 cores. Because
we enabled hyper-threading, it appears to have 24 processors. It has 32
GB of memory. For users who want to check the fingerprint of the public
RSA key before supplying their Science-password to the new server:
`aa:ad:c0:2e:60:9d:d3:cd:ca:a4:59:7d:d0:d8:4c:68`. Please [report all
problems](/en/howto/contact/) with this new version of Ubuntu Linux.
