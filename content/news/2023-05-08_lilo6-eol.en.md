---
title: Lilo6 turned off
author: sioo
date: 2023-05-08
cover:
  image: img/2023/lilo6-eol.png
---
The login server `lilo6` with Ubuntu 18.04 has been turned off. Login servers are available during the support lifetime of the Linux version we install on them when a new long term support (LTS) version is released. We always provide current OS versions on the login servers, the version installed on our terminal PC's is also on the login server called `lilo.science.ru.nl`. When newer LTS versions are available we also install a new [login server](/en/howto/hardware-servers/#linux-login-servers).