---
author: wim
date: 2010-05-20 13:24:00
title: Microsoft Office 2007
---
C&CZ gaat Microsoft Office 2007 Enterprise gefaseerd invoeren op alle
[Beheerde Werkplekken](/nl/howto/windows-beheerde-werkplek/). Tot nu toe
is Office 2007 alleen op verzoek van afdelingen geïnstalleerd op hun
Beheerde Werkplekken, vanwege de totaal andere layout en
compatibiliteitsissues. Binnenkort wordt Office2007 op alle PC’s in het
Studielandschap geïnstalleerd, in de zomervakantie volgen alle andere
[PC-zalen](/nl/howto/terminalkamers/) die voor onderwijs gebruikt worden.

Afdelingen kunnen zich bij C&CZ melden om voor hun Beheerde Werkplekken
een automatische uitrol van Office2007 uit te laten voeren. Voor
zelf-beheerde PC’s kan men via de [install
netwerkdisk](http://www.cncz.science.ru.nl/software/installscience) de
software installeren. De benodigde licentiecodes worden door Postmaster
op verzoek gemaild naar uw facultaire of RU mail-adres.

Er is hulp beschikbaar voor de overstap naar Office2007:

-   Een [Learning
    Guide](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm)
    voor alle onderdelen van Office2007 (Word, Excel, PowerPoint,
    Outlook, Access) en ook voor Windows7. Deze is uitsluitend vanaf de
    campus te raadplegen.
-   De [informatiesite van het project Uitrol Office
    2007](http://www.ru.nl/gdi/projecten/office_2007/) van de
    [GebruikersDienst ICT](http://www.ru.nl/gdi)
-   Inloopsessies bij een docent met uitleg over Office2007 en de
    Learning Guide: donderdag 27 mei in E.1 2.50 (Erasmusgebouw,
    laagbouw zaal 2.50). Tijden: 10:00-11:00, 11:00-12:00, 14:00-15:00,
    15:00-16:00. Dinsdag 8 juni in Gymnasion GN3. Tijden: 10:00-11:00,
    11:00-12:00.
