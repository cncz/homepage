---
author: petervc
date: 2008-03-05 17:48:00
title: Surfnet/Internetverbinding onderbroken maandag 10 maart 18:00 uur-22:00 uur
---
Het [UCI](http://www.ru.nl/uci) zal a.s. maandagavond de verbinding
tussen de RU en SURFnet upgraden van 1 Gb/s naar 10 Gb/s. Hierdoor zal
tijdens deze werkzaamheden de verbinding tussen de RU en SURFnet
onderbroken zijn. Hierna zal C&CZ een upgrade van de FNWI router naar 10
Gb/s plannen.
