---
cpk_affected: netwerk en vaste telefoons
cpk_begin: &id001 2017-05-08 23:30:00
cpk_end: 2017-05-09 02:00:00
cpk_number: 1204
date: *id001
tags:
- medewerkers
- studenten
title: RU netwerkonderhoud (vnl. vaste telefoons)
url: cpk/1204
---
Tussen 23.30 en 00.30 uur zullen de vaste telefoons circa een half uur
buiten gebruik zijn. Tussen 00.30 en 02.00 uur vindt een aantal keren
een internetonderbreking van enkele minuten plaats. Op die momenten zijn
de RU-services vanaf locaties van buiten de campus niet te benaderen.
