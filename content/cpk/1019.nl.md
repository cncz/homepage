---
cpk_affected: Gebruikers van diskruimte/netwerkschijven op de Stack.
cpk_begin: &id001 2013-04-29 04:08:00
cpk_end: 2013-05-01 09:15:00
cpk_number: 1019
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver stack gecrashed door defecte harddisk
url: cpk/1019
---
Probleem: Crash door defecte disk Oplossing: Gebruik gemaakt van hot
spare door disk uit te laten vallen en reboot
