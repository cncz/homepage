---
cpk_affected: Ubuntu 12_04 LTS servers / Ubuntu 12 (or older) clients
cpk_begin: &id001 2018-09-11 07:20:00
cpk_end: 2018-09-11 10:25:00
cpk_number: 1240
date: *id001
tags:
- medewerkers
- studenten
title: SSH connections of older clients/servers
url: cpk/1240
---
A configuration change was automatically put into production Tuesday
morning which had been prepared the previous day on Ubuntu 14\_04 LTS,
Ubuntu 16\_04 LTS and Ubuntu 18\_04 LTS in order to increase the level
of security of SSH connections within FNWI. The change had the side
effect of crashing the SSH daemon on Ubuntu 12\_04 LTS and preventing
older SSH clients from connecting at all. Users are encouraged to update
their SSH clients so that we can [gradually increase the level of
security](https://wiki.mozilla.org/Security/Guidelines/OpenSSH) again.
