---
cpk_affected: netwerk in Huygens, zie locaties hieronder
cpk_begin: &id001 2014-11-06 06:00:00
cpk_end: 2014-11-06 06:15:00
cpk_number: 1112
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkonderhoud in deel Huygens
url: cpk/1112
---
Voor een software upgrade in de netwerkswitches in bovenstaande
gebouwen/verdiepingen/locaties zullen deze om exact 06:00 uur worden
gereboot. Dat betekent dat het vaste en draadloze datanetwerk gedurende
10-15 min onderbroken zal zijn en er dus geen netwerkverkeer mogelijk
is. Dat geldt ook voor het netwerk van toegangscontrole, klimaatregeling
etc.

Locaties:

-   Huygens vleugel 3 alle verdiepingen
-   Huygens vleugel 4 alle verdiepingen
-   Huygens vleugel 8 alle verdiepingen
-   Huygens centrale straat tussen vleugels 6 en 8 alle verdiepingen
