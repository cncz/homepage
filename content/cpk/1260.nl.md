---
cpk_affected: Diverse websites, slurm
cpk_begin: &id001 2020-05-18 06:30:00
cpk_end: 2020-05-18 12:00:00
cpk_number: 1260
date: *id001
tags:
- medewerkers
- studenten
title: Sperwer Database server failure
url: cpk/1260
---
Door een mislukte BIOS update is de hardware van de database server
blijven hangen en start niet meer op (brick). De functionaliteit van de
server is overgezet op de hardware van cn00, waardoor die nu uit de
lucht is. Als de hardware van sperwer weer in orde is, wordt de situatie
weer hersteld.

Update 19 mei 12:15 : hardware gerepareerd, situatie is weer hersteld
naar origineel
