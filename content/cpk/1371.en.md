---
title: Email sent to too many people
author: bram
cpk_number: 1371
cpk_begin: 2024-06-17 10:00:00
cpk_end: 2024-06-17 10:00:00
cpk_affected: Some people who requested Science logins
date: 2024-06-17
tags: []
url: cpk/1371
---
We apologize for the inconvenience, but due to a software error, an
(end date notification email)[https://cncz.science.ru.nl/en/howto/emailcodes/#login-3] was
mistakenly sent to too many recipients. Please disregard and delete the email.
If the email was intended for you, we will resend it next week. We are working on a fix to 
prevend this kind of error in the future.
