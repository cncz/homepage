---
title: cpu vervanging in vmhost06
author: Ben Polman
cpk_number: 1340
cpk_begin: 2023-08-16 16:30:00
cpk_end: 2023-08-16 17:00:00
cpk_affected: gitlab.pep, slurm, jitsi, indicoimapp, pep3/4, mariavm01, smtp2
cpk_frontpage: false
date: 2023-08-14
tags: []
url: cpk/1340
---
Aankondiging onderhoud. Er wordt a.s. woensdagmiddag een cpu vervangen in
vmhost06, een van onze virtuele machine servers. De volgende vms (en
daarvan afhankelijke diensten):

gitlab9 (pep)
slurm22
pep3
jitsivm
poliep
indicoimapp2vm
pep4
mariavm01
smtp2

zullen dus ongeveer een uur niet beschikbaar zijn. Ook diverse websites en
andere diensten die gebruik maken van de databases op mariavm01 (roundcube,
slurm, etc.) zijn ook niet beschikbaar.

