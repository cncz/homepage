---
cpk_affected: Gebruikers die Science mail lezen
cpk_begin: &id001 2018-08-20 15:32:00
cpk_end: 2018-08-20 15:45:00
cpk_number: 1237
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkprobleem IMAP-mailserver t.g.v. fout bij update
url: cpk/1237
---
Tijdens het uittesten van een mogelijke update van de IMAP mailserver,
zorgde een fout ervoor dat de mailserver niet bereikbaar was gedurende
ca. een kwartier.
