---
cpk_affected: Gebruikers van de gedeelde schijven
cpk_begin: &id001 2016-02-09 09:00:00
cpk_end: 2016-02-09 15:37:00
cpk_number: 1163
date: *id001
tags:
- medewerkers
- studenten
title: Fileservers in de problemen
url: cpk/1163
---
De Ubuntu 14.04 fileservers “flock” en “muckle” hadden enkele keren een
probleem. Nadat we terug gegaan zijn naar de vorige versie van de kernel
lijkt het weer stabiel te zijn.
