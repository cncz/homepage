---
cpk_affected: GitLab gebruikers
cpk_begin: &id001 2016-04-29 14:30:00
cpk_end: 2016-04-25 14:45:00
cpk_number: 1178
date: *id001
tags:
- medewerkers
- studenten
title: Gitlab backup per ongeluk teruggezet op productie server
url: cpk/1178
---
De backup file van “Fri Apr 29 05:07:04 CEST 2016” is per ongeluk op de
[GitLab server](/nl/howto/gitlab/) teruggezet, terwijl dit op een
testserver had moeten gebeuren. Hierdoor zijn alle aanpassingen in de
repositories, wiki’s etc. teruggezet naar de toestand van vanmorgen
05:07. Commits die vandaag gepusht zijn naar de gitlab server moeten dus
opnieuw gepusht worden. Excuses voor het ongemak.
