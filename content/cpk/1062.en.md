---
cpk_affected: Users of the network in Huygens wing 1, especially the ground floor.
cpk_begin: &id001 2013-12-17 19:00:00
cpk_end: 2013-12-17 23:59:00
cpk_number: 1062
date: *id001
tags:
- medewerkers
- studenten
title: Interruption of network in Huygens wing 1
url: cpk/1062
---
Tuesday night December 17 19:00-24:00 hours, maintenance work will be
done affecting the data network in Huygens wing 1. The wired and
wireless network will suffer service interruptions a few times during
that period. The main inconvenience will be on the ground floor.
