---
cpk_affected: all users using the wireless networks ru-wlan and eduroam.
cpk_begin: &id001 2013-06-06 22:00:00
cpk_end: 2013-06-06 23:59:00
cpk_number: 1022
date: *id001
tags:
- medewerkers
- studenten
title: Maintenance Wireless@RU
url: cpk/1022
---
On Thursday June 6th from 10:00 pm the wireless networks ru-wlan and
eduroam will be unavailable for at least 2 hours. All existing
connections will be cut off. The wireless network Science however will
not be effected and will be kept available.
