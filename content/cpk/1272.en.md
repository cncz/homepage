---
cpk_affected: Users wanting to manage their science account
cpk_begin: &id001 2021-01-25 07:15:00
cpk_end: 2021-01-25 07:45:00
cpk_number: 1272
date: *id001
title: DIY temporarily not usable
url: cpk/1272
---
<itemTags>medewerkers, studenten</itemTags>


Due to a management operation (planned around this time), the DIY
website was unusable. Since the time was very early, it's expected
nobody was inconvenienced by this temporary unavailability.
