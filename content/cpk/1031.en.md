---
cpk_affected: Windows B-Fac Users with roaming profiles
cpk_begin: &id001 2013-08-01 00:00:00
cpk_end: 2013-08-07 12:00:00
cpk_number: 1031
date: *id001
tags:
- medewerkers
- studenten
title: Windows Roaming Profile problem
url: cpk/1031
---
An error in distributing a Group Policy Object (GPO) caused roaming
profiles to fail for the last week. The start date/time of the problem
is unknown, This is an estimate.
