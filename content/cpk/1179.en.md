---
cpk_affected: GitLab users
cpk_begin: &id001 2016-05-02 21:30:00
cpk_end: 2016-05-03 10:15:00
cpk_number: 1179
date: *id001
tags:
- medewerkers
- studenten
title: Gitlab temporarily unavailable
url: cpk/1179
---
Because of a [major security
update](https://about.gitlab.com/2016/04/28/gitlab-major-security-update-for-cve-2016-4340/)
that will be released tonight, [GitLab](/en/howto/gitlab/) will not be
available. We will update GitLab on Tuesday morning. After that,
gitlab.science.ru.nl will be available again.
