---
cpk_affected: n.v.t. (geen overlast voor gebruikers verwacht)
cpk_begin: &id001 2014-11-17 16:30:00
cpk_end: 2014-11-17 17:00:00
cpk_number: 1117
date: *id001
tags:
- medewerkers
- studenten
title: Server nachtwacht preventief onderhoud
url: cpk/1117
---
De server nachtwacht heeft een disk die elk moment defect kan raken.
Daarom wordt deze per omgaande vervangen.
