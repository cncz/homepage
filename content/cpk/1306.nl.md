---
title: Uitgaande science mail niet mogelijk na succesvolle phishing aanval
author: bram
cpk_number: 1306
cpk_begin: 2022-12-05 21:17:00
cpk_end: 2022-12-06 10:00:00
cpk_affected: Gebruikers van de Science mailomgeving
date: 2022-12-06
tags: []
url: cpk/1306
---
Nadat meerdere personen in een phishing mail zijn getrapt zijn via Science accounts enkele tienduizenden emails verstuurd. Dit zorgt ervoor dat onze mailservers geblokkeerd kunnen worden door mailservers op het internet.
We zijn op dit moment bezig met het opschonen van SPAM mails die nog in de uitgaande wachtrijen staan. Daarna zullen de mailservers weer bereikbaar worden gemaakt.
We beraden ons op maatregelen om deze vorm van overlast te voorkomen.
