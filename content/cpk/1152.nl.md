---
cpk_affected: Alle gebruikers van de RU Internetverbinding
cpk_begin: &id001 2015-11-23 15:52:00
cpk_end: 2015-11-23 16:09:00
cpk_number: 1152
date: *id001
tags:
- medewerkers
- studenten
title: RU Internetverbinding down
url: cpk/1152
---
Gistermiddag om ca. 15:52 uur crashte een core router van
[SURFnet](http://www.surf.nl), waardoor de Internetverbinding van de RU
wegviel. Om 16:09 was de verbinding weer hersteld. Dit werd gisteren
gemeld op [een tijdelijke SURFnet
storingspagina](http://grotestoring.surfnet.nl/) en op de [ISC/CIM
storingspagina](http://www.ru.nl/systeem-meldingen/). Details zijn te
vinden op de [SURFnet Network Tickets
maillijst](https://list.surfnet.nl/pipermail/nettic/).
