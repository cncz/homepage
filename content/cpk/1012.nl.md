---
cpk_affected: Gebruikers van diskruimte op de Pile.
cpk_begin: &id001 2013-03-18 06:30:00
cpk_end: 2013-03-18 07:36:00
cpk_number: 1012
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Disk server pile offline
url: cpk/1012
---
Stond te wachten op interactieve input na waarschuwing (\^d)
