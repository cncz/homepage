---
title: Some network shares temporary unavailable
author: bram
cpk_number: 1364
cpk_begin: 2024-04-17 15:00:00
cpk_end: 2024-04-18 09:20:00
cpk_affected: "338 network shares of fileserver 'peck' (bit too much to mention all of them here)"
# cpk_frontpage: true
date: 2024-04-18
tags: []
url: cpk/1364
---
Some of our network shares were temporarily unavailable earlier due to a configuration error. The issue has been resolved and all shares should be accessible now. We want to assure you that the configuration error that occurred did not impact the integrity or security of the data stored on the file server.
We apologize for any inconvenience this may have caused.
In the meantime, we're working on making information about network shares readily available on [DIY](https://diy.science.ru.nl) for your reference. 
