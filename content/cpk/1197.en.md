---
cpk_affected: VPNsec users
cpk_begin: &id001 2017-02-22 15:30:00
cpk_end: 2017-02-22 16:00:00
cpk_number: 1197
date: *id001
tags:
- medewerkers
- studenten
title: VPNsec upgrade
url: cpk/1197
---
Update of Strongswan server software with extra capabilities
