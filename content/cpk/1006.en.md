---
cpk_affected: all FNWI users with a homedirectory on fileserver bundle
cpk_begin: 2013-01-16 13:30:00
cpk_end: 2013-01-16 14:00:00
cpk_number: 1006
date: 2013-01-16
tags:
- studenten
- medewerkers
- docenten
title: Homeserver bundle crashed
url: cpk/1006
---
Because the file server crashed, it had to be rebooted.
