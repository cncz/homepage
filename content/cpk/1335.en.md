---
title: Mailman disruption
author: Erik Joost Visser
cpk_number: 1335
cpk_begin: 2023-06-30 14:36:54
cpk_end: 2023-07-03 16:19:21
cpk_affected: Science mail users
date: 2023-07-03
tags: [mail]
url: cpk/1335
---
Last friday, a change in the mailman configuration has been rolled out which had the inadvertent effect that mails were not delivered to external addresses anymore. However, these mailman posts were sent successfully to internal Science mail addresses.

The change has been rolled back for the moment but is a necessity meaning that we're looking for another solution.
