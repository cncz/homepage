---
cpk_affected: ca 30 pc's in Studielandschap Huygens
cpk_begin: &id001 2016-01-20 12:57:00
cpk_end: 2016-01-20 13:25:00
cpk_number: 1158
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkstoring in deel studielandschap
url: cpk/1158
---
In Huygens vleugel 2 is tweemaal een netwerk switchmodule uitgevallen,
waardoor ca 30 pc’s in het Studielandschap tijdelijk geen netwerk
hadden. De switch module zal worden vervangen op 2016-01-21 om 07:30
uur.
