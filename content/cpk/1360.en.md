---
title: mailserver will be replaced Easter weekend
author: sioo
cpk_number: 1360
cpk_begin: 2024-03-22 16:58:00
cpk_end: 2024-03-31 13:15:00
cpk_affected: "Mail clients for science mail"
# cpk_frontpage: true
date: 2024-03-22
tags: []
url: cpk/1360
---
In Easter Weekend (March 30th 2024) the new mailserver will replace the old one. The final switch will take a short while, during which mail clients (eg Thunderbird, mail on your phone, etc.) will not be able to connect to the science mailserver. (This is a week later than announced, due to some issues that need to be resolved before final migration)

Central mail services (Microsoft `@ru.nl`) are independent of our mailserver and thus will not be affected at all.
