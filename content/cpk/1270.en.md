---
cpk_affected: Science mail aliases of more than 1024 characters
cpk_begin: &id001 2021-01-21 15:52:00
cpk_end: 2021-01-22 09:55:00
cpk_number: 1270
date: *id001
title: Very long mail aliases temporarily not usable
url: cpk/1270
---
<itemTags>medewerkers, studenten</itemTags>


A configuration change had as unwanted effect the disappearance of all
very long mail aliases. When this was reported next morning, it was
repaired immediately.
