---
cpk_affected: alle 25 gigabit aangesloten machines (shares, websites, clusternodes)
cpk_begin: &id001 2021-12-15 12:45:00
cpk_end: 2021-12-15 13:42:00
cpk_number: 1290
date: *id001
title: Verbroken verbinding naar nieuwe datacenter switches
url: cpk/1290
---
Door een menselijke fout is de verbinding tussen onze nieuwe datacenter
switches en de centrale router verbroken geweest.
