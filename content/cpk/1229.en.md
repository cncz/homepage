---
cpk_affected: users wanting a restore of a recently created/changed file
cpk_begin: &id001 2018-03-17 00:00:00
cpk_end: 2018-03-26 00:00:00
cpk_number: 1229
date: *id001
tags:
- medewerkers
- studenten
title: Multiple backup server issues
url: cpk/1229
---
Backups were incomplete since March 17th. The tape robot couldn’t be
used because a tapedrive failed, probably due to dry weather conditions.
Therefore holding disks filled up. Only the backup of Tuesday upon
Wednesday is possibly complete, but we lack backups of March 21st and
22nd, which is caused by a filesystem inconsistency of the holding
disks, repairs already take two days. We’re trying hard to make backups
this weekend, but please be cautious with your files, since restores of
recently changed files might fail.
