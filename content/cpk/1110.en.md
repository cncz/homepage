---
cpk_affected: network in Huygens wing 7 and in the central street between wings 5
  and 7, all floors.
cpk_begin: &id001 2014-10-29 19:00:00
cpk_end: 2014-10-29 23:00:00
cpk_number: 1110
date: *id001
tags:
- medewerkers
- studenten
title: Network maintenance in Huygens wing 7
url: cpk/1110
---
Because of an upgrade of the network switches in the
buildings/floors/locations mentioned above, these network switches will
be replaced. This means that both wired en wireless networks at these
locations will be interrupted and network traffic will not be possible.
This includes the network for access control, climate control etc.
