---
cpk_begin: &id001 2011-02-12 07:00:00
cpk_end: 2011-02-12 11:00:00
cpk_number: 983
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Planned Service: Limited computer services'
url: cpk/983
---
A backup cooling system will be installed in our main computer room.
Therefore the air conditioning system must be switched off, which means
that most of the computer facilities in this room must be shut down.
This includes the cluster nodes cn00 through cn53 and many of the web-
and file- (network share) servers. It is advised to expect a very
limited service level. We will try to keep all home directories and the
mail system available. For detailed information about the impact please
[contact](/en/howto/contact/) C&CZ.
