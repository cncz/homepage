---
cpk_affected: Gebruikers van de KM MFP's buiten het Huygensgebouw
cpk_begin: &id001 2013-12-09 12:48:00
cpk_end: 2013-12-09 13:40:00
cpk_number: 1061
date: *id001
tags:
- medewerkers
- studenten
title: Storing Konica Minolta MFP's buiten het Huygensgebouw
url: cpk/1061
---
C&CZ had van KM begrepen dat alle KM’s na de upgrade van de nacht van 4
december identiek zouden zijn. Toen C&CZ de DHCP-configuratie aanpaste,
bleek dat alleen voor de KM’s in het Huygensgebouw te gelden. Na herstel
van de DHCP-configuratie en herstart van de MFP was het probleem over.
Overigens dient de firmware upgrade voor deze machines dus nog te
volgen.
