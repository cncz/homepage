---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-03-27 10:40:00
cpk_end: 2014-03-27 11:00:00
cpk_number: 1076
date: *id001
tags:
- medewerkers
- studenten
title: Print/phpMyAdmin server in de problemen
url: cpk/1076
---
De server reageerde niet meer, oorzaak onbekend. Na een herstart was het
probleem verdwenen.
