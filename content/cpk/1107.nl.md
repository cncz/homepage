---
cpk_affected: vrijwel alle computers in FNWI
cpk_begin: &id001 2014-09-20 10:45:00
cpk_end: 2014-09-20 14:04:00
cpk_number: 1107
date: *id001
tags:
- medewerkers
- studenten
title: DNS resolver probleem
url: cpk/1107
---
De server die binnen FNWI als eerste [DNS
resolver](http://en.wikipedia.org/wiki/Domain_Name_System#DNS_resolvers)
genoemd staat, crashte. Hierdoor werd voor veel computers in FNWI het
netwerk vrijwel onbruikbaar. Pas na een reboot van deze server was het
probleem verholpen. Maatregelen om de overlast van zo’n crash te
verminderen en om zo’n crash minder waarschijnlijk te maken, zijn deels
getroffen en deels nog in voorbereiding.
