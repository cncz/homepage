---
cpk_affected: Users of the linux loginserver lilo
cpk_begin: &id001 2014-05-12 06:30:00
cpk_end: 2014-05-12 09:36:00
cpk_number: 1090
date: *id001
tags:
- medewerkers
- studenten
title: lilo.science.ru.nl offline
url: cpk/1090
---
Lilo did not shut down correctly during the planned weekly reboot. After
a hardware reset, the machine booted just fine
