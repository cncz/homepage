---
cpk_affected: Gebruikers analoge telefonieapparatuur.
cpk_begin: &id001 2018-06-09 08:00:00
cpk_end: 2018-06-09 17:00:00
cpk_number: 1233
date: *id001
tags:
- medewerkers
- studenten
title: Onderbreking delen telefonieservice
url: cpk/1233
---
Op zaterdag 9 juni vanaf 8:00u tot circa 12:00u zal er geen analoog
telefoonverkeer mogelijk zijn vanaf de AudioCodes bij FNWI. Dan wordt de
bestaande apparatuur waarvan in het Huygens gebouw en periferie gebruik
wordt gemaakt verplaatst en opnieuw aangesloten. Op deze AudioCodes zijn
uitsluitend analoge lijnen aangesloten zoals DECT telefoons, Faxen en
dergelijke. De groene toestellen (noodtelefoons) zijn niet op deze
AudioCodes aangesloten maar op de calamiteiten centrale.

Die calamiteiten centrale zal in de middag kort uit dienst gaan i.v.m.
het opnieuw aansluiten van de spanning. Dit zal ca 10 minuten duren. De
meeste groene telefoons en liften in het Huygensgebouw zijn hier op
aangesloten.
