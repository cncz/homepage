---
title: DNS broken for z.science.ru.nl
author: Miek Gieben
cpk_number: 1352
cpk_begin: 2023-10-21 16:32:00
cpk_end: 2023-10-21 18:50:00
cpk_affected: Users of .z.science.nl shares
# cpk_frontpage: true
date: 2023-10-23
tags: []
url: cpk/1352
---
Due to a misconfiguration of the DNS in the z.science.ru.nl zone, all shares were not available
during the outage.

Extra tests are added to prevent a future occurrence.
