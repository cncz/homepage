---
title: Radboud internet 30 minuten down voor onderhoud op vrijdag 21 april tussen 18:30 en 20:30 uur
author: petervc
cpk_number: 1326
cpk_begin: 2023-04-21 18:30:00
cpk_end: 2023-04-21 20:30:00
cpk_affected:  netwerkgebruikers
date: 2023-04-20
tags: []
url: cpk/1326
---
RU ILS Connectivity kondigde aan dat het RU netwerk op vrijdag 21 april tussen 18:30 en 20:30 een half uur afgesloten zou zijn van internet, omdat er onderhoudswerkzaamheden verricht worden.
