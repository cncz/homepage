---
cpk_affected: network Huygens building, incl wireless and IP telephony
cpk_begin: &id001 2015-01-17 07:54:00
cpk_end: 2015-01-17 09:11:00
cpk_number: 1126
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Power dip Huygens building Saturday, January 17
url: cpk/1126
---
Saturday morning, during maintenance by [UVB](http://www.ru.nl/uvb) and
[Liander](https://www.liander.nl/), there were a few short unintended
power dips in the Huygens building. A large part of the network switches
does not have a
[UPS](http://en.wikipedia.org/wiki/Uninterruptible_power_supply) and
thus restarted. After 09:11 all switches were operational again.

Background: January 10 and 17, maintenance has been carried out on the
power transformers in the Huygens building. This work should be carried
out once every 5 years. Because a power failure of 6-8 hours (duration
of maintenance) is unacceptable for the Faculty of Science, last year
bypasses have been constructed, to make the transformers able to replace
each other. Beforehand, the chance that a power failure should occur,
was thought to be particularly small. The next time such a service is
planned, in 2020, everyone will be informed in advance.
