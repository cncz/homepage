---
title: Posterprinter kamerbreed defect
author: petervc
cpk_number: 1321
cpk_begin: 2023-03-20 12:00:00
cpk_end: 2023-04-11 09:00:00
cpk_affected: gebruikers van de posterprinter kamerbreed
date: 2023-03-24
tags: []
url: cpk/1321
---
Op 20 maart begon de posterprinter kamerbreed slechte kleuren te produceren. De nieuwe printkoppen die een paardagen later binnen kwamen, hebben het probleem niet opgelost.
Daarom hebben we gebeld voor een reparatie, die waarschijnlijk in de week van 17 april gepland zal worden.

In de tussentijd kunnen gebruikers die posters nodig hebben terecht bij [Radboud Post&Print](https://www.ru.nl/services/campusvoorzieningen/winkels/post-print).
