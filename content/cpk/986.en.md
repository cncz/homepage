---
cpk_affected: Users of fileserver plenty
cpk_begin: &id001 2011-07-07 13:00:00
cpk_end: 2011-07-07 14:27:00
cpk_number: 986
date: *id001
tags:
- studenten
- medewerkers
title: Windows server "plenty" with xpsoftware unavailable
url: cpk/986
---
Thursday July 7, around 13.00 hours the server “plenty” could not be
reached. Because this server serves the “xpsoftware” share for the
[Managed Windows PC’s](/en/howto/windows-beheerde-werkplek/), all these
PC’s had a problem. After the server was restarted and the disks had
been checked, it was available again at 14:26.
