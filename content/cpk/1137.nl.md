---
cpk_affected: Medewerkers die geauthenticeerd mail versturen via smtp.science.ru.nl
cpk_begin: &id001 2015-06-13 00:00:00
cpk_end: 2015-07-15 00:00:00
cpk_number: 1137
date: *id001
tags:
- medewerkers
title: SMTP server problemen vanwege te korte sleutel
url: cpk/1137
---
Sinds een maand konden mailprogramma’s een update krijgen vanwege
kwetsbaarheid voor een [LogJam](https://weakdh.org/) aanval. Hierna
weigerden ze een verbinding met een server met een te korte
beveiligingssleutel. De oude [authenticated
smtp-server](/nl/howto/email#uitgaande-mail) van C&CZ had zo’n te korte
sleutel. Gebruikers die dit probleem meldden (met de nieuwste versies
van Pine, Thunderbird en sinds kort ook Apple Mail), is aangeraden
tijdelijk een alternatieve server te gebruiken. Per vandaag is het
gebruik van die alternatieve server niet meer nodig, omdat de server
smtp.science.ru.nl vervangen is door een nieuwe server.
