---
cpk_affected: websites op deze server (wielewaal)
cpk_begin: &id001 2016-02-01 06:30:00
cpk_end: 2016-02-01 09:20:00
cpk_number: 1160
date: *id001
tags:
- medewerkers
- studenten
title: 'Webserver down: tftp & egw'
url: cpk/1160
---
Waardoor de agenda en enkele andere websites voor ICIS niet beschikbaar
waren. Daarnaast was er overlast voor het boot proces van werkplekken
(i.v.m. tftp). Reboot van de server lijkt voldoende als oplossing.

Getroffen websites:

`blackgem brainhealth croatia14 croatia15 cs eduspec eduspecorg egw`
`ethergids fbkr kickstart mhealth oni snn`
