---
cpk_affected: Users who installed Adobe Acrobat Pro DC from the install disc
cpk_begin: &id001 2017-12-02 16:20:00
cpk_end: 2017-12-05 16:14:00
cpk_number: 1222
date: *id001
tags:
- medewerkers
title: Incorrect Adobe Acrobat Pro DC package on install disc
url: cpk/1222
---
We placed an incorrectly packaged version on the install drive. After
users reported this, the ISC has created a new package and we have
replaced it.
