---
cpk_affected: Users in Huygens wing 5
cpk_begin: &id001 2014-02-03 19:00:00
cpk_end: 2014-02-03 23:59:00
cpk_number: 1069
date: *id001
tags:
- medewerkers
title: Network interruptions in Huygens wing 5
url: cpk/1069
---
On Monday evening, February 3rd between 19:00 - 24:00 (7:00 - 12:00 pm)
maintanance will be carried out on the network devices in Huygens Wing
5. Therefore the wired and wireless networks will not be available at
some moments at all locations in this Wing, on all floors.
