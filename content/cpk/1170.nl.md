---
cpk_affected: Gebruikers van shares/netwerk isolator
cpk_begin: &id001 2016-03-07 06:30:00
cpk_end: 2016-03-07 09:00:00
cpk_number: 1170
date: *id001
title: Server isolator geen private class netwerken
url: cpk/1170
---
Door onbekende redenen waren de interfaces voor de private class
netwerken niet correct opgebracht. Geinitialiseerd dat de grub boot
procedure faalde. Oplossing: Reboot
