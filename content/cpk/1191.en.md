---
cpk_affected: Users of the poster printer "kamerbreed"
cpk_begin: &id001 2016-12-02 00:00:00
cpk_end: 2016-12-22 00:00:00
cpk_number: 1191
date: *id001
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" broken
url: cpk/1191
---
The printer has to be repaired. Spare parts have been ordered. We hope
to have the printer operational again this month.
