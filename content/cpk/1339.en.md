---
title: motherboard replacement vmhost06
author: sioo
cpk_number: 1339
cpk_begin: 2023-08-01 13:20:00
cpk_end: 2023-08-01 15:10:00
cpk_affected: gitlab.pep, slurm, jitsi, indicoimapp, pep3/4, mariavm01, smtp2
date: 2023-08-01
tags: []
url: cpk/1339
---

Apologies for the short notice, we are now going to replace the motherboard of
one of our main vmhost servers, meaning vms

gitlab9 (pep)
slurm22
pep3
jitsivm
poliep
indicoimapp2vm
pep4
mariavm01
smtp2


will be down for up to 1 hour. several services depend on the mariavm01
(websites, slurm), so they are affected too.
