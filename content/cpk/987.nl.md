---
cpk_affected: Website-databases en misschien Linux clients
cpk_begin: &id001 2012-04-20 17:00:00
cpk_end: 2012-04-20 17:15:00
cpk_number: 987
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Gepland onderhoud: Website-databases en misschien Linux clients'
url: cpk/987
---
Een defecte harde schijf van een server is vervangen, maar de server
moet gereboot worden om zeker te stellen dat dit reboot-bestendig is.
Hierdoor zullen de MySQL-databases van ruwweg 70 websites even
onbereikbaar zijn. Omdat deze server ook de Kerberos-authenticatie van
Linux-clients verzorgt, kunnen Linux-clients ook even overlast
ondervinden.
