---
cpk_affected: 'Gebruikers van licenties van: Ansys Accelrys Genesys Idapro IntelCompiler
  Mathematica Matlab Orcad Originlab'
cpk_begin: &id001 2015-01-08 17:30:00
cpk_end: 2015-01-08 17:45:00
cpk_number: 1125
date: *id001
tags:
- medewerkers
- studenten
title: Upgrade van licentieserver
url: cpk/1125
---
``

De licentieserver heeft nog Ubuntu 10.04. Dat heeft als nadeel dat er
over enkele maanden geen security updates meer voor uitkomen. Ook willen
we de server taken als DNS/DNSSEC/DHCP-server geven, waarvoor Ubuntu
14.04 noodzakelijk is. Omdat het een licentieserver is, kan niet
overgegaan worden naar een nieuwe server, maar worden disks met het
nieuwe OS in de bestaande server geplaatst. Omdat dit maar weinig
downtijd betekent, zal de overlast minimaal zijn.
