---
title: FNWI en Radboud Nieuwsbrief september in Spam
author: petervc
cpk_number: 1343
cpk_begin: 2023-08-31 00:00:00
cpk_end: 2023-09-14 16:34:00
cpk_affected: Gebruikers die de FNWI Nieuwsbrief en het Radboud Weekbericht op Science mailservers ontvangen
date: 2023-09-07
tags: []
url: cpk/1343
---

De FNWI Nieuwsbrief wordt gestuurd vanaf een @ru.nl mail-adres. Onlangs is de @ru.nl mailservice
aangepast door [Exchange Online Protection (EOP)](https://learn.microsoft.com/nl-nl/microsoft-365/security/office-365-security/eop-about?view=o365-worldwide)
toe te voegen. Voor de RU is EOP de opvolger van het [Proofpoint e-mail beveiligingsfilter](https://www.proofpoint.com/uk).
EOP voegde een mail headerregel toe aan de nieuwsbrief:

`List-Unsubscribe: https://c8ce19ec62454d21b73b7d5a25559d8f.svc.dynamics.com/t/lu/7...`

waardoor de mail beoordeeld werd als spam door het Science spamfilter SpamAssassin.
`1.1 URI_HEX                URI: URI hostname has long hexadecimal sequence`

Toen we dat doorhadden, hebben we het afzenderadres communications-science@ru.nl op de welkomlijst gezet op 7 september,
zodat die mail nooit vals positief als spam aangemerkt wordt.
Op 14 september is ook communicatie@ru.nl op de welkomlijst gezet, omdat het Radboud Weekbericht hetzelfde probleem heeft.
