---
cpk_affected: Various services
cpk_begin: &id001 2016-03-29 10:43:00
cpk_end: 2016-03-29 11:20:00
cpk_number: 1174
tags:
- medewerkers
- studenten
date: *id001
title: Power failure
url: cpk/1174
---

Several services were inaccessible due to power loss of a rack switch
caused by a failing PDU (power distribution unit). The PDU has been
temporarily replaced.

Sevices include:

home directories, printing, authentication services, licenses, vpn,
websites, databases, mailman and dns resolvers.
