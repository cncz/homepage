---
cpk_affected: alle gebruikers die Science-mail willen lezen
cpk_begin: &id001 2016-02-15 11:20:00
cpk_end: 2016-02-15 13:45:00
cpk_number: 1164
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mailserver probleem
url: cpk/1164
---
Door nog onbekende oorzaak was de IMAP mailserver niet beschikbaar. Mail
diensten bleken na verloop van tijd te stoppen vanwege loggen van
mailactiviteit. Als tijdelijke workaround is voor UDP ipv TCP logging
gekozen.
