---
cpk_affected: 'Gebruikers van deze U: / home server'
cpk_begin: &id001 2013-09-30 13:46:00
cpk_end: 2013-09-30 14:02:00
cpk_number: 1048
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server bundle in de problemen'
url: cpk/1048
---
De server was gecrasht. Na een herstart was het probleem verdwenen.
