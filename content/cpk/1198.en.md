---
cpk_affected: Users of Eduroam who authenticate using their Science login
cpk_begin: &id001 2017-02-28 06:30:00
cpk_end: 2017-02-28 09:30:00
cpk_number: 1198
date: *id001
tags:
- medewerkers
- studenten
title: Science Radius down
url: cpk/1198
---
The Radius service didn’t start after the regular reboot, restarting the
Radius service solved the problem.
