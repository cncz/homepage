---
cpk_affected: alle gebruikers die Science-mail wilden lezen
cpk_begin: &id001 2014-05-27 14:05:00
cpk_end: 2014-05-27 14:10:00
cpk_number: 1095
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mailserver probleem
url: cpk/1095
---
De IMAP server was overbelast. Het was nodig de IMAP service te
herstarten.
