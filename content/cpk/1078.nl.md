---
cpk_affected: 'Gebruikers van deze U: / home server'
cpk_begin: &id001 2014-04-04 11:35:00
cpk_end: 2014-04-04 12:25:00
cpk_number: 1078
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server bundle in de problemen'
url: cpk/1078
---
Sinds de snapshots van gisteren 13:00 uur bleven er steeds meer
processen hangen. De eerste klachten hierover kwamen pas vanochtend ca.
11:15 uur bij C&CZ. Daarom is ca. 11:35 besloten de server te
herstarten. Na de herstart was het probleem verdwenen. Het aantal
snapshots wordt sterk verminderd, met het doel om volgende storingen
t.g.v. snapshots te voorkomen.
