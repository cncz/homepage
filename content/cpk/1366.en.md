---
title: license server down
author: sioo
cpk_number: 1366
cpk_begin: 2024-04-24 10:16:00
cpk_end:   2024-04-24 10:54:00
cpk_affected: licenses Cadence Cst Genesys Hdlworks IntelCompiler Labview Maple Mathematica Matlab Mentorgraphics Moe New Originlab Synopsys Xilinx
# cpk_frontpage: true
date: 2024-04-24
tags: []
url: cpk/1366
---
Due to some unexpected reaction to a config change, the licenseserver for
several of our licenses was misconfigured for about 40minutes. The change was
reverted, but it took long enough for users to run into license problems.
