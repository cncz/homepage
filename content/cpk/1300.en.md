---
cpk_affected: some mail couldn't be delivered
cpk_begin: &id001 2022-09-29 19:27:00
cpk_end: 2022-09-30 00:00:00
cpk_number: 1300
date: *id001
title: Mail problems because of spam run after phishing
url: cpk/1300
---
Some time ago, some Science users fell for a phishing mail, therefore
their account could be misused by spammers to send mail through our
mailservers. Therefore our mailservers ended up in blacklists yesterday
evening. Some mails could therefore not be delivered to other servers,
that mail was returned to the sender. This morning we removed the IP
addresses of our servers from several blacklists, we think that the
problems are resolved now.
