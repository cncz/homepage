---
cpk_affected: all RU/UMCN users
cpk_begin: &id001 2013-10-22 10:55:00
cpk_end: 2013-10-22 11:00:00
cpk_number: 1051
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Power dip October 22, ca. 11:00
url: cpk/1051
---
Tuesday morning around 11 o’clock, there was a [short power dip for
RU/UMCN](http://www.gelderlander.nl/regio/rijk-van-nijmegen/stroomstoringen-in-malden-en-nijmegen-1.4062999).
This power dip, that is not listed on the [power interruption
website](http://www.liander.nl/liander/storingen_onderbrekingen/actueel_storingsoverzicht.htm?postcode=6525ED&page=1&ordering=SortDatumVan&skipFlip=&sortPlaats=inactive&sortDatumVan=down&sortDatumTot=inactive&sortTypeOnderbreking=inactive&sortUitgevallenComponenten=inactive&sortOorzaak=inactive&sortEnergiesoort=inactive&sortGetroffenAfnemers=inactive&toonAlles=false&invoerDatumStart=22-10-2013&invoerDatumEind=22-10-2013&energiesoort=E&energietype=option3),
made all systems restart that were not on emergency power. Because only
the network switches in Huygens wing 1 and 7 are on emergency power, a
lot of users lost their connection to the network, including wireless
and IP-telephony, for about 5 minutes. Apparatus that restarted faster
than the network, might have needed an extra restart to restore the
connection to the network. A department reported that a departmental
printer did not survive the power dip.
