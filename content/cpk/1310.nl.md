---
title: Vleugel 6 netwerkonderhoud maandag 16 januari 19:00-23:00
author: petervc
cpk_number: 1310
cpk_begin: 2023-01-16 19:00:00
cpk_end: 2023-01-16 23:00:00
cpk_affected: Bedrade en draadloze netwerkgebruikers in en om vleugel 6 van het Huygensgebouw
date: 2023-01-12
tags: []
url: cpk/1310
---
ILS Connectivity kondigde aan dat aanstaande maandagavond netwerkonderhoud zal worden uitgevoerd dat
korte verstoringen zal veroorzaken voor bedrade en draadloze systemen in en om vleugel 6 van het Huygensgebouw.
Voor bedrade netwerkaansluitingen kan men het outletnummer op de muur controleren (beginnend met 112- voor getroffen systemen)
of dat op te zoeken in de [FNWI ethergids](https://cncz.science.ru.nl/nl/howto/netwerk-ethergids/).
