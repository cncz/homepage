---
cpk_affected: alle RU/UMCN gebruikers
cpk_begin: &id001 2013-10-22 10:55:00
cpk_end: 2013-10-22 11:00:00
cpk_number: 1051
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Stroomstoring 22 oktober rond 11 uur
url: cpk/1051
---
Dinsdagochtend rond 11 uur was er een [kortdurende stroomstoring voor
RU/UMCN](http://www.gelderlander.nl/regio/rijk-van-nijmegen/stroomstoringen-in-malden-en-nijmegen-1.4062999).
Deze storing, die niet vermeld staat op de [stroomstoringen
website](http://www.liander.nl/liander/storingen_onderbrekingen/actueel_storingsoverzicht.htm?postcode=6525ED&page=1&ordering=SortDatumVan&skipFlip=&sortPlaats=inactive&sortDatumVan=down&sortDatumTot=inactive&sortTypeOnderbreking=inactive&sortUitgevallenComponenten=inactive&sortOorzaak=inactive&sortEnergiesoort=inactive&sortGetroffenAfnemers=inactive&toonAlles=false&invoerDatumStart=22-10-2013&invoerDatumEind=22-10-2013&energiesoort=E&energietype=option3),
zorgde ervoor dat alle systemen die niet op noodstroom aangesloten
waren, herstarten. Omdat de netwerkswitches alleen in vleugel 1 en 7 op
noodstroom zitten, verloren ook veel gebruikers gedurende ca. 5 minuten
de toegang tot het netwerk, waaronder draadloos en IP-telefoons.
Apparaten die sneller herstart waren dan het netwerk, kunnen nog een
latere herstart nodig gehad hebben om weer toegang tot het netwerk te
krijgen. We hebben ook van een afdeling gehoord dat een afdelingsprinter
bij deze spanningsdip niet overleefd heeft.
