---
title: DHCP server down vanwege configuratieprobleem
author: petervc
cpk_number: 1354
cpk_begin: 2023-11-09 13:28:15
cpk_end: 2023-11-09 14:10:15
cpk_affected: enkele netwerk pc's (die aangezet werden of met een verlopen IP-adres)
date: 2023-11-09
tags: []
url: cpk/1354
---
Een tikfout in de DHCP-configuratie had tot effect dat sommige netwerk
pc's geen IP-adres kregen toen ze aangezet werden en dat andere, waarvan
de lease van het IP-adres verlopen was, hun IP-adres verloren en daarmee
de toegang tot het netwerk.

We zullen het proces verbeteren zodat een tikfout in de toekomst de DHCP-server niet meer down brengt.
