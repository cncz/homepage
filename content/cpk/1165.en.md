---
cpk_affected: 'all users wanting to read Science mail '
cpk_begin: &id001 2016-02-22 06:30:00
cpk_end: 2016-02-22 08:03:00
cpk_number: 1165
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mail server problem
url: cpk/1165
---
For unknown reasons, the IMAP mail server was unavailable. The service
cyrus-imapd was not correctly started. Starting this service
**service cyrus-imapd start** solved the problem.
