---
title: Roundcube problem
author: Ben Polman
cpk_number: 1348
cpk_begin: 2023-09-25 17:30:00
cpk_end: 2023-09-26 09:20:00
cpk_affected: Roundcube users
cpk_frontpage: true
date: 2023-09-26
tags: []
url: cpk/1348
---
After an upgrade of Roundcube it was temporarily not possible to send e-mails.

After a small change in the configuration file the problem was resolved.

