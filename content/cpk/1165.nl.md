---
cpk_affected: alle gebruikers die Science-mail willen lezen
cpk_begin: &id001 2016-02-22 06:30:00
cpk_end: 2016-02-22 08:03:00
cpk_number: 1165
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mailserver probleem
url: cpk/1165
---
Door nog onbekende oorzaak was de IMAP mailserver niet beschikbaar. De
service cyrus-imapd was niet correct gestart. Opstarten van de service
**service cyrus-imapd start** loste het probleem op.
