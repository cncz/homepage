---
cpk_affected: gebruikers van gitlab
cpk_begin: &id001 2016-02-26 09:00:00
cpk_end: 2016-02-26 10:45:00
cpk_number: 1168
tags:
- medewerkers
- studenten
date: *id001
title: Gitlab upgrade
url: cpk/1168
---

Vanwege een upgrade van de [GitLab](/nl/howto/gitlab/) server naar versie
8.5.1 is deze tijdelijk niet beschikbaar.
