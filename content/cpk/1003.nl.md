---
cpk_affected: alle FNWI gebruikers met homedirectories op de bundle
cpk_begin: 2012-10-24 12:45:00
cpk_end: 2012-10-24 13:00:00
cpk_number: 1003
date: 2012-10-24
title: Homeserver bundle wordt gereboot
url: cpk/1003
---
Omdat de fileserver weigert een reservedisk te accepteren, moet het
gereboot worden.
