---
cpk_affected: Users of licenses
cpk_begin: &id001 2015-05-11 06:30:00
cpk_end: 2015-05-11 09:11:00
cpk_number: 1130
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: License server reboot problem
url: cpk/1130
---
Today the license server booted without configuring the network. The
cause of the problem was an error in a configuration file. This has been
corrected.

Affected licenses:

`Altera Ansys Accelrys Genesys Hdlworks Idapro`
`Xilinx IntelCompiler Ingr Maple Mathematica Matlab MentorGraphics Mestrelab`
`Topspin Orcad Originlab`
