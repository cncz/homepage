---
cpk_affected: Gebruikers buiten de campus die Science services wilden gebruiken
cpk_begin: &id001 2019-05-14 16:13:00
cpk_end: 2019-05-15 21:22:00
cpk_number: 1247
date: *id001
tags:
- medewerkers
- studenten
title: Storing DNS nameservice voor Science-domeinen
url: cpk/1247
---
Gistermiddag is er een DNS fout gemaakt met science.ru.nl (vanwege de
invoering van het veilige DNSSEC), waardoor science.ru.nl van het
internet verdween voor veel gebruikers buiten de campus. ’s Avonds is
deze fout gecorrigeerd. Vanwege het tijdelijk onthouden van
DNS-antwoorden (caching) zullen pas vanavond de laatste problemen
hiermee opgelost zijn. In de tussentijd kan men evt. omwegen gebruiken
als het herstarten van de thuisrouter en pc, het gebruik van een ander
netwerk (mobiele provider) of rainloop.science.ru.nl gebruiken in plaats
van roundcube.science.ru.nl voor Science webmail.
