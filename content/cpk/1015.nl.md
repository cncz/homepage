---
cpk_affected: Gebruikers van diskruimte op de Pile (homedisks).
cpk_begin: &id001 2013-04-08 06:30:00
cpk_end: 2013-04-08 08:15:00
cpk_number: 1015
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Disk server pile offline
url: cpk/1015
---
Kernel panic tijdens wekelijkse reboot; stond te wachten in console
Oplossing: Power-cycle van het systeem
