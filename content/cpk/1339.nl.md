---
title: motherboard replacement vmhost06
author: sioo
cpk_number: 1339
cpk_begin: 2023-08-01 13:20:00
cpk_end: 2023-08-01 15:10:00
cpk_affected: gitlab.pep, slurm, jitsi, indicoimapp, pep3/4, mariavm01, smtp2
date: 2023-08-01
tags: []
url: cpk/1339
---
Excuses voor de late waarschuwing. Er wordt momenteel een nieuw moederbord in
de vmhost06, een van onze virtuele machine servers, gezet. De volgende vms (en
daarvan afhankelijke diensten):

gitlab9 (pep)
slurm22
pep3
jitsivm
poliep
indicoimapp2vm
pep4
mariavm01
smtp2

zullen dus ongeveer een uur niet beschikbaar zijn. Ook diverse websites en
andere diensten die gebruik maken van de databases op mariavm01 (roundcube,
slurm, etc.) zijn ook niet beschikbaar.

