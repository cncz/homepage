---
cpk_affected: alle FNWI gebruikers met homedirectories op de bundle
cpk_begin: &id001 2013-05-21 17:00:00
cpk_end: 2013-05-21 17:27:00
cpk_number: 1020
date: *id001
tags:
- medewerkers
- studenten
title: Homeserver bundle crashte
url: cpk/1020
---
De fileserver is gecrashed. Na een reboot werkte alles weer.
