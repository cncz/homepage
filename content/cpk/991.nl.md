---
cpk_affected: 'Science mail gebruikers die mail wilden sturen naar MS-domeinen: hotmail.com,
  live.com, ...'
cpk_begin: &id001 2012-07-11 03:08:00
cpk_end: 2012-07-11 14:55:00
cpk_number: 991
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Uitgaande mailserver op zwarte lijst MS Live Hotmail
url: cpk/991
---
Vanochtend werd gemeld dat mail vanaf smtp.science.ru.nl naar
hotmail-gebruikers niet door hotmail mailservers geaccepteerd werd. We
hebben geprobeerd om de hotmail beheerders dit snel te laten wijzigen,
maar toen dat niet lukte onze smtp-server maar een nieuw IP-nummer
gegeven, dat niet geblokkeerd is.
