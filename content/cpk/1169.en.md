---
cpk_affected: CnCZ
cpk_begin: &id001 2016-03-07 06:30:00
cpk_end: 2016-03-07 07:45:00
cpk_number: 1169
date: *id001
title: emoe stuck at reboot
url: cpk/1169
---
At reboot time some file system (/opt) was not recognized, resulting in
a kernel panic. Solutio: Reboot
