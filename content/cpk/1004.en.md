---
cpk_affected: Users of horde webmail and users wanting to send mail to e.g. hotmail.com
cpk_begin: 2012-11-16 04:45:00
cpk_end: 2012-11-17 12:00:00
cpk_number: 1004
date: 2012-11-16
tags:
- studenten
- medewerkers
- docenten
- mail
title: Mail problems after supplying password to phishers
url: cpk/1004
---
Horde webmail again appeared to be misused for sending spam. This could
happen because a naive user gave the Science password to
phishers/spammers. After first stopping horde, early Friday morning we
disabled the account of the naive user and restarted horde. Saturday
morning it appeared that this short spam-outbreak had caused
administrators of hotmail.com to add our mail server to their blacklist.
Therefore we switched the IP-number of this mail server Saturday
morning.
