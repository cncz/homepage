---
cpk_affected: CN69  - CN83, archimedes, nauta, hubel, homedcc
cpk_begin: &id001 2016-11-14 20:40:00
cpk_end: 2016-11-14 22:50:00
cpk_number: 1189
date: *id001
title: Diverse machines down door defecte stroomverdeler
url: cpk/1189
---
Door een defecte stroomverdeler (PDU) is een switch stroomloos geraakt.
Daardoor verloren genoemde machines hun netwerk connectiviteit. Na
omstekkeren van de switch naar een andere PDU waren de machines weer
bereikbaar.
