---
cpk_affected: Eduroam draadloos in o.a. alle FNWI-gebouwen
cpk_begin: &id001 2022-09-27 23:30:00
cpk_end: 2022-09-28 00:30:00
cpk_number: 1298
date: *id001
title: 'Eduroam: dinsdag 27 september 23:30 1 uur down voor onderhoud'
url: cpk/1298
---
ILS netwerkbeheer deelde mee: op dinsdag 27 september om 23:30 zal het
Eduroam draadloze netwerk down zijn voor onderhoud. Op de meeste
plaatsen zal het onderhoud minimaal 30 en maximaal 60 minuten duren.
