---
cpk_affected: Eduroam-gebruikers op de campus
cpk_begin: 2020-07-01 00:00:00
cpk_end: 2020-07-10 00:00:00
cpk_number: 1267
title: Eduroam-probleem op de campus
date: 2020-07-01 00:00:00
url: cpk/1267
---
Het ISC deelde mee: Om beveiligingsredenen wordt vrijdagavond 10 juli
het certificaat van de wifi-server vervangen. Dit heeft gevolgen voor
het verbinden van je mobile device met Eduroam als je op de campus bent:

• Als je de melding krijgt dat je het nieuwe certificaat moet accepteren
om gebruik te maken van Eduroam, kies dan voor ‘ja’. Je kunt dan weer
gewoon gebruik maken van Eduroam;

• Als je deze melding niet krijgt én geen verbinding met Eduroam kunt
maken, kies dan voor het draadloze netwerk ‘eduroam-config’. Accepteer
de voorwaarden. Volg de instructies om Eduroam opnieuw te installeren.

Meer informatie vind je ook via [www.ru.nl/wifi](https://www.ru.nl/wifi)
(hier heb je een internetverbinding voor nodig).

Met vragen kun je terecht bij de ICT Helpdesk (024 – 36 22222).
