---
cpk_affected: fnwi login server users
cpk_begin: &id001 2015-10-05 06:30:00
cpk_end: 2015-10-05 07:45:00
cpk_number: 1146
date: *id001
tags:
- medewerkers
- studenten
title: Server lilo4 Monday morning reboot failed
url: cpk/1146
---
The server lilo4 stalled at shutdown during the monday morning reboot.
Solution: power cycle. Cause currently unknown.
