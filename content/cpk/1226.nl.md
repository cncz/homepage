---
cpk_affected: Science gebruikers die mail wilden versturen via smtp.science.ru.nl
cpk_begin: &id001 2018-02-08 02:39:00
cpk_end: 2018-02-08 15:39:00
cpk_number: 1226
date: *id001
tags:
- medewerkers
title: Probleem met uitgaande mailserver (smtp)
url: cpk/1226
---
Gisterochtend kregen we alarm omdat de smtp-service niet goed werkte. De
oorzaak bleek te zijn dat vanaf diverse PC’s uit o.a. China verbindingen
werden opgebouwd met een van de smtp-servers, zonder daadwerkelijk een
mail te sturen, waardoor al deze verbindingen bleven hangen. Bij een te
groot aantal staande verbindingen werd de server onbruikbaar voor
anderen. Deze service moet vanwege [authenticated
SMTP](../../howto/email#uitgaande-mail) vanaf het Internet bereikbaar zijn.
Nadat eerst enkele PC’s geblokkeerd waren, is om 15:39
[Fail2ban](https://www.fail2ban.org) aangezet, zoals dat ook gebruikt
wordt ter bescherming van andere services die vanaf het Internet
bereikbaar zijn, zoals [mailservers](/nl/howto/hardware-servers/), [Linux
loginservers](/nl/howto/hardware-servers/),
[WWW-servers](/nl/howto/hardware-servers/) en [GitLab
servers](/nl/howto/hardware-servers/).
