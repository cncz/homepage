---
cpk_affected: Users of the printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-10 12:54:00
cpk_end: 2014-04-10 13:15:00
cpk_number: 1079
date: *id001
tags:
- medewerkers
- studenten
title: Again Print/phpMyAdmin server problem
url: cpk/1079
---
Just like two weeks ago, the server didn’t react for unknown reasons. A
reboot of the machine solved the problem. Together with the supplier we
will try to find out what to replace to prevent this in the future.
