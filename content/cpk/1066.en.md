---
cpk_affected: Users in Huygens wing 1 and corridors between wings 1 and 3
cpk_begin: &id001 2014-01-13 19:00:00
cpk_end: 2014-01-13 23:59:00
cpk_number: 1066
date: *id001
tags:
- medewerkers
title: Network interruptions in Huygens wing 1
url: cpk/1066
---
On Monday evening January 13 between 7:00 pm and 12:00 pm maintanance
will be carried out on the network devices in Huygens wing 1. Therefore
the wired and wireless networks will not be available at some moments in
many locations in wing 1 and corridors between wings 1 and 3, on all
floors.
