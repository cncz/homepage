---
title: Jupyter and Chemotion offline
author: bram
cpk_number: 1346
cpk_begin: 2023-09-20 06:30:00
cpk_end: 2023-09-20 09:46:00
cpk_affected:
# cpk_frontpage: true
date: 2023-09-20
tags: []
url: cpk/1346
---
For an unknown reason, the Jupiter and Chemotion servers didn't start. We are currently looking at how we can start the machine again.

The virtual machines have been rebuild, based on their existing disks.
