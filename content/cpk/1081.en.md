---
cpk_affected: Users of this U:/ home server
cpk_begin: &id001 2014-04-16 17:42:00
cpk_end: 2014-04-16 18:00:00
cpk_number: 1081
date: *id001
tags:
- medewerkers
- studenten
title: U:/ home server pile problem
url: cpk/1081
---
The server had a problem with one partition, which had started during
the creation of new snapshots. We waited with the reboot untill after
working hours. A reboot of the machine solved the problem. To prevent
these problems in the future, we will no longer make local snapshots of
homeservers, but of course the daily backups of the homeservers by the
backup server will be continued.
