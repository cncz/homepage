---
cpk_affected: TCM disk users
cpk_begin: &id001 2017-05-29 06:30:00
cpk_end: 2017-05-30 09:30:00
cpk_number: 1208
date: *id001
tags:
- medewerkers
- studenten
title: Server TCMR storing na disk-falen
url: cpk/1208
---
Sunday evening, one of the archive disks failed. The data on it was
lost, as it wasn’t in a redundant setup. After the weekly reboot, the
server hung on this disk being broken. It took us a while to get the
server up again.
