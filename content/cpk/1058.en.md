---
cpk_affected: Users of the poster printer "kamerbreed"
cpk_begin: &id001 2013-09-25 17:00:00
cpk_end: 2013-11-20 14:00:00
cpk_number: 1058
date: *id001
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" broken
url: cpk/1058
---
The motherboard of the printer had to be replaced. Because there is no
maintenance contract for this old printer and spare parts were hard to
get, repair has taken a long time.
