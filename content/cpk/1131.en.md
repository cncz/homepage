---
cpk_affected: Users of the GitLab server gitlab.science.ru.nl
cpk_begin: &id001 2015-05-30 07:34:00
cpk_end: 2015-05-31 00:36:00
cpk_number: 1131
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: GitLab server problems
url: cpk/1131
---
A configuration error caused the automatic installation of an older
version of GitLab (7.7.1) over the the most recent version (7.11.4).
Because the GitLab version did not correspond to the state of the
underlying database, what caused multiple problems. Among which the
gitlab shell for ssh access and inaccessibility of some parts of the web
interface. The problems were solved after upgrading to version 7.11.4.
