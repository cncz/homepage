---
cpk_affected: 'all users wanting to read Science mail '
cpk_begin: &id001 2014-05-27 14:05:00
cpk_end: 2014-05-27 14:10:00
cpk_number: 1095
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mail server problem
url: cpk/1095
---
The IMAP server got overloaded. Restarting the IMAP service was required
to bring it back in operation.
