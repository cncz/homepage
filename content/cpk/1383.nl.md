---
title: lilo7 toegang met managed SSH keys
author: Arnoud
cpk_number: 1383
cpk_begin: 2024-11-04 10:30:00
cpk_end: 2025-02-07 12:15:00
cpk_affected: 
cpk_frontpage: true
date: 2024-12-04
tags: []
url: cpk/1383
---
We hebben het authenticatieproces voor [login server](/nl/howto/hardware-servers/#linux-loginservers) `lilo7` (lilo7.science.ru.nl) bijgewerkt als 
onderdeel van een test.

Vanaf nu wordt je `~/.ssh/authorized_keys` bestand op lilo7 genegeerd. Deze wijziging is bedoeld om te voorkomen dat slechte actoren gemakkelijk blijvende toegang kunnen krijgen.

Het goede nieuws is dat we nu toegang kunnen verlenen tot `lilo7` van buiten de voorheen zeer beperkte IP-bereiken.

Om toegang te krijgen vanaf een nieuw IP-bereik, moet u [ons](/nl/howto/contact) voorzien van uw openbare SSH-sleutel en het IP-bereik waarmee u verbinding gaat maken.

Voor verbindingen uit het beperkte bereik blijven zowel wachtwoord- als sleutelauthenticatie beschikbare opties.

Een sleutelpaar genereren kan met:

```schil
ssh-keygen -t ed25519 -C "uw_e-mailadres@ru.nl"
```

Als deze test succesvol is, zijn we van plan dit nieuwe authenticatieproces uit te rollen naar alle inlogservers.

UPDATE: De test was succesvol, [zie nieuwsbericht](/nl/news/2025-01-28_lilo_access_via_ssh_pubkey_and_ip_range/)
