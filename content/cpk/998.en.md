---
cpk_affected: incoming Eduroam users with an iPhone/iPad/iPod
cpk_begin: &id001 2012-03-20 00:00:00
cpk_end: 2012-10-05 00:00:00
cpk_number: 998
date: *id001
tags:
- studenten
title: Eduroam incoming doesn't work for iPhone/iPad/iPod
url: cpk/998
---
The [UCI network management](http://www.ru.nl/uci) reports that at this
moment the
[incoming](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/eduroam/)
version of [Eduroam](http://www.eduroam.nl) doesn’t work for
iPhone/iPad/iPod. A solution is being worked upon. Eduroam incoming
means that one uses the wireless network of a remote institute, with
authentication (login/password) being checked by RU or Science.
