---
cpk_affected: gebruikers die iets willen veranderen aan hun account of aliases
cpk_begin: &id001 2017-09-27 15:00:00
cpk_end: 2017-09-27 16:45:00
cpk_number: 1217
date: *id001
tags:
- medewerkers
- studenten
title: DHZ onbereikbaar ivm hardware storing
url: cpk/1217
---
De server van DHZ was stuk, de hardware is gerepareerd op vrijdag 29
september, de DHZ website kon op een andere machine verder lopen, zodat
de storing wat dat betreft beperkt is gebleven tot een paar uur.
