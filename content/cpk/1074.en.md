---
cpk_affected: Users of this U:/ home server
cpk_begin: &id001 2014-03-24 17:25:00
cpk_end: 2014-03-24 17:35:00
cpk_number: 1074
date: *id001
tags:
- medewerkers
- studenten
title: U:/ home server pile problem
url: cpk/1074
---
The server had a problem with one partition. A reboot of the machine
solved the problem.
