---
cpk_affected: Users with home directories / data on the goudsmit fileserver
cpk_begin: &id001 2015-11-04 23:20:00
cpk_end: 2015-11-05 10:30:00
cpk_number: 1149
date: *id001
tags:
- medewerkers
title: Fileserver goudsmit problems
url: cpk/1149
---
System crashed possibly caused by an erroneous harddisk causing all CPUs
to lockup.
