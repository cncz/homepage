---
cpk_affected: Eduroam users with SUSE Linux 15.3
cpk_begin: &id001 2022-02-14 00:00:00
cpk_end: *id001
cpk_number: 1292
date: *id001
tags:
- medewerkers
- studenten
title: SUSE Linux 15.3 Eduroam doesn't work with U- or s-number, but does with Science
  account
url: cpk/1292
---
February 14, ILS switched off antique versions of
[TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) (1.0 and
1.1) for the [Eduroam](https://www.eduroam.nl/en/) authentication on
[ILS](https://www.ru.nl/ict-uk/) LDAP servers. From then on, SUSE Linux
15.3 clients can't authenticate with U- or s-number. They only have
TLS1.2 and the ILS servers offer TLS1.3 first, after that an error
occurs. By using the Science-account to authenticate, these users
succeed in connecting to Eduroam.
