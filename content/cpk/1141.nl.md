---
cpk_affected: Gebruikers van de KM MFP pr-hg-03-825
cpk_begin: &id001 2015-08-31 00:00:00
cpk_end: 2015-09-03 00:00:00
cpk_number: 1141
date: *id001
tags:
- medewerkers
title: Waterschade bij pr-hg-03-825
url: cpk/1141
---
Door een lekkage boven de KM MFP pr-hg-03-825 is deze defect geraakt en
niet meer te repareren. We hopen dit door verplaatsing van een weinig
gebruikte KM MFP van hetzelfde type snel op te kunnen lossen.
