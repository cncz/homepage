---
cpk_affected: Nightly users of the KM MFP's
cpk_begin: &id001 2013-12-04 02:00:00
cpk_end: 2013-12-04 06:00:00
cpk_number: 1060
date: *id001
tags:
- medewerkers
- studenten
title: Konica Minolta MFP's firmware upgrade
url: cpk/1060
---
Wednesday night, the firmware of the Konica Minolta multifunctionals
will be upgraded. This should resolve existing problems, like the not
waking up from sleep mode. Please report all remaining problems, for
MFP-hardware and paper to KM via phone: 55955 option 4.
