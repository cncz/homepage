---
title: Vleugel 5 Huygens netwerkonderhoud woensdag 15 feb 19:00-20:30
author: petervc
cpk_number: 1315
cpk_begin: 2023-02-15 19:00:00
cpk_end:  2023-02-15 20:30:00
cpk_affected: Bedrade en draadloze netwerkgebruikers in en om vleugel 5 van het Huygensgebouw, alle verdiepingen
date: 2023-02-10
tags: []
url: cpk/1315
---
RU ILS Connectivity kondigde aan dat aanstaande woensdagavond
netwerkonderhoud zal worden uitgevoerd
dat korte verstoringen zal veroorzaken voor bedrade en draadloze
systemen in en om Huygens vleugel 5.
Voor bedrade netwerkaansluitingen kan men het
outletnummer op de muur controleren (beginnend met 111- voor
getroffen systemen) of dat op te zoeken in de
[FNWI ethergids](https://cncz.science.ru.nl/nl/howto/netwerk-ethergids/).

