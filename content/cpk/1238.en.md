---
cpk_affected: Users wanting to read Science mail
cpk_begin: &id001 2018-08-09 13:05:00
cpk_end: 2018-08-22 09:25:00
cpk_number: 1238
date: *id001
tags:
- medewerkers
- studenten
title: Bug in new IMAP mailserver resolved
url: cpk/1238
---
The new version of the IMAP mailserver software contained a bug in the
handling of SSL connections, that led to connections being dropped and
depending on the mailclient led to a mailserver that could hardly be
used. This morning a patch for this bug has been installed.
