---
title: Jupyterhub22 refused to start this morning
author: bram
cpk_number: 1349
cpk_begin: 2023-09-27 06:30:00
cpk_end: 2023-09-27 08:35:54
cpk_affected: Users of jupyterhub22.science.ru.nl
# cpk_frontpage: true
date: 2023-09-27
tags: []
url: cpk/1349
---
After a scheduled reboot of the machine running jupyterhub, jupyter failed to start properly. The service started after invoking a manual start command.
