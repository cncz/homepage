---
cpk_affected: Gebruikers van mail lijsten
cpk_begin: &id001 2015-09-07 06:30:00
cpk_end: 2015-09-07 12:24:00
cpk_number: 1143
date: *id001
title: Mailman maillijsten probleem
url: cpk/1143
---
Vanochtend werd duidelijk dat berichten naar maillijsten niet
geaccepteerd werden door de server van de maillijsten. Dit bleek te
wijten te zijn aan aan aanpassing die afgelopen vrijdag gemaakt was in
verband met het inzetten van de nieuwe smtp-server half juli. Nadat deze
aanpassing gecorrigeerd was, werden de mails weer geaccepteerd.
