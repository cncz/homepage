---
cpk_affected: Users of one of the many servers behind this switch
cpk_begin: &id001 2021-10-12 11:50:00
cpk_end: 2021-10-12 12:05:00
cpk_number: 1287
date: *id001
tags:
- medewerkers
- studenten
- docenten
title: Server room network switch powerless
url: cpk/1287
---
Two modules of an important switch in the main C&CZ server room lost
power during the preparation of planned maintenance. This disconnected
ca. 75% of the servers in the room from the network. Moving the modules
to new PDU's kimited the downtime to ca. 15 minutes.
