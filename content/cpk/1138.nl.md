---
cpk_affected: gitlab/redmine users
cpk_begin: &id001 2015-07-31 09:00:00
cpk_end: 2015-07-31 16:00:00
cpk_number: 1138
date: *id001
tags:
- medewerkers
- studenten
title: Server vos (gitlab3/redmine3/smtp2) preventief onderhoud
url: cpk/1138
---
De server vos krijgt extra disken, tevens wordt de configuratie
aangepast. Hierdoor zullen o.a. de virtuele client gitlab3, redmine2 en
smtp2 gedurende het onderhoud offline zijn.
