---
cpk_affected: 'all users until 09:30; afterwards: "bundle" home directories, wireless,
  "plus" network shares and several websites'
cpk_begin: 2012-10-18 03:00:00
cpk_end: 2012-10-18 10:00:00
cpk_number: 1001
date: 2012-10-18
tags:
- studenten
- medewerkers
- docenten
title: Services unavailable due to power and network outage
url: cpk/1001
---
During the night of wednesday on thursday a power outage resulted in a
network outage in the basement computing facilities. The power was
restored to the network equipment using a bypass thus circumventing the
UPS at about 09:15. Further checks implied that most servers had not
become powerless so that most services became automatically available
again. Network drivers on “bundle” had to be restarted in order to get
access to home directories for a large number of users. Furthermore,
several websites had to be restarted which made it possible for PC’s to
boot properly. During the day, an unrelated issue with the RAID storage
of “plus” has been fixed as well granting access to the following
network shares: sofie, ams\*, molchem, mb\*, encapson, milkun4, snn,
neuropi, digicd. carta, … Since wireless devices were unable to acquire
IP addresses, i.e. gain access to the network, a split-brain situation
was diagnosed within the DHCP service which was resolved around 13:00.
