---
cpk_affected: Gebruikers met homedirectory op "pile" (te zien op `<http://DHZ.science.ru.nl>`)
cpk_begin: 2012-10-12 07:00:00
cpk_end: 2012-10-12 09:00:00
cpk_number: 1000
date: 2012-10-12
tags:
- studenten
- medewerkers
- docenten
title: 'Aangekondigd onderhoud: home-server "pile" down voor reboot'
url: cpk/1000
---
A.s. vrijdagochtend wordt de nieuwe homeserver “pile” gepland gereboot.
Er zijn problemen met de [snapshots](/nl/howto/backup/), waardoor een
reboot langer zou kunnen duren dan normaal. Daarom wordt dit voor a.s.
vrijdag vroeg gepland.
