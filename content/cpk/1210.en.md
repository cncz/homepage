---
cpk_affected: employees and students at Faculty of Science (eg using lilo4)
cpk_begin: &id001 2017-06-26 06:30:00
cpk_end: 2017-07-19 00:00:00
cpk_number: 1210
date: *id001
tags:
- medewerkers
- studenten
title: LibreOffice problem using Ubuntu 14.04
url: cpk/1210
---
LibreOffice and other software suites using Java (jvm) internally are
affected by the Ubuntu 14.04 kernel update patching the Stack Clash. It
seems that Linux kernel developers, Ubuntu developers and JVM developers
are working hard world-wide in finding a proper solution. Work-around
‘JAVA\_TOOL\_OPTIONS=-Xss1280k libreoffice’. Ubunti released a fix.
