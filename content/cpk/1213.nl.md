---
cpk_affected: gebruikers die iets willen veranderen aan hun account of aliases
cpk_begin: &id001 2017-08-10 08:00:00
cpk_end: 2017-08-11 11:00:00
cpk_number: 1213
date: *id001
tags:
- medewerkers
- studenten
title: DHZ onbereikbaar ivm hardware storing
url: cpk/1213
---
De server van DHZ is stuk, veranderingen kun je ook via ons (tel: 53535
of postmaster\@science.ru.nl) regelen.
