---
title: Poster printer kamerbreed broken
author: petervc
cpk_number: 1321
cpk_begin: 2023-03-20 12:00:00
cpk_end: 2023-04-11 09:00:00
cpk_affected: users of the poster printer kamerbreed
date: 2023-03-24
tags: []
url: cpk/1321
---
March 20, the poster printer kamerbreed started producing bad colours. The new print heads that were received a fw days later didn't solve the problem.
Therefore we called for a repair, that will probably be scheduled in the week of April 17.

In the meantime, users that need posters could go to [Radboud Post&Print](https://www.ru.nl/en/services/services-and-facilities/shops/post-print).
