---
cpk_affected: several users in Huygens wing 8
cpk_begin: &id001 2017-01-24 10:50:00
cpk_end: 2017-01-24 11:55:00
cpk_number: 1193
date: *id001
tags:
- medewerkers
- studenten
title: Network failure in Huygens wing 8
url: cpk/1193
---
In Huygens wing 8 a network switchmodule crashed. Several users lost
their connection to the data and telephone network for some time. The
switchmodule has been replaced.
