---
cpk_affected: Gebruikers van crestron (AVD) gitlab7 (PEP) goudsmit (NMR) msql01
cpk_begin: &id001 2018-08-23 01:41:00
cpk_end: 2018-08-23 11:35:00
cpk_number: 1239
date: *id001
tags:
- medewerkers
- studenten
title: Hardware van host van virtuele machines defect
url: cpk/1239
---
Vannacht raakte de host van een aantal vm’s (oscar) defect. Door het
inzetten van een vervangende server (joule) is dit probleem tijdelijk
opgelost. Morgen wordt de machine gerepareerd en zal gepland worden
wanneer de machines weer gewisseld worden.
