---
cpk_affected: OpenVPN users
cpk_begin: &id001 2017-08-14 06:31:00
cpk_end: 2017-08-15 11:36:00
cpk_number: 1214
date: *id001
tags:
- medewerkers
- studenten
title: OpenVPN not started at reboot
url: cpk/1214
---
For yet unknown reasons the OpenVPN service did not start at the Monday
morning reboot. We started the service manually.
