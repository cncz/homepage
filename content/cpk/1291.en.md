---
cpk_affected: Users of the Coma compute cluster
cpk_begin: &id001 2022-02-22 13:10:00
cpk_end: 2022-02-22 15:47:00
cpk_number: 1291
date: *id001
title: Network switch of Astro Coma cluster down
url: cpk/1291
---
The network switch of the Coma cluster seems to be broken, all attached
nodes are separated from the rest of the network. We'll replace the
switch a.s.a.p. and (let) analyze the problem after that.
