.PHONY: serve fixnewline mirror test redirects resize_images

serve: test
	echo $$(git rev-parse HEAD)
# set CI_COMMIT_SHA here for testing locally instead of in the pipeline
	CI_COMMIT_SHA=$$(git rev-parse HEAD) hugo server --verbose --navigateToChanged --buildFuture

pages: redirects
	hugo --buildFuture --printI18nWarnings

redirects:
	echo '/nl/wiki/* /nl/howto/:splat 302' >  public/_redirects
	echo '/en/wiki/* /en/howto/:splat 302' >> public/_redirects

test:
	bin/check_frontmatter
	bin/check_lang

fixnewline:
	tr -d "\n" < layouts/_default/_markup/render-link.html > MAG_GEEN_NEWLINE_HEBBEN
	mv MAG_GEEN_NEWLINE_HEBBEN layouts/_default/_markup/render-link.html

mirror:
	CI_COMMIT_SHA=$$(git rev-parse HEAD) hugo server -b home.cncz.nl -d /tmp/xxx

STATIC_IMAGE_FILES := $(shell find static/img \( -iname "*.jpg" -or -iname "*.jpeg" -or -iname "*.png"  \))

resize_images:
	bin/resize_images $(STATIC_IMAGE_FILES)