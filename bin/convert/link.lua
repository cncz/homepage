local text = pandoc.text

-- See https://pandoc.org/lua-filters.html#type-link .
function Link(el)
    lang = os.getenv("LUA_LANG")

    if el.title == "wikilink" then

        el.title = ""
        -- normalize link, lower case and _ -> -, e.g. Vpnsec_Linux_install -> vpnsec-linux-install
        el.target = text.lower(el.target)

        if string.sub(el.target, 1, 1) == "#" then
            -- local link, leave as-is
            return el
        end

        -- [...](media:.....\.pdf) - download link
        if string.sub(el.target, 1, 6) == "media:" then
            el.target = string.gsub(el.target, "^media:", "/download/old/")
            return el
        end

        -- [categorie:storingen](categorie:storingen "wikilink") -> naar tags
        if string.sub(el.target, 1, 10) == "categorie:" then
            el.target = string.gsub(el.target, "^categorie:", "")
            el.target = "/" .. lang .. "/tags/" .. el.target
            return el
        end

        -- [install](:categorie:storingen "wikilink") -> naar tags
        if string.sub(el.target, 1, 11) == ":categorie:" then
            el.target = string.gsub(el.target, "^:categorie:", "")
            el.target = "/" .. lang .. "/tags/" .. el.target
            return el
        end

        -- [name](gebruiker:<login>) -> short code van maken
        if string.sub(el.target, 1, 10) == "gebruiker:" then
            login = string.gsub(el.target, "^gebruiker:", "")
            s = pandoc.Str()
            s.text = '{{< author "' .. login .. '" >}}'
            return s
        end

        el.target = string.gsub(el.target, "_", "-")
        -- altijd een wiki link met "wikilink"
        el.target = "/" .. lang .. "/howto/" .. el.target .. "/"
        return el
    end

    -- http://wiki.science.ru.nl/cncz/Hardware_servers#Linux_login-servers -> '/howto/hardware-servers'
    -- http://wiki.science.ru.nl/cncz                                     -> '/' (home)
    -- http://wiki.science.ru.nl/cncz/Categorie:Telefonie#                -> '/tags/telefonie' (??)
    -- http://wiki.science.ru.nl/cncz/Printers_en_printen?setlang=en      -> '/howto/printers-en-printen'
    if string.sub(el.target , 1, 19) == "http://wiki.science" or string.sub(el.target , 1, 20) == "https://wiki.science" then

        -- https://wiki.science.ru.nl/cncz/index.php?title=Peage&setlang=en -> '/wiki/peage'
        i, j = string.find(el.target, "index%.php%?title=")
        if i ~= nil then
            remainder = string.sub(el.target, i+16)
            i, j = string.find(remainder, "&")
            page = remainder
            if i ~= nil then
                page = string.sub(remainder, 1, i-1)
            end

            el.target = "/" .. lang .. "/howto/" .. page .. "/"
            el.target = text.lower(el.target)
            el.target = string.gsub(el.target, "_", "-")
            return el
        end

        -- http://wiki.science.ru.nl/cncz/Categorie:Telefonie#           -> '/tags/telefonie'
        -- http://wiki.science.ru.nl/cncz/Categorie:Telefonie            -> '/tags/telefonie'
        -- http://wiki.science.ru.nl/cncz/Categorie:Email?               -> '/tags/telefonie'
        i, j = string.find(el.target, "Categorie:")
        if i ~= nil then
            remainder = string.sub(el.target, i+10)
            -- dan tot ? of # of gehele string
            remainder = string.gsub(remainder, "#.*$", "")
            page = string.gsub(remainder, "%?.*$", "")

            el.target = "/" .. lang .. "/tags/" .. page .. "/"
            el.target = text.lower(el.target)
            el.target = string.gsub(el.target, "_", "-")
            return el
        end

        -- als hier, dan moet het zo'n link zijn...
        -- http://wiki.science.ru.nl/cncz/Printers_en_printen?setlang=en      -> '/howto/printers-en-printen'
        i, j = string.find(el.target, "/cncz/")
        if i ~= nil then
            remainder = string.sub(el.target, i+5)
            -- dan tot ? of # of gehele string
            remainder = string.gsub(remainder, "#.*$", "")
            page = string.gsub(remainder, "%?.*$", "")

            el.target = "/" .. lang .. "/howto" .. page .. "/"
            el.target = text.lower(el.target)
            el.target = string.gsub(el.target, "_", "-")
            return el
        end

        if el.target == "http://wiki.science.ru.nl/cncz" then
            el.target = "/"
            return el
        end
        if el.target == "https://wiki.science.ru.nl/cncz" then
            el.target = "/"
            return el
        end

    end

end
