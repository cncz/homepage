import requests
from bs4 import BeautifulSoup
import urllib.request

name = "bram"
URL = "https://www.ru.nl/personen/daams-b"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")
image_tags = soup.find_all("img", id="photo")

for image_tag in image_tags:
    print(image_tag['src'])

#    urllib.request.urlretrieve(image_tag['src'], f"/tmp/{ name }.png')
    urllib.request.urlretrieve(image_tag['src'], "/tmp/bram.png")
